// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public final class CreateAvailabilityMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createAvailability($userId: Long, $scheduleLayoutId: Long, $startDate: String, $endDate: String, $startTime: String, $endTime: String, $onDate: [String]) {
      createAvailability(userId: $userId, scheduleLayoutId: $scheduleLayoutId, startDate: $startDate, endDate: $endDate, startTime: $startTime, endTime: $endTime, onDate: $onDate) {
        __typename
        userId
      }
    }
    """

  public let operationName: String = "createAvailability"

  public var userId: String?
  public var scheduleLayoutId: String?
  public var startDate: String?
  public var endDate: String?
  public var startTime: String?
  public var endTime: String?
  public var onDate: [String?]?

  public init(userId: String? = nil, scheduleLayoutId: String? = nil, startDate: String? = nil, endDate: String? = nil, startTime: String? = nil, endTime: String? = nil, onDate: [String?]? = nil) {
    self.userId = userId
    self.scheduleLayoutId = scheduleLayoutId
    self.startDate = startDate
    self.endDate = endDate
    self.startTime = startTime
    self.endTime = endTime
    self.onDate = onDate
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "scheduleLayoutId": scheduleLayoutId, "startDate": startDate, "endDate": endDate, "startTime": startTime, "endTime": endTime, "onDate": onDate]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createAvailability", arguments: ["userId": GraphQLVariable("userId"), "scheduleLayoutId": GraphQLVariable("scheduleLayoutId"), "startDate": GraphQLVariable("startDate"), "endDate": GraphQLVariable("endDate"), "startTime": GraphQLVariable("startTime"), "endTime": GraphQLVariable("endTime"), "onDate": GraphQLVariable("onDate")], type: .object(CreateAvailability.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createAvailability: CreateAvailability? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createAvailability": createAvailability.flatMap { (value: CreateAvailability) -> ResultMap in value.resultMap }])
    }

    public var createAvailability: CreateAvailability? {
      get {
        return (resultMap["createAvailability"] as? ResultMap).flatMap { CreateAvailability(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createAvailability")
      }
    }

    public struct CreateAvailability: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Availability"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Availability", "userId": userId])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }
    }
  }
}

public final class CreateScheduleAvailabilityMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createScheduleAvailability($userId: Long, $startDate: String, $endDate: String, $startTime: String, $endTime: String, $onDate: [String], $scheduleLayoutId: Long) {
      createAvailability(userId: $userId, startDate: $startDate, endDate: $endDate, startTime: $startTime, endTime: $endTime, onDate: $onDate, scheduleLayoutId: $scheduleLayoutId) {
        __typename
        userId
      }
    }
    """

  public let operationName: String = "createScheduleAvailability"

  public var userId: String?
  public var startDate: String?
  public var endDate: String?
  public var startTime: String?
  public var endTime: String?
  public var onDate: [String?]?
  public var scheduleLayoutId: String?

  public init(userId: String? = nil, startDate: String? = nil, endDate: String? = nil, startTime: String? = nil, endTime: String? = nil, onDate: [String?]? = nil, scheduleLayoutId: String? = nil) {
    self.userId = userId
    self.startDate = startDate
    self.endDate = endDate
    self.startTime = startTime
    self.endTime = endTime
    self.onDate = onDate
    self.scheduleLayoutId = scheduleLayoutId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "startDate": startDate, "endDate": endDate, "startTime": startTime, "endTime": endTime, "onDate": onDate, "scheduleLayoutId": scheduleLayoutId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createAvailability", arguments: ["userId": GraphQLVariable("userId"), "startDate": GraphQLVariable("startDate"), "endDate": GraphQLVariable("endDate"), "startTime": GraphQLVariable("startTime"), "endTime": GraphQLVariable("endTime"), "onDate": GraphQLVariable("onDate"), "scheduleLayoutId": GraphQLVariable("scheduleLayoutId")], type: .object(CreateAvailability.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createAvailability: CreateAvailability? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createAvailability": createAvailability.flatMap { (value: CreateAvailability) -> ResultMap in value.resultMap }])
    }

    public var createAvailability: CreateAvailability? {
      get {
        return (resultMap["createAvailability"] as? ResultMap).flatMap { CreateAvailability(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createAvailability")
      }
    }

    public struct CreateAvailability: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Availability"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Availability", "userId": userId])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }
    }
  }
}

public final class CreateARequestMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createARequest($userId: Long!, $worksiteId: Long!, $departmentId: Long!, $requestTypeId: Long!, $onDate: [String], $shiftId: Long, $notes: String) {
      createARequest(userId: $userId, worksiteId: $worksiteId, departmentId: $departmentId, requestTypeId: $requestTypeId, onDate: $onDate, shiftId: $shiftId, notes: $notes) {
        __typename
        requestId
        worksiteId
        worksiteName
        departmentId
        requestTypeId
        requestTypeName
        userId
        onDate
        shiftId
        colleagueId
        colleagueName
        notes
      }
    }
    """

  public let operationName: String = "createARequest"

  public var userId: String
  public var worksiteId: String
  public var departmentId: String
  public var requestTypeId: String
  public var onDate: [String?]?
  public var shiftId: String?
  public var notes: String?

  public init(userId: String, worksiteId: String, departmentId: String, requestTypeId: String, onDate: [String?]? = nil, shiftId: String? = nil, notes: String? = nil) {
    self.userId = userId
    self.worksiteId = worksiteId
    self.departmentId = departmentId
    self.requestTypeId = requestTypeId
    self.onDate = onDate
    self.shiftId = shiftId
    self.notes = notes
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "worksiteId": worksiteId, "departmentId": departmentId, "requestTypeId": requestTypeId, "onDate": onDate, "shiftId": shiftId, "notes": notes]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createARequest", arguments: ["userId": GraphQLVariable("userId"), "worksiteId": GraphQLVariable("worksiteId"), "departmentId": GraphQLVariable("departmentId"), "requestTypeId": GraphQLVariable("requestTypeId"), "onDate": GraphQLVariable("onDate"), "shiftId": GraphQLVariable("shiftId"), "notes": GraphQLVariable("notes")], type: .object(CreateARequest.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createARequest: CreateARequest? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createARequest": createARequest.flatMap { (value: CreateARequest) -> ResultMap in value.resultMap }])
    }

    public var createARequest: CreateARequest? {
      get {
        return (resultMap["createARequest"] as? ResultMap).flatMap { CreateARequest(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createARequest")
      }
    }

    public struct CreateARequest: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Requests"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("requestId", type: .scalar(String.self)),
        GraphQLField("worksiteId", type: .scalar(String.self)),
        GraphQLField("worksiteName", type: .scalar(String.self)),
        GraphQLField("departmentId", type: .scalar(String.self)),
        GraphQLField("requestTypeId", type: .scalar(String.self)),
        GraphQLField("requestTypeName", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("onDate", type: .list(.scalar(String.self))),
        GraphQLField("shiftId", type: .scalar(String.self)),
        GraphQLField("colleagueId", type: .scalar(String.self)),
        GraphQLField("colleagueName", type: .scalar(String.self)),
        GraphQLField("notes", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(requestId: String? = nil, worksiteId: String? = nil, worksiteName: String? = nil, departmentId: String? = nil, requestTypeId: String? = nil, requestTypeName: String? = nil, userId: String? = nil, onDate: [String?]? = nil, shiftId: String? = nil, colleagueId: String? = nil, colleagueName: String? = nil, notes: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Requests", "requestId": requestId, "worksiteId": worksiteId, "worksiteName": worksiteName, "departmentId": departmentId, "requestTypeId": requestTypeId, "requestTypeName": requestTypeName, "userId": userId, "onDate": onDate, "shiftId": shiftId, "colleagueId": colleagueId, "colleagueName": colleagueName, "notes": notes])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var requestId: String? {
        get {
          return resultMap["requestId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestId")
        }
      }

      public var worksiteId: String? {
        get {
          return resultMap["worksiteId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteId")
        }
      }

      public var worksiteName: String? {
        get {
          return resultMap["worksiteName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteName")
        }
      }

      public var departmentId: String? {
        get {
          return resultMap["departmentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentId")
        }
      }

      public var requestTypeId: String? {
        get {
          return resultMap["requestTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestTypeId")
        }
      }

      public var requestTypeName: String? {
        get {
          return resultMap["requestTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestTypeName")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var onDate: [String?]? {
        get {
          return resultMap["onDate"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "onDate")
        }
      }

      public var shiftId: String? {
        get {
          return resultMap["shiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftId")
        }
      }

      public var colleagueId: String? {
        get {
          return resultMap["colleagueId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "colleagueId")
        }
      }

      public var colleagueName: String? {
        get {
          return resultMap["colleagueName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "colleagueName")
        }
      }

      public var notes: String? {
        get {
          return resultMap["notes"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "notes")
        }
      }
    }
  }
}

public final class CreateMessagesMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createMessages($text: String, $senderId: Long, $receiverId: Long, $timeStamp: String, $timeZone: String) {
      createMessages(text: $text, senderId: $senderId, receiverId: $receiverId, timeStamp: $timeStamp, timeZone: $timeZone) {
        __typename
        messageId
        text
        senderId
        receiverId
        timeStamp
      }
    }
    """

  public let operationName: String = "createMessages"

  public var text: String?
  public var senderId: String?
  public var receiverId: String?
  public var timeStamp: String?
  public var timeZone: String?

  public init(text: String? = nil, senderId: String? = nil, receiverId: String? = nil, timeStamp: String? = nil, timeZone: String? = nil) {
    self.text = text
    self.senderId = senderId
    self.receiverId = receiverId
    self.timeStamp = timeStamp
    self.timeZone = timeZone
  }

  public var variables: GraphQLMap? {
    return ["text": text, "senderId": senderId, "receiverId": receiverId, "timeStamp": timeStamp, "timeZone": timeZone]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createMessages", arguments: ["text": GraphQLVariable("text"), "senderId": GraphQLVariable("senderId"), "receiverId": GraphQLVariable("receiverId"), "timeStamp": GraphQLVariable("timeStamp"), "timeZone": GraphQLVariable("timeZone")], type: .object(CreateMessage.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createMessages: CreateMessage? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createMessages": createMessages.flatMap { (value: CreateMessage) -> ResultMap in value.resultMap }])
    }

    public var createMessages: CreateMessage? {
      get {
        return (resultMap["createMessages"] as? ResultMap).flatMap { CreateMessage(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createMessages")
      }
    }

    public struct CreateMessage: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Messages"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("messageId", type: .scalar(String.self)),
        GraphQLField("text", type: .scalar(String.self)),
        GraphQLField("senderId", type: .scalar(String.self)),
        GraphQLField("receiverId", type: .scalar(String.self)),
        GraphQLField("timeStamp", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(messageId: String? = nil, text: String? = nil, senderId: String? = nil, receiverId: String? = nil, timeStamp: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Messages", "messageId": messageId, "text": text, "senderId": senderId, "receiverId": receiverId, "timeStamp": timeStamp])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var messageId: String? {
        get {
          return resultMap["messageId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "messageId")
        }
      }

      public var text: String? {
        get {
          return resultMap["text"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "text")
        }
      }

      public var senderId: String? {
        get {
          return resultMap["senderId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "senderId")
        }
      }

      public var receiverId: String? {
        get {
          return resultMap["receiverId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "receiverId")
        }
      }

      public var timeStamp: String? {
        get {
          return resultMap["timeStamp"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "timeStamp")
        }
      }
    }
  }
}

public final class DeleteMessagesOfUserMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteMessagesOfUser($senderId: Long, $receiverId: Long) {
      deleteMessagesOfUser(senderId: $senderId, receiverId: $receiverId)
    }
    """

  public let operationName: String = "deleteMessagesOfUser"

  public var senderId: String?
  public var receiverId: String?

  public init(senderId: String? = nil, receiverId: String? = nil) {
    self.senderId = senderId
    self.receiverId = receiverId
  }

  public var variables: GraphQLMap? {
    return ["senderId": senderId, "receiverId": receiverId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteMessagesOfUser", arguments: ["senderId": GraphQLVariable("senderId"), "receiverId": GraphQLVariable("receiverId")], type: .scalar(Bool.self)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteMessagesOfUser: Bool? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "deleteMessagesOfUser": deleteMessagesOfUser])
    }

    public var deleteMessagesOfUser: Bool? {
      get {
        return resultMap["deleteMessagesOfUser"] as? Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "deleteMessagesOfUser")
      }
    }
  }
}

public final class CreateUserCredentialsMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createUserCredentials($userId: Long!, $credentialIcon: String!, $credentialName: String!, $credentialTypeId: Long!, $credentialSubTypeId: Long!, $expirationDate: String!, $credentialStatusId: Long!, $licenseNumber: String!, $frontFilePhoto: String!, $backFilePhoto: String!, $state: String!, $isCompactLicense: Boolean!) {
      createUserCredentials(userId: $userId, credentialIcon: $credentialIcon, credentialName: $credentialName, credentialTypeId: $credentialTypeId, credentialSubTypeId: $credentialSubTypeId, expirationDate: $expirationDate, credentialStatusId: $credentialStatusId, licenseNumber: $licenseNumber, backFilePhoto: $backFilePhoto, frontFilePhoto: $frontFilePhoto, state: $state, isCompactLicense: $isCompactLicense) {
        __typename
        userCredentialId
        userId
        credentialIcon
        credentialName
        credentialTypeId
        credentialType
        expirationDate
        credentialStatus
        credentialStatusId
        licenseNumber
        frontFilePhoto
        backFilePhoto
        state
        isCompactLicense
      }
    }
    """

  public let operationName: String = "createUserCredentials"

  public var userId: String
  public var credentialIcon: String
  public var credentialName: String
  public var credentialTypeId: String
  public var credentialSubTypeId: String
  public var expirationDate: String
  public var credentialStatusId: String
  public var licenseNumber: String
  public var frontFilePhoto: String
  public var backFilePhoto: String
  public var state: String
  public var isCompactLicense: Bool

  public init(userId: String, credentialIcon: String, credentialName: String, credentialTypeId: String, credentialSubTypeId: String, expirationDate: String, credentialStatusId: String, licenseNumber: String, frontFilePhoto: String, backFilePhoto: String, state: String, isCompactLicense: Bool) {
    self.userId = userId
    self.credentialIcon = credentialIcon
    self.credentialName = credentialName
    self.credentialTypeId = credentialTypeId
    self.credentialSubTypeId = credentialSubTypeId
    self.expirationDate = expirationDate
    self.credentialStatusId = credentialStatusId
    self.licenseNumber = licenseNumber
    self.frontFilePhoto = frontFilePhoto
    self.backFilePhoto = backFilePhoto
    self.state = state
    self.isCompactLicense = isCompactLicense
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "credentialIcon": credentialIcon, "credentialName": credentialName, "credentialTypeId": credentialTypeId, "credentialSubTypeId": credentialSubTypeId, "expirationDate": expirationDate, "credentialStatusId": credentialStatusId, "licenseNumber": licenseNumber, "frontFilePhoto": frontFilePhoto, "backFilePhoto": backFilePhoto, "state": state, "isCompactLicense": isCompactLicense]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUserCredentials", arguments: ["userId": GraphQLVariable("userId"), "credentialIcon": GraphQLVariable("credentialIcon"), "credentialName": GraphQLVariable("credentialName"), "credentialTypeId": GraphQLVariable("credentialTypeId"), "credentialSubTypeId": GraphQLVariable("credentialSubTypeId"), "expirationDate": GraphQLVariable("expirationDate"), "credentialStatusId": GraphQLVariable("credentialStatusId"), "licenseNumber": GraphQLVariable("licenseNumber"), "backFilePhoto": GraphQLVariable("backFilePhoto"), "frontFilePhoto": GraphQLVariable("frontFilePhoto"), "state": GraphQLVariable("state"), "isCompactLicense": GraphQLVariable("isCompactLicense")], type: .object(CreateUserCredential.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createUserCredentials: CreateUserCredential? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createUserCredentials": createUserCredentials.flatMap { (value: CreateUserCredential) -> ResultMap in value.resultMap }])
    }

    public var createUserCredentials: CreateUserCredential? {
      get {
        return (resultMap["createUserCredentials"] as? ResultMap).flatMap { CreateUserCredential(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createUserCredentials")
      }
    }

    public struct CreateUserCredential: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserCredentials"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userCredentialId", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("credentialIcon", type: .scalar(String.self)),
        GraphQLField("credentialName", type: .scalar(String.self)),
        GraphQLField("credentialTypeId", type: .scalar(String.self)),
        GraphQLField("credentialType", type: .scalar(String.self)),
        GraphQLField("expirationDate", type: .scalar(String.self)),
        GraphQLField("credentialStatus", type: .scalar(String.self)),
        GraphQLField("credentialStatusId", type: .scalar(String.self)),
        GraphQLField("licenseNumber", type: .scalar(String.self)),
        GraphQLField("frontFilePhoto", type: .scalar(String.self)),
        GraphQLField("backFilePhoto", type: .scalar(String.self)),
        GraphQLField("state", type: .scalar(String.self)),
        GraphQLField("isCompactLicense", type: .scalar(Bool.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userCredentialId: String? = nil, userId: String? = nil, credentialIcon: String? = nil, credentialName: String? = nil, credentialTypeId: String? = nil, credentialType: String? = nil, expirationDate: String? = nil, credentialStatus: String? = nil, credentialStatusId: String? = nil, licenseNumber: String? = nil, frontFilePhoto: String? = nil, backFilePhoto: String? = nil, state: String? = nil, isCompactLicense: Bool? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserCredentials", "userCredentialId": userCredentialId, "userId": userId, "credentialIcon": credentialIcon, "credentialName": credentialName, "credentialTypeId": credentialTypeId, "credentialType": credentialType, "expirationDate": expirationDate, "credentialStatus": credentialStatus, "credentialStatusId": credentialStatusId, "licenseNumber": licenseNumber, "frontFilePhoto": frontFilePhoto, "backFilePhoto": backFilePhoto, "state": state, "isCompactLicense": isCompactLicense])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userCredentialId: String? {
        get {
          return resultMap["userCredentialId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userCredentialId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var credentialIcon: String? {
        get {
          return resultMap["credentialIcon"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialIcon")
        }
      }

      public var credentialName: String? {
        get {
          return resultMap["credentialName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialName")
        }
      }

      public var credentialTypeId: String? {
        get {
          return resultMap["credentialTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialTypeId")
        }
      }

      public var credentialType: String? {
        get {
          return resultMap["credentialType"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialType")
        }
      }

      public var expirationDate: String? {
        get {
          return resultMap["expirationDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "expirationDate")
        }
      }

      public var credentialStatus: String? {
        get {
          return resultMap["credentialStatus"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialStatus")
        }
      }

      public var credentialStatusId: String? {
        get {
          return resultMap["credentialStatusId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialStatusId")
        }
      }

      public var licenseNumber: String? {
        get {
          return resultMap["licenseNumber"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "licenseNumber")
        }
      }

      public var frontFilePhoto: String? {
        get {
          return resultMap["frontFilePhoto"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "frontFilePhoto")
        }
      }

      public var backFilePhoto: String? {
        get {
          return resultMap["backFilePhoto"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "backFilePhoto")
        }
      }

      public var state: String? {
        get {
          return resultMap["state"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "state")
        }
      }

      public var isCompactLicense: Bool? {
        get {
          return resultMap["isCompactLicense"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isCompactLicense")
        }
      }
    }
  }
}

public final class UpdateUserCredentialsMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateUserCredentials($userId: Long!, $credentialIcon: String!, $credentialName: String!, $credentialTypeId: Long!, $credentialSubTypeId: Long!, $expirationDate: String!, $credentialStatusId: Long!, $licenseNumber: String!, $frontFilePhoto: String!, $backFilePhoto: String!, $state: String!, $isCompactLicense: Boolean!) {
      createUserCredentials(userId: $userId, credentialIcon: $credentialIcon, credentialName: $credentialName, credentialTypeId: $credentialTypeId, credentialSubTypeId: $credentialSubTypeId, expirationDate: $expirationDate, credentialStatusId: $credentialStatusId, licenseNumber: $licenseNumber, backFilePhoto: $backFilePhoto, frontFilePhoto: $frontFilePhoto, state: $state, isCompactLicense: $isCompactLicense) {
        __typename
        userCredentialId
        userId
        credentialIcon
        credentialName
        credentialTypeId
        credentialType
        expirationDate
        credentialStatus
        credentialStatusId
        licenseNumber
        frontFilePhoto
        backFilePhoto
        state
        isCompactLicense
      }
    }
    """

  public let operationName: String = "updateUserCredentials"

  public var userId: String
  public var credentialIcon: String
  public var credentialName: String
  public var credentialTypeId: String
  public var credentialSubTypeId: String
  public var expirationDate: String
  public var credentialStatusId: String
  public var licenseNumber: String
  public var frontFilePhoto: String
  public var backFilePhoto: String
  public var state: String
  public var isCompactLicense: Bool

  public init(userId: String, credentialIcon: String, credentialName: String, credentialTypeId: String, credentialSubTypeId: String, expirationDate: String, credentialStatusId: String, licenseNumber: String, frontFilePhoto: String, backFilePhoto: String, state: String, isCompactLicense: Bool) {
    self.userId = userId
    self.credentialIcon = credentialIcon
    self.credentialName = credentialName
    self.credentialTypeId = credentialTypeId
    self.credentialSubTypeId = credentialSubTypeId
    self.expirationDate = expirationDate
    self.credentialStatusId = credentialStatusId
    self.licenseNumber = licenseNumber
    self.frontFilePhoto = frontFilePhoto
    self.backFilePhoto = backFilePhoto
    self.state = state
    self.isCompactLicense = isCompactLicense
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "credentialIcon": credentialIcon, "credentialName": credentialName, "credentialTypeId": credentialTypeId, "credentialSubTypeId": credentialSubTypeId, "expirationDate": expirationDate, "credentialStatusId": credentialStatusId, "licenseNumber": licenseNumber, "frontFilePhoto": frontFilePhoto, "backFilePhoto": backFilePhoto, "state": state, "isCompactLicense": isCompactLicense]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUserCredentials", arguments: ["userId": GraphQLVariable("userId"), "credentialIcon": GraphQLVariable("credentialIcon"), "credentialName": GraphQLVariable("credentialName"), "credentialTypeId": GraphQLVariable("credentialTypeId"), "credentialSubTypeId": GraphQLVariable("credentialSubTypeId"), "expirationDate": GraphQLVariable("expirationDate"), "credentialStatusId": GraphQLVariable("credentialStatusId"), "licenseNumber": GraphQLVariable("licenseNumber"), "backFilePhoto": GraphQLVariable("backFilePhoto"), "frontFilePhoto": GraphQLVariable("frontFilePhoto"), "state": GraphQLVariable("state"), "isCompactLicense": GraphQLVariable("isCompactLicense")], type: .object(CreateUserCredential.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createUserCredentials: CreateUserCredential? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createUserCredentials": createUserCredentials.flatMap { (value: CreateUserCredential) -> ResultMap in value.resultMap }])
    }

    public var createUserCredentials: CreateUserCredential? {
      get {
        return (resultMap["createUserCredentials"] as? ResultMap).flatMap { CreateUserCredential(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createUserCredentials")
      }
    }

    public struct CreateUserCredential: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserCredentials"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userCredentialId", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("credentialIcon", type: .scalar(String.self)),
        GraphQLField("credentialName", type: .scalar(String.self)),
        GraphQLField("credentialTypeId", type: .scalar(String.self)),
        GraphQLField("credentialType", type: .scalar(String.self)),
        GraphQLField("expirationDate", type: .scalar(String.self)),
        GraphQLField("credentialStatus", type: .scalar(String.self)),
        GraphQLField("credentialStatusId", type: .scalar(String.self)),
        GraphQLField("licenseNumber", type: .scalar(String.self)),
        GraphQLField("frontFilePhoto", type: .scalar(String.self)),
        GraphQLField("backFilePhoto", type: .scalar(String.self)),
        GraphQLField("state", type: .scalar(String.self)),
        GraphQLField("isCompactLicense", type: .scalar(Bool.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userCredentialId: String? = nil, userId: String? = nil, credentialIcon: String? = nil, credentialName: String? = nil, credentialTypeId: String? = nil, credentialType: String? = nil, expirationDate: String? = nil, credentialStatus: String? = nil, credentialStatusId: String? = nil, licenseNumber: String? = nil, frontFilePhoto: String? = nil, backFilePhoto: String? = nil, state: String? = nil, isCompactLicense: Bool? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserCredentials", "userCredentialId": userCredentialId, "userId": userId, "credentialIcon": credentialIcon, "credentialName": credentialName, "credentialTypeId": credentialTypeId, "credentialType": credentialType, "expirationDate": expirationDate, "credentialStatus": credentialStatus, "credentialStatusId": credentialStatusId, "licenseNumber": licenseNumber, "frontFilePhoto": frontFilePhoto, "backFilePhoto": backFilePhoto, "state": state, "isCompactLicense": isCompactLicense])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userCredentialId: String? {
        get {
          return resultMap["userCredentialId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userCredentialId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var credentialIcon: String? {
        get {
          return resultMap["credentialIcon"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialIcon")
        }
      }

      public var credentialName: String? {
        get {
          return resultMap["credentialName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialName")
        }
      }

      public var credentialTypeId: String? {
        get {
          return resultMap["credentialTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialTypeId")
        }
      }

      public var credentialType: String? {
        get {
          return resultMap["credentialType"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialType")
        }
      }

      public var expirationDate: String? {
        get {
          return resultMap["expirationDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "expirationDate")
        }
      }

      public var credentialStatus: String? {
        get {
          return resultMap["credentialStatus"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialStatus")
        }
      }

      public var credentialStatusId: String? {
        get {
          return resultMap["credentialStatusId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialStatusId")
        }
      }

      public var licenseNumber: String? {
        get {
          return resultMap["licenseNumber"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "licenseNumber")
        }
      }

      public var frontFilePhoto: String? {
        get {
          return resultMap["frontFilePhoto"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "frontFilePhoto")
        }
      }

      public var backFilePhoto: String? {
        get {
          return resultMap["backFilePhoto"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "backFilePhoto")
        }
      }

      public var state: String? {
        get {
          return resultMap["state"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "state")
        }
      }

      public var isCompactLicense: Bool? {
        get {
          return resultMap["isCompactLicense"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isCompactLicense")
        }
      }
    }
  }
}

public final class DeleteARequestMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation deleteARequest($requestId: Long!) {
      deleteARequest(requestId: $requestId)
    }
    """

  public let operationName: String = "deleteARequest"

  public var requestId: String

  public init(requestId: String) {
    self.requestId = requestId
  }

  public var variables: GraphQLMap? {
    return ["requestId": requestId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteARequest", arguments: ["requestId": GraphQLVariable("requestId")], type: .scalar(Bool.self)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(deleteARequest: Bool? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "deleteARequest": deleteARequest])
    }

    public var deleteARequest: Bool? {
      get {
        return resultMap["deleteARequest"] as? Bool
      }
      set {
        resultMap.updateValue(newValue, forKey: "deleteARequest")
      }
    }
  }
}

public final class DisplayAllMessageOfUserSubscription: GraphQLSubscription {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    subscription displayAllMessageOfUser($senderId: Long!, $receiverId: Long!) {
      displayAllMessagesOfUser(senderId: $senderId, receiverId: $receiverId) {
        __typename
        messageId
        text
        senderId
        receiverId
        timeStamp
        receiverInformation {
          __typename
          firstName
          lastName
          profilePhoto
        }
        senderInformation {
          __typename
          firstName
          lastName
          profilePhoto
        }
      }
    }
    """

  public let operationName: String = "displayAllMessageOfUser"

  public var senderId: String
  public var receiverId: String

  public init(senderId: String, receiverId: String) {
    self.senderId = senderId
    self.receiverId = receiverId
  }

  public var variables: GraphQLMap? {
    return ["senderId": senderId, "receiverId": receiverId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("displayAllMessagesOfUser", arguments: ["senderId": GraphQLVariable("senderId"), "receiverId": GraphQLVariable("receiverId")], type: .list(.object(DisplayAllMessagesOfUser.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(displayAllMessagesOfUser: [DisplayAllMessagesOfUser?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Subscription", "displayAllMessagesOfUser": displayAllMessagesOfUser.flatMap { (value: [DisplayAllMessagesOfUser?]) -> [ResultMap?] in value.map { (value: DisplayAllMessagesOfUser?) -> ResultMap? in value.flatMap { (value: DisplayAllMessagesOfUser) -> ResultMap in value.resultMap } } }])
    }

    public var displayAllMessagesOfUser: [DisplayAllMessagesOfUser?]? {
      get {
        return (resultMap["displayAllMessagesOfUser"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [DisplayAllMessagesOfUser?] in value.map { (value: ResultMap?) -> DisplayAllMessagesOfUser? in value.flatMap { (value: ResultMap) -> DisplayAllMessagesOfUser in DisplayAllMessagesOfUser(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [DisplayAllMessagesOfUser?]) -> [ResultMap?] in value.map { (value: DisplayAllMessagesOfUser?) -> ResultMap? in value.flatMap { (value: DisplayAllMessagesOfUser) -> ResultMap in value.resultMap } } }, forKey: "displayAllMessagesOfUser")
      }
    }

    public struct DisplayAllMessagesOfUser: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Messages"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("messageId", type: .scalar(String.self)),
        GraphQLField("text", type: .scalar(String.self)),
        GraphQLField("senderId", type: .scalar(String.self)),
        GraphQLField("receiverId", type: .scalar(String.self)),
        GraphQLField("timeStamp", type: .scalar(String.self)),
        GraphQLField("receiverInformation", type: .object(ReceiverInformation.selections)),
        GraphQLField("senderInformation", type: .object(SenderInformation.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(messageId: String? = nil, text: String? = nil, senderId: String? = nil, receiverId: String? = nil, timeStamp: String? = nil, receiverInformation: ReceiverInformation? = nil, senderInformation: SenderInformation? = nil) {
        self.init(unsafeResultMap: ["__typename": "Messages", "messageId": messageId, "text": text, "senderId": senderId, "receiverId": receiverId, "timeStamp": timeStamp, "receiverInformation": receiverInformation.flatMap { (value: ReceiverInformation) -> ResultMap in value.resultMap }, "senderInformation": senderInformation.flatMap { (value: SenderInformation) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var messageId: String? {
        get {
          return resultMap["messageId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "messageId")
        }
      }

      public var text: String? {
        get {
          return resultMap["text"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "text")
        }
      }

      public var senderId: String? {
        get {
          return resultMap["senderId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "senderId")
        }
      }

      public var receiverId: String? {
        get {
          return resultMap["receiverId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "receiverId")
        }
      }

      public var timeStamp: String? {
        get {
          return resultMap["timeStamp"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "timeStamp")
        }
      }

      public var receiverInformation: ReceiverInformation? {
        get {
          return (resultMap["receiverInformation"] as? ResultMap).flatMap { ReceiverInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "receiverInformation")
        }
      }

      public var senderInformation: SenderInformation? {
        get {
          return (resultMap["senderInformation"] as? ResultMap).flatMap { SenderInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "senderInformation")
        }
      }

      public struct ReceiverInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String, lastName: String, profilePhoto: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }
      }

      public struct SenderInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String, lastName: String, profilePhoto: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }
      }
    }
  }
}

public final class DisplayAllMessagesSubscription: GraphQLSubscription {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    subscription displayAllMessages($senderId: Long) {
      displayAllMessages(senderId: $senderId) {
        __typename
        messageId
        text
        senderId
        receiverId
        timeStamp
        receiverInformation {
          __typename
          firstName
          lastName
          profilePhoto
        }
        senderInformation {
          __typename
          firstName
          lastName
          profilePhoto
        }
      }
    }
    """

  public let operationName: String = "displayAllMessages"

  public var senderId: String?

  public init(senderId: String? = nil) {
    self.senderId = senderId
  }

  public var variables: GraphQLMap? {
    return ["senderId": senderId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("displayAllMessages", arguments: ["senderId": GraphQLVariable("senderId")], type: .list(.object(DisplayAllMessage.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(displayAllMessages: [DisplayAllMessage?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Subscription", "displayAllMessages": displayAllMessages.flatMap { (value: [DisplayAllMessage?]) -> [ResultMap?] in value.map { (value: DisplayAllMessage?) -> ResultMap? in value.flatMap { (value: DisplayAllMessage) -> ResultMap in value.resultMap } } }])
    }

    public var displayAllMessages: [DisplayAllMessage?]? {
      get {
        return (resultMap["displayAllMessages"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [DisplayAllMessage?] in value.map { (value: ResultMap?) -> DisplayAllMessage? in value.flatMap { (value: ResultMap) -> DisplayAllMessage in DisplayAllMessage(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [DisplayAllMessage?]) -> [ResultMap?] in value.map { (value: DisplayAllMessage?) -> ResultMap? in value.flatMap { (value: DisplayAllMessage) -> ResultMap in value.resultMap } } }, forKey: "displayAllMessages")
      }
    }

    public struct DisplayAllMessage: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Messages"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("messageId", type: .scalar(String.self)),
        GraphQLField("text", type: .scalar(String.self)),
        GraphQLField("senderId", type: .scalar(String.self)),
        GraphQLField("receiverId", type: .scalar(String.self)),
        GraphQLField("timeStamp", type: .scalar(String.self)),
        GraphQLField("receiverInformation", type: .object(ReceiverInformation.selections)),
        GraphQLField("senderInformation", type: .object(SenderInformation.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(messageId: String? = nil, text: String? = nil, senderId: String? = nil, receiverId: String? = nil, timeStamp: String? = nil, receiverInformation: ReceiverInformation? = nil, senderInformation: SenderInformation? = nil) {
        self.init(unsafeResultMap: ["__typename": "Messages", "messageId": messageId, "text": text, "senderId": senderId, "receiverId": receiverId, "timeStamp": timeStamp, "receiverInformation": receiverInformation.flatMap { (value: ReceiverInformation) -> ResultMap in value.resultMap }, "senderInformation": senderInformation.flatMap { (value: SenderInformation) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var messageId: String? {
        get {
          return resultMap["messageId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "messageId")
        }
      }

      public var text: String? {
        get {
          return resultMap["text"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "text")
        }
      }

      public var senderId: String? {
        get {
          return resultMap["senderId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "senderId")
        }
      }

      public var receiverId: String? {
        get {
          return resultMap["receiverId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "receiverId")
        }
      }

      public var timeStamp: String? {
        get {
          return resultMap["timeStamp"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "timeStamp")
        }
      }

      public var receiverInformation: ReceiverInformation? {
        get {
          return (resultMap["receiverInformation"] as? ResultMap).flatMap { ReceiverInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "receiverInformation")
        }
      }

      public var senderInformation: SenderInformation? {
        get {
          return (resultMap["senderInformation"] as? ResultMap).flatMap { SenderInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "senderInformation")
        }
      }

      public struct ReceiverInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String, lastName: String, profilePhoto: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }
      }

      public struct SenderInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String, lastName: String, profilePhoto: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }
      }
    }
  }
}

public final class FindMessagesOfSenderAndReceiverIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query findMessagesOfSenderAndReceiverId($senderId: Long!, $receiverId: Long!) {
      findMessagesOfSenderAndReceiverId(senderId: $senderId, receiverId: $receiverId) {
        __typename
        messageId
        text
        senderId
        receiverId
        timeStamp
        receiverInformation {
          __typename
          firstName
          lastName
          profilePhoto
        }
        senderInformation {
          __typename
          firstName
          lastName
          profilePhoto
        }
      }
    }
    """

  public let operationName: String = "findMessagesOfSenderAndReceiverId"

  public var senderId: String
  public var receiverId: String

  public init(senderId: String, receiverId: String) {
    self.senderId = senderId
    self.receiverId = receiverId
  }

  public var variables: GraphQLMap? {
    return ["senderId": senderId, "receiverId": receiverId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findMessagesOfSenderAndReceiverId", arguments: ["senderId": GraphQLVariable("senderId"), "receiverId": GraphQLVariable("receiverId")], type: .list(.object(FindMessagesOfSenderAndReceiverId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findMessagesOfSenderAndReceiverId: [FindMessagesOfSenderAndReceiverId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findMessagesOfSenderAndReceiverId": findMessagesOfSenderAndReceiverId.flatMap { (value: [FindMessagesOfSenderAndReceiverId?]) -> [ResultMap?] in value.map { (value: FindMessagesOfSenderAndReceiverId?) -> ResultMap? in value.flatMap { (value: FindMessagesOfSenderAndReceiverId) -> ResultMap in value.resultMap } } }])
    }

    public var findMessagesOfSenderAndReceiverId: [FindMessagesOfSenderAndReceiverId?]? {
      get {
        return (resultMap["findMessagesOfSenderAndReceiverId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FindMessagesOfSenderAndReceiverId?] in value.map { (value: ResultMap?) -> FindMessagesOfSenderAndReceiverId? in value.flatMap { (value: ResultMap) -> FindMessagesOfSenderAndReceiverId in FindMessagesOfSenderAndReceiverId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [FindMessagesOfSenderAndReceiverId?]) -> [ResultMap?] in value.map { (value: FindMessagesOfSenderAndReceiverId?) -> ResultMap? in value.flatMap { (value: FindMessagesOfSenderAndReceiverId) -> ResultMap in value.resultMap } } }, forKey: "findMessagesOfSenderAndReceiverId")
      }
    }

    public struct FindMessagesOfSenderAndReceiverId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Messages"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("messageId", type: .scalar(String.self)),
        GraphQLField("text", type: .scalar(String.self)),
        GraphQLField("senderId", type: .scalar(String.self)),
        GraphQLField("receiverId", type: .scalar(String.self)),
        GraphQLField("timeStamp", type: .scalar(String.self)),
        GraphQLField("receiverInformation", type: .object(ReceiverInformation.selections)),
        GraphQLField("senderInformation", type: .object(SenderInformation.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(messageId: String? = nil, text: String? = nil, senderId: String? = nil, receiverId: String? = nil, timeStamp: String? = nil, receiverInformation: ReceiverInformation? = nil, senderInformation: SenderInformation? = nil) {
        self.init(unsafeResultMap: ["__typename": "Messages", "messageId": messageId, "text": text, "senderId": senderId, "receiverId": receiverId, "timeStamp": timeStamp, "receiverInformation": receiverInformation.flatMap { (value: ReceiverInformation) -> ResultMap in value.resultMap }, "senderInformation": senderInformation.flatMap { (value: SenderInformation) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var messageId: String? {
        get {
          return resultMap["messageId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "messageId")
        }
      }

      public var text: String? {
        get {
          return resultMap["text"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "text")
        }
      }

      public var senderId: String? {
        get {
          return resultMap["senderId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "senderId")
        }
      }

      public var receiverId: String? {
        get {
          return resultMap["receiverId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "receiverId")
        }
      }

      public var timeStamp: String? {
        get {
          return resultMap["timeStamp"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "timeStamp")
        }
      }

      public var receiverInformation: ReceiverInformation? {
        get {
          return (resultMap["receiverInformation"] as? ResultMap).flatMap { ReceiverInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "receiverInformation")
        }
      }

      public var senderInformation: SenderInformation? {
        get {
          return (resultMap["senderInformation"] as? ResultMap).flatMap { SenderInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "senderInformation")
        }
      }

      public struct ReceiverInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String, lastName: String, profilePhoto: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }
      }

      public struct SenderInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String, lastName: String, profilePhoto: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }
      }
    }
  }
}

public final class FindMessagesOfUserQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query findMessagesOfUser($senderId: Long) {
      findMessagesOfUser(senderId: $senderId) {
        __typename
        messageId
        text
        senderId
        receiverId
        timeStamp
        receiverInformation {
          __typename
          firstName
          lastName
          profilePhoto
        }
        senderInformation {
          __typename
          firstName
          lastName
          profilePhoto
        }
      }
    }
    """

  public let operationName: String = "findMessagesOfUser"

  public var senderId: String?

  public init(senderId: String? = nil) {
    self.senderId = senderId
  }

  public var variables: GraphQLMap? {
    return ["senderId": senderId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findMessagesOfUser", arguments: ["senderId": GraphQLVariable("senderId")], type: .list(.object(FindMessagesOfUser.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findMessagesOfUser: [FindMessagesOfUser?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findMessagesOfUser": findMessagesOfUser.flatMap { (value: [FindMessagesOfUser?]) -> [ResultMap?] in value.map { (value: FindMessagesOfUser?) -> ResultMap? in value.flatMap { (value: FindMessagesOfUser) -> ResultMap in value.resultMap } } }])
    }

    public var findMessagesOfUser: [FindMessagesOfUser?]? {
      get {
        return (resultMap["findMessagesOfUser"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FindMessagesOfUser?] in value.map { (value: ResultMap?) -> FindMessagesOfUser? in value.flatMap { (value: ResultMap) -> FindMessagesOfUser in FindMessagesOfUser(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [FindMessagesOfUser?]) -> [ResultMap?] in value.map { (value: FindMessagesOfUser?) -> ResultMap? in value.flatMap { (value: FindMessagesOfUser) -> ResultMap in value.resultMap } } }, forKey: "findMessagesOfUser")
      }
    }

    public struct FindMessagesOfUser: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Messages"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("messageId", type: .scalar(String.self)),
        GraphQLField("text", type: .scalar(String.self)),
        GraphQLField("senderId", type: .scalar(String.self)),
        GraphQLField("receiverId", type: .scalar(String.self)),
        GraphQLField("timeStamp", type: .scalar(String.self)),
        GraphQLField("receiverInformation", type: .object(ReceiverInformation.selections)),
        GraphQLField("senderInformation", type: .object(SenderInformation.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(messageId: String? = nil, text: String? = nil, senderId: String? = nil, receiverId: String? = nil, timeStamp: String? = nil, receiverInformation: ReceiverInformation? = nil, senderInformation: SenderInformation? = nil) {
        self.init(unsafeResultMap: ["__typename": "Messages", "messageId": messageId, "text": text, "senderId": senderId, "receiverId": receiverId, "timeStamp": timeStamp, "receiverInformation": receiverInformation.flatMap { (value: ReceiverInformation) -> ResultMap in value.resultMap }, "senderInformation": senderInformation.flatMap { (value: SenderInformation) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var messageId: String? {
        get {
          return resultMap["messageId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "messageId")
        }
      }

      public var text: String? {
        get {
          return resultMap["text"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "text")
        }
      }

      public var senderId: String? {
        get {
          return resultMap["senderId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "senderId")
        }
      }

      public var receiverId: String? {
        get {
          return resultMap["receiverId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "receiverId")
        }
      }

      public var timeStamp: String? {
        get {
          return resultMap["timeStamp"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "timeStamp")
        }
      }

      public var receiverInformation: ReceiverInformation? {
        get {
          return (resultMap["receiverInformation"] as? ResultMap).flatMap { ReceiverInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "receiverInformation")
        }
      }

      public var senderInformation: SenderInformation? {
        get {
          return (resultMap["senderInformation"] as? ResultMap).flatMap { SenderInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "senderInformation")
        }
      }

      public struct ReceiverInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String, lastName: String, profilePhoto: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }
      }

      public struct SenderInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String, lastName: String, profilePhoto: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }
      }
    }
  }
}

public final class CreateEducationMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation CreateEducation($userId: Long, $school: String!, $degreeTypeId: Long!, $degreeOfStudy: String!, $graduationDate: String!, $location: String, $state: String) {
      createUserEducation(userId: $userId, school: $school, degreeTypeId: $degreeTypeId, degreeOfStudy: $degreeOfStudy, graduationDate: $graduationDate, location: $location, state: $state) {
        __typename
        userId
      }
    }
    """

  public let operationName: String = "CreateEducation"

  public var userId: String?
  public var school: String
  public var degreeTypeId: String
  public var degreeOfStudy: String
  public var graduationDate: String
  public var location: String?
  public var state: String?

  public init(userId: String? = nil, school: String, degreeTypeId: String, degreeOfStudy: String, graduationDate: String, location: String? = nil, state: String? = nil) {
    self.userId = userId
    self.school = school
    self.degreeTypeId = degreeTypeId
    self.degreeOfStudy = degreeOfStudy
    self.graduationDate = graduationDate
    self.location = location
    self.state = state
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "school": school, "degreeTypeId": degreeTypeId, "degreeOfStudy": degreeOfStudy, "graduationDate": graduationDate, "location": location, "state": state]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUserEducation", arguments: ["userId": GraphQLVariable("userId"), "school": GraphQLVariable("school"), "degreeTypeId": GraphQLVariable("degreeTypeId"), "degreeOfStudy": GraphQLVariable("degreeOfStudy"), "graduationDate": GraphQLVariable("graduationDate"), "location": GraphQLVariable("location"), "state": GraphQLVariable("state")], type: .nonNull(.object(CreateUserEducation.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createUserEducation: CreateUserEducation) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createUserEducation": createUserEducation.resultMap])
    }

    public var createUserEducation: CreateUserEducation {
      get {
        return CreateUserEducation(unsafeResultMap: resultMap["createUserEducation"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "createUserEducation")
      }
    }

    public struct CreateUserEducation: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserEducation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String) {
        self.init(unsafeResultMap: ["__typename": "UserEducation", "userId": userId])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["userId"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }
    }
  }
}

public final class FetchEducationQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query FetchEducation($userId: Long!) {
      findAllUserEducationByUserId(userId: $userId) {
        __typename
        userId
        school
        degreeTypeId
        degreeOfStudy
        graduationDate
        location
        state
      }
    }
    """

  public let operationName: String = "FetchEducation"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findAllUserEducationByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(FindAllUserEducationByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findAllUserEducationByUserId: [FindAllUserEducationByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findAllUserEducationByUserId": findAllUserEducationByUserId.flatMap { (value: [FindAllUserEducationByUserId?]) -> [ResultMap?] in value.map { (value: FindAllUserEducationByUserId?) -> ResultMap? in value.flatMap { (value: FindAllUserEducationByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var findAllUserEducationByUserId: [FindAllUserEducationByUserId?]? {
      get {
        return (resultMap["findAllUserEducationByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FindAllUserEducationByUserId?] in value.map { (value: ResultMap?) -> FindAllUserEducationByUserId? in value.flatMap { (value: ResultMap) -> FindAllUserEducationByUserId in FindAllUserEducationByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [FindAllUserEducationByUserId?]) -> [ResultMap?] in value.map { (value: FindAllUserEducationByUserId?) -> ResultMap? in value.flatMap { (value: FindAllUserEducationByUserId) -> ResultMap in value.resultMap } } }, forKey: "findAllUserEducationByUserId")
      }
    }

    public struct FindAllUserEducationByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserEducation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("school", type: .nonNull(.scalar(String.self))),
        GraphQLField("degreeTypeId", type: .nonNull(.scalar(String.self))),
        GraphQLField("degreeOfStudy", type: .nonNull(.scalar(String.self))),
        GraphQLField("graduationDate", type: .nonNull(.scalar(String.self))),
        GraphQLField("location", type: .scalar(String.self)),
        GraphQLField("state", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, school: String, degreeTypeId: String, degreeOfStudy: String, graduationDate: String, location: String? = nil, state: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserEducation", "userId": userId, "school": school, "degreeTypeId": degreeTypeId, "degreeOfStudy": degreeOfStudy, "graduationDate": graduationDate, "location": location, "state": state])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["userId"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var school: String {
        get {
          return resultMap["school"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "school")
        }
      }

      public var degreeTypeId: String {
        get {
          return resultMap["degreeTypeId"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "degreeTypeId")
        }
      }

      public var degreeOfStudy: String {
        get {
          return resultMap["degreeOfStudy"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "degreeOfStudy")
        }
      }

      public var graduationDate: String {
        get {
          return resultMap["graduationDate"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "graduationDate")
        }
      }

      public var location: String? {
        get {
          return resultMap["location"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "location")
        }
      }

      public var state: String? {
        get {
          return resultMap["state"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "state")
        }
      }
    }
  }
}

public final class FetchColleaguesFromWorkSpaceQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query FetchColleaguesFromWorkSpace($userId: Long!) {
      getWorkspaceByUserId(userId: $userId) {
        __typename
        colleagues {
          __typename
          firstName
          profilePhoto
          userId
        }
      }
    }
    """

  public let operationName: String = "FetchColleaguesFromWorkSpace"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getWorkspaceByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(GetWorkspaceByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getWorkspaceByUserId: [GetWorkspaceByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getWorkspaceByUserId": getWorkspaceByUserId.flatMap { (value: [GetWorkspaceByUserId?]) -> [ResultMap?] in value.map { (value: GetWorkspaceByUserId?) -> ResultMap? in value.flatMap { (value: GetWorkspaceByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var getWorkspaceByUserId: [GetWorkspaceByUserId?]? {
      get {
        return (resultMap["getWorkspaceByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetWorkspaceByUserId?] in value.map { (value: ResultMap?) -> GetWorkspaceByUserId? in value.flatMap { (value: ResultMap) -> GetWorkspaceByUserId in GetWorkspaceByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetWorkspaceByUserId?]) -> [ResultMap?] in value.map { (value: GetWorkspaceByUserId?) -> ResultMap? in value.flatMap { (value: GetWorkspaceByUserId) -> ResultMap in value.resultMap } } }, forKey: "getWorkspaceByUserId")
      }
    }

    public struct GetWorkspaceByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserWorkspaces"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("colleagues", type: .list(.object(Colleague.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(colleagues: [Colleague?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserWorkspaces", "colleagues": colleagues.flatMap { (value: [Colleague?]) -> [ResultMap?] in value.map { (value: Colleague?) -> ResultMap? in value.flatMap { (value: Colleague) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var colleagues: [Colleague?]? {
        get {
          return (resultMap["colleagues"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Colleague?] in value.map { (value: ResultMap?) -> Colleague? in value.flatMap { (value: ResultMap) -> Colleague in Colleague(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Colleague?]) -> [ResultMap?] in value.map { (value: Colleague?) -> ResultMap? in value.flatMap { (value: Colleague) -> ResultMap in value.resultMap } } }, forKey: "colleagues")
        }
      }

      public struct Colleague: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String, profilePhoto: String? = nil, userId: String) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "profilePhoto": profilePhoto, "userId": userId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }

        public var userId: String {
          get {
            return resultMap["userId"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }
      }
    }
  }
}

public final class FetchNotificationsFromWorkSpaceQueryQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query FetchNotificationsFromWorkSpaceQuery($userId: Long!) {
      findUserNotificationByUserId(userId: $userId) {
        __typename
        userNotificationId
        userId
        notificationTypeId
        notificationTypeName
        isEnablePush
        isEnableSMS
        isEnableEmail
      }
    }
    """

  public let operationName: String = "FetchNotificationsFromWorkSpaceQuery"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findUserNotificationByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(FindUserNotificationByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findUserNotificationByUserId: [FindUserNotificationByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findUserNotificationByUserId": findUserNotificationByUserId.flatMap { (value: [FindUserNotificationByUserId?]) -> [ResultMap?] in value.map { (value: FindUserNotificationByUserId?) -> ResultMap? in value.flatMap { (value: FindUserNotificationByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var findUserNotificationByUserId: [FindUserNotificationByUserId?]? {
      get {
        return (resultMap["findUserNotificationByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FindUserNotificationByUserId?] in value.map { (value: ResultMap?) -> FindUserNotificationByUserId? in value.flatMap { (value: ResultMap) -> FindUserNotificationByUserId in FindUserNotificationByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [FindUserNotificationByUserId?]) -> [ResultMap?] in value.map { (value: FindUserNotificationByUserId?) -> ResultMap? in value.flatMap { (value: FindUserNotificationByUserId) -> ResultMap in value.resultMap } } }, forKey: "findUserNotificationByUserId")
      }
    }

    public struct FindUserNotificationByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserNotification"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userNotificationId", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("notificationTypeId", type: .scalar(String.self)),
        GraphQLField("notificationTypeName", type: .scalar(String.self)),
        GraphQLField("isEnablePush", type: .scalar(Bool.self)),
        GraphQLField("isEnableSMS", type: .scalar(Bool.self)),
        GraphQLField("isEnableEmail", type: .scalar(Bool.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userNotificationId: String? = nil, userId: String? = nil, notificationTypeId: String? = nil, notificationTypeName: String? = nil, isEnablePush: Bool? = nil, isEnableSms: Bool? = nil, isEnableEmail: Bool? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserNotification", "userNotificationId": userNotificationId, "userId": userId, "notificationTypeId": notificationTypeId, "notificationTypeName": notificationTypeName, "isEnablePush": isEnablePush, "isEnableSMS": isEnableSms, "isEnableEmail": isEnableEmail])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userNotificationId: String? {
        get {
          return resultMap["userNotificationId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userNotificationId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var notificationTypeId: String? {
        get {
          return resultMap["notificationTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificationTypeId")
        }
      }

      public var notificationTypeName: String? {
        get {
          return resultMap["notificationTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificationTypeName")
        }
      }

      public var isEnablePush: Bool? {
        get {
          return resultMap["isEnablePush"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isEnablePush")
        }
      }

      public var isEnableSms: Bool? {
        get {
          return resultMap["isEnableSMS"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isEnableSMS")
        }
      }

      public var isEnableEmail: Bool? {
        get {
          return resultMap["isEnableEmail"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isEnableEmail")
        }
      }
    }
  }
}

public final class GetNotificationsByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getNotificationsByUserId($userId: Long!) {
      getNotificationsByUserId(userId: $userId) {
        __typename
        notificationId
        userId
        notificationTypeId
        notificationType
        isRead
        onDate
        content
      }
    }
    """

  public let operationName: String = "getNotificationsByUserId"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getNotificationsByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(GetNotificationsByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getNotificationsByUserId: [GetNotificationsByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getNotificationsByUserId": getNotificationsByUserId.flatMap { (value: [GetNotificationsByUserId?]) -> [ResultMap?] in value.map { (value: GetNotificationsByUserId?) -> ResultMap? in value.flatMap { (value: GetNotificationsByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var getNotificationsByUserId: [GetNotificationsByUserId?]? {
      get {
        return (resultMap["getNotificationsByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetNotificationsByUserId?] in value.map { (value: ResultMap?) -> GetNotificationsByUserId? in value.flatMap { (value: ResultMap) -> GetNotificationsByUserId in GetNotificationsByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetNotificationsByUserId?]) -> [ResultMap?] in value.map { (value: GetNotificationsByUserId?) -> ResultMap? in value.flatMap { (value: GetNotificationsByUserId) -> ResultMap in value.resultMap } } }, forKey: "getNotificationsByUserId")
      }
    }

    public struct GetNotificationsByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Notifications"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("notificationId", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("notificationTypeId", type: .scalar(String.self)),
        GraphQLField("notificationType", type: .scalar(String.self)),
        GraphQLField("isRead", type: .scalar(Bool.self)),
        GraphQLField("onDate", type: .scalar(String.self)),
        GraphQLField("content", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(notificationId: String? = nil, userId: String? = nil, notificationTypeId: String? = nil, notificationType: String? = nil, isRead: Bool? = nil, onDate: String? = nil, content: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Notifications", "notificationId": notificationId, "userId": userId, "notificationTypeId": notificationTypeId, "notificationType": notificationType, "isRead": isRead, "onDate": onDate, "content": content])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var notificationId: String? {
        get {
          return resultMap["notificationId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificationId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var notificationTypeId: String? {
        get {
          return resultMap["notificationTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificationTypeId")
        }
      }

      public var notificationType: String? {
        get {
          return resultMap["notificationType"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificationType")
        }
      }

      public var isRead: Bool? {
        get {
          return resultMap["isRead"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isRead")
        }
      }

      public var onDate: String? {
        get {
          return resultMap["onDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "onDate")
        }
      }

      public var content: String? {
        get {
          return resultMap["content"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "content")
        }
      }
    }
  }
}

public final class FindSchedulesByDepartmentIdAndUserIdAndSelectedDateQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query findSchedulesByDepartmentIdAndUserIdAndSelectedDate($userId: Long!, $departmentId: Long!, $onDate: String!) {
      findSchedulesByDepartmentIdAndUserIdAndSelectedDate(userId: $userId, departmentId: $departmentId, onDate: $onDate) {
        __typename
        userId
        scheduleId
        worksiteId
        worksiteName
        worksiteColor
        departmentId
        departmentName
        userId
        staffCoreTypeName
        shiftTypeId
        shiftTypeName
        shiftDate
        startTime
        endTime
        eventType
        isOnCallRequest
        voluntaryLowCensus
        userDetails {
          __typename
          userId
          organizationId
          firstName
          lastName
          middleName
          gender
        }
        workingColleagues {
          __typename
          userId
          organizationId
          firstName
          lastName
          middleName
          gender
        }
        userDetails {
          __typename
          lastName
          firstName
          userId
        }
        worksiteDetails {
          __typename
          businessId
          organizationId
          name
          businessTypeId
          businessType
          businessSubTypeId
          businessTypeCount
          businessSubType
          phoneNumber
          email
          address
          city
          state
          postalCode
          country
        }
      }
    }
    """

  public let operationName: String = "findSchedulesByDepartmentIdAndUserIdAndSelectedDate"

  public var userId: String
  public var departmentId: String
  public var onDate: String

  public init(userId: String, departmentId: String, onDate: String) {
    self.userId = userId
    self.departmentId = departmentId
    self.onDate = onDate
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "departmentId": departmentId, "onDate": onDate]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findSchedulesByDepartmentIdAndUserIdAndSelectedDate", arguments: ["userId": GraphQLVariable("userId"), "departmentId": GraphQLVariable("departmentId"), "onDate": GraphQLVariable("onDate")], type: .list(.object(FindSchedulesByDepartmentIdAndUserIdAndSelectedDate.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findSchedulesByDepartmentIdAndUserIdAndSelectedDate: [FindSchedulesByDepartmentIdAndUserIdAndSelectedDate?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findSchedulesByDepartmentIdAndUserIdAndSelectedDate": findSchedulesByDepartmentIdAndUserIdAndSelectedDate.flatMap { (value: [FindSchedulesByDepartmentIdAndUserIdAndSelectedDate?]) -> [ResultMap?] in value.map { (value: FindSchedulesByDepartmentIdAndUserIdAndSelectedDate?) -> ResultMap? in value.flatMap { (value: FindSchedulesByDepartmentIdAndUserIdAndSelectedDate) -> ResultMap in value.resultMap } } }])
    }

    public var findSchedulesByDepartmentIdAndUserIdAndSelectedDate: [FindSchedulesByDepartmentIdAndUserIdAndSelectedDate?]? {
      get {
        return (resultMap["findSchedulesByDepartmentIdAndUserIdAndSelectedDate"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FindSchedulesByDepartmentIdAndUserIdAndSelectedDate?] in value.map { (value: ResultMap?) -> FindSchedulesByDepartmentIdAndUserIdAndSelectedDate? in value.flatMap { (value: ResultMap) -> FindSchedulesByDepartmentIdAndUserIdAndSelectedDate in FindSchedulesByDepartmentIdAndUserIdAndSelectedDate(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [FindSchedulesByDepartmentIdAndUserIdAndSelectedDate?]) -> [ResultMap?] in value.map { (value: FindSchedulesByDepartmentIdAndUserIdAndSelectedDate?) -> ResultMap? in value.flatMap { (value: FindSchedulesByDepartmentIdAndUserIdAndSelectedDate) -> ResultMap in value.resultMap } } }, forKey: "findSchedulesByDepartmentIdAndUserIdAndSelectedDate")
      }
    }

    public struct FindSchedulesByDepartmentIdAndUserIdAndSelectedDate: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Schedules"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("scheduleId", type: .scalar(String.self)),
        GraphQLField("worksiteId", type: .scalar(String.self)),
        GraphQLField("worksiteName", type: .scalar(String.self)),
        GraphQLField("worksiteColor", type: .scalar(String.self)),
        GraphQLField("departmentId", type: .scalar(String.self)),
        GraphQLField("departmentName", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("staffCoreTypeName", type: .scalar(String.self)),
        GraphQLField("shiftTypeId", type: .scalar(String.self)),
        GraphQLField("shiftTypeName", type: .scalar(String.self)),
        GraphQLField("shiftDate", type: .scalar(String.self)),
        GraphQLField("startTime", type: .scalar(String.self)),
        GraphQLField("endTime", type: .scalar(String.self)),
        GraphQLField("eventType", type: .scalar(String.self)),
        GraphQLField("isOnCallRequest", type: .scalar(Bool.self)),
        GraphQLField("voluntaryLowCensus", type: .scalar(Bool.self)),
        GraphQLField("userDetails", type: .object(UserDetail.selections)),
        GraphQLField("workingColleagues", type: .list(.object(WorkingColleague.selections))),
        GraphQLField("userDetails", type: .object(UserDetail.selections)),
        GraphQLField("worksiteDetails", type: .object(WorksiteDetail.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String? = nil, scheduleId: String? = nil, worksiteId: String? = nil, worksiteName: String? = nil, worksiteColor: String? = nil, departmentId: String? = nil, departmentName: String? = nil, staffCoreTypeName: String? = nil, shiftTypeId: String? = nil, shiftTypeName: String? = nil, shiftDate: String? = nil, startTime: String? = nil, endTime: String? = nil, eventType: String? = nil, isOnCallRequest: Bool? = nil, voluntaryLowCensus: Bool? = nil, userDetails: UserDetail? = nil, workingColleagues: [WorkingColleague?]? = nil, worksiteDetails: WorksiteDetail? = nil) {
        self.init(unsafeResultMap: ["__typename": "Schedules", "userId": userId, "scheduleId": scheduleId, "worksiteId": worksiteId, "worksiteName": worksiteName, "worksiteColor": worksiteColor, "departmentId": departmentId, "departmentName": departmentName, "staffCoreTypeName": staffCoreTypeName, "shiftTypeId": shiftTypeId, "shiftTypeName": shiftTypeName, "shiftDate": shiftDate, "startTime": startTime, "endTime": endTime, "eventType": eventType, "isOnCallRequest": isOnCallRequest, "voluntaryLowCensus": voluntaryLowCensus, "userDetails": userDetails.flatMap { (value: UserDetail) -> ResultMap in value.resultMap }, "workingColleagues": workingColleagues.flatMap { (value: [WorkingColleague?]) -> [ResultMap?] in value.map { (value: WorkingColleague?) -> ResultMap? in value.flatMap { (value: WorkingColleague) -> ResultMap in value.resultMap } } }, "worksiteDetails": worksiteDetails.flatMap { (value: WorksiteDetail) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var scheduleId: String? {
        get {
          return resultMap["scheduleId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "scheduleId")
        }
      }

      public var worksiteId: String? {
        get {
          return resultMap["worksiteId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteId")
        }
      }

      public var worksiteName: String? {
        get {
          return resultMap["worksiteName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteName")
        }
      }

      public var worksiteColor: String? {
        get {
          return resultMap["worksiteColor"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteColor")
        }
      }

      public var departmentId: String? {
        get {
          return resultMap["departmentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentId")
        }
      }

      public var departmentName: String? {
        get {
          return resultMap["departmentName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentName")
        }
      }

      public var staffCoreTypeName: String? {
        get {
          return resultMap["staffCoreTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffCoreTypeName")
        }
      }

      public var shiftTypeId: String? {
        get {
          return resultMap["shiftTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftTypeId")
        }
      }

      public var shiftTypeName: String? {
        get {
          return resultMap["shiftTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftTypeName")
        }
      }

      public var shiftDate: String? {
        get {
          return resultMap["shiftDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftDate")
        }
      }

      public var startTime: String? {
        get {
          return resultMap["startTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: String? {
        get {
          return resultMap["endTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "endTime")
        }
      }

      public var eventType: String? {
        get {
          return resultMap["eventType"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "eventType")
        }
      }

      public var isOnCallRequest: Bool? {
        get {
          return resultMap["isOnCallRequest"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isOnCallRequest")
        }
      }

      public var voluntaryLowCensus: Bool? {
        get {
          return resultMap["voluntaryLowCensus"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "voluntaryLowCensus")
        }
      }

      public var userDetails: UserDetail? {
        get {
          return (resultMap["userDetails"] as? ResultMap).flatMap { UserDetail(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "userDetails")
        }
      }

      public var workingColleagues: [WorkingColleague?]? {
        get {
          return (resultMap["workingColleagues"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [WorkingColleague?] in value.map { (value: ResultMap?) -> WorkingColleague? in value.flatMap { (value: ResultMap) -> WorkingColleague in WorkingColleague(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [WorkingColleague?]) -> [ResultMap?] in value.map { (value: WorkingColleague?) -> ResultMap? in value.flatMap { (value: WorkingColleague) -> ResultMap in value.resultMap } } }, forKey: "workingColleagues")
        }
      }

      public var worksiteDetails: WorksiteDetail? {
        get {
          return (resultMap["worksiteDetails"] as? ResultMap).flatMap { WorksiteDetail(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "worksiteDetails")
        }
      }

      public struct UserDetail: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
          GraphQLField("organizationId", type: .scalar(String.self)),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("middleName", type: .scalar(String.self)),
          GraphQLField("gender", type: .scalar(String.self)),
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, organizationId: String? = nil, firstName: String, lastName: String, middleName: String? = nil, gender: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "userId": userId, "organizationId": organizationId, "firstName": firstName, "lastName": lastName, "middleName": middleName, "gender": gender])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["userId"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var organizationId: String? {
          get {
            return resultMap["organizationId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "organizationId")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var middleName: String? {
          get {
            return resultMap["middleName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "middleName")
          }
        }

        public var gender: String? {
          get {
            return resultMap["gender"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "gender")
          }
        }
      }

      public struct WorkingColleague: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
          GraphQLField("organizationId", type: .scalar(String.self)),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("middleName", type: .scalar(String.self)),
          GraphQLField("gender", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, organizationId: String? = nil, firstName: String, lastName: String, middleName: String? = nil, gender: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "userId": userId, "organizationId": organizationId, "firstName": firstName, "lastName": lastName, "middleName": middleName, "gender": gender])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["userId"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var organizationId: String? {
          get {
            return resultMap["organizationId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "organizationId")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var middleName: String? {
          get {
            return resultMap["middleName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "middleName")
          }
        }

        public var gender: String? {
          get {
            return resultMap["gender"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "gender")
          }
        }
      }

      public struct WorksiteDetail: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Businesses"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("businessId", type: .scalar(String.self)),
          GraphQLField("organizationId", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("businessTypeId", type: .scalar(String.self)),
          GraphQLField("businessType", type: .scalar(String.self)),
          GraphQLField("businessSubTypeId", type: .scalar(String.self)),
          GraphQLField("businessTypeCount", type: .scalar(String.self)),
          GraphQLField("businessSubType", type: .scalar(String.self)),
          GraphQLField("phoneNumber", type: .scalar(String.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("address", type: .scalar(String.self)),
          GraphQLField("city", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("postalCode", type: .scalar(String.self)),
          GraphQLField("country", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(businessId: String? = nil, organizationId: String, name: String? = nil, businessTypeId: String? = nil, businessType: String? = nil, businessSubTypeId: String? = nil, businessTypeCount: String? = nil, businessSubType: String? = nil, phoneNumber: String? = nil, email: String? = nil, address: String? = nil, city: String? = nil, state: String? = nil, postalCode: String? = nil, country: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Businesses", "businessId": businessId, "organizationId": organizationId, "name": name, "businessTypeId": businessTypeId, "businessType": businessType, "businessSubTypeId": businessSubTypeId, "businessTypeCount": businessTypeCount, "businessSubType": businessSubType, "phoneNumber": phoneNumber, "email": email, "address": address, "city": city, "state": state, "postalCode": postalCode, "country": country])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var businessId: String? {
          get {
            return resultMap["businessId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessId")
          }
        }

        public var organizationId: String {
          get {
            return resultMap["organizationId"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "organizationId")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var businessTypeId: String? {
          get {
            return resultMap["businessTypeId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessTypeId")
          }
        }

        public var businessType: String? {
          get {
            return resultMap["businessType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessType")
          }
        }

        public var businessSubTypeId: String? {
          get {
            return resultMap["businessSubTypeId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessSubTypeId")
          }
        }

        public var businessTypeCount: String? {
          get {
            return resultMap["businessTypeCount"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessTypeCount")
          }
        }

        public var businessSubType: String? {
          get {
            return resultMap["businessSubType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessSubType")
          }
        }

        public var phoneNumber: String? {
          get {
            return resultMap["phoneNumber"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var address: String? {
          get {
            return resultMap["address"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "address")
          }
        }

        public var city: String? {
          get {
            return resultMap["city"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "city")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var postalCode: String? {
          get {
            return resultMap["postalCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "postalCode")
          }
        }

        public var country: String? {
          get {
            return resultMap["country"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "country")
          }
        }
      }
    }
  }
}

public final class CreateSwapShiftRequestMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createSwapShiftRequest($senderId: Long!, $receiverId: Long!, $sourceShiftId: Long!, $requestedShiftId: Long!) {
      createSwapShiftRequest(senderId: $senderId, receiverId: $receiverId, sourceShiftId: $sourceShiftId, requestedShiftId: $requestedShiftId) {
        __typename
        swapShiftRequestId
      }
    }
    """

  public let operationName: String = "createSwapShiftRequest"

  public var senderId: String
  public var receiverId: String
  public var sourceShiftId: String
  public var requestedShiftId: String

  public init(senderId: String, receiverId: String, sourceShiftId: String, requestedShiftId: String) {
    self.senderId = senderId
    self.receiverId = receiverId
    self.sourceShiftId = sourceShiftId
    self.requestedShiftId = requestedShiftId
  }

  public var variables: GraphQLMap? {
    return ["senderId": senderId, "receiverId": receiverId, "sourceShiftId": sourceShiftId, "requestedShiftId": requestedShiftId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createSwapShiftRequest", arguments: ["senderId": GraphQLVariable("senderId"), "receiverId": GraphQLVariable("receiverId"), "sourceShiftId": GraphQLVariable("sourceShiftId"), "requestedShiftId": GraphQLVariable("requestedShiftId")], type: .object(CreateSwapShiftRequest.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createSwapShiftRequest: CreateSwapShiftRequest? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createSwapShiftRequest": createSwapShiftRequest.flatMap { (value: CreateSwapShiftRequest) -> ResultMap in value.resultMap }])
    }

    public var createSwapShiftRequest: CreateSwapShiftRequest? {
      get {
        return (resultMap["createSwapShiftRequest"] as? ResultMap).flatMap { CreateSwapShiftRequest(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createSwapShiftRequest")
      }
    }

    public struct CreateSwapShiftRequest: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SwapShiftRequests"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("swapShiftRequestId", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(swapShiftRequestId: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "SwapShiftRequests", "swapShiftRequestId": swapShiftRequestId])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var swapShiftRequestId: String? {
        get {
          return resultMap["swapShiftRequestId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "swapShiftRequestId")
        }
      }
    }
  }
}

public final class GetScheduleByUserIdAndSelectedDayQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getScheduleByUserIdAndSelectedDay($userId: Long!, $selectedDate: String!) {
      getScheduleByUserIdAndSelectedDay(userId: $userId, selectedDate: $selectedDate) {
        __typename
        userId
        scheduleId
        worksiteId
        worksiteName
        worksiteColor
        departmentId
        departmentName
        userId
        staffCoreTypeName
        shiftTypeId
        shiftTypeName
        shiftDate
        startTime
        endTime
        eventType
        isOnCallRequest
        voluntaryLowCensus
        userDetails {
          __typename
          userId
          organizationId
          firstName
          lastName
          middleName
          gender
        }
        workingColleagues {
          __typename
          userId
          organizationId
          firstName
          lastName
          middleName
          gender
        }
        userDetails {
          __typename
          lastName
          firstName
          userId
        }
        worksiteDetails {
          __typename
          businessId
          organizationId
          name
          businessTypeId
          businessType
          businessSubTypeId
          businessTypeCount
          businessSubType
          phoneNumber
          email
          address
          city
          state
          postalCode
          country
        }
      }
    }
    """

  public let operationName: String = "getScheduleByUserIdAndSelectedDay"

  public var userId: String
  public var selectedDate: String

  public init(userId: String, selectedDate: String) {
    self.userId = userId
    self.selectedDate = selectedDate
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "selectedDate": selectedDate]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getScheduleByUserIdAndSelectedDay", arguments: ["userId": GraphQLVariable("userId"), "selectedDate": GraphQLVariable("selectedDate")], type: .list(.object(GetScheduleByUserIdAndSelectedDay.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getScheduleByUserIdAndSelectedDay: [GetScheduleByUserIdAndSelectedDay?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getScheduleByUserIdAndSelectedDay": getScheduleByUserIdAndSelectedDay.flatMap { (value: [GetScheduleByUserIdAndSelectedDay?]) -> [ResultMap?] in value.map { (value: GetScheduleByUserIdAndSelectedDay?) -> ResultMap? in value.flatMap { (value: GetScheduleByUserIdAndSelectedDay) -> ResultMap in value.resultMap } } }])
    }

    public var getScheduleByUserIdAndSelectedDay: [GetScheduleByUserIdAndSelectedDay?]? {
      get {
        return (resultMap["getScheduleByUserIdAndSelectedDay"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetScheduleByUserIdAndSelectedDay?] in value.map { (value: ResultMap?) -> GetScheduleByUserIdAndSelectedDay? in value.flatMap { (value: ResultMap) -> GetScheduleByUserIdAndSelectedDay in GetScheduleByUserIdAndSelectedDay(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetScheduleByUserIdAndSelectedDay?]) -> [ResultMap?] in value.map { (value: GetScheduleByUserIdAndSelectedDay?) -> ResultMap? in value.flatMap { (value: GetScheduleByUserIdAndSelectedDay) -> ResultMap in value.resultMap } } }, forKey: "getScheduleByUserIdAndSelectedDay")
      }
    }

    public struct GetScheduleByUserIdAndSelectedDay: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Schedules"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("scheduleId", type: .scalar(String.self)),
        GraphQLField("worksiteId", type: .scalar(String.self)),
        GraphQLField("worksiteName", type: .scalar(String.self)),
        GraphQLField("worksiteColor", type: .scalar(String.self)),
        GraphQLField("departmentId", type: .scalar(String.self)),
        GraphQLField("departmentName", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("staffCoreTypeName", type: .scalar(String.self)),
        GraphQLField("shiftTypeId", type: .scalar(String.self)),
        GraphQLField("shiftTypeName", type: .scalar(String.self)),
        GraphQLField("shiftDate", type: .scalar(String.self)),
        GraphQLField("startTime", type: .scalar(String.self)),
        GraphQLField("endTime", type: .scalar(String.self)),
        GraphQLField("eventType", type: .scalar(String.self)),
        GraphQLField("isOnCallRequest", type: .scalar(Bool.self)),
        GraphQLField("voluntaryLowCensus", type: .scalar(Bool.self)),
        GraphQLField("userDetails", type: .object(UserDetail.selections)),
        GraphQLField("workingColleagues", type: .list(.object(WorkingColleague.selections))),
        GraphQLField("userDetails", type: .object(UserDetail.selections)),
        GraphQLField("worksiteDetails", type: .object(WorksiteDetail.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String? = nil, scheduleId: String? = nil, worksiteId: String? = nil, worksiteName: String? = nil, worksiteColor: String? = nil, departmentId: String? = nil, departmentName: String? = nil, staffCoreTypeName: String? = nil, shiftTypeId: String? = nil, shiftTypeName: String? = nil, shiftDate: String? = nil, startTime: String? = nil, endTime: String? = nil, eventType: String? = nil, isOnCallRequest: Bool? = nil, voluntaryLowCensus: Bool? = nil, userDetails: UserDetail? = nil, workingColleagues: [WorkingColleague?]? = nil, worksiteDetails: WorksiteDetail? = nil) {
        self.init(unsafeResultMap: ["__typename": "Schedules", "userId": userId, "scheduleId": scheduleId, "worksiteId": worksiteId, "worksiteName": worksiteName, "worksiteColor": worksiteColor, "departmentId": departmentId, "departmentName": departmentName, "staffCoreTypeName": staffCoreTypeName, "shiftTypeId": shiftTypeId, "shiftTypeName": shiftTypeName, "shiftDate": shiftDate, "startTime": startTime, "endTime": endTime, "eventType": eventType, "isOnCallRequest": isOnCallRequest, "voluntaryLowCensus": voluntaryLowCensus, "userDetails": userDetails.flatMap { (value: UserDetail) -> ResultMap in value.resultMap }, "workingColleagues": workingColleagues.flatMap { (value: [WorkingColleague?]) -> [ResultMap?] in value.map { (value: WorkingColleague?) -> ResultMap? in value.flatMap { (value: WorkingColleague) -> ResultMap in value.resultMap } } }, "worksiteDetails": worksiteDetails.flatMap { (value: WorksiteDetail) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var scheduleId: String? {
        get {
          return resultMap["scheduleId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "scheduleId")
        }
      }

      public var worksiteId: String? {
        get {
          return resultMap["worksiteId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteId")
        }
      }

      public var worksiteName: String? {
        get {
          return resultMap["worksiteName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteName")
        }
      }

      public var worksiteColor: String? {
        get {
          return resultMap["worksiteColor"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteColor")
        }
      }

      public var departmentId: String? {
        get {
          return resultMap["departmentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentId")
        }
      }

      public var departmentName: String? {
        get {
          return resultMap["departmentName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentName")
        }
      }

      public var staffCoreTypeName: String? {
        get {
          return resultMap["staffCoreTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffCoreTypeName")
        }
      }

      public var shiftTypeId: String? {
        get {
          return resultMap["shiftTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftTypeId")
        }
      }

      public var shiftTypeName: String? {
        get {
          return resultMap["shiftTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftTypeName")
        }
      }

      public var shiftDate: String? {
        get {
          return resultMap["shiftDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftDate")
        }
      }

      public var startTime: String? {
        get {
          return resultMap["startTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: String? {
        get {
          return resultMap["endTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "endTime")
        }
      }

      public var eventType: String? {
        get {
          return resultMap["eventType"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "eventType")
        }
      }

      public var isOnCallRequest: Bool? {
        get {
          return resultMap["isOnCallRequest"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isOnCallRequest")
        }
      }

      public var voluntaryLowCensus: Bool? {
        get {
          return resultMap["voluntaryLowCensus"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "voluntaryLowCensus")
        }
      }

      public var userDetails: UserDetail? {
        get {
          return (resultMap["userDetails"] as? ResultMap).flatMap { UserDetail(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "userDetails")
        }
      }

      public var workingColleagues: [WorkingColleague?]? {
        get {
          return (resultMap["workingColleagues"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [WorkingColleague?] in value.map { (value: ResultMap?) -> WorkingColleague? in value.flatMap { (value: ResultMap) -> WorkingColleague in WorkingColleague(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [WorkingColleague?]) -> [ResultMap?] in value.map { (value: WorkingColleague?) -> ResultMap? in value.flatMap { (value: WorkingColleague) -> ResultMap in value.resultMap } } }, forKey: "workingColleagues")
        }
      }

      public var worksiteDetails: WorksiteDetail? {
        get {
          return (resultMap["worksiteDetails"] as? ResultMap).flatMap { WorksiteDetail(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "worksiteDetails")
        }
      }

      public struct UserDetail: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
          GraphQLField("organizationId", type: .scalar(String.self)),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("middleName", type: .scalar(String.self)),
          GraphQLField("gender", type: .scalar(String.self)),
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, organizationId: String? = nil, firstName: String, lastName: String, middleName: String? = nil, gender: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "userId": userId, "organizationId": organizationId, "firstName": firstName, "lastName": lastName, "middleName": middleName, "gender": gender])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["userId"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var organizationId: String? {
          get {
            return resultMap["organizationId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "organizationId")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var middleName: String? {
          get {
            return resultMap["middleName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "middleName")
          }
        }

        public var gender: String? {
          get {
            return resultMap["gender"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "gender")
          }
        }
      }

      public struct WorkingColleague: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
          GraphQLField("organizationId", type: .scalar(String.self)),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("middleName", type: .scalar(String.self)),
          GraphQLField("gender", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, organizationId: String? = nil, firstName: String, lastName: String, middleName: String? = nil, gender: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "userId": userId, "organizationId": organizationId, "firstName": firstName, "lastName": lastName, "middleName": middleName, "gender": gender])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["userId"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var organizationId: String? {
          get {
            return resultMap["organizationId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "organizationId")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var middleName: String? {
          get {
            return resultMap["middleName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "middleName")
          }
        }

        public var gender: String? {
          get {
            return resultMap["gender"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "gender")
          }
        }
      }

      public struct WorksiteDetail: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Businesses"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("businessId", type: .scalar(String.self)),
          GraphQLField("organizationId", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("businessTypeId", type: .scalar(String.self)),
          GraphQLField("businessType", type: .scalar(String.self)),
          GraphQLField("businessSubTypeId", type: .scalar(String.self)),
          GraphQLField("businessTypeCount", type: .scalar(String.self)),
          GraphQLField("businessSubType", type: .scalar(String.self)),
          GraphQLField("phoneNumber", type: .scalar(String.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("address", type: .scalar(String.self)),
          GraphQLField("city", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("postalCode", type: .scalar(String.self)),
          GraphQLField("country", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(businessId: String? = nil, organizationId: String, name: String? = nil, businessTypeId: String? = nil, businessType: String? = nil, businessSubTypeId: String? = nil, businessTypeCount: String? = nil, businessSubType: String? = nil, phoneNumber: String? = nil, email: String? = nil, address: String? = nil, city: String? = nil, state: String? = nil, postalCode: String? = nil, country: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Businesses", "businessId": businessId, "organizationId": organizationId, "name": name, "businessTypeId": businessTypeId, "businessType": businessType, "businessSubTypeId": businessSubTypeId, "businessTypeCount": businessTypeCount, "businessSubType": businessSubType, "phoneNumber": phoneNumber, "email": email, "address": address, "city": city, "state": state, "postalCode": postalCode, "country": country])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var businessId: String? {
          get {
            return resultMap["businessId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessId")
          }
        }

        public var organizationId: String {
          get {
            return resultMap["organizationId"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "organizationId")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var businessTypeId: String? {
          get {
            return resultMap["businessTypeId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessTypeId")
          }
        }

        public var businessType: String? {
          get {
            return resultMap["businessType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessType")
          }
        }

        public var businessSubTypeId: String? {
          get {
            return resultMap["businessSubTypeId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessSubTypeId")
          }
        }

        public var businessTypeCount: String? {
          get {
            return resultMap["businessTypeCount"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessTypeCount")
          }
        }

        public var businessSubType: String? {
          get {
            return resultMap["businessSubType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessSubType")
          }
        }

        public var phoneNumber: String? {
          get {
            return resultMap["phoneNumber"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var address: String? {
          get {
            return resultMap["address"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "address")
          }
        }

        public var city: String? {
          get {
            return resultMap["city"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "city")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var postalCode: String? {
          get {
            return resultMap["postalCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "postalCode")
          }
        }

        public var country: String? {
          get {
            return resultMap["country"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "country")
          }
        }
      }
    }
  }
}

public final class GetSwapShiftRequestsRecievedByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getSwapShiftRequestsRecievedByUserId($userId: Long!) {
      getSwapShiftRequestsRecievedByUserId(userId: $userId) {
        __typename
        swapShiftRequestId
        senderId
        receiverId
        sourceShiftId
        requestedShiftId
        isRecieverAccepted
        isSupervisorAccepted
        createdOn
        updatedOn
        status
        senderInformation {
          __typename
          firstName
          createdOn
          staffUserCoreTypeName
        }
        receiverInformation {
          __typename
          firstName
        }
        sourceShiftInformation {
          __typename
          shiftTypeName
          staffCoreTypeName
          departmentName
        }
        requestedShiftInformation {
          __typename
          departmentName
          worksiteColor
          shiftTypeName
        }
      }
    }
    """

  public let operationName: String = "getSwapShiftRequestsRecievedByUserId"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getSwapShiftRequestsRecievedByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(GetSwapShiftRequestsRecievedByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getSwapShiftRequestsRecievedByUserId: [GetSwapShiftRequestsRecievedByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getSwapShiftRequestsRecievedByUserId": getSwapShiftRequestsRecievedByUserId.flatMap { (value: [GetSwapShiftRequestsRecievedByUserId?]) -> [ResultMap?] in value.map { (value: GetSwapShiftRequestsRecievedByUserId?) -> ResultMap? in value.flatMap { (value: GetSwapShiftRequestsRecievedByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var getSwapShiftRequestsRecievedByUserId: [GetSwapShiftRequestsRecievedByUserId?]? {
      get {
        return (resultMap["getSwapShiftRequestsRecievedByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetSwapShiftRequestsRecievedByUserId?] in value.map { (value: ResultMap?) -> GetSwapShiftRequestsRecievedByUserId? in value.flatMap { (value: ResultMap) -> GetSwapShiftRequestsRecievedByUserId in GetSwapShiftRequestsRecievedByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetSwapShiftRequestsRecievedByUserId?]) -> [ResultMap?] in value.map { (value: GetSwapShiftRequestsRecievedByUserId?) -> ResultMap? in value.flatMap { (value: GetSwapShiftRequestsRecievedByUserId) -> ResultMap in value.resultMap } } }, forKey: "getSwapShiftRequestsRecievedByUserId")
      }
    }

    public struct GetSwapShiftRequestsRecievedByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SwapShiftRequests"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("swapShiftRequestId", type: .scalar(String.self)),
        GraphQLField("senderId", type: .scalar(String.self)),
        GraphQLField("receiverId", type: .scalar(String.self)),
        GraphQLField("sourceShiftId", type: .scalar(String.self)),
        GraphQLField("requestedShiftId", type: .scalar(String.self)),
        GraphQLField("isRecieverAccepted", type: .scalar(Bool.self)),
        GraphQLField("isSupervisorAccepted", type: .scalar(Bool.self)),
        GraphQLField("createdOn", type: .scalar(String.self)),
        GraphQLField("updatedOn", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("senderInformation", type: .object(SenderInformation.selections)),
        GraphQLField("receiverInformation", type: .object(ReceiverInformation.selections)),
        GraphQLField("sourceShiftInformation", type: .object(SourceShiftInformation.selections)),
        GraphQLField("requestedShiftInformation", type: .object(RequestedShiftInformation.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(swapShiftRequestId: String? = nil, senderId: String? = nil, receiverId: String? = nil, sourceShiftId: String? = nil, requestedShiftId: String? = nil, isRecieverAccepted: Bool? = nil, isSupervisorAccepted: Bool? = nil, createdOn: String? = nil, updatedOn: String? = nil, status: String? = nil, senderInformation: SenderInformation? = nil, receiverInformation: ReceiverInformation? = nil, sourceShiftInformation: SourceShiftInformation? = nil, requestedShiftInformation: RequestedShiftInformation? = nil) {
        self.init(unsafeResultMap: ["__typename": "SwapShiftRequests", "swapShiftRequestId": swapShiftRequestId, "senderId": senderId, "receiverId": receiverId, "sourceShiftId": sourceShiftId, "requestedShiftId": requestedShiftId, "isRecieverAccepted": isRecieverAccepted, "isSupervisorAccepted": isSupervisorAccepted, "createdOn": createdOn, "updatedOn": updatedOn, "status": status, "senderInformation": senderInformation.flatMap { (value: SenderInformation) -> ResultMap in value.resultMap }, "receiverInformation": receiverInformation.flatMap { (value: ReceiverInformation) -> ResultMap in value.resultMap }, "sourceShiftInformation": sourceShiftInformation.flatMap { (value: SourceShiftInformation) -> ResultMap in value.resultMap }, "requestedShiftInformation": requestedShiftInformation.flatMap { (value: RequestedShiftInformation) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var swapShiftRequestId: String? {
        get {
          return resultMap["swapShiftRequestId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "swapShiftRequestId")
        }
      }

      public var senderId: String? {
        get {
          return resultMap["senderId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "senderId")
        }
      }

      public var receiverId: String? {
        get {
          return resultMap["receiverId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "receiverId")
        }
      }

      public var sourceShiftId: String? {
        get {
          return resultMap["sourceShiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "sourceShiftId")
        }
      }

      public var requestedShiftId: String? {
        get {
          return resultMap["requestedShiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestedShiftId")
        }
      }

      public var isRecieverAccepted: Bool? {
        get {
          return resultMap["isRecieverAccepted"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isRecieverAccepted")
        }
      }

      public var isSupervisorAccepted: Bool? {
        get {
          return resultMap["isSupervisorAccepted"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isSupervisorAccepted")
        }
      }

      public var createdOn: String? {
        get {
          return resultMap["createdOn"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdOn")
        }
      }

      public var updatedOn: String? {
        get {
          return resultMap["updatedOn"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "updatedOn")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var senderInformation: SenderInformation? {
        get {
          return (resultMap["senderInformation"] as? ResultMap).flatMap { SenderInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "senderInformation")
        }
      }

      public var receiverInformation: ReceiverInformation? {
        get {
          return (resultMap["receiverInformation"] as? ResultMap).flatMap { ReceiverInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "receiverInformation")
        }
      }

      public var sourceShiftInformation: SourceShiftInformation? {
        get {
          return (resultMap["sourceShiftInformation"] as? ResultMap).flatMap { SourceShiftInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "sourceShiftInformation")
        }
      }

      public var requestedShiftInformation: RequestedShiftInformation? {
        get {
          return (resultMap["requestedShiftInformation"] as? ResultMap).flatMap { RequestedShiftInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "requestedShiftInformation")
        }
      }

      public struct SenderInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("createdOn", type: .scalar(String.self)),
          GraphQLField("staffUserCoreTypeName", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String, createdOn: String? = nil, staffUserCoreTypeName: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "createdOn": createdOn, "staffUserCoreTypeName": staffUserCoreTypeName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var createdOn: String? {
          get {
            return resultMap["createdOn"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdOn")
          }
        }

        public var staffUserCoreTypeName: String? {
          get {
            return resultMap["staffUserCoreTypeName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "staffUserCoreTypeName")
          }
        }
      }

      public struct ReceiverInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }
      }

      public struct SourceShiftInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Schedules"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("shiftTypeName", type: .scalar(String.self)),
          GraphQLField("staffCoreTypeName", type: .scalar(String.self)),
          GraphQLField("departmentName", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(shiftTypeName: String? = nil, staffCoreTypeName: String? = nil, departmentName: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Schedules", "shiftTypeName": shiftTypeName, "staffCoreTypeName": staffCoreTypeName, "departmentName": departmentName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var shiftTypeName: String? {
          get {
            return resultMap["shiftTypeName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "shiftTypeName")
          }
        }

        public var staffCoreTypeName: String? {
          get {
            return resultMap["staffCoreTypeName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "staffCoreTypeName")
          }
        }

        public var departmentName: String? {
          get {
            return resultMap["departmentName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "departmentName")
          }
        }
      }

      public struct RequestedShiftInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Schedules"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("departmentName", type: .scalar(String.self)),
          GraphQLField("worksiteColor", type: .scalar(String.self)),
          GraphQLField("shiftTypeName", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(departmentName: String? = nil, worksiteColor: String? = nil, shiftTypeName: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Schedules", "departmentName": departmentName, "worksiteColor": worksiteColor, "shiftTypeName": shiftTypeName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var departmentName: String? {
          get {
            return resultMap["departmentName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "departmentName")
          }
        }

        public var worksiteColor: String? {
          get {
            return resultMap["worksiteColor"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "worksiteColor")
          }
        }

        public var shiftTypeName: String? {
          get {
            return resultMap["shiftTypeName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "shiftTypeName")
          }
        }
      }
    }
  }
}

public final class GetSwapShiftRequestsSentByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getSwapShiftRequestsSentByUserId($userId: Long!) {
      getSwapShiftRequestsSentByUserId(userId: $userId) {
        __typename
        swapShiftRequestId
        senderId
        senderInformation {
          __typename
          firstName
        }
        receiverId
        receiverInformation {
          __typename
          firstName
        }
        sourceShiftId
        sourceShiftInformation {
          __typename
          shiftTypeName
          staffCoreTypeName
          departmentName
        }
        requestedShiftId
        requestedShiftInformation {
          __typename
          departmentName
          worksiteColor
          shiftTypeName
        }
        isRecieverAccepted
        isSupervisorAccepted
        createdOn
        updatedOn
        status
      }
    }
    """

  public let operationName: String = "getSwapShiftRequestsSentByUserId"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getSwapShiftRequestsSentByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(GetSwapShiftRequestsSentByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getSwapShiftRequestsSentByUserId: [GetSwapShiftRequestsSentByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getSwapShiftRequestsSentByUserId": getSwapShiftRequestsSentByUserId.flatMap { (value: [GetSwapShiftRequestsSentByUserId?]) -> [ResultMap?] in value.map { (value: GetSwapShiftRequestsSentByUserId?) -> ResultMap? in value.flatMap { (value: GetSwapShiftRequestsSentByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var getSwapShiftRequestsSentByUserId: [GetSwapShiftRequestsSentByUserId?]? {
      get {
        return (resultMap["getSwapShiftRequestsSentByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetSwapShiftRequestsSentByUserId?] in value.map { (value: ResultMap?) -> GetSwapShiftRequestsSentByUserId? in value.flatMap { (value: ResultMap) -> GetSwapShiftRequestsSentByUserId in GetSwapShiftRequestsSentByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetSwapShiftRequestsSentByUserId?]) -> [ResultMap?] in value.map { (value: GetSwapShiftRequestsSentByUserId?) -> ResultMap? in value.flatMap { (value: GetSwapShiftRequestsSentByUserId) -> ResultMap in value.resultMap } } }, forKey: "getSwapShiftRequestsSentByUserId")
      }
    }

    public struct GetSwapShiftRequestsSentByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["SwapShiftRequests"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("swapShiftRequestId", type: .scalar(String.self)),
        GraphQLField("senderId", type: .scalar(String.self)),
        GraphQLField("senderInformation", type: .object(SenderInformation.selections)),
        GraphQLField("receiverId", type: .scalar(String.self)),
        GraphQLField("receiverInformation", type: .object(ReceiverInformation.selections)),
        GraphQLField("sourceShiftId", type: .scalar(String.self)),
        GraphQLField("sourceShiftInformation", type: .object(SourceShiftInformation.selections)),
        GraphQLField("requestedShiftId", type: .scalar(String.self)),
        GraphQLField("requestedShiftInformation", type: .object(RequestedShiftInformation.selections)),
        GraphQLField("isRecieverAccepted", type: .scalar(Bool.self)),
        GraphQLField("isSupervisorAccepted", type: .scalar(Bool.self)),
        GraphQLField("createdOn", type: .scalar(String.self)),
        GraphQLField("updatedOn", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(swapShiftRequestId: String? = nil, senderId: String? = nil, senderInformation: SenderInformation? = nil, receiverId: String? = nil, receiverInformation: ReceiverInformation? = nil, sourceShiftId: String? = nil, sourceShiftInformation: SourceShiftInformation? = nil, requestedShiftId: String? = nil, requestedShiftInformation: RequestedShiftInformation? = nil, isRecieverAccepted: Bool? = nil, isSupervisorAccepted: Bool? = nil, createdOn: String? = nil, updatedOn: String? = nil, status: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "SwapShiftRequests", "swapShiftRequestId": swapShiftRequestId, "senderId": senderId, "senderInformation": senderInformation.flatMap { (value: SenderInformation) -> ResultMap in value.resultMap }, "receiverId": receiverId, "receiverInformation": receiverInformation.flatMap { (value: ReceiverInformation) -> ResultMap in value.resultMap }, "sourceShiftId": sourceShiftId, "sourceShiftInformation": sourceShiftInformation.flatMap { (value: SourceShiftInformation) -> ResultMap in value.resultMap }, "requestedShiftId": requestedShiftId, "requestedShiftInformation": requestedShiftInformation.flatMap { (value: RequestedShiftInformation) -> ResultMap in value.resultMap }, "isRecieverAccepted": isRecieverAccepted, "isSupervisorAccepted": isSupervisorAccepted, "createdOn": createdOn, "updatedOn": updatedOn, "status": status])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var swapShiftRequestId: String? {
        get {
          return resultMap["swapShiftRequestId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "swapShiftRequestId")
        }
      }

      public var senderId: String? {
        get {
          return resultMap["senderId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "senderId")
        }
      }

      public var senderInformation: SenderInformation? {
        get {
          return (resultMap["senderInformation"] as? ResultMap).flatMap { SenderInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "senderInformation")
        }
      }

      public var receiverId: String? {
        get {
          return resultMap["receiverId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "receiverId")
        }
      }

      public var receiverInformation: ReceiverInformation? {
        get {
          return (resultMap["receiverInformation"] as? ResultMap).flatMap { ReceiverInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "receiverInformation")
        }
      }

      public var sourceShiftId: String? {
        get {
          return resultMap["sourceShiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "sourceShiftId")
        }
      }

      public var sourceShiftInformation: SourceShiftInformation? {
        get {
          return (resultMap["sourceShiftInformation"] as? ResultMap).flatMap { SourceShiftInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "sourceShiftInformation")
        }
      }

      public var requestedShiftId: String? {
        get {
          return resultMap["requestedShiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestedShiftId")
        }
      }

      public var requestedShiftInformation: RequestedShiftInformation? {
        get {
          return (resultMap["requestedShiftInformation"] as? ResultMap).flatMap { RequestedShiftInformation(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "requestedShiftInformation")
        }
      }

      public var isRecieverAccepted: Bool? {
        get {
          return resultMap["isRecieverAccepted"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isRecieverAccepted")
        }
      }

      public var isSupervisorAccepted: Bool? {
        get {
          return resultMap["isSupervisorAccepted"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isSupervisorAccepted")
        }
      }

      public var createdOn: String? {
        get {
          return resultMap["createdOn"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdOn")
        }
      }

      public var updatedOn: String? {
        get {
          return resultMap["updatedOn"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "updatedOn")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public struct SenderInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }
      }

      public struct ReceiverInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }
      }

      public struct SourceShiftInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Schedules"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("shiftTypeName", type: .scalar(String.self)),
          GraphQLField("staffCoreTypeName", type: .scalar(String.self)),
          GraphQLField("departmentName", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(shiftTypeName: String? = nil, staffCoreTypeName: String? = nil, departmentName: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Schedules", "shiftTypeName": shiftTypeName, "staffCoreTypeName": staffCoreTypeName, "departmentName": departmentName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var shiftTypeName: String? {
          get {
            return resultMap["shiftTypeName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "shiftTypeName")
          }
        }

        public var staffCoreTypeName: String? {
          get {
            return resultMap["staffCoreTypeName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "staffCoreTypeName")
          }
        }

        public var departmentName: String? {
          get {
            return resultMap["departmentName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "departmentName")
          }
        }
      }

      public struct RequestedShiftInformation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Schedules"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("departmentName", type: .scalar(String.self)),
          GraphQLField("worksiteColor", type: .scalar(String.self)),
          GraphQLField("shiftTypeName", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(departmentName: String? = nil, worksiteColor: String? = nil, shiftTypeName: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Schedules", "departmentName": departmentName, "worksiteColor": worksiteColor, "shiftTypeName": shiftTypeName])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var departmentName: String? {
          get {
            return resultMap["departmentName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "departmentName")
          }
        }

        public var worksiteColor: String? {
          get {
            return resultMap["worksiteColor"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "worksiteColor")
          }
        }

        public var shiftTypeName: String? {
          get {
            return resultMap["shiftTypeName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "shiftTypeName")
          }
        }
      }
    }
  }
}

public final class FindUsersByIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query findUsersById($userId: Long!) {
      findUsersById(userId: $userId) {
        __typename
        userId
        firstName
        lastName
        middleName
        email
        password
        preferredName
        address1
        address2
        city
        state
        phone
        profilePhoto
        title
        userTypeId
        staffUserCoreTypeId
        dateOfBirth
        linkedInProfile
        organizationId
        gender
        bioInformation
        createdOn
        secondaryPhoneNumber
        secondaryEmail
        staffUserCoreTypeName
        fcmToken
        muteAllNotifications
      }
    }
    """

  public let operationName: String = "findUsersById"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findUsersById", arguments: ["userId": GraphQLVariable("userId")], type: .object(FindUsersById.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findUsersById: FindUsersById? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findUsersById": findUsersById.flatMap { (value: FindUsersById) -> ResultMap in value.resultMap }])
    }

    public var findUsersById: FindUsersById? {
      get {
        return (resultMap["findUsersById"] as? ResultMap).flatMap { FindUsersById(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "findUsersById")
      }
    }

    public struct FindUsersById: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserBasicInformation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
        GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
        GraphQLField("middleName", type: .scalar(String.self)),
        GraphQLField("email", type: .nonNull(.scalar(String.self))),
        GraphQLField("password", type: .scalar(String.self)),
        GraphQLField("preferredName", type: .scalar(String.self)),
        GraphQLField("address1", type: .scalar(String.self)),
        GraphQLField("address2", type: .scalar(String.self)),
        GraphQLField("city", type: .scalar(String.self)),
        GraphQLField("state", type: .scalar(String.self)),
        GraphQLField("phone", type: .nonNull(.scalar(String.self))),
        GraphQLField("profilePhoto", type: .scalar(String.self)),
        GraphQLField("title", type: .scalar(String.self)),
        GraphQLField("userTypeId", type: .scalar(String.self)),
        GraphQLField("staffUserCoreTypeId", type: .scalar(String.self)),
        GraphQLField("dateOfBirth", type: .scalar(String.self)),
        GraphQLField("linkedInProfile", type: .scalar(String.self)),
        GraphQLField("organizationId", type: .scalar(String.self)),
        GraphQLField("gender", type: .scalar(String.self)),
        GraphQLField("bioInformation", type: .scalar(String.self)),
        GraphQLField("createdOn", type: .scalar(String.self)),
        GraphQLField("secondaryPhoneNumber", type: .scalar(String.self)),
        GraphQLField("secondaryEmail", type: .scalar(String.self)),
        GraphQLField("staffUserCoreTypeName", type: .scalar(String.self)),
        GraphQLField("fcmToken", type: .scalar(String.self)),
        GraphQLField("muteAllNotifications", type: .scalar(Bool.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, firstName: String, lastName: String, middleName: String? = nil, email: String, password: String? = nil, preferredName: String? = nil, address1: String? = nil, address2: String? = nil, city: String? = nil, state: String? = nil, phone: String, profilePhoto: String? = nil, title: String? = nil, userTypeId: String? = nil, staffUserCoreTypeId: String? = nil, dateOfBirth: String? = nil, linkedInProfile: String? = nil, organizationId: String? = nil, gender: String? = nil, bioInformation: String? = nil, createdOn: String? = nil, secondaryPhoneNumber: String? = nil, secondaryEmail: String? = nil, staffUserCoreTypeName: String? = nil, fcmToken: String? = nil, muteAllNotifications: Bool? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "userId": userId, "firstName": firstName, "lastName": lastName, "middleName": middleName, "email": email, "password": password, "preferredName": preferredName, "address1": address1, "address2": address2, "city": city, "state": state, "phone": phone, "profilePhoto": profilePhoto, "title": title, "userTypeId": userTypeId, "staffUserCoreTypeId": staffUserCoreTypeId, "dateOfBirth": dateOfBirth, "linkedInProfile": linkedInProfile, "organizationId": organizationId, "gender": gender, "bioInformation": bioInformation, "createdOn": createdOn, "secondaryPhoneNumber": secondaryPhoneNumber, "secondaryEmail": secondaryEmail, "staffUserCoreTypeName": staffUserCoreTypeName, "fcmToken": fcmToken, "muteAllNotifications": muteAllNotifications])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["userId"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var firstName: String {
        get {
          return resultMap["firstName"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String {
        get {
          return resultMap["lastName"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "lastName")
        }
      }

      public var middleName: String? {
        get {
          return resultMap["middleName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "middleName")
        }
      }

      public var email: String {
        get {
          return resultMap["email"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "email")
        }
      }

      public var password: String? {
        get {
          return resultMap["password"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "password")
        }
      }

      public var preferredName: String? {
        get {
          return resultMap["preferredName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "preferredName")
        }
      }

      public var address1: String? {
        get {
          return resultMap["address1"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "address1")
        }
      }

      public var address2: String? {
        get {
          return resultMap["address2"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "address2")
        }
      }

      public var city: String? {
        get {
          return resultMap["city"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "city")
        }
      }

      public var state: String? {
        get {
          return resultMap["state"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "state")
        }
      }

      public var phone: String {
        get {
          return resultMap["phone"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "phone")
        }
      }

      public var profilePhoto: String? {
        get {
          return resultMap["profilePhoto"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "profilePhoto")
        }
      }

      public var title: String? {
        get {
          return resultMap["title"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "title")
        }
      }

      public var userTypeId: String? {
        get {
          return resultMap["userTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userTypeId")
        }
      }

      public var staffUserCoreTypeId: String? {
        get {
          return resultMap["staffUserCoreTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffUserCoreTypeId")
        }
      }

      public var dateOfBirth: String? {
        get {
          return resultMap["dateOfBirth"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "dateOfBirth")
        }
      }

      public var linkedInProfile: String? {
        get {
          return resultMap["linkedInProfile"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "linkedInProfile")
        }
      }

      public var organizationId: String? {
        get {
          return resultMap["organizationId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "organizationId")
        }
      }

      public var gender: String? {
        get {
          return resultMap["gender"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "gender")
        }
      }

      public var bioInformation: String? {
        get {
          return resultMap["bioInformation"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "bioInformation")
        }
      }

      public var createdOn: String? {
        get {
          return resultMap["createdOn"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdOn")
        }
      }

      public var secondaryPhoneNumber: String? {
        get {
          return resultMap["secondaryPhoneNumber"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "secondaryPhoneNumber")
        }
      }

      public var secondaryEmail: String? {
        get {
          return resultMap["secondaryEmail"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "secondaryEmail")
        }
      }

      public var staffUserCoreTypeName: String? {
        get {
          return resultMap["staffUserCoreTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffUserCoreTypeName")
        }
      }

      public var fcmToken: String? {
        get {
          return resultMap["fcmToken"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "fcmToken")
        }
      }

      public var muteAllNotifications: Bool? {
        get {
          return resultMap["muteAllNotifications"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "muteAllNotifications")
        }
      }
    }
  }
}

public final class FindAllUserCredentialsByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query findAllUserCredentialsByUserId($userId: Long!) {
      findAllUserCredentialsByUserId(userId: $userId) {
        __typename
        userCredentialId
        userId
        credentialIcon
        credentialName
        credentialTypeId
        credentialSubTypeId
        credentialSubTypeName
        credentialType
        expirationDate
        credentialStatus
        credentialStatusId
        licenseNumber
        frontFilePhoto
        backFilePhoto
        state
        isCompactLicense
      }
    }
    """

  public let operationName: String = "findAllUserCredentialsByUserId"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findAllUserCredentialsByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(FindAllUserCredentialsByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findAllUserCredentialsByUserId: [FindAllUserCredentialsByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findAllUserCredentialsByUserId": findAllUserCredentialsByUserId.flatMap { (value: [FindAllUserCredentialsByUserId?]) -> [ResultMap?] in value.map { (value: FindAllUserCredentialsByUserId?) -> ResultMap? in value.flatMap { (value: FindAllUserCredentialsByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var findAllUserCredentialsByUserId: [FindAllUserCredentialsByUserId?]? {
      get {
        return (resultMap["findAllUserCredentialsByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FindAllUserCredentialsByUserId?] in value.map { (value: ResultMap?) -> FindAllUserCredentialsByUserId? in value.flatMap { (value: ResultMap) -> FindAllUserCredentialsByUserId in FindAllUserCredentialsByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [FindAllUserCredentialsByUserId?]) -> [ResultMap?] in value.map { (value: FindAllUserCredentialsByUserId?) -> ResultMap? in value.flatMap { (value: FindAllUserCredentialsByUserId) -> ResultMap in value.resultMap } } }, forKey: "findAllUserCredentialsByUserId")
      }
    }

    public struct FindAllUserCredentialsByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserCredentials"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userCredentialId", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("credentialIcon", type: .scalar(String.self)),
        GraphQLField("credentialName", type: .scalar(String.self)),
        GraphQLField("credentialTypeId", type: .scalar(String.self)),
        GraphQLField("credentialSubTypeId", type: .scalar(String.self)),
        GraphQLField("credentialSubTypeName", type: .scalar(String.self)),
        GraphQLField("credentialType", type: .scalar(String.self)),
        GraphQLField("expirationDate", type: .scalar(String.self)),
        GraphQLField("credentialStatus", type: .scalar(String.self)),
        GraphQLField("credentialStatusId", type: .scalar(String.self)),
        GraphQLField("licenseNumber", type: .scalar(String.self)),
        GraphQLField("frontFilePhoto", type: .scalar(String.self)),
        GraphQLField("backFilePhoto", type: .scalar(String.self)),
        GraphQLField("state", type: .scalar(String.self)),
        GraphQLField("isCompactLicense", type: .scalar(Bool.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userCredentialId: String? = nil, userId: String? = nil, credentialIcon: String? = nil, credentialName: String? = nil, credentialTypeId: String? = nil, credentialSubTypeId: String? = nil, credentialSubTypeName: String? = nil, credentialType: String? = nil, expirationDate: String? = nil, credentialStatus: String? = nil, credentialStatusId: String? = nil, licenseNumber: String? = nil, frontFilePhoto: String? = nil, backFilePhoto: String? = nil, state: String? = nil, isCompactLicense: Bool? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserCredentials", "userCredentialId": userCredentialId, "userId": userId, "credentialIcon": credentialIcon, "credentialName": credentialName, "credentialTypeId": credentialTypeId, "credentialSubTypeId": credentialSubTypeId, "credentialSubTypeName": credentialSubTypeName, "credentialType": credentialType, "expirationDate": expirationDate, "credentialStatus": credentialStatus, "credentialStatusId": credentialStatusId, "licenseNumber": licenseNumber, "frontFilePhoto": frontFilePhoto, "backFilePhoto": backFilePhoto, "state": state, "isCompactLicense": isCompactLicense])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userCredentialId: String? {
        get {
          return resultMap["userCredentialId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userCredentialId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var credentialIcon: String? {
        get {
          return resultMap["credentialIcon"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialIcon")
        }
      }

      public var credentialName: String? {
        get {
          return resultMap["credentialName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialName")
        }
      }

      public var credentialTypeId: String? {
        get {
          return resultMap["credentialTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialTypeId")
        }
      }

      public var credentialSubTypeId: String? {
        get {
          return resultMap["credentialSubTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialSubTypeId")
        }
      }

      public var credentialSubTypeName: String? {
        get {
          return resultMap["credentialSubTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialSubTypeName")
        }
      }

      public var credentialType: String? {
        get {
          return resultMap["credentialType"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialType")
        }
      }

      public var expirationDate: String? {
        get {
          return resultMap["expirationDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "expirationDate")
        }
      }

      public var credentialStatus: String? {
        get {
          return resultMap["credentialStatus"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialStatus")
        }
      }

      public var credentialStatusId: String? {
        get {
          return resultMap["credentialStatusId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "credentialStatusId")
        }
      }

      public var licenseNumber: String? {
        get {
          return resultMap["licenseNumber"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "licenseNumber")
        }
      }

      public var frontFilePhoto: String? {
        get {
          return resultMap["frontFilePhoto"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "frontFilePhoto")
        }
      }

      public var backFilePhoto: String? {
        get {
          return resultMap["backFilePhoto"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "backFilePhoto")
        }
      }

      public var state: String? {
        get {
          return resultMap["state"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "state")
        }
      }

      public var isCompactLicense: Bool? {
        get {
          return resultMap["isCompactLicense"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isCompactLicense")
        }
      }
    }
  }
}

public final class FetchScheduleByDayQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query FetchScheduleByDay($userId: Long!, $selectedDate: String!) {
      getScheduleByUserIdAndSelectedDay(userId: $userId, selectedDate: $selectedDate) {
        __typename
        scheduleId
        worksiteId
        worksiteName
        departmentId
        departmentName
        userId
        shiftTypeId
        shiftTypeName
        shiftDate
        startTime
        endTime
        isOnCallRequest
        voluntaryLowCensus
      }
    }
    """

  public let operationName: String = "FetchScheduleByDay"

  public var userId: String
  public var selectedDate: String

  public init(userId: String, selectedDate: String) {
    self.userId = userId
    self.selectedDate = selectedDate
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "selectedDate": selectedDate]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getScheduleByUserIdAndSelectedDay", arguments: ["userId": GraphQLVariable("userId"), "selectedDate": GraphQLVariable("selectedDate")], type: .list(.object(GetScheduleByUserIdAndSelectedDay.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getScheduleByUserIdAndSelectedDay: [GetScheduleByUserIdAndSelectedDay?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getScheduleByUserIdAndSelectedDay": getScheduleByUserIdAndSelectedDay.flatMap { (value: [GetScheduleByUserIdAndSelectedDay?]) -> [ResultMap?] in value.map { (value: GetScheduleByUserIdAndSelectedDay?) -> ResultMap? in value.flatMap { (value: GetScheduleByUserIdAndSelectedDay) -> ResultMap in value.resultMap } } }])
    }

    public var getScheduleByUserIdAndSelectedDay: [GetScheduleByUserIdAndSelectedDay?]? {
      get {
        return (resultMap["getScheduleByUserIdAndSelectedDay"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetScheduleByUserIdAndSelectedDay?] in value.map { (value: ResultMap?) -> GetScheduleByUserIdAndSelectedDay? in value.flatMap { (value: ResultMap) -> GetScheduleByUserIdAndSelectedDay in GetScheduleByUserIdAndSelectedDay(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetScheduleByUserIdAndSelectedDay?]) -> [ResultMap?] in value.map { (value: GetScheduleByUserIdAndSelectedDay?) -> ResultMap? in value.flatMap { (value: GetScheduleByUserIdAndSelectedDay) -> ResultMap in value.resultMap } } }, forKey: "getScheduleByUserIdAndSelectedDay")
      }
    }

    public struct GetScheduleByUserIdAndSelectedDay: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Schedules"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("scheduleId", type: .scalar(String.self)),
        GraphQLField("worksiteId", type: .scalar(String.self)),
        GraphQLField("worksiteName", type: .scalar(String.self)),
        GraphQLField("departmentId", type: .scalar(String.self)),
        GraphQLField("departmentName", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("shiftTypeId", type: .scalar(String.self)),
        GraphQLField("shiftTypeName", type: .scalar(String.self)),
        GraphQLField("shiftDate", type: .scalar(String.self)),
        GraphQLField("startTime", type: .scalar(String.self)),
        GraphQLField("endTime", type: .scalar(String.self)),
        GraphQLField("isOnCallRequest", type: .scalar(Bool.self)),
        GraphQLField("voluntaryLowCensus", type: .scalar(Bool.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(scheduleId: String? = nil, worksiteId: String? = nil, worksiteName: String? = nil, departmentId: String? = nil, departmentName: String? = nil, userId: String? = nil, shiftTypeId: String? = nil, shiftTypeName: String? = nil, shiftDate: String? = nil, startTime: String? = nil, endTime: String? = nil, isOnCallRequest: Bool? = nil, voluntaryLowCensus: Bool? = nil) {
        self.init(unsafeResultMap: ["__typename": "Schedules", "scheduleId": scheduleId, "worksiteId": worksiteId, "worksiteName": worksiteName, "departmentId": departmentId, "departmentName": departmentName, "userId": userId, "shiftTypeId": shiftTypeId, "shiftTypeName": shiftTypeName, "shiftDate": shiftDate, "startTime": startTime, "endTime": endTime, "isOnCallRequest": isOnCallRequest, "voluntaryLowCensus": voluntaryLowCensus])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var scheduleId: String? {
        get {
          return resultMap["scheduleId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "scheduleId")
        }
      }

      public var worksiteId: String? {
        get {
          return resultMap["worksiteId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteId")
        }
      }

      public var worksiteName: String? {
        get {
          return resultMap["worksiteName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteName")
        }
      }

      public var departmentId: String? {
        get {
          return resultMap["departmentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentId")
        }
      }

      public var departmentName: String? {
        get {
          return resultMap["departmentName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentName")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var shiftTypeId: String? {
        get {
          return resultMap["shiftTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftTypeId")
        }
      }

      public var shiftTypeName: String? {
        get {
          return resultMap["shiftTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftTypeName")
        }
      }

      public var shiftDate: String? {
        get {
          return resultMap["shiftDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftDate")
        }
      }

      public var startTime: String? {
        get {
          return resultMap["startTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: String? {
        get {
          return resultMap["endTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "endTime")
        }
      }

      public var isOnCallRequest: Bool? {
        get {
          return resultMap["isOnCallRequest"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isOnCallRequest")
        }
      }

      public var voluntaryLowCensus: Bool? {
        get {
          return resultMap["voluntaryLowCensus"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "voluntaryLowCensus")
        }
      }
    }
  }
}

public final class GetAllRequestsByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getAllRequestsByUserId($userId: Long!) {
      getAllRequestsByUserId(userId: $userId) {
        __typename
        requestId
        worksiteId
        worksiteName
        departmentId
        requestTypeId
        requestTypeName
        userId
        onDate
        shiftId
        colleagueId
        colleagueName
        notes
      }
    }
    """

  public let operationName: String = "getAllRequestsByUserId"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getAllRequestsByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(GetAllRequestsByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getAllRequestsByUserId: [GetAllRequestsByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getAllRequestsByUserId": getAllRequestsByUserId.flatMap { (value: [GetAllRequestsByUserId?]) -> [ResultMap?] in value.map { (value: GetAllRequestsByUserId?) -> ResultMap? in value.flatMap { (value: GetAllRequestsByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var getAllRequestsByUserId: [GetAllRequestsByUserId?]? {
      get {
        return (resultMap["getAllRequestsByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetAllRequestsByUserId?] in value.map { (value: ResultMap?) -> GetAllRequestsByUserId? in value.flatMap { (value: ResultMap) -> GetAllRequestsByUserId in GetAllRequestsByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetAllRequestsByUserId?]) -> [ResultMap?] in value.map { (value: GetAllRequestsByUserId?) -> ResultMap? in value.flatMap { (value: GetAllRequestsByUserId) -> ResultMap in value.resultMap } } }, forKey: "getAllRequestsByUserId")
      }
    }

    public struct GetAllRequestsByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Requests"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("requestId", type: .scalar(String.self)),
        GraphQLField("worksiteId", type: .scalar(String.self)),
        GraphQLField("worksiteName", type: .scalar(String.self)),
        GraphQLField("departmentId", type: .scalar(String.self)),
        GraphQLField("requestTypeId", type: .scalar(String.self)),
        GraphQLField("requestTypeName", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("onDate", type: .list(.scalar(String.self))),
        GraphQLField("shiftId", type: .scalar(String.self)),
        GraphQLField("colleagueId", type: .scalar(String.self)),
        GraphQLField("colleagueName", type: .scalar(String.self)),
        GraphQLField("notes", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(requestId: String? = nil, worksiteId: String? = nil, worksiteName: String? = nil, departmentId: String? = nil, requestTypeId: String? = nil, requestTypeName: String? = nil, userId: String? = nil, onDate: [String?]? = nil, shiftId: String? = nil, colleagueId: String? = nil, colleagueName: String? = nil, notes: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Requests", "requestId": requestId, "worksiteId": worksiteId, "worksiteName": worksiteName, "departmentId": departmentId, "requestTypeId": requestTypeId, "requestTypeName": requestTypeName, "userId": userId, "onDate": onDate, "shiftId": shiftId, "colleagueId": colleagueId, "colleagueName": colleagueName, "notes": notes])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var requestId: String? {
        get {
          return resultMap["requestId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestId")
        }
      }

      public var worksiteId: String? {
        get {
          return resultMap["worksiteId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteId")
        }
      }

      public var worksiteName: String? {
        get {
          return resultMap["worksiteName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteName")
        }
      }

      public var departmentId: String? {
        get {
          return resultMap["departmentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentId")
        }
      }

      public var requestTypeId: String? {
        get {
          return resultMap["requestTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestTypeId")
        }
      }

      public var requestTypeName: String? {
        get {
          return resultMap["requestTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestTypeName")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var onDate: [String?]? {
        get {
          return resultMap["onDate"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "onDate")
        }
      }

      public var shiftId: String? {
        get {
          return resultMap["shiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftId")
        }
      }

      public var colleagueId: String? {
        get {
          return resultMap["colleagueId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "colleagueId")
        }
      }

      public var colleagueName: String? {
        get {
          return resultMap["colleagueName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "colleagueName")
        }
      }

      public var notes: String? {
        get {
          return resultMap["notes"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "notes")
        }
      }
    }
  }
}

public final class GetWorkspaceByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getWorkspaceByUserId($userId: Long!) {
      getWorkspaceByUserId(userId: $userId) {
        __typename
        userWorkspaceId
        userId
        workspaceId
        workspaceType
        workspaceName
        fteStatus
        isChargeRole
        isPreceptorRole
        organizationId
        invitationCode
        isJoined
        joinedDate
        isSentEmail
        isSentSms
        organizationName
        businessId
        staffUserCoreTypeId
        staffUserCoreTypeName
        departmentShiftId
        hours
        hiredOn
        worksites {
          __typename
          city
          state
          address
          postalCode
          name
          coverPhoto
          businessId
          organizationId
        }
      }
    }
    """

  public let operationName: String = "getWorkspaceByUserId"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getWorkspaceByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(GetWorkspaceByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getWorkspaceByUserId: [GetWorkspaceByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getWorkspaceByUserId": getWorkspaceByUserId.flatMap { (value: [GetWorkspaceByUserId?]) -> [ResultMap?] in value.map { (value: GetWorkspaceByUserId?) -> ResultMap? in value.flatMap { (value: GetWorkspaceByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var getWorkspaceByUserId: [GetWorkspaceByUserId?]? {
      get {
        return (resultMap["getWorkspaceByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetWorkspaceByUserId?] in value.map { (value: ResultMap?) -> GetWorkspaceByUserId? in value.flatMap { (value: ResultMap) -> GetWorkspaceByUserId in GetWorkspaceByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetWorkspaceByUserId?]) -> [ResultMap?] in value.map { (value: GetWorkspaceByUserId?) -> ResultMap? in value.flatMap { (value: GetWorkspaceByUserId) -> ResultMap in value.resultMap } } }, forKey: "getWorkspaceByUserId")
      }
    }

    public struct GetWorkspaceByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserWorkspaces"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userWorkspaceId", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("workspaceId", type: .scalar(String.self)),
        GraphQLField("workspaceType", type: .scalar(String.self)),
        GraphQLField("workspaceName", type: .scalar(String.self)),
        GraphQLField("fteStatus", type: .scalar(String.self)),
        GraphQLField("isChargeRole", type: .scalar(Bool.self)),
        GraphQLField("isPreceptorRole", type: .scalar(Bool.self)),
        GraphQLField("organizationId", type: .scalar(String.self)),
        GraphQLField("invitationCode", type: .scalar(String.self)),
        GraphQLField("isJoined", type: .scalar(Bool.self)),
        GraphQLField("joinedDate", type: .scalar(String.self)),
        GraphQLField("isSentEmail", type: .scalar(Bool.self)),
        GraphQLField("isSentSms", type: .scalar(Bool.self)),
        GraphQLField("organizationName", type: .scalar(String.self)),
        GraphQLField("businessId", type: .scalar(String.self)),
        GraphQLField("staffUserCoreTypeId", type: .scalar(String.self)),
        GraphQLField("staffUserCoreTypeName", type: .scalar(String.self)),
        GraphQLField("departmentShiftId", type: .scalar(String.self)),
        GraphQLField("hours", type: .scalar(String.self)),
        GraphQLField("hiredOn", type: .scalar(String.self)),
        GraphQLField("worksites", type: .list(.object(Worksite.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userWorkspaceId: String, userId: String? = nil, workspaceId: String? = nil, workspaceType: String? = nil, workspaceName: String? = nil, fteStatus: String? = nil, isChargeRole: Bool? = nil, isPreceptorRole: Bool? = nil, organizationId: String? = nil, invitationCode: String? = nil, isJoined: Bool? = nil, joinedDate: String? = nil, isSentEmail: Bool? = nil, isSentSms: Bool? = nil, organizationName: String? = nil, businessId: String? = nil, staffUserCoreTypeId: String? = nil, staffUserCoreTypeName: String? = nil, departmentShiftId: String? = nil, hours: String? = nil, hiredOn: String? = nil, worksites: [Worksite?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserWorkspaces", "userWorkspaceId": userWorkspaceId, "userId": userId, "workspaceId": workspaceId, "workspaceType": workspaceType, "workspaceName": workspaceName, "fteStatus": fteStatus, "isChargeRole": isChargeRole, "isPreceptorRole": isPreceptorRole, "organizationId": organizationId, "invitationCode": invitationCode, "isJoined": isJoined, "joinedDate": joinedDate, "isSentEmail": isSentEmail, "isSentSms": isSentSms, "organizationName": organizationName, "businessId": businessId, "staffUserCoreTypeId": staffUserCoreTypeId, "staffUserCoreTypeName": staffUserCoreTypeName, "departmentShiftId": departmentShiftId, "hours": hours, "hiredOn": hiredOn, "worksites": worksites.flatMap { (value: [Worksite?]) -> [ResultMap?] in value.map { (value: Worksite?) -> ResultMap? in value.flatMap { (value: Worksite) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userWorkspaceId: String {
        get {
          return resultMap["userWorkspaceId"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userWorkspaceId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var workspaceId: String? {
        get {
          return resultMap["workspaceId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "workspaceId")
        }
      }

      public var workspaceType: String? {
        get {
          return resultMap["workspaceType"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "workspaceType")
        }
      }

      public var workspaceName: String? {
        get {
          return resultMap["workspaceName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "workspaceName")
        }
      }

      public var fteStatus: String? {
        get {
          return resultMap["fteStatus"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "fteStatus")
        }
      }

      public var isChargeRole: Bool? {
        get {
          return resultMap["isChargeRole"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isChargeRole")
        }
      }

      public var isPreceptorRole: Bool? {
        get {
          return resultMap["isPreceptorRole"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isPreceptorRole")
        }
      }

      public var organizationId: String? {
        get {
          return resultMap["organizationId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "organizationId")
        }
      }

      public var invitationCode: String? {
        get {
          return resultMap["invitationCode"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "invitationCode")
        }
      }

      public var isJoined: Bool? {
        get {
          return resultMap["isJoined"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isJoined")
        }
      }

      public var joinedDate: String? {
        get {
          return resultMap["joinedDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "joinedDate")
        }
      }

      public var isSentEmail: Bool? {
        get {
          return resultMap["isSentEmail"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isSentEmail")
        }
      }

      public var isSentSms: Bool? {
        get {
          return resultMap["isSentSms"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isSentSms")
        }
      }

      public var organizationName: String? {
        get {
          return resultMap["organizationName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "organizationName")
        }
      }

      public var businessId: String? {
        get {
          return resultMap["businessId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "businessId")
        }
      }

      public var staffUserCoreTypeId: String? {
        get {
          return resultMap["staffUserCoreTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffUserCoreTypeId")
        }
      }

      public var staffUserCoreTypeName: String? {
        get {
          return resultMap["staffUserCoreTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffUserCoreTypeName")
        }
      }

      public var departmentShiftId: String? {
        get {
          return resultMap["departmentShiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentShiftId")
        }
      }

      public var hours: String? {
        get {
          return resultMap["hours"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "hours")
        }
      }

      public var hiredOn: String? {
        get {
          return resultMap["hiredOn"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "hiredOn")
        }
      }

      public var worksites: [Worksite?]? {
        get {
          return (resultMap["worksites"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Worksite?] in value.map { (value: ResultMap?) -> Worksite? in value.flatMap { (value: ResultMap) -> Worksite in Worksite(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Worksite?]) -> [ResultMap?] in value.map { (value: Worksite?) -> ResultMap? in value.flatMap { (value: Worksite) -> ResultMap in value.resultMap } } }, forKey: "worksites")
        }
      }

      public struct Worksite: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Businesses"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("city", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("address", type: .scalar(String.self)),
          GraphQLField("postalCode", type: .scalar(String.self)),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("coverPhoto", type: .scalar(String.self)),
          GraphQLField("businessId", type: .scalar(String.self)),
          GraphQLField("organizationId", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(city: String? = nil, state: String? = nil, address: String? = nil, postalCode: String? = nil, name: String? = nil, coverPhoto: String? = nil, businessId: String? = nil, organizationId: String) {
          self.init(unsafeResultMap: ["__typename": "Businesses", "city": city, "state": state, "address": address, "postalCode": postalCode, "name": name, "coverPhoto": coverPhoto, "businessId": businessId, "organizationId": organizationId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var city: String? {
          get {
            return resultMap["city"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "city")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var address: String? {
          get {
            return resultMap["address"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "address")
          }
        }

        public var postalCode: String? {
          get {
            return resultMap["postalCode"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "postalCode")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var coverPhoto: String? {
          get {
            return resultMap["coverPhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "coverPhoto")
          }
        }

        public var businessId: String? {
          get {
            return resultMap["businessId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessId")
          }
        }

        public var organizationId: String {
          get {
            return resultMap["organizationId"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "organizationId")
          }
        }
      }
    }
  }
}

public final class GetOpenShiftDetailsByWorksiteIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getOpenShiftDetailsByWorksiteId($worksiteId: Long!) {
      getOpenShiftDetailsByWorksiteId(worksiteId: $worksiteId) {
        __typename
        openShiftId
        worksiteId
        departmentId
        onDate
        startTime
        endTime
        staffUserName
        quota
      }
    }
    """

  public let operationName: String = "getOpenShiftDetailsByWorksiteId"

  public var worksiteId: String

  public init(worksiteId: String) {
    self.worksiteId = worksiteId
  }

  public var variables: GraphQLMap? {
    return ["worksiteId": worksiteId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getOpenShiftDetailsByWorksiteId", arguments: ["worksiteId": GraphQLVariable("worksiteId")], type: .list(.object(GetOpenShiftDetailsByWorksiteId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getOpenShiftDetailsByWorksiteId: [GetOpenShiftDetailsByWorksiteId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getOpenShiftDetailsByWorksiteId": getOpenShiftDetailsByWorksiteId.flatMap { (value: [GetOpenShiftDetailsByWorksiteId?]) -> [ResultMap?] in value.map { (value: GetOpenShiftDetailsByWorksiteId?) -> ResultMap? in value.flatMap { (value: GetOpenShiftDetailsByWorksiteId) -> ResultMap in value.resultMap } } }])
    }

    public var getOpenShiftDetailsByWorksiteId: [GetOpenShiftDetailsByWorksiteId?]? {
      get {
        return (resultMap["getOpenShiftDetailsByWorksiteId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetOpenShiftDetailsByWorksiteId?] in value.map { (value: ResultMap?) -> GetOpenShiftDetailsByWorksiteId? in value.flatMap { (value: ResultMap) -> GetOpenShiftDetailsByWorksiteId in GetOpenShiftDetailsByWorksiteId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetOpenShiftDetailsByWorksiteId?]) -> [ResultMap?] in value.map { (value: GetOpenShiftDetailsByWorksiteId?) -> ResultMap? in value.flatMap { (value: GetOpenShiftDetailsByWorksiteId) -> ResultMap in value.resultMap } } }, forKey: "getOpenShiftDetailsByWorksiteId")
      }
    }

    public struct GetOpenShiftDetailsByWorksiteId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["OpenShifts"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("openShiftId", type: .scalar(String.self)),
        GraphQLField("worksiteId", type: .scalar(String.self)),
        GraphQLField("departmentId", type: .scalar(String.self)),
        GraphQLField("onDate", type: .scalar(String.self)),
        GraphQLField("startTime", type: .scalar(String.self)),
        GraphQLField("endTime", type: .scalar(String.self)),
        GraphQLField("staffUserName", type: .scalar(String.self)),
        GraphQLField("quota", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(openShiftId: String? = nil, worksiteId: String? = nil, departmentId: String? = nil, onDate: String? = nil, startTime: String? = nil, endTime: String? = nil, staffUserName: String? = nil, quota: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "OpenShifts", "openShiftId": openShiftId, "worksiteId": worksiteId, "departmentId": departmentId, "onDate": onDate, "startTime": startTime, "endTime": endTime, "staffUserName": staffUserName, "quota": quota])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var openShiftId: String? {
        get {
          return resultMap["openShiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "openShiftId")
        }
      }

      public var worksiteId: String? {
        get {
          return resultMap["worksiteId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteId")
        }
      }

      public var departmentId: String? {
        get {
          return resultMap["departmentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentId")
        }
      }

      public var onDate: String? {
        get {
          return resultMap["onDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "onDate")
        }
      }

      public var startTime: String? {
        get {
          return resultMap["startTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: String? {
        get {
          return resultMap["endTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "endTime")
        }
      }

      public var staffUserName: String? {
        get {
          return resultMap["staffUserName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffUserName")
        }
      }

      public var quota: String? {
        get {
          return resultMap["quota"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "quota")
        }
      }
    }
  }
}

public final class GetOpenShiftsByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getOpenShiftsByUserId($userId: Long!) {
      getOpenShiftsByUserId(userId: $userId) {
        __typename
        openShiftId
        worksiteId
        departmentId
        departmentName
        worksiteName
        worksiteAddress
        shiftTypeId
        onDate
        startTime
        endTime
        staffUserCoreTypeId
        staffUserName
        quota
        isUrgent
        isSendNotifications
        isAutoApprove
        isAutoClose
        isAllowPartial
        note
        status
        invitees {
          __typename
          firstName
          lastName
          profilePhoto
          isApproved
          isAccepted
          userId
        }
        organization {
          __typename
          organizationId
          logo
        }
      }
    }
    """

  public let operationName: String = "getOpenShiftsByUserId"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getOpenShiftsByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(GetOpenShiftsByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getOpenShiftsByUserId: [GetOpenShiftsByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getOpenShiftsByUserId": getOpenShiftsByUserId.flatMap { (value: [GetOpenShiftsByUserId?]) -> [ResultMap?] in value.map { (value: GetOpenShiftsByUserId?) -> ResultMap? in value.flatMap { (value: GetOpenShiftsByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var getOpenShiftsByUserId: [GetOpenShiftsByUserId?]? {
      get {
        return (resultMap["getOpenShiftsByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetOpenShiftsByUserId?] in value.map { (value: ResultMap?) -> GetOpenShiftsByUserId? in value.flatMap { (value: ResultMap) -> GetOpenShiftsByUserId in GetOpenShiftsByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetOpenShiftsByUserId?]) -> [ResultMap?] in value.map { (value: GetOpenShiftsByUserId?) -> ResultMap? in value.flatMap { (value: GetOpenShiftsByUserId) -> ResultMap in value.resultMap } } }, forKey: "getOpenShiftsByUserId")
      }
    }

    public struct GetOpenShiftsByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["OpenShifts"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("openShiftId", type: .scalar(String.self)),
        GraphQLField("worksiteId", type: .scalar(String.self)),
        GraphQLField("departmentId", type: .scalar(String.self)),
        GraphQLField("departmentName", type: .scalar(String.self)),
        GraphQLField("worksiteName", type: .scalar(String.self)),
        GraphQLField("worksiteAddress", type: .scalar(String.self)),
        GraphQLField("shiftTypeId", type: .scalar(String.self)),
        GraphQLField("onDate", type: .scalar(String.self)),
        GraphQLField("startTime", type: .scalar(String.self)),
        GraphQLField("endTime", type: .scalar(String.self)),
        GraphQLField("staffUserCoreTypeId", type: .scalar(String.self)),
        GraphQLField("staffUserName", type: .scalar(String.self)),
        GraphQLField("quota", type: .scalar(String.self)),
        GraphQLField("isUrgent", type: .scalar(Bool.self)),
        GraphQLField("isSendNotifications", type: .scalar(Bool.self)),
        GraphQLField("isAutoApprove", type: .scalar(Bool.self)),
        GraphQLField("isAutoClose", type: .scalar(Bool.self)),
        GraphQLField("isAllowPartial", type: .scalar(Bool.self)),
        GraphQLField("note", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(String.self)),
        GraphQLField("invitees", type: .list(.object(Invitee.selections))),
        GraphQLField("organization", type: .object(Organization.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(openShiftId: String? = nil, worksiteId: String? = nil, departmentId: String? = nil, departmentName: String? = nil, worksiteName: String? = nil, worksiteAddress: String? = nil, shiftTypeId: String? = nil, onDate: String? = nil, startTime: String? = nil, endTime: String? = nil, staffUserCoreTypeId: String? = nil, staffUserName: String? = nil, quota: String? = nil, isUrgent: Bool? = nil, isSendNotifications: Bool? = nil, isAutoApprove: Bool? = nil, isAutoClose: Bool? = nil, isAllowPartial: Bool? = nil, note: String? = nil, status: String? = nil, invitees: [Invitee?]? = nil, organization: Organization? = nil) {
        self.init(unsafeResultMap: ["__typename": "OpenShifts", "openShiftId": openShiftId, "worksiteId": worksiteId, "departmentId": departmentId, "departmentName": departmentName, "worksiteName": worksiteName, "worksiteAddress": worksiteAddress, "shiftTypeId": shiftTypeId, "onDate": onDate, "startTime": startTime, "endTime": endTime, "staffUserCoreTypeId": staffUserCoreTypeId, "staffUserName": staffUserName, "quota": quota, "isUrgent": isUrgent, "isSendNotifications": isSendNotifications, "isAutoApprove": isAutoApprove, "isAutoClose": isAutoClose, "isAllowPartial": isAllowPartial, "note": note, "status": status, "invitees": invitees.flatMap { (value: [Invitee?]) -> [ResultMap?] in value.map { (value: Invitee?) -> ResultMap? in value.flatMap { (value: Invitee) -> ResultMap in value.resultMap } } }, "organization": organization.flatMap { (value: Organization) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var openShiftId: String? {
        get {
          return resultMap["openShiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "openShiftId")
        }
      }

      public var worksiteId: String? {
        get {
          return resultMap["worksiteId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteId")
        }
      }

      public var departmentId: String? {
        get {
          return resultMap["departmentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentId")
        }
      }

      public var departmentName: String? {
        get {
          return resultMap["departmentName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentName")
        }
      }

      public var worksiteName: String? {
        get {
          return resultMap["worksiteName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteName")
        }
      }

      public var worksiteAddress: String? {
        get {
          return resultMap["worksiteAddress"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteAddress")
        }
      }

      public var shiftTypeId: String? {
        get {
          return resultMap["shiftTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftTypeId")
        }
      }

      public var onDate: String? {
        get {
          return resultMap["onDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "onDate")
        }
      }

      public var startTime: String? {
        get {
          return resultMap["startTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: String? {
        get {
          return resultMap["endTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "endTime")
        }
      }

      public var staffUserCoreTypeId: String? {
        get {
          return resultMap["staffUserCoreTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffUserCoreTypeId")
        }
      }

      public var staffUserName: String? {
        get {
          return resultMap["staffUserName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffUserName")
        }
      }

      public var quota: String? {
        get {
          return resultMap["quota"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "quota")
        }
      }

      public var isUrgent: Bool? {
        get {
          return resultMap["isUrgent"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isUrgent")
        }
      }

      public var isSendNotifications: Bool? {
        get {
          return resultMap["isSendNotifications"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isSendNotifications")
        }
      }

      public var isAutoApprove: Bool? {
        get {
          return resultMap["isAutoApprove"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isAutoApprove")
        }
      }

      public var isAutoClose: Bool? {
        get {
          return resultMap["isAutoClose"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isAutoClose")
        }
      }

      public var isAllowPartial: Bool? {
        get {
          return resultMap["isAllowPartial"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isAllowPartial")
        }
      }

      public var note: String? {
        get {
          return resultMap["note"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "note")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var invitees: [Invitee?]? {
        get {
          return (resultMap["invitees"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Invitee?] in value.map { (value: ResultMap?) -> Invitee? in value.flatMap { (value: ResultMap) -> Invitee in Invitee(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Invitee?]) -> [ResultMap?] in value.map { (value: Invitee?) -> ResultMap? in value.flatMap { (value: Invitee) -> ResultMap in value.resultMap } } }, forKey: "invitees")
        }
      }

      public var organization: Organization? {
        get {
          return (resultMap["organization"] as? ResultMap).flatMap { Organization(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "organization")
        }
      }

      public struct Invitee: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["OpenShiftInvitees"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .scalar(String.self)),
          GraphQLField("lastName", type: .scalar(String.self)),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
          GraphQLField("isApproved", type: .scalar(Bool.self)),
          GraphQLField("isAccepted", type: .scalar(Bool.self)),
          GraphQLField("userId", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(firstName: String? = nil, lastName: String? = nil, profilePhoto: String? = nil, isApproved: Bool? = nil, isAccepted: Bool? = nil, userId: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "OpenShiftInvitees", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto, "isApproved": isApproved, "isAccepted": isAccepted, "userId": userId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var firstName: String? {
          get {
            return resultMap["firstName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String? {
          get {
            return resultMap["lastName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }

        public var isApproved: Bool? {
          get {
            return resultMap["isApproved"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isApproved")
          }
        }

        public var isAccepted: Bool? {
          get {
            return resultMap["isAccepted"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isAccepted")
          }
        }

        public var userId: String? {
          get {
            return resultMap["userId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }
      }

      public struct Organization: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Organizations"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("organizationId", type: .scalar(String.self)),
          GraphQLField("logo", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(organizationId: String? = nil, logo: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Organizations", "organizationId": organizationId, "logo": logo])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var organizationId: String? {
          get {
            return resultMap["organizationId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "organizationId")
          }
        }

        public var logo: String? {
          get {
            return resultMap["logo"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "logo")
          }
        }
      }
    }
  }
}

public final class FindOrganizationByIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query findOrganizationById($organizationId: Long!) {
      findOrganizationById(organizationId: $organizationId) {
        __typename
        name
        address
        logo
        businesses {
          __typename
          description
        }
        staffMembers {
          __typename
          userId
          firstName
          lastName
          middleName
          email
          password
          preferredName
          address1
          address2
          city
          state
          phone
          profilePhoto
          title
          userTypeId
          staffUserCoreTypeId
          dateOfBirth
          linkedInProfile
          organizationId
          gender
          bioInformation
          createdOn
          secondaryPhoneNumber
          secondaryEmail
        }
      }
    }
    """

  public let operationName: String = "findOrganizationById"

  public var organizationId: String

  public init(organizationId: String) {
    self.organizationId = organizationId
  }

  public var variables: GraphQLMap? {
    return ["organizationId": organizationId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findOrganizationById", arguments: ["organizationId": GraphQLVariable("organizationId")], type: .list(.object(FindOrganizationById.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findOrganizationById: [FindOrganizationById?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findOrganizationById": findOrganizationById.flatMap { (value: [FindOrganizationById?]) -> [ResultMap?] in value.map { (value: FindOrganizationById?) -> ResultMap? in value.flatMap { (value: FindOrganizationById) -> ResultMap in value.resultMap } } }])
    }

    public var findOrganizationById: [FindOrganizationById?]? {
      get {
        return (resultMap["findOrganizationById"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FindOrganizationById?] in value.map { (value: ResultMap?) -> FindOrganizationById? in value.flatMap { (value: ResultMap) -> FindOrganizationById in FindOrganizationById(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [FindOrganizationById?]) -> [ResultMap?] in value.map { (value: FindOrganizationById?) -> ResultMap? in value.flatMap { (value: FindOrganizationById) -> ResultMap in value.resultMap } } }, forKey: "findOrganizationById")
      }
    }

    public struct FindOrganizationById: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Organizations"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("address", type: .scalar(String.self)),
        GraphQLField("logo", type: .scalar(String.self)),
        GraphQLField("businesses", type: .list(.object(Business.selections))),
        GraphQLField("staffMembers", type: .list(.object(StaffMember.selections))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String, address: String? = nil, logo: String? = nil, businesses: [Business?]? = nil, staffMembers: [StaffMember?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "Organizations", "name": name, "address": address, "logo": logo, "businesses": businesses.flatMap { (value: [Business?]) -> [ResultMap?] in value.map { (value: Business?) -> ResultMap? in value.flatMap { (value: Business) -> ResultMap in value.resultMap } } }, "staffMembers": staffMembers.flatMap { (value: [StaffMember?]) -> [ResultMap?] in value.map { (value: StaffMember?) -> ResultMap? in value.flatMap { (value: StaffMember) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var address: String? {
        get {
          return resultMap["address"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "address")
        }
      }

      public var logo: String? {
        get {
          return resultMap["logo"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "logo")
        }
      }

      public var businesses: [Business?]? {
        get {
          return (resultMap["businesses"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Business?] in value.map { (value: ResultMap?) -> Business? in value.flatMap { (value: ResultMap) -> Business in Business(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Business?]) -> [ResultMap?] in value.map { (value: Business?) -> ResultMap? in value.flatMap { (value: Business) -> ResultMap in value.resultMap } } }, forKey: "businesses")
        }
      }

      public var staffMembers: [StaffMember?]? {
        get {
          return (resultMap["staffMembers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [StaffMember?] in value.map { (value: ResultMap?) -> StaffMember? in value.flatMap { (value: ResultMap) -> StaffMember in StaffMember(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [StaffMember?]) -> [ResultMap?] in value.map { (value: StaffMember?) -> ResultMap? in value.flatMap { (value: StaffMember) -> ResultMap in value.resultMap } } }, forKey: "staffMembers")
        }
      }

      public struct Business: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Businesses"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("description", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(description: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Businesses", "description": description])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var description: String? {
          get {
            return resultMap["description"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "description")
          }
        }
      }

      public struct StaffMember: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UserBasicInformation"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
          GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
          GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
          GraphQLField("middleName", type: .scalar(String.self)),
          GraphQLField("email", type: .nonNull(.scalar(String.self))),
          GraphQLField("password", type: .scalar(String.self)),
          GraphQLField("preferredName", type: .scalar(String.self)),
          GraphQLField("address1", type: .scalar(String.self)),
          GraphQLField("address2", type: .scalar(String.self)),
          GraphQLField("city", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("phone", type: .nonNull(.scalar(String.self))),
          GraphQLField("profilePhoto", type: .scalar(String.self)),
          GraphQLField("title", type: .scalar(String.self)),
          GraphQLField("userTypeId", type: .scalar(String.self)),
          GraphQLField("staffUserCoreTypeId", type: .scalar(String.self)),
          GraphQLField("dateOfBirth", type: .scalar(String.self)),
          GraphQLField("linkedInProfile", type: .scalar(String.self)),
          GraphQLField("organizationId", type: .scalar(String.self)),
          GraphQLField("gender", type: .scalar(String.self)),
          GraphQLField("bioInformation", type: .scalar(String.self)),
          GraphQLField("createdOn", type: .scalar(String.self)),
          GraphQLField("secondaryPhoneNumber", type: .scalar(String.self)),
          GraphQLField("secondaryEmail", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(userId: String, firstName: String, lastName: String, middleName: String? = nil, email: String, password: String? = nil, preferredName: String? = nil, address1: String? = nil, address2: String? = nil, city: String? = nil, state: String? = nil, phone: String, profilePhoto: String? = nil, title: String? = nil, userTypeId: String? = nil, staffUserCoreTypeId: String? = nil, dateOfBirth: String? = nil, linkedInProfile: String? = nil, organizationId: String? = nil, gender: String? = nil, bioInformation: String? = nil, createdOn: String? = nil, secondaryPhoneNumber: String? = nil, secondaryEmail: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "userId": userId, "firstName": firstName, "lastName": lastName, "middleName": middleName, "email": email, "password": password, "preferredName": preferredName, "address1": address1, "address2": address2, "city": city, "state": state, "phone": phone, "profilePhoto": profilePhoto, "title": title, "userTypeId": userTypeId, "staffUserCoreTypeId": staffUserCoreTypeId, "dateOfBirth": dateOfBirth, "linkedInProfile": linkedInProfile, "organizationId": organizationId, "gender": gender, "bioInformation": bioInformation, "createdOn": createdOn, "secondaryPhoneNumber": secondaryPhoneNumber, "secondaryEmail": secondaryEmail])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return resultMap["userId"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userId")
          }
        }

        public var firstName: String {
          get {
            return resultMap["firstName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "firstName")
          }
        }

        public var lastName: String {
          get {
            return resultMap["lastName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastName")
          }
        }

        public var middleName: String? {
          get {
            return resultMap["middleName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "middleName")
          }
        }

        public var email: String {
          get {
            return resultMap["email"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var password: String? {
          get {
            return resultMap["password"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "password")
          }
        }

        public var preferredName: String? {
          get {
            return resultMap["preferredName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "preferredName")
          }
        }

        public var address1: String? {
          get {
            return resultMap["address1"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "address1")
          }
        }

        public var address2: String? {
          get {
            return resultMap["address2"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "address2")
          }
        }

        public var city: String? {
          get {
            return resultMap["city"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "city")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var phone: String {
          get {
            return resultMap["phone"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phone")
          }
        }

        public var profilePhoto: String? {
          get {
            return resultMap["profilePhoto"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profilePhoto")
          }
        }

        public var title: String? {
          get {
            return resultMap["title"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "title")
          }
        }

        public var userTypeId: String? {
          get {
            return resultMap["userTypeId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "userTypeId")
          }
        }

        public var staffUserCoreTypeId: String? {
          get {
            return resultMap["staffUserCoreTypeId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "staffUserCoreTypeId")
          }
        }

        public var dateOfBirth: String? {
          get {
            return resultMap["dateOfBirth"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "dateOfBirth")
          }
        }

        public var linkedInProfile: String? {
          get {
            return resultMap["linkedInProfile"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "linkedInProfile")
          }
        }

        public var organizationId: String? {
          get {
            return resultMap["organizationId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "organizationId")
          }
        }

        public var gender: String? {
          get {
            return resultMap["gender"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "gender")
          }
        }

        public var bioInformation: String? {
          get {
            return resultMap["bioInformation"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "bioInformation")
          }
        }

        public var createdOn: String? {
          get {
            return resultMap["createdOn"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "createdOn")
          }
        }

        public var secondaryPhoneNumber: String? {
          get {
            return resultMap["secondaryPhoneNumber"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "secondaryPhoneNumber")
          }
        }

        public var secondaryEmail: String? {
          get {
            return resultMap["secondaryEmail"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "secondaryEmail")
          }
        }
      }
    }
  }
}

public final class CreateGeneralAvailabilityMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createGeneralAvailability($userId: Long, $scheduleLayoutId: Long, $startDate: String, $endDate: String, $startTime: String, $endTime: String) {
      createGeneralAvailability(userId: $userId, scheduleLayoutId: $scheduleLayoutId, startDate: $startDate, endDate: $endDate, startTime: $startTime, endTime: $endTime) {
        __typename
        generalAvailabilityId
      }
    }
    """

  public let operationName: String = "createGeneralAvailability"

  public var userId: String?
  public var scheduleLayoutId: String?
  public var startDate: String?
  public var endDate: String?
  public var startTime: String?
  public var endTime: String?

  public init(userId: String? = nil, scheduleLayoutId: String? = nil, startDate: String? = nil, endDate: String? = nil, startTime: String? = nil, endTime: String? = nil) {
    self.userId = userId
    self.scheduleLayoutId = scheduleLayoutId
    self.startDate = startDate
    self.endDate = endDate
    self.startTime = startTime
    self.endTime = endTime
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "scheduleLayoutId": scheduleLayoutId, "startDate": startDate, "endDate": endDate, "startTime": startTime, "endTime": endTime]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createGeneralAvailability", arguments: ["userId": GraphQLVariable("userId"), "scheduleLayoutId": GraphQLVariable("scheduleLayoutId"), "startDate": GraphQLVariable("startDate"), "endDate": GraphQLVariable("endDate"), "startTime": GraphQLVariable("startTime"), "endTime": GraphQLVariable("endTime")], type: .object(CreateGeneralAvailability.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createGeneralAvailability: CreateGeneralAvailability? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createGeneralAvailability": createGeneralAvailability.flatMap { (value: CreateGeneralAvailability) -> ResultMap in value.resultMap }])
    }

    public var createGeneralAvailability: CreateGeneralAvailability? {
      get {
        return (resultMap["createGeneralAvailability"] as? ResultMap).flatMap { CreateGeneralAvailability(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createGeneralAvailability")
      }
    }

    public struct CreateGeneralAvailability: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["GeneralAvailability"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("generalAvailabilityId", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(generalAvailabilityId: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "GeneralAvailability", "generalAvailabilityId": generalAvailabilityId])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var generalAvailabilityId: String? {
        get {
          return resultMap["generalAvailabilityId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "generalAvailabilityId")
        }
      }
    }
  }
}

public final class FindUserPrivacySettingsByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query findUserPrivacySettingsByUserId($userId: Long!) {
      findUserPrivacySettingsByUserId(userId: $userId) {
        __typename
        userId
        userPrivacySettingId
        isVisibleToPublic
        isVisibleToWorkspace
        isVisibleToCollegues
      }
    }
    """

  public let operationName: String = "findUserPrivacySettingsByUserId"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findUserPrivacySettingsByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(FindUserPrivacySettingsByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findUserPrivacySettingsByUserId: [FindUserPrivacySettingsByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findUserPrivacySettingsByUserId": findUserPrivacySettingsByUserId.flatMap { (value: [FindUserPrivacySettingsByUserId?]) -> [ResultMap?] in value.map { (value: FindUserPrivacySettingsByUserId?) -> ResultMap? in value.flatMap { (value: FindUserPrivacySettingsByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var findUserPrivacySettingsByUserId: [FindUserPrivacySettingsByUserId?]? {
      get {
        return (resultMap["findUserPrivacySettingsByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FindUserPrivacySettingsByUserId?] in value.map { (value: ResultMap?) -> FindUserPrivacySettingsByUserId? in value.flatMap { (value: ResultMap) -> FindUserPrivacySettingsByUserId in FindUserPrivacySettingsByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [FindUserPrivacySettingsByUserId?]) -> [ResultMap?] in value.map { (value: FindUserPrivacySettingsByUserId?) -> ResultMap? in value.flatMap { (value: FindUserPrivacySettingsByUserId) -> ResultMap in value.resultMap } } }, forKey: "findUserPrivacySettingsByUserId")
      }
    }

    public struct FindUserPrivacySettingsByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserPrivacySettings"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("userPrivacySettingId", type: .scalar(String.self)),
        GraphQLField("isVisibleToPublic", type: .scalar(Bool.self)),
        GraphQLField("isVisibleToWorkspace", type: .scalar(Bool.self)),
        GraphQLField("isVisibleToCollegues", type: .scalar(Bool.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String? = nil, userPrivacySettingId: String? = nil, isVisibleToPublic: Bool? = nil, isVisibleToWorkspace: Bool? = nil, isVisibleToCollegues: Bool? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserPrivacySettings", "userId": userId, "userPrivacySettingId": userPrivacySettingId, "isVisibleToPublic": isVisibleToPublic, "isVisibleToWorkspace": isVisibleToWorkspace, "isVisibleToCollegues": isVisibleToCollegues])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var userPrivacySettingId: String? {
        get {
          return resultMap["userPrivacySettingId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userPrivacySettingId")
        }
      }

      public var isVisibleToPublic: Bool? {
        get {
          return resultMap["isVisibleToPublic"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isVisibleToPublic")
        }
      }

      public var isVisibleToWorkspace: Bool? {
        get {
          return resultMap["isVisibleToWorkspace"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isVisibleToWorkspace")
        }
      }

      public var isVisibleToCollegues: Bool? {
        get {
          return resultMap["isVisibleToCollegues"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isVisibleToCollegues")
        }
      }
    }
  }
}

public final class CreateUserPrivacySettingsMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createUserPrivacySettings($userId: Long, $isVisibleToPublic: Boolean, $isVisibleToWorkspace: Boolean, $isVisibleToCollegues: Boolean) {
      createUserPrivacySettings(userId: $userId, isVisibleToPublic: $isVisibleToPublic, isVisibleToWorkspace: $isVisibleToWorkspace, isVisibleToCollegues: $isVisibleToCollegues) {
        __typename
        userId
      }
    }
    """

  public let operationName: String = "createUserPrivacySettings"

  public var userId: String?
  public var isVisibleToPublic: Bool?
  public var isVisibleToWorkspace: Bool?
  public var isVisibleToCollegues: Bool?

  public init(userId: String? = nil, isVisibleToPublic: Bool? = nil, isVisibleToWorkspace: Bool? = nil, isVisibleToCollegues: Bool? = nil) {
    self.userId = userId
    self.isVisibleToPublic = isVisibleToPublic
    self.isVisibleToWorkspace = isVisibleToWorkspace
    self.isVisibleToCollegues = isVisibleToCollegues
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "isVisibleToPublic": isVisibleToPublic, "isVisibleToWorkspace": isVisibleToWorkspace, "isVisibleToCollegues": isVisibleToCollegues]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUserPrivacySettings", arguments: ["userId": GraphQLVariable("userId"), "isVisibleToPublic": GraphQLVariable("isVisibleToPublic"), "isVisibleToWorkspace": GraphQLVariable("isVisibleToWorkspace"), "isVisibleToCollegues": GraphQLVariable("isVisibleToCollegues")], type: .object(CreateUserPrivacySetting.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createUserPrivacySettings: CreateUserPrivacySetting? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createUserPrivacySettings": createUserPrivacySettings.flatMap { (value: CreateUserPrivacySetting) -> ResultMap in value.resultMap }])
    }

    public var createUserPrivacySettings: CreateUserPrivacySetting? {
      get {
        return (resultMap["createUserPrivacySettings"] as? ResultMap).flatMap { CreateUserPrivacySetting(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createUserPrivacySettings")
      }
    }

    public struct CreateUserPrivacySetting: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserPrivacySettings"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserPrivacySettings", "userId": userId])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }
    }
  }
}

public final class CreateUserBasicInformationMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation CreateUserBasicInformation($firstName: String!, $organizationId: Long!, $lastName: String!, $email: String!, $phoneNumber: String!, $password: String!, $dateOfBirth: String!, $gender: String!, $profilePhoto: String!, $userTypeId: Long!, $staffUserCoreTypeId: Long!) {
      createUserBasicInformation(organizationId: $organizationId, firstName: $firstName, lastName: $lastName, email: $email, phone: $phoneNumber, password: $password, dateOfBirth: $dateOfBirth, gender: $gender, profilePhoto: $profilePhoto, userTypeId: $userTypeId, staffUserCoreTypeId: $staffUserCoreTypeId) {
        __typename
        firstName
        lastName
        email
      }
    }
    """

  public let operationName: String = "CreateUserBasicInformation"

  public var firstName: String
  public var organizationId: String
  public var lastName: String
  public var email: String
  public var phoneNumber: String
  public var password: String
  public var dateOfBirth: String
  public var gender: String
  public var profilePhoto: String
  public var userTypeId: String
  public var staffUserCoreTypeId: String

  public init(firstName: String, organizationId: String, lastName: String, email: String, phoneNumber: String, password: String, dateOfBirth: String, gender: String, profilePhoto: String, userTypeId: String, staffUserCoreTypeId: String) {
    self.firstName = firstName
    self.organizationId = organizationId
    self.lastName = lastName
    self.email = email
    self.phoneNumber = phoneNumber
    self.password = password
    self.dateOfBirth = dateOfBirth
    self.gender = gender
    self.profilePhoto = profilePhoto
    self.userTypeId = userTypeId
    self.staffUserCoreTypeId = staffUserCoreTypeId
  }

  public var variables: GraphQLMap? {
    return ["firstName": firstName, "organizationId": organizationId, "lastName": lastName, "email": email, "phoneNumber": phoneNumber, "password": password, "dateOfBirth": dateOfBirth, "gender": gender, "profilePhoto": profilePhoto, "userTypeId": userTypeId, "staffUserCoreTypeId": staffUserCoreTypeId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUserBasicInformation", arguments: ["organizationId": GraphQLVariable("organizationId"), "firstName": GraphQLVariable("firstName"), "lastName": GraphQLVariable("lastName"), "email": GraphQLVariable("email"), "phone": GraphQLVariable("phoneNumber"), "password": GraphQLVariable("password"), "dateOfBirth": GraphQLVariable("dateOfBirth"), "gender": GraphQLVariable("gender"), "profilePhoto": GraphQLVariable("profilePhoto"), "userTypeId": GraphQLVariable("userTypeId"), "staffUserCoreTypeId": GraphQLVariable("staffUserCoreTypeId")], type: .object(CreateUserBasicInformation.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createUserBasicInformation: CreateUserBasicInformation? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createUserBasicInformation": createUserBasicInformation.flatMap { (value: CreateUserBasicInformation) -> ResultMap in value.resultMap }])
    }

    public var createUserBasicInformation: CreateUserBasicInformation? {
      get {
        return (resultMap["createUserBasicInformation"] as? ResultMap).flatMap { CreateUserBasicInformation(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createUserBasicInformation")
      }
    }

    public struct CreateUserBasicInformation: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserBasicInformation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
        GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
        GraphQLField("email", type: .nonNull(.scalar(String.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(firstName: String, lastName: String, email: String) {
        self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "email": email])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var firstName: String {
        get {
          return resultMap["firstName"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String {
        get {
          return resultMap["lastName"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "lastName")
        }
      }

      public var email: String {
        get {
          return resultMap["email"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "email")
        }
      }
    }
  }
}

public final class FindScheduleLayoutByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query findScheduleLayoutByUserId($userId: Long!) {
      findScheduleLayoutByUserId(userId: $userId) {
        __typename
        scheduleLayoutId
        departmentId
        departmentName
        shiftPlanningStartDate
        shiftPlanningEndDate
        scheduleStartDate
        scheduleEndDate
        shiftPlanningPhaseLength
        makeStaffAvailabilityDeadLineLength
        makeStaffAvailabilityDeadLine
        maxQuota
        scheduleLength
        publishedOn
      }
    }
    """

  public let operationName: String = "findScheduleLayoutByUserId"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findScheduleLayoutByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(FindScheduleLayoutByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findScheduleLayoutByUserId: [FindScheduleLayoutByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findScheduleLayoutByUserId": findScheduleLayoutByUserId.flatMap { (value: [FindScheduleLayoutByUserId?]) -> [ResultMap?] in value.map { (value: FindScheduleLayoutByUserId?) -> ResultMap? in value.flatMap { (value: FindScheduleLayoutByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var findScheduleLayoutByUserId: [FindScheduleLayoutByUserId?]? {
      get {
        return (resultMap["findScheduleLayoutByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FindScheduleLayoutByUserId?] in value.map { (value: ResultMap?) -> FindScheduleLayoutByUserId? in value.flatMap { (value: ResultMap) -> FindScheduleLayoutByUserId in FindScheduleLayoutByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [FindScheduleLayoutByUserId?]) -> [ResultMap?] in value.map { (value: FindScheduleLayoutByUserId?) -> ResultMap? in value.flatMap { (value: FindScheduleLayoutByUserId) -> ResultMap in value.resultMap } } }, forKey: "findScheduleLayoutByUserId")
      }
    }

    public struct FindScheduleLayoutByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ScheduleLayout"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("scheduleLayoutId", type: .scalar(String.self)),
        GraphQLField("departmentId", type: .scalar(String.self)),
        GraphQLField("departmentName", type: .scalar(String.self)),
        GraphQLField("shiftPlanningStartDate", type: .scalar(String.self)),
        GraphQLField("shiftPlanningEndDate", type: .scalar(String.self)),
        GraphQLField("scheduleStartDate", type: .scalar(String.self)),
        GraphQLField("scheduleEndDate", type: .scalar(String.self)),
        GraphQLField("shiftPlanningPhaseLength", type: .scalar(String.self)),
        GraphQLField("makeStaffAvailabilityDeadLineLength", type: .scalar(String.self)),
        GraphQLField("makeStaffAvailabilityDeadLine", type: .scalar(String.self)),
        GraphQLField("maxQuota", type: .scalar(String.self)),
        GraphQLField("scheduleLength", type: .scalar(String.self)),
        GraphQLField("publishedOn", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(scheduleLayoutId: String? = nil, departmentId: String? = nil, departmentName: String? = nil, shiftPlanningStartDate: String? = nil, shiftPlanningEndDate: String? = nil, scheduleStartDate: String? = nil, scheduleEndDate: String? = nil, shiftPlanningPhaseLength: String? = nil, makeStaffAvailabilityDeadLineLength: String? = nil, makeStaffAvailabilityDeadLine: String? = nil, maxQuota: String? = nil, scheduleLength: String? = nil, publishedOn: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "ScheduleLayout", "scheduleLayoutId": scheduleLayoutId, "departmentId": departmentId, "departmentName": departmentName, "shiftPlanningStartDate": shiftPlanningStartDate, "shiftPlanningEndDate": shiftPlanningEndDate, "scheduleStartDate": scheduleStartDate, "scheduleEndDate": scheduleEndDate, "shiftPlanningPhaseLength": shiftPlanningPhaseLength, "makeStaffAvailabilityDeadLineLength": makeStaffAvailabilityDeadLineLength, "makeStaffAvailabilityDeadLine": makeStaffAvailabilityDeadLine, "maxQuota": maxQuota, "scheduleLength": scheduleLength, "publishedOn": publishedOn])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var scheduleLayoutId: String? {
        get {
          return resultMap["scheduleLayoutId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "scheduleLayoutId")
        }
      }

      public var departmentId: String? {
        get {
          return resultMap["departmentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentId")
        }
      }

      public var departmentName: String? {
        get {
          return resultMap["departmentName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentName")
        }
      }

      public var shiftPlanningStartDate: String? {
        get {
          return resultMap["shiftPlanningStartDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftPlanningStartDate")
        }
      }

      public var shiftPlanningEndDate: String? {
        get {
          return resultMap["shiftPlanningEndDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftPlanningEndDate")
        }
      }

      public var scheduleStartDate: String? {
        get {
          return resultMap["scheduleStartDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "scheduleStartDate")
        }
      }

      public var scheduleEndDate: String? {
        get {
          return resultMap["scheduleEndDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "scheduleEndDate")
        }
      }

      public var shiftPlanningPhaseLength: String? {
        get {
          return resultMap["shiftPlanningPhaseLength"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftPlanningPhaseLength")
        }
      }

      public var makeStaffAvailabilityDeadLineLength: String? {
        get {
          return resultMap["makeStaffAvailabilityDeadLineLength"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "makeStaffAvailabilityDeadLineLength")
        }
      }

      public var makeStaffAvailabilityDeadLine: String? {
        get {
          return resultMap["makeStaffAvailabilityDeadLine"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "makeStaffAvailabilityDeadLine")
        }
      }

      public var maxQuota: String? {
        get {
          return resultMap["maxQuota"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "maxQuota")
        }
      }

      public var scheduleLength: String? {
        get {
          return resultMap["scheduleLength"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "scheduleLength")
        }
      }

      public var publishedOn: String? {
        get {
          return resultMap["publishedOn"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "publishedOn")
        }
      }
    }
  }
}

public final class UpdateOpenShiftsMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateOpenShifts($userId: Long, $openShiftId: Long, $isAccepted: Boolean, $isApproved: Boolean, $status: String) {
      updateOpenShifts(userId: $userId, openShiftId: $openShiftId, isAccepted: $isAccepted, isApproved: $isApproved, status: $status) {
        __typename
        openShiftId
      }
    }
    """

  public let operationName: String = "updateOpenShifts"

  public var userId: String?
  public var openShiftId: String?
  public var isAccepted: Bool?
  public var isApproved: Bool?
  public var status: String?

  public init(userId: String? = nil, openShiftId: String? = nil, isAccepted: Bool? = nil, isApproved: Bool? = nil, status: String? = nil) {
    self.userId = userId
    self.openShiftId = openShiftId
    self.isAccepted = isAccepted
    self.isApproved = isApproved
    self.status = status
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "openShiftId": openShiftId, "isAccepted": isAccepted, "isApproved": isApproved, "status": status]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateOpenShifts", arguments: ["userId": GraphQLVariable("userId"), "openShiftId": GraphQLVariable("openShiftId"), "isAccepted": GraphQLVariable("isAccepted"), "isApproved": GraphQLVariable("isApproved"), "status": GraphQLVariable("status")], type: .object(UpdateOpenShift.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateOpenShifts: UpdateOpenShift? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "updateOpenShifts": updateOpenShifts.flatMap { (value: UpdateOpenShift) -> ResultMap in value.resultMap }])
    }

    public var updateOpenShifts: UpdateOpenShift? {
      get {
        return (resultMap["updateOpenShifts"] as? ResultMap).flatMap { UpdateOpenShift(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateOpenShifts")
      }
    }

    public struct UpdateOpenShift: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["OpenShifts"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("openShiftId", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(openShiftId: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "OpenShifts", "openShiftId": openShiftId])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var openShiftId: String? {
        get {
          return resultMap["openShiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "openShiftId")
        }
      }
    }
  }
}

public final class UpdateARequestMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateARequest($requestId: Long!, $userId: Long!, $worksiteId: Long!, $requestTypeId: Long!, $onDate: [String], $shiftId: Long, $notes: String) {
      updateARequest(userId: $userId, worksiteId: $worksiteId, requestId: $requestId, requestTypeId: $requestTypeId, onDate: $onDate, shiftId: $shiftId, notes: $notes) {
        __typename
        requestId
        worksiteId
        worksiteName
        departmentId
        requestTypeId
        requestTypeName
        userId
        onDate
        shiftId
        colleagueId
        colleagueName
        notes
      }
    }
    """

  public let operationName: String = "updateARequest"

  public var requestId: String
  public var userId: String
  public var worksiteId: String
  public var requestTypeId: String
  public var onDate: [String?]?
  public var shiftId: String?
  public var notes: String?

  public init(requestId: String, userId: String, worksiteId: String, requestTypeId: String, onDate: [String?]? = nil, shiftId: String? = nil, notes: String? = nil) {
    self.requestId = requestId
    self.userId = userId
    self.worksiteId = worksiteId
    self.requestTypeId = requestTypeId
    self.onDate = onDate
    self.shiftId = shiftId
    self.notes = notes
  }

  public var variables: GraphQLMap? {
    return ["requestId": requestId, "userId": userId, "worksiteId": worksiteId, "requestTypeId": requestTypeId, "onDate": onDate, "shiftId": shiftId, "notes": notes]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateARequest", arguments: ["userId": GraphQLVariable("userId"), "worksiteId": GraphQLVariable("worksiteId"), "requestId": GraphQLVariable("requestId"), "requestTypeId": GraphQLVariable("requestTypeId"), "onDate": GraphQLVariable("onDate"), "shiftId": GraphQLVariable("shiftId"), "notes": GraphQLVariable("notes")], type: .object(UpdateARequest.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateARequest: UpdateARequest? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "updateARequest": updateARequest.flatMap { (value: UpdateARequest) -> ResultMap in value.resultMap }])
    }

    public var updateARequest: UpdateARequest? {
      get {
        return (resultMap["updateARequest"] as? ResultMap).flatMap { UpdateARequest(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateARequest")
      }
    }

    public struct UpdateARequest: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Requests"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("requestId", type: .scalar(String.self)),
        GraphQLField("worksiteId", type: .scalar(String.self)),
        GraphQLField("worksiteName", type: .scalar(String.self)),
        GraphQLField("departmentId", type: .scalar(String.self)),
        GraphQLField("requestTypeId", type: .scalar(String.self)),
        GraphQLField("requestTypeName", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("onDate", type: .list(.scalar(String.self))),
        GraphQLField("shiftId", type: .scalar(String.self)),
        GraphQLField("colleagueId", type: .scalar(String.self)),
        GraphQLField("colleagueName", type: .scalar(String.self)),
        GraphQLField("notes", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(requestId: String? = nil, worksiteId: String? = nil, worksiteName: String? = nil, departmentId: String? = nil, requestTypeId: String? = nil, requestTypeName: String? = nil, userId: String? = nil, onDate: [String?]? = nil, shiftId: String? = nil, colleagueId: String? = nil, colleagueName: String? = nil, notes: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Requests", "requestId": requestId, "worksiteId": worksiteId, "worksiteName": worksiteName, "departmentId": departmentId, "requestTypeId": requestTypeId, "requestTypeName": requestTypeName, "userId": userId, "onDate": onDate, "shiftId": shiftId, "colleagueId": colleagueId, "colleagueName": colleagueName, "notes": notes])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var requestId: String? {
        get {
          return resultMap["requestId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestId")
        }
      }

      public var worksiteId: String? {
        get {
          return resultMap["worksiteId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteId")
        }
      }

      public var worksiteName: String? {
        get {
          return resultMap["worksiteName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteName")
        }
      }

      public var departmentId: String? {
        get {
          return resultMap["departmentId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentId")
        }
      }

      public var requestTypeId: String? {
        get {
          return resultMap["requestTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestTypeId")
        }
      }

      public var requestTypeName: String? {
        get {
          return resultMap["requestTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "requestTypeName")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var onDate: [String?]? {
        get {
          return resultMap["onDate"] as? [String?]
        }
        set {
          resultMap.updateValue(newValue, forKey: "onDate")
        }
      }

      public var shiftId: String? {
        get {
          return resultMap["shiftId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "shiftId")
        }
      }

      public var colleagueId: String? {
        get {
          return resultMap["colleagueId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "colleagueId")
        }
      }

      public var colleagueName: String? {
        get {
          return resultMap["colleagueName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "colleagueName")
        }
      }

      public var notes: String? {
        get {
          return resultMap["notes"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "notes")
        }
      }
    }
  }
}

public final class FindUserNotificationByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query findUserNotificationByUserId($userId: Long) {
      findUserNotificationByUserId(userId: $userId) {
        __typename
        userNotificationId
        userId
        notificationTypeId
        isEnablePush
        isEnableSMS
        isEnableEmail
      }
    }
    """

  public let operationName: String = "findUserNotificationByUserId"

  public var userId: String?

  public init(userId: String? = nil) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("findUserNotificationByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(FindUserNotificationByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(findUserNotificationByUserId: [FindUserNotificationByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "findUserNotificationByUserId": findUserNotificationByUserId.flatMap { (value: [FindUserNotificationByUserId?]) -> [ResultMap?] in value.map { (value: FindUserNotificationByUserId?) -> ResultMap? in value.flatMap { (value: FindUserNotificationByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var findUserNotificationByUserId: [FindUserNotificationByUserId?]? {
      get {
        return (resultMap["findUserNotificationByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [FindUserNotificationByUserId?] in value.map { (value: ResultMap?) -> FindUserNotificationByUserId? in value.flatMap { (value: ResultMap) -> FindUserNotificationByUserId in FindUserNotificationByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [FindUserNotificationByUserId?]) -> [ResultMap?] in value.map { (value: FindUserNotificationByUserId?) -> ResultMap? in value.flatMap { (value: FindUserNotificationByUserId) -> ResultMap in value.resultMap } } }, forKey: "findUserNotificationByUserId")
      }
    }

    public struct FindUserNotificationByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserNotification"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userNotificationId", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("notificationTypeId", type: .scalar(String.self)),
        GraphQLField("isEnablePush", type: .scalar(Bool.self)),
        GraphQLField("isEnableSMS", type: .scalar(Bool.self)),
        GraphQLField("isEnableEmail", type: .scalar(Bool.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userNotificationId: String? = nil, userId: String? = nil, notificationTypeId: String? = nil, isEnablePush: Bool? = nil, isEnableSms: Bool? = nil, isEnableEmail: Bool? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserNotification", "userNotificationId": userNotificationId, "userId": userId, "notificationTypeId": notificationTypeId, "isEnablePush": isEnablePush, "isEnableSMS": isEnableSms, "isEnableEmail": isEnableEmail])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userNotificationId: String? {
        get {
          return resultMap["userNotificationId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userNotificationId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var notificationTypeId: String? {
        get {
          return resultMap["notificationTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificationTypeId")
        }
      }

      public var isEnablePush: Bool? {
        get {
          return resultMap["isEnablePush"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isEnablePush")
        }
      }

      public var isEnableSms: Bool? {
        get {
          return resultMap["isEnableSMS"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isEnableSMS")
        }
      }

      public var isEnableEmail: Bool? {
        get {
          return resultMap["isEnableEmail"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isEnableEmail")
        }
      }
    }
  }
}

public final class CreateUserNotificationMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createUserNotification($userId: Long, $notificationTypeId: Long, $isMuteAllEnable: Boolean, $isEnablePush: Boolean, $isEnableSMS: Boolean, $isEnableEmail: Boolean) {
      createUserNotification(userId: $userId, notificationTypeId: $notificationTypeId, isMuteAllEnable: $isMuteAllEnable, isEnablePush: $isEnablePush, isEnableSMS: $isEnableSMS, isEnableEmail: $isEnableEmail) {
        __typename
        userNotificationId
        userId
        notificationTypeId
        isEnablePush
        isEnableSMS
        isEnableEmail
      }
    }
    """

  public let operationName: String = "createUserNotification"

  public var userId: String?
  public var notificationTypeId: String?
  public var isMuteAllEnable: Bool?
  public var isEnablePush: Bool?
  public var isEnableSMS: Bool?
  public var isEnableEmail: Bool?

  public init(userId: String? = nil, notificationTypeId: String? = nil, isMuteAllEnable: Bool? = nil, isEnablePush: Bool? = nil, isEnableSMS: Bool? = nil, isEnableEmail: Bool? = nil) {
    self.userId = userId
    self.notificationTypeId = notificationTypeId
    self.isMuteAllEnable = isMuteAllEnable
    self.isEnablePush = isEnablePush
    self.isEnableSMS = isEnableSMS
    self.isEnableEmail = isEnableEmail
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "notificationTypeId": notificationTypeId, "isMuteAllEnable": isMuteAllEnable, "isEnablePush": isEnablePush, "isEnableSMS": isEnableSMS, "isEnableEmail": isEnableEmail]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUserNotification", arguments: ["userId": GraphQLVariable("userId"), "notificationTypeId": GraphQLVariable("notificationTypeId"), "isMuteAllEnable": GraphQLVariable("isMuteAllEnable"), "isEnablePush": GraphQLVariable("isEnablePush"), "isEnableSMS": GraphQLVariable("isEnableSMS"), "isEnableEmail": GraphQLVariable("isEnableEmail")], type: .object(CreateUserNotification.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createUserNotification: CreateUserNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "createUserNotification": createUserNotification.flatMap { (value: CreateUserNotification) -> ResultMap in value.resultMap }])
    }

    public var createUserNotification: CreateUserNotification? {
      get {
        return (resultMap["createUserNotification"] as? ResultMap).flatMap { CreateUserNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createUserNotification")
      }
    }

    public struct CreateUserNotification: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserNotification"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userNotificationId", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("notificationTypeId", type: .scalar(String.self)),
        GraphQLField("isEnablePush", type: .scalar(Bool.self)),
        GraphQLField("isEnableSMS", type: .scalar(Bool.self)),
        GraphQLField("isEnableEmail", type: .scalar(Bool.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userNotificationId: String? = nil, userId: String? = nil, notificationTypeId: String? = nil, isEnablePush: Bool? = nil, isEnableSms: Bool? = nil, isEnableEmail: Bool? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserNotification", "userNotificationId": userNotificationId, "userId": userId, "notificationTypeId": notificationTypeId, "isEnablePush": isEnablePush, "isEnableSMS": isEnableSms, "isEnableEmail": isEnableEmail])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userNotificationId: String? {
        get {
          return resultMap["userNotificationId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userNotificationId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var notificationTypeId: String? {
        get {
          return resultMap["notificationTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificationTypeId")
        }
      }

      public var isEnablePush: Bool? {
        get {
          return resultMap["isEnablePush"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isEnablePush")
        }
      }

      public var isEnableSms: Bool? {
        get {
          return resultMap["isEnableSMS"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isEnableSMS")
        }
      }

      public var isEnableEmail: Bool? {
        get {
          return resultMap["isEnableEmail"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isEnableEmail")
        }
      }
    }
  }
}

public final class UpdateUserBasicInformationMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateUserBasicInformation($userId: Long!, $firstName: String, $lastName: String, $email: String, $phone: String, $preferredName: String, $address1: String, $gender: String, $dateOfBirth: String, $bioInformation: String, $profilePhoto: String, $secondaryPhoneNumber: String, $secondaryEmail: String, $password: String, $fcmToken: String, $deviceType: String) {
      updateUserBasicInformation(userId: $userId, firstName: $firstName, lastName: $lastName, email: $email, phone: $phone, preferredName: $preferredName, address1: $address1, gender: $gender, dateOfBirth: $dateOfBirth, bioInformation: $bioInformation, profilePhoto: $profilePhoto, secondaryPhoneNumber: $secondaryPhoneNumber, secondaryEmail: $secondaryEmail, password: $password, fcmToken: $fcmToken, deviceType: $deviceType) {
        __typename
        userId
        firstName
        lastName
        middleName
        email
        password
        preferredName
        address1
        address2
        city
        state
        phone
        profilePhoto
        title
        userTypeId
        staffUserCoreTypeId
        dateOfBirth
        linkedInProfile
        organizationId
        gender
        bioInformation
        createdOn
        secondaryPhoneNumber
        secondaryEmail
        password
        fcmToken
        staffUserCoreTypeName
        deviceType
      }
    }
    """

  public let operationName: String = "updateUserBasicInformation"

  public var userId: String
  public var firstName: String?
  public var lastName: String?
  public var email: String?
  public var phone: String?
  public var preferredName: String?
  public var address1: String?
  public var gender: String?
  public var dateOfBirth: String?
  public var bioInformation: String?
  public var profilePhoto: String?
  public var secondaryPhoneNumber: String?
  public var secondaryEmail: String?
  public var password: String?
  public var fcmToken: String?
  public var deviceType: String?

  public init(userId: String, firstName: String? = nil, lastName: String? = nil, email: String? = nil, phone: String? = nil, preferredName: String? = nil, address1: String? = nil, gender: String? = nil, dateOfBirth: String? = nil, bioInformation: String? = nil, profilePhoto: String? = nil, secondaryPhoneNumber: String? = nil, secondaryEmail: String? = nil, password: String? = nil, fcmToken: String? = nil, deviceType: String? = nil) {
    self.userId = userId
    self.firstName = firstName
    self.lastName = lastName
    self.email = email
    self.phone = phone
    self.preferredName = preferredName
    self.address1 = address1
    self.gender = gender
    self.dateOfBirth = dateOfBirth
    self.bioInformation = bioInformation
    self.profilePhoto = profilePhoto
    self.secondaryPhoneNumber = secondaryPhoneNumber
    self.secondaryEmail = secondaryEmail
    self.password = password
    self.fcmToken = fcmToken
    self.deviceType = deviceType
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "firstName": firstName, "lastName": lastName, "email": email, "phone": phone, "preferredName": preferredName, "address1": address1, "gender": gender, "dateOfBirth": dateOfBirth, "bioInformation": bioInformation, "profilePhoto": profilePhoto, "secondaryPhoneNumber": secondaryPhoneNumber, "secondaryEmail": secondaryEmail, "password": password, "fcmToken": fcmToken, "deviceType": deviceType]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateUserBasicInformation", arguments: ["userId": GraphQLVariable("userId"), "firstName": GraphQLVariable("firstName"), "lastName": GraphQLVariable("lastName"), "email": GraphQLVariable("email"), "phone": GraphQLVariable("phone"), "preferredName": GraphQLVariable("preferredName"), "address1": GraphQLVariable("address1"), "gender": GraphQLVariable("gender"), "dateOfBirth": GraphQLVariable("dateOfBirth"), "bioInformation": GraphQLVariable("bioInformation"), "profilePhoto": GraphQLVariable("profilePhoto"), "secondaryPhoneNumber": GraphQLVariable("secondaryPhoneNumber"), "secondaryEmail": GraphQLVariable("secondaryEmail"), "password": GraphQLVariable("password"), "fcmToken": GraphQLVariable("fcmToken"), "deviceType": GraphQLVariable("deviceType")], type: .object(UpdateUserBasicInformation.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateUserBasicInformation: UpdateUserBasicInformation? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "updateUserBasicInformation": updateUserBasicInformation.flatMap { (value: UpdateUserBasicInformation) -> ResultMap in value.resultMap }])
    }

    public var updateUserBasicInformation: UpdateUserBasicInformation? {
      get {
        return (resultMap["updateUserBasicInformation"] as? ResultMap).flatMap { UpdateUserBasicInformation(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateUserBasicInformation")
      }
    }

    public struct UpdateUserBasicInformation: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserBasicInformation"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
        GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
        GraphQLField("middleName", type: .scalar(String.self)),
        GraphQLField("email", type: .nonNull(.scalar(String.self))),
        GraphQLField("password", type: .scalar(String.self)),
        GraphQLField("preferredName", type: .scalar(String.self)),
        GraphQLField("address1", type: .scalar(String.self)),
        GraphQLField("address2", type: .scalar(String.self)),
        GraphQLField("city", type: .scalar(String.self)),
        GraphQLField("state", type: .scalar(String.self)),
        GraphQLField("phone", type: .nonNull(.scalar(String.self))),
        GraphQLField("profilePhoto", type: .scalar(String.self)),
        GraphQLField("title", type: .scalar(String.self)),
        GraphQLField("userTypeId", type: .scalar(String.self)),
        GraphQLField("staffUserCoreTypeId", type: .scalar(String.self)),
        GraphQLField("dateOfBirth", type: .scalar(String.self)),
        GraphQLField("linkedInProfile", type: .scalar(String.self)),
        GraphQLField("organizationId", type: .scalar(String.self)),
        GraphQLField("gender", type: .scalar(String.self)),
        GraphQLField("bioInformation", type: .scalar(String.self)),
        GraphQLField("createdOn", type: .scalar(String.self)),
        GraphQLField("secondaryPhoneNumber", type: .scalar(String.self)),
        GraphQLField("secondaryEmail", type: .scalar(String.self)),
        GraphQLField("password", type: .scalar(String.self)),
        GraphQLField("fcmToken", type: .scalar(String.self)),
        GraphQLField("staffUserCoreTypeName", type: .scalar(String.self)),
        GraphQLField("deviceType", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(userId: String, firstName: String, lastName: String, middleName: String? = nil, email: String, password: String? = nil, preferredName: String? = nil, address1: String? = nil, address2: String? = nil, city: String? = nil, state: String? = nil, phone: String, profilePhoto: String? = nil, title: String? = nil, userTypeId: String? = nil, staffUserCoreTypeId: String? = nil, dateOfBirth: String? = nil, linkedInProfile: String? = nil, organizationId: String? = nil, gender: String? = nil, bioInformation: String? = nil, createdOn: String? = nil, secondaryPhoneNumber: String? = nil, secondaryEmail: String? = nil, fcmToken: String? = nil, staffUserCoreTypeName: String? = nil, deviceType: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "userId": userId, "firstName": firstName, "lastName": lastName, "middleName": middleName, "email": email, "password": password, "preferredName": preferredName, "address1": address1, "address2": address2, "city": city, "state": state, "phone": phone, "profilePhoto": profilePhoto, "title": title, "userTypeId": userTypeId, "staffUserCoreTypeId": staffUserCoreTypeId, "dateOfBirth": dateOfBirth, "linkedInProfile": linkedInProfile, "organizationId": organizationId, "gender": gender, "bioInformation": bioInformation, "createdOn": createdOn, "secondaryPhoneNumber": secondaryPhoneNumber, "secondaryEmail": secondaryEmail, "fcmToken": fcmToken, "staffUserCoreTypeName": staffUserCoreTypeName, "deviceType": deviceType])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return resultMap["userId"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var firstName: String {
        get {
          return resultMap["firstName"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "firstName")
        }
      }

      public var lastName: String {
        get {
          return resultMap["lastName"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "lastName")
        }
      }

      public var middleName: String? {
        get {
          return resultMap["middleName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "middleName")
        }
      }

      public var email: String {
        get {
          return resultMap["email"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "email")
        }
      }

      public var password: String? {
        get {
          return resultMap["password"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "password")
        }
      }

      public var preferredName: String? {
        get {
          return resultMap["preferredName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "preferredName")
        }
      }

      public var address1: String? {
        get {
          return resultMap["address1"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "address1")
        }
      }

      public var address2: String? {
        get {
          return resultMap["address2"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "address2")
        }
      }

      public var city: String? {
        get {
          return resultMap["city"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "city")
        }
      }

      public var state: String? {
        get {
          return resultMap["state"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "state")
        }
      }

      public var phone: String {
        get {
          return resultMap["phone"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "phone")
        }
      }

      public var profilePhoto: String? {
        get {
          return resultMap["profilePhoto"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "profilePhoto")
        }
      }

      public var title: String? {
        get {
          return resultMap["title"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "title")
        }
      }

      public var userTypeId: String? {
        get {
          return resultMap["userTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userTypeId")
        }
      }

      public var staffUserCoreTypeId: String? {
        get {
          return resultMap["staffUserCoreTypeId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffUserCoreTypeId")
        }
      }

      public var dateOfBirth: String? {
        get {
          return resultMap["dateOfBirth"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "dateOfBirth")
        }
      }

      public var linkedInProfile: String? {
        get {
          return resultMap["linkedInProfile"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "linkedInProfile")
        }
      }

      public var organizationId: String? {
        get {
          return resultMap["organizationId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "organizationId")
        }
      }

      public var gender: String? {
        get {
          return resultMap["gender"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "gender")
        }
      }

      public var bioInformation: String? {
        get {
          return resultMap["bioInformation"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "bioInformation")
        }
      }

      public var createdOn: String? {
        get {
          return resultMap["createdOn"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "createdOn")
        }
      }

      public var secondaryPhoneNumber: String? {
        get {
          return resultMap["secondaryPhoneNumber"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "secondaryPhoneNumber")
        }
      }

      public var secondaryEmail: String? {
        get {
          return resultMap["secondaryEmail"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "secondaryEmail")
        }
      }

      public var fcmToken: String? {
        get {
          return resultMap["fcmToken"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "fcmToken")
        }
      }

      public var staffUserCoreTypeName: String? {
        get {
          return resultMap["staffUserCoreTypeName"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "staffUserCoreTypeName")
        }
      }

      public var deviceType: String? {
        get {
          return resultMap["deviceType"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "deviceType")
        }
      }
    }
  }
}

public final class UpdateUserWorkspaceMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation updateUserWorkspace($invitationCode: String, $userId: Long) {
      updateUserWorkspace(invitationCode: $invitationCode, userId: $userId) {
        __typename
        organizationId
        workspaceId
        userId
      }
    }
    """

  public let operationName: String = "updateUserWorkspace"

  public var invitationCode: String?
  public var userId: String?

  public init(invitationCode: String? = nil, userId: String? = nil) {
    self.invitationCode = invitationCode
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["invitationCode": invitationCode, "userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateUserWorkspace", arguments: ["invitationCode": GraphQLVariable("invitationCode"), "userId": GraphQLVariable("userId")], type: .object(UpdateUserWorkspace.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateUserWorkspace: UpdateUserWorkspace? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "updateUserWorkspace": updateUserWorkspace.flatMap { (value: UpdateUserWorkspace) -> ResultMap in value.resultMap }])
    }

    public var updateUserWorkspace: UpdateUserWorkspace? {
      get {
        return (resultMap["updateUserWorkspace"] as? ResultMap).flatMap { UpdateUserWorkspace(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateUserWorkspace")
      }
    }

    public struct UpdateUserWorkspace: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UserWorkspaces"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("organizationId", type: .scalar(String.self)),
        GraphQLField("workspaceId", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(organizationId: String? = nil, workspaceId: String? = nil, userId: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "UserWorkspaces", "organizationId": organizationId, "workspaceId": workspaceId, "userId": userId])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var organizationId: String? {
        get {
          return resultMap["organizationId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "organizationId")
        }
      }

      public var workspaceId: String? {
        get {
          return resultMap["workspaceId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "workspaceId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }
    }
  }
}

public final class GetWorksiteSettingsByUserIdQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query GetWorksiteSettingsByUserId($userId: Long!) {
      getWorksiteSettingsByUserId(userId: $userId) {
        __typename
        worksiteSettingId
        userId
        worksiteId
        organizationId
        departmentCount
        color
        departmentDetails {
          __typename
          departmentId
          businessId
          staffRoleByUserId(userId: $userId)
          fteStatusOfStaffUser(userId: $userId)
          departmentTypeName
          phoneNumber
          extension
          email
          address
          description
          status
          colleagues {
            __typename
            firstName
            lastName
            profilePhoto
            userId
            staffUserCoreTypeName
          }
        }
        worksiteDetails {
          __typename
          name
          department {
            __typename
            departmentTypeName
            address
          }
        }
        organizationDetails {
          __typename
          name
          address
          city
          state
          province
          staffMembers {
            __typename
            firstName
            lastName
            userId
            profilePhoto
          }
        }
      }
    }
    """

  public let operationName: String = "GetWorksiteSettingsByUserId"

  public var userId: String

  public init(userId: String) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getWorksiteSettingsByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .list(.object(GetWorksiteSettingsByUserId.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getWorksiteSettingsByUserId: [GetWorksiteSettingsByUserId?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getWorksiteSettingsByUserId": getWorksiteSettingsByUserId.flatMap { (value: [GetWorksiteSettingsByUserId?]) -> [ResultMap?] in value.map { (value: GetWorksiteSettingsByUserId?) -> ResultMap? in value.flatMap { (value: GetWorksiteSettingsByUserId) -> ResultMap in value.resultMap } } }])
    }

    public var getWorksiteSettingsByUserId: [GetWorksiteSettingsByUserId?]? {
      get {
        return (resultMap["getWorksiteSettingsByUserId"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetWorksiteSettingsByUserId?] in value.map { (value: ResultMap?) -> GetWorksiteSettingsByUserId? in value.flatMap { (value: ResultMap) -> GetWorksiteSettingsByUserId in GetWorksiteSettingsByUserId(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetWorksiteSettingsByUserId?]) -> [ResultMap?] in value.map { (value: GetWorksiteSettingsByUserId?) -> ResultMap? in value.flatMap { (value: GetWorksiteSettingsByUserId) -> ResultMap in value.resultMap } } }, forKey: "getWorksiteSettingsByUserId")
      }
    }

    public struct GetWorksiteSettingsByUserId: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["WorksiteSettings"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("worksiteSettingId", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("worksiteId", type: .scalar(String.self)),
        GraphQLField("organizationId", type: .scalar(String.self)),
        GraphQLField("departmentCount", type: .scalar(Int.self)),
        GraphQLField("color", type: .scalar(String.self)),
        GraphQLField("departmentDetails", type: .list(.object(DepartmentDetail.selections))),
        GraphQLField("worksiteDetails", type: .object(WorksiteDetail.selections)),
        GraphQLField("organizationDetails", type: .object(OrganizationDetail.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(worksiteSettingId: String? = nil, userId: String? = nil, worksiteId: String? = nil, organizationId: String? = nil, departmentCount: Int? = nil, color: String? = nil, departmentDetails: [DepartmentDetail?]? = nil, worksiteDetails: WorksiteDetail? = nil, organizationDetails: OrganizationDetail? = nil) {
        self.init(unsafeResultMap: ["__typename": "WorksiteSettings", "worksiteSettingId": worksiteSettingId, "userId": userId, "worksiteId": worksiteId, "organizationId": organizationId, "departmentCount": departmentCount, "color": color, "departmentDetails": departmentDetails.flatMap { (value: [DepartmentDetail?]) -> [ResultMap?] in value.map { (value: DepartmentDetail?) -> ResultMap? in value.flatMap { (value: DepartmentDetail) -> ResultMap in value.resultMap } } }, "worksiteDetails": worksiteDetails.flatMap { (value: WorksiteDetail) -> ResultMap in value.resultMap }, "organizationDetails": organizationDetails.flatMap { (value: OrganizationDetail) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var worksiteSettingId: String? {
        get {
          return resultMap["worksiteSettingId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteSettingId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var worksiteId: String? {
        get {
          return resultMap["worksiteId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteId")
        }
      }

      public var organizationId: String? {
        get {
          return resultMap["organizationId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "organizationId")
        }
      }

      public var departmentCount: Int? {
        get {
          return resultMap["departmentCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentCount")
        }
      }

      public var color: String? {
        get {
          return resultMap["color"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "color")
        }
      }

      public var departmentDetails: [DepartmentDetail?]? {
        get {
          return (resultMap["departmentDetails"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [DepartmentDetail?] in value.map { (value: ResultMap?) -> DepartmentDetail? in value.flatMap { (value: ResultMap) -> DepartmentDetail in DepartmentDetail(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [DepartmentDetail?]) -> [ResultMap?] in value.map { (value: DepartmentDetail?) -> ResultMap? in value.flatMap { (value: DepartmentDetail) -> ResultMap in value.resultMap } } }, forKey: "departmentDetails")
        }
      }

      public var worksiteDetails: WorksiteDetail? {
        get {
          return (resultMap["worksiteDetails"] as? ResultMap).flatMap { WorksiteDetail(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "worksiteDetails")
        }
      }

      public var organizationDetails: OrganizationDetail? {
        get {
          return (resultMap["organizationDetails"] as? ResultMap).flatMap { OrganizationDetail(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "organizationDetails")
        }
      }

      public struct DepartmentDetail: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Department"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("departmentId", type: .scalar(String.self)),
          GraphQLField("businessId", type: .scalar(String.self)),
          GraphQLField("staffRoleByUserId", arguments: ["userId": GraphQLVariable("userId")], type: .scalar(String.self)),
          GraphQLField("fteStatusOfStaffUser", arguments: ["userId": GraphQLVariable("userId")], type: .scalar(String.self)),
          GraphQLField("departmentTypeName", type: .scalar(String.self)),
          GraphQLField("phoneNumber", type: .scalar(String.self)),
          GraphQLField("extension", type: .scalar(String.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("address", type: .scalar(String.self)),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("status", type: .scalar(String.self)),
          GraphQLField("colleagues", type: .list(.object(Colleague.selections))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(departmentId: String? = nil, businessId: String? = nil, staffRoleByUserId: String? = nil, fteStatusOfStaffUser: String? = nil, departmentTypeName: String? = nil, phoneNumber: String? = nil, `extension`: String? = nil, email: String? = nil, address: String? = nil, description: String? = nil, status: String? = nil, colleagues: [Colleague?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Department", "departmentId": departmentId, "businessId": businessId, "staffRoleByUserId": staffRoleByUserId, "fteStatusOfStaffUser": fteStatusOfStaffUser, "departmentTypeName": departmentTypeName, "phoneNumber": phoneNumber, "extension": `extension`, "email": email, "address": address, "description": description, "status": status, "colleagues": colleagues.flatMap { (value: [Colleague?]) -> [ResultMap?] in value.map { (value: Colleague?) -> ResultMap? in value.flatMap { (value: Colleague) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var departmentId: String? {
          get {
            return resultMap["departmentId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "departmentId")
          }
        }

        public var businessId: String? {
          get {
            return resultMap["businessId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessId")
          }
        }

        public var staffRoleByUserId: String? {
          get {
            return resultMap["staffRoleByUserId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "staffRoleByUserId")
          }
        }

        public var fteStatusOfStaffUser: String? {
          get {
            return resultMap["fteStatusOfStaffUser"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "fteStatusOfStaffUser")
          }
        }

        public var departmentTypeName: String? {
          get {
            return resultMap["departmentTypeName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "departmentTypeName")
          }
        }

        public var phoneNumber: String? {
          get {
            return resultMap["phoneNumber"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var `extension`: String? {
          get {
            return resultMap["extension"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "extension")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var address: String? {
          get {
            return resultMap["address"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "address")
          }
        }

        public var description: String? {
          get {
            return resultMap["description"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "description")
          }
        }

        public var status: String? {
          get {
            return resultMap["status"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var colleagues: [Colleague?]? {
          get {
            return (resultMap["colleagues"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Colleague?] in value.map { (value: ResultMap?) -> Colleague? in value.flatMap { (value: ResultMap) -> Colleague in Colleague(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Colleague?]) -> [ResultMap?] in value.map { (value: Colleague?) -> ResultMap? in value.flatMap { (value: Colleague) -> ResultMap in value.resultMap } } }, forKey: "colleagues")
          }
        }

        public struct Colleague: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["UserBasicInformation"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
            GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
            GraphQLField("profilePhoto", type: .scalar(String.self)),
            GraphQLField("userId", type: .nonNull(.scalar(String.self))),
            GraphQLField("staffUserCoreTypeName", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(firstName: String, lastName: String, profilePhoto: String? = nil, userId: String, staffUserCoreTypeName: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto, "userId": userId, "staffUserCoreTypeName": staffUserCoreTypeName])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var firstName: String {
            get {
              return resultMap["firstName"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "firstName")
            }
          }

          public var lastName: String {
            get {
              return resultMap["lastName"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastName")
            }
          }

          public var profilePhoto: String? {
            get {
              return resultMap["profilePhoto"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "profilePhoto")
            }
          }

          public var userId: String {
            get {
              return resultMap["userId"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var staffUserCoreTypeName: String? {
            get {
              return resultMap["staffUserCoreTypeName"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "staffUserCoreTypeName")
            }
          }
        }
      }

      public struct WorksiteDetail: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Businesses"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("department", type: .list(.object(Department.selections))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String? = nil, department: [Department?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Businesses", "name": name, "department": department.flatMap { (value: [Department?]) -> [ResultMap?] in value.map { (value: Department?) -> ResultMap? in value.flatMap { (value: Department) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var department: [Department?]? {
          get {
            return (resultMap["department"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Department?] in value.map { (value: ResultMap?) -> Department? in value.flatMap { (value: ResultMap) -> Department in Department(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Department?]) -> [ResultMap?] in value.map { (value: Department?) -> ResultMap? in value.flatMap { (value: Department) -> ResultMap in value.resultMap } } }, forKey: "department")
          }
        }

        public struct Department: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Department"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("departmentTypeName", type: .scalar(String.self)),
            GraphQLField("address", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(departmentTypeName: String? = nil, address: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Department", "departmentTypeName": departmentTypeName, "address": address])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var departmentTypeName: String? {
            get {
              return resultMap["departmentTypeName"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "departmentTypeName")
            }
          }

          public var address: String? {
            get {
              return resultMap["address"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "address")
            }
          }
        }
      }

      public struct OrganizationDetail: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Organizations"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("address", type: .scalar(String.self)),
          GraphQLField("city", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("province", type: .scalar(String.self)),
          GraphQLField("staffMembers", type: .list(.object(StaffMember.selections))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String, address: String? = nil, city: String? = nil, state: String? = nil, province: String? = nil, staffMembers: [StaffMember?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Organizations", "name": name, "address": address, "city": city, "state": state, "province": province, "staffMembers": staffMembers.flatMap { (value: [StaffMember?]) -> [ResultMap?] in value.map { (value: StaffMember?) -> ResultMap? in value.flatMap { (value: StaffMember) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var address: String? {
          get {
            return resultMap["address"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "address")
          }
        }

        public var city: String? {
          get {
            return resultMap["city"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "city")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var province: String? {
          get {
            return resultMap["province"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "province")
          }
        }

        public var staffMembers: [StaffMember?]? {
          get {
            return (resultMap["staffMembers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [StaffMember?] in value.map { (value: ResultMap?) -> StaffMember? in value.flatMap { (value: ResultMap) -> StaffMember in StaffMember(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [StaffMember?]) -> [ResultMap?] in value.map { (value: StaffMember?) -> ResultMap? in value.flatMap { (value: StaffMember) -> ResultMap in value.resultMap } } }, forKey: "staffMembers")
          }
        }

        public struct StaffMember: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["UserBasicInformation"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
            GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
            GraphQLField("userId", type: .nonNull(.scalar(String.self))),
            GraphQLField("profilePhoto", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(firstName: String, lastName: String, userId: String, profilePhoto: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "userId": userId, "profilePhoto": profilePhoto])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var firstName: String {
            get {
              return resultMap["firstName"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "firstName")
            }
          }

          public var lastName: String {
            get {
              return resultMap["lastName"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastName")
            }
          }

          public var userId: String {
            get {
              return resultMap["userId"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var profilePhoto: String? {
            get {
              return resultMap["profilePhoto"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "profilePhoto")
            }
          }
        }
      }
    }
  }
}

public final class UpdateWorkSiteSettingsMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation UpdateWorkSiteSettings($userId: Long, $worksiteSettingId: Long, $worksiteId: Long, $organizationId: Long, $color: String) {
      updateWorksiteSetting(userId: $userId, worksiteSettingId: $worksiteSettingId, worksiteId: $worksiteId, organizationId: $organizationId, color: $color) {
        __typename
        worksiteSettingId
        userId
        worksiteId
        organizationId
        departmentCount
        color
        departmentDetails {
          __typename
          departmentId
          businessId
          departmentTypeName
          phoneNumber
          extension
          email
          address
          description
          status
          colleagues {
            __typename
            firstName
            lastName
            profilePhoto
            userId
            staffUserCoreTypeName
          }
        }
        worksiteDetails {
          __typename
          name
          department {
            __typename
            departmentTypeName
            address
          }
        }
        organizationDetails {
          __typename
          name
          address
          city
          state
          province
          staffMembers {
            __typename
            firstName
            lastName
            userId
            profilePhoto
          }
        }
      }
    }
    """

  public let operationName: String = "UpdateWorkSiteSettings"

  public var userId: String?
  public var worksiteSettingId: String?
  public var worksiteId: String?
  public var organizationId: String?
  public var color: String?

  public init(userId: String? = nil, worksiteSettingId: String? = nil, worksiteId: String? = nil, organizationId: String? = nil, color: String? = nil) {
    self.userId = userId
    self.worksiteSettingId = worksiteSettingId
    self.worksiteId = worksiteId
    self.organizationId = organizationId
    self.color = color
  }

  public var variables: GraphQLMap? {
    return ["userId": userId, "worksiteSettingId": worksiteSettingId, "worksiteId": worksiteId, "organizationId": organizationId, "color": color]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["MutationDefault"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateWorksiteSetting", arguments: ["userId": GraphQLVariable("userId"), "worksiteSettingId": GraphQLVariable("worksiteSettingId"), "worksiteId": GraphQLVariable("worksiteId"), "organizationId": GraphQLVariable("organizationId"), "color": GraphQLVariable("color")], type: .object(UpdateWorksiteSetting.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(updateWorksiteSetting: UpdateWorksiteSetting? = nil) {
      self.init(unsafeResultMap: ["__typename": "MutationDefault", "updateWorksiteSetting": updateWorksiteSetting.flatMap { (value: UpdateWorksiteSetting) -> ResultMap in value.resultMap }])
    }

    public var updateWorksiteSetting: UpdateWorksiteSetting? {
      get {
        return (resultMap["updateWorksiteSetting"] as? ResultMap).flatMap { UpdateWorksiteSetting(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "updateWorksiteSetting")
      }
    }

    public struct UpdateWorksiteSetting: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["WorksiteSettings"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("worksiteSettingId", type: .scalar(String.self)),
        GraphQLField("userId", type: .scalar(String.self)),
        GraphQLField("worksiteId", type: .scalar(String.self)),
        GraphQLField("organizationId", type: .scalar(String.self)),
        GraphQLField("departmentCount", type: .scalar(Int.self)),
        GraphQLField("color", type: .scalar(String.self)),
        GraphQLField("departmentDetails", type: .list(.object(DepartmentDetail.selections))),
        GraphQLField("worksiteDetails", type: .object(WorksiteDetail.selections)),
        GraphQLField("organizationDetails", type: .object(OrganizationDetail.selections)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(worksiteSettingId: String? = nil, userId: String? = nil, worksiteId: String? = nil, organizationId: String? = nil, departmentCount: Int? = nil, color: String? = nil, departmentDetails: [DepartmentDetail?]? = nil, worksiteDetails: WorksiteDetail? = nil, organizationDetails: OrganizationDetail? = nil) {
        self.init(unsafeResultMap: ["__typename": "WorksiteSettings", "worksiteSettingId": worksiteSettingId, "userId": userId, "worksiteId": worksiteId, "organizationId": organizationId, "departmentCount": departmentCount, "color": color, "departmentDetails": departmentDetails.flatMap { (value: [DepartmentDetail?]) -> [ResultMap?] in value.map { (value: DepartmentDetail?) -> ResultMap? in value.flatMap { (value: DepartmentDetail) -> ResultMap in value.resultMap } } }, "worksiteDetails": worksiteDetails.flatMap { (value: WorksiteDetail) -> ResultMap in value.resultMap }, "organizationDetails": organizationDetails.flatMap { (value: OrganizationDetail) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var worksiteSettingId: String? {
        get {
          return resultMap["worksiteSettingId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteSettingId")
        }
      }

      public var userId: String? {
        get {
          return resultMap["userId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userId")
        }
      }

      public var worksiteId: String? {
        get {
          return resultMap["worksiteId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "worksiteId")
        }
      }

      public var organizationId: String? {
        get {
          return resultMap["organizationId"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "organizationId")
        }
      }

      public var departmentCount: Int? {
        get {
          return resultMap["departmentCount"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "departmentCount")
        }
      }

      public var color: String? {
        get {
          return resultMap["color"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "color")
        }
      }

      public var departmentDetails: [DepartmentDetail?]? {
        get {
          return (resultMap["departmentDetails"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [DepartmentDetail?] in value.map { (value: ResultMap?) -> DepartmentDetail? in value.flatMap { (value: ResultMap) -> DepartmentDetail in DepartmentDetail(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [DepartmentDetail?]) -> [ResultMap?] in value.map { (value: DepartmentDetail?) -> ResultMap? in value.flatMap { (value: DepartmentDetail) -> ResultMap in value.resultMap } } }, forKey: "departmentDetails")
        }
      }

      public var worksiteDetails: WorksiteDetail? {
        get {
          return (resultMap["worksiteDetails"] as? ResultMap).flatMap { WorksiteDetail(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "worksiteDetails")
        }
      }

      public var organizationDetails: OrganizationDetail? {
        get {
          return (resultMap["organizationDetails"] as? ResultMap).flatMap { OrganizationDetail(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "organizationDetails")
        }
      }

      public struct DepartmentDetail: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Department"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("departmentId", type: .scalar(String.self)),
          GraphQLField("businessId", type: .scalar(String.self)),
          GraphQLField("departmentTypeName", type: .scalar(String.self)),
          GraphQLField("phoneNumber", type: .scalar(String.self)),
          GraphQLField("extension", type: .scalar(String.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("address", type: .scalar(String.self)),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("status", type: .scalar(String.self)),
          GraphQLField("colleagues", type: .list(.object(Colleague.selections))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(departmentId: String? = nil, businessId: String? = nil, departmentTypeName: String? = nil, phoneNumber: String? = nil, `extension`: String? = nil, email: String? = nil, address: String? = nil, description: String? = nil, status: String? = nil, colleagues: [Colleague?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Department", "departmentId": departmentId, "businessId": businessId, "departmentTypeName": departmentTypeName, "phoneNumber": phoneNumber, "extension": `extension`, "email": email, "address": address, "description": description, "status": status, "colleagues": colleagues.flatMap { (value: [Colleague?]) -> [ResultMap?] in value.map { (value: Colleague?) -> ResultMap? in value.flatMap { (value: Colleague) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var departmentId: String? {
          get {
            return resultMap["departmentId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "departmentId")
          }
        }

        public var businessId: String? {
          get {
            return resultMap["businessId"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "businessId")
          }
        }

        public var departmentTypeName: String? {
          get {
            return resultMap["departmentTypeName"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "departmentTypeName")
          }
        }

        public var phoneNumber: String? {
          get {
            return resultMap["phoneNumber"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "phoneNumber")
          }
        }

        public var `extension`: String? {
          get {
            return resultMap["extension"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "extension")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var address: String? {
          get {
            return resultMap["address"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "address")
          }
        }

        public var description: String? {
          get {
            return resultMap["description"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "description")
          }
        }

        public var status: String? {
          get {
            return resultMap["status"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "status")
          }
        }

        public var colleagues: [Colleague?]? {
          get {
            return (resultMap["colleagues"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Colleague?] in value.map { (value: ResultMap?) -> Colleague? in value.flatMap { (value: ResultMap) -> Colleague in Colleague(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Colleague?]) -> [ResultMap?] in value.map { (value: Colleague?) -> ResultMap? in value.flatMap { (value: Colleague) -> ResultMap in value.resultMap } } }, forKey: "colleagues")
          }
        }

        public struct Colleague: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["UserBasicInformation"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
            GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
            GraphQLField("profilePhoto", type: .scalar(String.self)),
            GraphQLField("userId", type: .nonNull(.scalar(String.self))),
            GraphQLField("staffUserCoreTypeName", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(firstName: String, lastName: String, profilePhoto: String? = nil, userId: String, staffUserCoreTypeName: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "profilePhoto": profilePhoto, "userId": userId, "staffUserCoreTypeName": staffUserCoreTypeName])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var firstName: String {
            get {
              return resultMap["firstName"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "firstName")
            }
          }

          public var lastName: String {
            get {
              return resultMap["lastName"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastName")
            }
          }

          public var profilePhoto: String? {
            get {
              return resultMap["profilePhoto"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "profilePhoto")
            }
          }

          public var userId: String {
            get {
              return resultMap["userId"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var staffUserCoreTypeName: String? {
            get {
              return resultMap["staffUserCoreTypeName"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "staffUserCoreTypeName")
            }
          }
        }
      }

      public struct WorksiteDetail: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Businesses"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("department", type: .list(.object(Department.selections))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String? = nil, department: [Department?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Businesses", "name": name, "department": department.flatMap { (value: [Department?]) -> [ResultMap?] in value.map { (value: Department?) -> ResultMap? in value.flatMap { (value: Department) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var department: [Department?]? {
          get {
            return (resultMap["department"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Department?] in value.map { (value: ResultMap?) -> Department? in value.flatMap { (value: ResultMap) -> Department in Department(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Department?]) -> [ResultMap?] in value.map { (value: Department?) -> ResultMap? in value.flatMap { (value: Department) -> ResultMap in value.resultMap } } }, forKey: "department")
          }
        }

        public struct Department: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Department"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("departmentTypeName", type: .scalar(String.self)),
            GraphQLField("address", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(departmentTypeName: String? = nil, address: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Department", "departmentTypeName": departmentTypeName, "address": address])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var departmentTypeName: String? {
            get {
              return resultMap["departmentTypeName"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "departmentTypeName")
            }
          }

          public var address: String? {
            get {
              return resultMap["address"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "address")
            }
          }
        }
      }

      public struct OrganizationDetail: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Organizations"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("address", type: .scalar(String.self)),
          GraphQLField("city", type: .scalar(String.self)),
          GraphQLField("state", type: .scalar(String.self)),
          GraphQLField("province", type: .scalar(String.self)),
          GraphQLField("staffMembers", type: .list(.object(StaffMember.selections))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String, address: String? = nil, city: String? = nil, state: String? = nil, province: String? = nil, staffMembers: [StaffMember?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Organizations", "name": name, "address": address, "city": city, "state": state, "province": province, "staffMembers": staffMembers.flatMap { (value: [StaffMember?]) -> [ResultMap?] in value.map { (value: StaffMember?) -> ResultMap? in value.flatMap { (value: StaffMember) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var address: String? {
          get {
            return resultMap["address"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "address")
          }
        }

        public var city: String? {
          get {
            return resultMap["city"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "city")
          }
        }

        public var state: String? {
          get {
            return resultMap["state"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "state")
          }
        }

        public var province: String? {
          get {
            return resultMap["province"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "province")
          }
        }

        public var staffMembers: [StaffMember?]? {
          get {
            return (resultMap["staffMembers"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [StaffMember?] in value.map { (value: ResultMap?) -> StaffMember? in value.flatMap { (value: ResultMap) -> StaffMember in StaffMember(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [StaffMember?]) -> [ResultMap?] in value.map { (value: StaffMember?) -> ResultMap? in value.flatMap { (value: StaffMember) -> ResultMap in value.resultMap } } }, forKey: "staffMembers")
          }
        }

        public struct StaffMember: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["UserBasicInformation"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("firstName", type: .nonNull(.scalar(String.self))),
            GraphQLField("lastName", type: .nonNull(.scalar(String.self))),
            GraphQLField("userId", type: .nonNull(.scalar(String.self))),
            GraphQLField("profilePhoto", type: .scalar(String.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(firstName: String, lastName: String, userId: String, profilePhoto: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "UserBasicInformation", "firstName": firstName, "lastName": lastName, "userId": userId, "profilePhoto": profilePhoto])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var firstName: String {
            get {
              return resultMap["firstName"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "firstName")
            }
          }

          public var lastName: String {
            get {
              return resultMap["lastName"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastName")
            }
          }

          public var userId: String {
            get {
              return resultMap["userId"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "userId")
            }
          }

          public var profilePhoto: String? {
            get {
              return resultMap["profilePhoto"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "profilePhoto")
            }
          }
        }
      }
    }
  }
}
