//
//  AppDelegate.swift
//  Floatcare
//
//  Created by BEAST on 10/05/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        ApolloManager.shared.basicConfig()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
//        IQKeyboardManager.shared.enableAutoToolbar = false
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        registerForRemoteNotifications(application: application)
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("RemoteMsg Received")
    }
    
    func registerForRemoteNotifications(application: UIApplication) {
//        if #available(iOS 10.0, *) {
//          // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
//
//          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//          UNUserNotificationCenter.current().requestAuthorization(
//            options: authOptions,
//            completionHandler: {_, _ in })
//        } else {
//          let settings: UIUserNotificationSettings =
//          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//          application.registerUserNotificationSettings(settings)
//        }
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            if let error = error {
                print("D'oh: \(error.localizedDescription)")
            } else {
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
                
            }
        }
        
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = deviceToken.map { String(format: "%.2hhx", $0) }.joined()

        print(token)
        Messaging.messaging().apnsToken = deviceToken

        print("Successfully registered for notifications!")
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for notifications: \(error.localizedDescription)")
    }
    
    func getToken()  {
        InstanceID.instanceID().instanceID { (result, error) in
          if let error = error {
            print("Error fetching remote instance ID: \(error)")
          } else if let result = result {
            print("Remote instance ID token: \(result.token)")
//            self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            print("Device Token",result.token)
            
            print("insrance id",result.instanceID)
          }
        }
    }
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        Messaging.messaging().apnsToken = deviceToken as Data
//        print(String(data: deviceToken as Data, encoding: .utf8)!)
//    }
}


extension AppDelegate : UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        // Print full message.
        
        print(userInfo)
        
        NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "refreshChat"), object: nil)
     //   self.extractUserInfo(userInfo: userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
         
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print full message.
        
        self.extractUserInfo(userInfo: userInfo)
        for key in userInfo.keys {
            print(key)
        }
        print(userInfo)
        print(response)
    }
    
    func extractUserInfo(userInfo: [AnyHashable : Any]) -> (title: String, body: String) {
        var info = (title: "", body: "")
        
        if let deeplink = userInfo["gcm.notification.deepLink"] as? String {
            
            self.handleDeeplink(deeplink: deeplink, userInfo: userInfo)
        }
        guard let aps = userInfo["aps"] as? [String: Any] else { return info }
        guard let alert = aps["alert"] as? [String: Any] else { return info }
        let title = alert["title"] as? String ?? ""
        let body = alert["body"] as? String ?? ""
        info = (title: title, body: body)
        return info
    }
    
    func handleDeeplink(deeplink: String, userInfo: [AnyHashable : Any]) {
        
        switch deeplink {
            case "1":
                self.navifateToWorkspace()
            case "2":
                self.navigateToConversations(userInfo: userInfo)
            default:
                self.navigateToOpenShifts(userInfo: userInfo)
        }
        
    }
    
    func navigateToOpenShifts(userInfo: [AnyHashable : Any]) {
        
        self.window?.rootViewController?.navigationController?.popToRootViewController(animated: false)
        let viewcontroller = Utility.topViewController()
        if let navVC = viewcontroller as? UINavigationController {
            for vc in navVC.viewControllers {
                
                if let schedulesViewController = vc as? SchedulesViewController {
                    navVC.popToViewController(schedulesViewController, animated: false)
                    schedulesViewController.movetoPageIndex(page: .openShiftsPage)
                    break
                }
            }
        }
    }
    func navigateToConversations(userInfo: [AnyHashable : Any]) {
        
        if let userId = userInfo["gcm.notification.userId"] as? String {
            
            self.window?.rootViewController?.navigationController?.popToRootViewController(animated: false)
            let viewcontroller = Utility.topViewController()
            if let navVC = viewcontroller as? UINavigationController {
                for vc in navVC.viewControllers {
                    
                    if let schedulesViewController = vc as? SchedulesViewController {
                        navVC.popToViewController(schedulesViewController, animated: false)
                        schedulesViewController.navigateToChat(chatId: "\(userId)")
                        break
                    }
                }
            }
        }
    }
    
    func navifateToWorkspace() {
        
        self.window?.rootViewController?.navigationController?.popToRootViewController(animated: false)
        let viewcontroller = Utility.topViewController()
        let storyB = UIStoryboard.init(name: "WorkSpace", bundle: nil)
        let wsVC = storyB.instantiateViewController(identifier: "WorkSpaceListViewController") as! WorkSpaceListViewController
        wsVC.modalPresentationStyle = .fullScreen
        if let navVc = viewcontroller as? UINavigationController {
            
             navVc.pushViewController(wsVC, animated: true)
        } else if let navVc = viewcontroller?.navigationController {
            
            navVc.pushViewController(wsVC, animated: true)
        } else {
            viewcontroller?.present(wsVC, animated: true, completion: nil)
        }
       
    }
}


extension AppDelegate : MessagingDelegate{
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      print("Firebase registration token: \(fcmToken)")

      let dataDict:[String: String] = ["token": fcmToken]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
        
        self.getToken()
        FloatCareUserDefaults().fcmToken = fcmToken
    }
 
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage)
    }
}
