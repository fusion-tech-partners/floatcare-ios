//
//  BioAuthViewController.swift
//  Floatcare
//
//  Created by BEAST on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

protocol BioVCDefinable: class {
    func showNextScreen()
}

class BioAuthViewController: UIViewController  {
    weak var bioVCDelegate : BioVCDefinable!
    let touchMe = BiometricIDAuth()
    //    var isFromRegisStration = false
    
    @IBOutlet var bioView: BioAuthView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bioView.nextScreenDelegate = self
    }
}

extension BioAuthViewController: BioViewToControllerDefinable {
    func bioEnabled(_ status: Bool) {
        
        if status {
            touchMe.authenticateUser() { [weak self] message in
                if let message = message {
                    DispatchQueue.main.async {
                        let alertView = UIAlertController(title: "Error",
                                                          message: message,
                                                          preferredStyle: .alert)
                        let okAction = UIAlertAction.init(title: "Retry", style: .default) { (action) in
                            DispatchQueue.main.async {
                                self?.bioView.bioTable.reloadData()
                            }
                        }
                        alertView.addAction(okAction)
                        self?.present(alertView, animated: true)
                    }
                } else {
                    self?.saveTokeyChain()
                    let animation = FloatCareUserDefaults.shared.isUserLogedIn ? false : true                           
                    self?.dismiss(animated: animation, completion: {
                        self?.bioVCDelegate?.showNextScreen()
                    })          }
            }
        } else {
            self.dismiss(animated: true, completion: {
                self.bioVCDelegate?.showNextScreen()
            })
        }
    }
}

extension BioAuthViewController {
    func saveTokeyChain() {
        guard let email = LoginModel.sharedInstance.email else {
                   return
               }
        guard let password = LoginModel.sharedInstance.password else {
                   return
               }

        KeychainService.savePassword(service: "email", account: "account", data: email)
        
        KeychainService.savePassword(service: "password", account: "account", data: password)

    }
}

