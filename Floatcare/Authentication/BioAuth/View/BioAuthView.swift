//
//  BioAuthView.swift
//  Floatcare
//
//  Created by BEAST on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

protocol BioViewToControllerDefinable: class {
    func bioEnabled(_ status: Bool)
}

class BioAuthView: UIView {
    weak var nextScreenDelegate : BioViewToControllerDefinable!
    @IBOutlet weak var bioTable: UITableView!
   
}

extension BioAuthView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BioTableViewCell.self), for: indexPath) as? BioTableViewCell {
            cell.nextScreenDelegate = self
            if FloatCareUserDefaults.shared.isUserLogedIn{
                cell.showOnlyFaceIDView.isHidden = false
                cell.enableAuthButtonTapped(UIButton())
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height
    }
}


extension BioAuthView: BioNextScreenDefinable{
    func bioEnabled(_ status: Bool) {
        nextScreenDelegate?.bioEnabled(status)
    }
    
    
}
