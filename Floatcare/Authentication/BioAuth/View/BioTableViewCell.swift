//
//  BioTableViewCell.swift
//  Floatcare
//
//  Created by BEAST on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import TTTAttributedLabel

protocol BioNextScreenDefinable: class {
    func bioEnabled(_ status: Bool)
}

class BioTableViewCell: UITableViewCell {
    @IBOutlet weak var enableAuthButton: FloatButton!
    @IBOutlet weak var showOnlyFaceIDView: UIView!
    @IBOutlet weak var skipForNowButton: UIButton!
    @IBOutlet weak var authTypeImage: UIImageView!
    @IBOutlet weak var subTitle: LabelGray!
    @IBOutlet weak var headerTitle: TTTAttributedLabel!
    weak var nextScreenDelegate : BioNextScreenDefinable!
    let touchMe = BiometricIDAuth()

   @IBAction func enableAuthButtonTapped(_ sender: Any) {
    FloatCareUserDefaults.shared.faceID = true
    nextScreenDelegate?.bioEnabled(true)
      }
    
    @IBAction func skipForNowTapped(_ sender: Any) {
        FloatCareUserDefaults.shared.faceID = false
        nextScreenDelegate?.bioEnabled(false)
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        bioAuthButton.isHidden = !touchMe.canEvaluatePolicy()
        switch touchMe.biometricType() {
        case .faceID:
            authTypeImage.image = #imageLiteral(resourceName: "face")
        default:
            authTypeImage.image = #imageLiteral(resourceName: "Touch")
        }

        authTypeImage?.image = authTypeImage.image?.withRenderingMode(.alwaysTemplate)
        authTypeImage?.tintColor = #colorLiteral(red: 0.4358979464, green: 0.4750115871, blue: 0.7274814248, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
