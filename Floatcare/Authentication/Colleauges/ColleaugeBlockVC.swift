//
//  ColleaugeBlockVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 03/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ColleaugeBlockVC: UIViewController {

    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bottomView.roundCorners(corners: [.topLeft,.topRight], radius: 15)
        yesBtn.layer.cornerRadius = 6
        noBtn.layer.cornerRadius = 6
        yesBtn.layer.borderWidth = 1
        yesBtn.layer.borderColor = #colorLiteral(red: 0.2823529412, green: 0.3647058824, blue: 0.7294117647, alpha: 0.3)
    }
    
    @IBAction func yesClick(_ sender: Any) {
    }
    
    @IBAction func noClick(_ sender: Any) {
    }
    @IBAction func dismissClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
