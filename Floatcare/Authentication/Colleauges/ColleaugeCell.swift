//
//  ColleaugeCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ColleaugeCell: UITableViewCell {
 @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var thumbnailImg: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var accessoryButton: UIButton!
    
    @IBOutlet weak var mySwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        uiSetup()
        mySwitch.onTintColor = #colorLiteral(red: 0.3607843137, green: 0.3882352941, blue: 0.6705882353, alpha: 1)
        thumbnailImg.layer.cornerRadius = 10
    }
    func uiSetup()  {
        thumbnailImg.layer.cornerRadius = 10
        titleLabel.font = UIFont.init(name: Fonts.sFProText, size: 16)
        descriptionLabel.font = UIFont.init(name: Fonts.sFProText, size: 13)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func switchChange(_ sender: Any) {
    }
    @IBAction func accessoryClick(_ sender: Any) {
    }
    
}
