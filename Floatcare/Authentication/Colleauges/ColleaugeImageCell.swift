//
//  ColleaugeImageCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ColleaugeImageCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
     @IBOutlet weak var bioDescription: UILabel!
    @IBOutlet weak var bioTitle: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        uiSetUp()
        contentView.roundCorners(corners: [.bottomLeft], radius: 40)
        bgView.roundCorners(corners: [.bottomLeft], radius: 40)
        profileImageView.layer.cornerRadius = 15.0
    }

    func uiSetUp() {
        titleLabel.font = UIFont.init(name: Fonts.sFProText, size: 14)
        nameLabel.font = UIFont.init(name: Fonts.athleasBold, size: 23)
        cityLabel.font = UIFont.init(name: Fonts.sFProText, size: 16)
        durationLabel.font = UIFont.init(name: Fonts.sFProText, size: 14)
        bioTitle.font = UIFont.init(name: Fonts.sFProText, size: 14)
        bioDescription.font = UIFont.init(name: Fonts.sFProText, size: 15)
        bgView.roundCorners(corners: [.bottomLeft,], radius: 40)
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
