//
//  ColleaugeProfileVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
enum CellHeights: CGFloat {
    case imageCell = 200
    case headerCell = 60
    case dataCell = 64
    
}

class ColleaugeVM {
    static let sharedInstance = ColleaugeVM()
    var title = ""
    var name = ""
    var address1 = ""
    var year = ""
    var profileImg = ""
    var description = ""
    let sectionOne = [["title" : "Basic Account Info",
     "description": "Get to know them better",
        "image":"Basic Account"],
    ["title" : "Mention",
    "description": "Push, SMS, Email",
    "image":"Schedule"],
    
    ["title" : "Notifications",
    "description": "Pause all notifications",
        "image":"Notifications"]]
    let sectionTwo = [["title" : "Block user",
     "description": "Stop receiving any messages",
        "image":"Block user"],
    ["title" : "Report user",
    "description": "Flag this account for review",
        "image":"Workspaces"]]
    
}


class ColleaugeProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var colleaugeVM = ColleaugeVM.sharedInstance
    var colleaugeId = "0"
    override func viewDidLoad() {
        super.viewDidLoad()
    
        ReusableAlertController.showAlert(title: "Float Care", message: "Colleague In Progress")
        // Do any additional setup after loading the view.
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")

        }
        self.fetchColleaugeDetails()

    }
    
    func fetchColleaugeDetails(){
        ApolloManager.shared.client.fetch(query: FindUsersByIdQuery(userId:self.colleaugeId),
                     cachePolicy: .fetchIgnoringCacheData,
                     context: nil,
                     queue: .main) { result in
                        guard let user = try? result.get().data?.findUsersById?.resultMap else {
                            return
                        }
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            let decoder = JSONDecoder()
                            let userBasicInformation = try? decoder.decode(UserBasicInformation.self, from: jsonData)
                            self.updateViewModel(userBasicInfo: userBasicInformation!)
                            print(user)
                        }catch {
                            
                        }
        }
    }
    
    func updateViewModel(userBasicInfo:UserBasicInformation) {
        colleaugeVM.title = userBasicInfo.staffUserCoreTypeName ?? ""
        colleaugeVM.name = userBasicInfo.firstName
        colleaugeVM.description = userBasicInfo.bioInformation ?? ""
        colleaugeVM.address1 = userBasicInfo.city ?? ""
        colleaugeVM.year = "Menber Since"
        colleaugeVM.profileImg = amazonURL + (userBasicInfo.profilePhoto?.fillSpaces ?? "")
        tableView.reloadData()
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return colleaugeVM.sectionOne.count
        case 2:
            return colleaugeVM.sectionTwo.count
            
        default:
            return 0
        }
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 && indexPath.section == 0{
            return UITableView.automaticDimension
        }else{
            return CellHeights.dataCell.rawValue
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 && indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeImageCell", for: indexPath) as! ColleaugeImageCell
            cell.nameLabel.text = colleaugeVM.name
            cell.titleLabel.text = colleaugeVM.title
            cell.cityLabel.text = colleaugeVM.address1
            cell.durationLabel.text = colleaugeVM.year
//            cell.bioTitle.text = colleaugeVM.description
            cell.bioDescription.text = colleaugeVM.description
            cell.profileImageView!.sd_setImage(with: NSURL(string: colleaugeVM.profileImg) as URL?, completed: nil)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeCell", for: indexPath) as! ColleaugeCell
            if indexPath.row == 2 && indexPath.section == 1{
                cell.mySwitch.isHidden = false
                cell.accessoryButton.isHidden = true
            }else{
                cell.accessoryButton.isHidden = false
                cell.mySwitch.isHidden = true
            }
            if indexPath.section == 1{
            cell.titleLabel.text = colleaugeVM.sectionOne[indexPath.row]["title"]
            cell.descriptionLabel.text = colleaugeVM.sectionOne[indexPath.row]["description"]
                cell.imgVw.image = UIImage.init(named: colleaugeVM.sectionOne[indexPath.row]["image"]!)
            
            }
            if indexPath.section == 2{
            cell.titleLabel.text = colleaugeVM.sectionTwo[indexPath.row]["title"]
            cell.descriptionLabel.text = colleaugeVM.sectionTwo[indexPath.row]["description"]
                cell.imgVw.image = UIImage.init(named: colleaugeVM.sectionTwo[indexPath.row]["image"]!)
            }
        
            
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 || section == 2{
            return CellHeights.headerCell.rawValue
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell

        if section == 1{
            cell.headerTitle.text = "Profile Settings"
        }
        if section == 2{
                 cell.headerTitle.text = "Security & Privacy"
             }
        return cell.contentView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1{
            return 3
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let vw = UIView.init(frame: CGRect.zero)
        vw.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.9843137255, alpha: 1)
        return vw
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 && indexPath.section == 2{
            let vc = self.storyboard?.instantiateViewController(identifier: "ColleaugeBlockVC") as! ColleaugeBlockVC
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
