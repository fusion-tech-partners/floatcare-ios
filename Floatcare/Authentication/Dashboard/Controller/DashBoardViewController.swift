//
//  DashBoardViewController.swift
//  Floatcare
//
//  Created by BEAST on 10/05/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class DashBoardViewController: UIViewController {
   
    @IBOutlet var dashboardView: DashboardView!
    let touchMe = BiometricIDAuth()
    private let sessionProvider = URLSessionProvider()

    override func viewDidLoad() {
        super.viewDidLoad()
        dashboardView.dashBoardDelegate = self
        KeychainService.removePassword(service: "password", account: "account")
        KeychainService.removePassword(service: "email", account: "account")

        
        guard let passwordKeyChain = KeychainService.loadPassword(service: "password", account: "account") else {
                                     return
                                 }
                                 
              guard let emailKeyChain = KeychainService.loadPassword(service: "email", account: "account") else {
                                     return
                                 }
        
        if passwordKeyChain != "", emailKeyChain != "" {
            self.checkBio()
        }
    }
    
    func checkBio() {
        let touchBool = touchMe.canEvaluatePolicy()
        if touchBool {
            
            touchMe.authenticateUser() { [weak self] message in
                if let message = message {

                } else {
                    guard let password = KeychainService.loadPassword(service: "password", account: "account") else {
                        return
                    }
                    
                    guard let email = KeychainService.loadPassword(service: "email", account: "account") else {
                        return
                    }
                    print(password,email)
                    self?.callLoginApi()
                }
            }        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        if FloatCareUserDefaults.shared.isUserLogedIn {
            navigateToLoginScreen(isForLogin: false,withAnimation: false)
        }
    }
}

extension DashBoardViewController: DashBoardActionable {
    func registrationTapped() {
        navigateToLoginScreen(isForLogin: false, withAnimation: true)
    }

    func loginTapped() {
        navigateToLoginScreen(isForLogin: true, withAnimation: true  )
    }
    
    func navigateToLoginScreen(isForLogin: Bool,withAnimation:Bool) {
        keepReset()
        
        
        
//        let schedulesViewController = UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: String(describing: SchedulesViewController.self)) as! SchedulesViewController
//        self.navigationController?.pushViewController(schedulesViewController, animated: true)
        
        let loginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: String(describing: LoginViewController.self)) as! LoginViewController
        loginVC.isForLogin = isForLogin
        self.navigationController?.pushViewController(loginVC, animated: withAnimation)
    }
    
    func keepReset() {
        RegistrationModel.sharedInstance.birthday = nil
        RegistrationModel.sharedInstance.firstName = nil
        RegistrationModel.sharedInstance.lastName = nil
        RegistrationModel.sharedInstance.gender = nil

        LoginModel.sharedInstance.email = nil
        LoginModel.sharedInstance.password = nil
        LoginModel.sharedInstance.visibility = false
        
        MobileNumberModel.sharedInstance.otp = nil
        MobileNumberModel.sharedInstance.mobileNumber = nil
    }
}

extension DashBoardViewController {
    func callLoginApi() {
        Progress.shared.showProgressView()
        
        guard let passwordKeyChain = KeychainService.loadPassword(service: "password", account: "account") else {
                               return
                           }
                           
        guard let emailKeyChain = KeychainService.loadPassword(service: "email", account: "account") else {
                               return
                           }
        
        LoginModel.sharedInstance.email = emailKeyChain
        LoginModel.sharedInstance.password = passwordKeyChain
        
        guard let email = LoginModel.sharedInstance.email else {
            return
        }
        
        guard let password = LoginModel.sharedInstance.password else {
            return
        }
        
        sessionProvider.request(type: LoginResponse.self, service: LoginService.login(email: email, password: password)) { response in
            switch response {
            case let .success(posts):
                Profile.shared.loginResponse = posts
                Progress.shared.hideProgressView()
                DispatchQueue.main.async {
                    if posts.status == 403 {
                        self.showAlert(posts.message)
                    } else {
                        self.showFeaturesVC()
                    }
                }
            case let .failure(error):
                
                DispatchQueue.main.async {
                    Progress.shared.hideProgressView()
                }
                if error == .irrelevent {
                    DispatchQueue.main.async {
                        self.showAlert("Please enter valid credentials.")
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert("Somethig went wrong")
                    }
                }
                
            }
        }
    }
    
    fileprivate func showAlert(_ message: String) {
        let alert = UIAlertController(title: "Float Care", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in

        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showFeaturesVC() {
        
        let schedulesViewController = UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: String(describing: SchedulesViewController.self)) as! SchedulesViewController
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.pushViewController(schedulesViewController, animated: true)
    }
}
