//
//  DashboardView.swift
//  Floatcare
//
//  Created by BEAST on 10/05/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

protocol DashBoardActionable: class {
    func registrationTapped()
    func loginTapped()
}

class DashboardView: UIView {

    @IBOutlet weak var dashBoardTable: UITableView!
    weak var dashBoardDelegate: DashBoardActionable!

    override func awakeFromNib() {
        superview?.awakeFromNib()
        dashBoardTable.estimatedRowHeight = 774.0
        dashBoardTable.isScrollEnabled = true
    }
}

extension DashboardView: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DashBoardCell.self), for: indexPath) as? DashBoardCell {
            cell.selectionStyle = .none
            cell.dashBoardCellDelegate = self
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height
    }
    
}

extension DashboardView: DashBoardCellActionable {
    func registrationTapped() {
        self.dashBoardDelegate.registrationTapped()
    }

    func loginTapped() {
        self.dashBoardDelegate.loginTapped()
    }
}
