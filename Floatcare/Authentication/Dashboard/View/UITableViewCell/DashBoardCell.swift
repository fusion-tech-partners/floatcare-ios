//
//  DashBoardCell.swift
//  Floatcare
//
//  Created by BEAST on 10/05/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

protocol DashBoardCellActionable: class {
    func registrationTapped()
    func loginTapped()
}


class DashBoardCell: UITableViewCell {
    weak var dashBoardCellDelegate: DashBoardCellActionable!

    @IBOutlet weak var registerButton: FloatButton!
    @IBOutlet weak var loginButton: FloatButton!
    @IBOutlet weak var subTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
//        setUpSubTitleLabel()
    }

    func setUpSubTitleLabel() {
        subTitleLabel.lineBreakMode = .byWordWrapping
        subTitleLabel.numberOfLines = 0
        subTitleLabel.textColor = UIColor(red: 0.57, green: 0.57, blue: 0.62, alpha: 1)
        let textContent = "A new continuity of care"
        let textString = NSMutableAttributedString(
          string: textContent,
          attributes: [
            NSAttributedString.Key.font: UIFont(name: "OpenSans-Light", size: 16)!
          ]
        )
        let textRange = NSRange(location: 0, length: textString.length)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.1875
        paragraphStyle.alignment = .center
        textString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: textRange)
        textString.addAttribute(NSAttributedString.Key.kern, value: 0.75, range: textRange)
        subTitleLabel.attributedText = textString
        subTitleLabel.sizeToFit()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func registrationTapped(_ sender: UIButton) {
        self.dashBoardCellDelegate.registrationTapped()
    }

    @IBAction func loginTapped(_ sender: UIButton) {
        self.dashBoardCellDelegate.loginTapped()
    }

}


