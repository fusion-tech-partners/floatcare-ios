//
//  LoginViewController.swift
//  Floatcare
//
//  Created by BEAST on 22/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

enum ShowAlertNavigations: Int {
    case registration = 1
    case login
    case none
}

class LoginViewController: KeyBoardNotifierViewController {
    
    @IBOutlet weak var bioAuthButton: UIButton!
    @IBOutlet weak var bioContainer: UIView!
    
    private let sessionProvider = URLSessionProvider()
    var isForLogin = false
    @IBOutlet var loginView: LoginView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginView.isForLogin = isForLogin
        enrollTableForKeyBoardNotification(table: loginView.loginTable)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        if FloatCareUserDefaults.shared.isUserLogedIn{
            
            LoginModel.sharedInstance.email = FloatCareUserDefaults.shared.userName
            LoginModel.sharedInstance.password = FloatCareUserDefaults.shared.password
            callLoginApi()
            showBioAuthViewController()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNaigationBar()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    fileprivate func setUpNaigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = UIColor.clear
        
        let button = UIButton(type:.custom)
        button.setImage(UIImage(named: "Back"), for: .normal)
        button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func backTapped() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        
        view.endEditing(true)
        if let status = LoginModel.sharedInstance.email?.isValidEmail(), status {

            if ConnectionCheck.isConnectedToNetwork() {
                if isForLogin {
                    callLoginApi()
                } else {
                    guard let pwd = LoginModel.sharedInstance.password, !(pwd.isEmpty) else
                    {
                        ReusableAlertController.showAlert(title: "Float Care", message: "Password should not be empty")
                        return
                    }
                    verifyEmailAndRegister()
                }
            } else {
                ReusableAlertController.showAlert(title: "Float Care", message: "Please check your internet connection")
            }
        } else {
            ReusableAlertController.showAlert(title: "Float Care", message: "Enter Your Valid Email")
        }

}

}

// Login flow
extension LoginViewController {
    func callLoginApi() {
        Progress.shared.showProgressView()
        
        guard let email = LoginModel.sharedInstance.email else {
            return
        }
        
        guard let password = LoginModel.sharedInstance.password else {
            return
        }
       
        sessionProvider.request(type: LoginResponse.self, service: LoginService.login(email: email, password: password)) { response in
            switch response {
            case let .success(posts):
                
                FloatCareUserDefaults.shared.userName = email
                FloatCareUserDefaults.shared.password = password
                Profile.shared.loginResponse = posts
                
                FetchWorkSiteSettings().fetchWorksitesByUserId(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)") { (settings) in
                    print(settings)
                    GetWorkSiteDetails().checkForWorkSpaceId(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", completion:{ [weak self] (userInfo) in
                                    
                    //                self?.userBasicInformation = userInfo
                    //                self?.updateuserDetails()
                        print(userInfo) })
                }
                
                
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification_Myrequest), object: nil, userInfo: nil)
                ProfileUpdateAPI().updateFCMToken(fcmToken:FloatCareUserDefaults().fcmToken ?? "")
                Progress.shared.hideProgressView()
                if !FloatCareUserDefaults.shared.isUserLogedIn{
                    DispatchQueue.main.async {
                        if posts.status == 403 {
                            self.showAlert(posts.message, navigation: .none)
                        } else {
                            self.showAlert(posts.message, navigation: .login)
                        }
                    }
                }
            case let .failure(error):
                
                DispatchQueue.main.async {
                    Progress.shared.hideProgressView()
                }
                if error == .irrelevent {
                    DispatchQueue.main.async {
                        self.showAlert("Please enter valid credentials.", navigation: .none)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert("Somethig went wrong", navigation: .none)
                    }
                }
                
            }
        }
    }
}

// Registration flow for email existance
extension LoginViewController {
    func verifyEmailAndRegister() {
        Progress.shared.showProgressView()
        guard let email = LoginModel.sharedInstance.email else {
            return
        }
        
        sessionProvider.request(type: Email.self, service: VerifyEmailExistenceService.email(email: email)) { response in
            switch response {
            case let .success(posts):
                Progress.shared.hideProgressView()
                DispatchQueue.main.async {
                    if posts.status == 403 {
                        self.showAlert(posts.message, navigation: .none)
                    } else {
                        self.showAlert(posts.message, navigation: .registration)
                    }
                }
            case let .failure(error):
                DispatchQueue.main.async {
                    Progress.shared.hideProgressView()
                }
                DispatchQueue.main.async {
                    self.showAlert("Somethig went wrong", navigation: .none)
                }
            }
        }
    }
}
// Sho Alert
extension LoginViewController {
    fileprivate func showAlert(_ message: String, navigation: ShowAlertNavigations) {
        let alert = UIAlertController(title: "Float Care", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            switch navigation {
            case .login:
                self.showBioAuthViewController()
            case .registration:
                self.showRegistrationVC()
            case .none:
                break
            }
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}

//Navigation to....
extension LoginViewController {
    func showRegistrationVC() {
        let registrationVC = UIStoryboard(name: "Registration", bundle: nil).instantiateViewController(withIdentifier: String(describing: RegistrationViewController.self)) as! RegistrationViewController
        self.navigationController?.pushViewController(registrationVC, animated: true)
    }
    
    func showFeaturesVC(withAnimation:Bool) {
        let schedulesViewController = UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: String(describing: SchedulesViewController.self)) as! SchedulesViewController
        self.navigationController?.setNavigationBarHidden(true, animated: withAnimation)
        self.navigationController?.pushViewController(schedulesViewController, animated: withAnimation)
    }
    
    func showBioAuthViewController() {
        let bioVC = UIStoryboard(name: "BioAuth", bundle: nil).instantiateViewController(withIdentifier: String(describing: BioAuthViewController.self)) as! BioAuthViewController
        bioVC.bioVCDelegate = self
        bioVC.modalPresentationStyle = .fullScreen
        let animation = FloatCareUserDefaults.shared.isUserLogedIn ? false : true
        self.present(bioVC, animated: animation, completion: nil)
    }
}

extension LoginViewController: BioVCDefinable {
    func showNextScreen() {
        let animation = FloatCareUserDefaults.shared.isUserLogedIn ? false : true
        showFeaturesVC(withAnimation: animation)
    }
}
