//
//  LoginFormFieldCell.swift
//  Floatcare
//
//  Created by BEAST on 22/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

enum LoginFormFieldsType: Int {
    case EmailAddress = 1
    case Password
}

enum LoginFieldTypes: String {
    case EmailAddress = "Email Address"
    case Password = "Password"
}

protocol LoginTextFieldActionable: class {
    func reloadData()
}

class LoginFormFieldCell: UITableViewCell {
    @IBOutlet weak var textField: SkyFloatingLabelTextFieldWithIcon!
    weak var testFieldActionDelegate: LoginTextFieldActionable!
    internal typealias TextFieldTitles = LoginFieldTypes

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}

extension LoginFormFieldCell {
    fileprivate func rightImgeWithFormField(_ row: Int) {
        extractInitialSetUp(textField,
                            LoginIconName.password.rawValue,
                            row,
                            TextFieldTitles.Password.rawValue)
        textField.text = LoginModel.sharedInstance.password
 
        
        if let status = LoginModel.sharedInstance.visibility  {
            textField.isSecureTextEntry = !status
            textField.text = LoginModel.sharedInstance.password 

            func setButtonAsRightImage() -> UIButton {
                let button = UIButton(type: .custom)
                button.tintColor = LoginModel.sharedInstance.visibility == true ? #colorLiteral(red: 0.4358979464, green: 0.4750115871, blue: 0.7274814248, alpha: 1) : #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                button.frame = CGRect(x: CGFloat(textField.frame.size.width - 40), y: CGFloat(5), width: CGFloat(40), height: CGFloat(30))
                button.backgroundColor = UIColor.clear
                button.addTarget(self, action: #selector(self.refreshContent), for: .touchUpInside)
                
                // +RightImage
                let imageName = status ? LoginIconName.visible.rawValue : LoginIconName.invisible.rawValue
                
                button.setImage(UIImage.init(named: imageName), for: .normal)
                
                return button
            }
            
            var textFieldBtn: UIButton {
                return setButtonAsRightImage()
             }
            
            textField.rightView = textFieldBtn
            textField.rightViewMode = .always
        }
    }
    
    
    @objc func refreshContent() {
        LoginModel.sharedInstance.visibility = !LoginModel.sharedInstance.visibility!
        testFieldActionDelegate?.reloadData()
    }
    
    func extractInitialSetUp(
         _ textField: SkyFloatingLabelTextFieldWithIcon,
         _ icon: String,
         _ rowTag: Int,
         _ title: String) {

         textField.iconImage = UIImage(named: icon)
         textField.placeholder = title
         textField.title = title
         textField.selectedTitleColor = #colorLiteral(red: 0.4358979464, green: 0.4750115871, blue: 0.7274814248, alpha: 1)
         textField.delegate = self
         textField.tag = rowTag
     }
}

extension LoginFormFieldCell: UITextFieldDelegate  {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case LoginFormFieldsType.EmailAddress.rawValue:
            break
        case LoginFormFieldsType.Password.rawValue:
            break
        default:
            break
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case LoginFormFieldsType.EmailAddress.rawValue:
            LoginModel.sharedInstance.email = textField.text
            break
        case LoginFormFieldsType.Password.rawValue:
            LoginModel.sharedInstance.password = textField.text
            break
        default:
            break
        }

        testFieldActionDelegate?.reloadData()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension LoginFormFieldCell {
internal func initialSetUpBasedOn(_ row: Int) {
    switch LoginFormFieldsType(rawValue: row) {
    case .EmailAddress:
        extractInitialSetUp(textField,
                            LoginIconName.email.rawValue,
                            row,
                            TextFieldTitles.EmailAddress.rawValue)
    case .Password:
        rightImgeWithFormField(row)
    case .none:
        break
    }
}
}
