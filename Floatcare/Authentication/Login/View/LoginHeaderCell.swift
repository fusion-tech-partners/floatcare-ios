//
//  LoginHeaderCell.swift
//  Floatcare
//
//  Created by BEAST on 22/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class LoginHeaderCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var headersubViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none

        titleLabel?.font = UIFont(name: "Athelas-Regular", size: 32)
        titleLabel?.textColor = UIColor(red: 0.43, green: 0.45, blue: 0.47, alpha: 1)
        titleLabel?.sizeToFit()
            
        container.heightAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.75).isActive = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
