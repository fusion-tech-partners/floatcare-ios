//
//  LoginView.swift
//  Floatcare
//
//  Created by BEAST on 22/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

enum LoginIconName: String {
    case email = "email"
    case password = "secure"
    case visible = "eye"
    case invisible = "eye.slash"
}


class LoginView: UIView {
    
        
    @IBOutlet weak var loginTable: UITableView!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var rightImage: UIImageView!
    @IBOutlet weak var leftImage: UIImageView!
    var isForLogin = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        clipsToBounds = true
        leftImage.clipsToBounds = true
        rightImage.clipsToBounds = true
        leftImage.sendSubviewToBack(backGroundView)
        rightImage.sendSubviewToBack(backGroundView)

        loginTable.separatorStyle = .none
        loginTable.backgroundColor = .clear
        loginTable.isOpaque = false
        loginTable.backgroundView = nil
    }
    
}

extension LoginView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LoginHeaderCell.self), for: indexPath) as? LoginHeaderCell {
                cell.titleLabel.text = isForLogin ? "Login" : "Create Account"
                return cell
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LoginFormFieldCell.self), for: indexPath) as? LoginFormFieldCell {
                cell.textField.keyboardType = .emailAddress
                cell.initialSetUpBasedOn(indexPath.row)
                return cell
            }
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LoginFormFieldCell.self), for: indexPath) as? LoginFormFieldCell {

                cell.testFieldActionDelegate = self
                cell.initialSetUpBasedOn(indexPath.row)
                
                return cell
            }
            
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {            
        return UITableView.automaticDimension
    }

}

extension LoginView: LoginTextFieldActionable {
    func reloadData() {
        loginTable?.reloadData()
    }
}
