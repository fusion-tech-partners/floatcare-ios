//
//  LoginModel.swift
//  Floatcare
//
//  Created by BEAST on 22/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

class LoginModel {
    static let sharedInstance = LoginModel()
    private init() {

    }
    var email: String?
    var password: String?
    var visibility: Bool? = false

    init(email:String?, password: String?, visibility:Bool = false) {
        self.email = email
        self.password = password
        self.visibility = visibility
    }
}
