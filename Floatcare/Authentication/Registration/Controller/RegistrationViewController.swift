//
//  RegistrationViewController.swift
//  Floatcare
//
//  Created by BEAST on 13/05/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import Toast_Swift
enum GenderType: String, CaseIterable {
   case female = "Female"
   case male = "Male"
   case other = "Other"
   case none = "Cancel"
}

class RegistrationViewController: KeyBoardNotifierViewController {

    @IBOutlet var registrationView: RegistrationView!
    
    @IBAction func nextTapped(_ sender: UIButton) {
        guard RegistrationModel.sharedInstance.profileImage != nil else {
            self.view.makeToast("Please upload Profile Image", duration: 3.0, position: .center)
            return
        }
        let roleSelectionVC = UIStoryboard(name: "RoleSelection", bundle: nil).instantiateViewController(withIdentifier: String(describing: RoleSelectionViewController.self)) as! RoleSelectionViewController
        self.navigationController?.pushViewController(roleSelectionVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        registrationView.handleRegistrationButton()
        enrollTableForKeyBoardNotification(table: registrationView.registrationTable)
        registrationView.delegate = self
        setUpNaigationBar()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    fileprivate func setUpNaigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = UIColor.clear

        let button = UIButton(type:.custom)
        button.setImage(UIImage(named: "Back"), for: .normal)
        button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }

    @objc func backTapped() {
        self.navigationController?.popToRootViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension RegistrationViewController: ViewToControllerDeleagte, UIActionSheetDelegate {

    fileprivate func showActionSheet() {
        let optionMenu = UIAlertController(title: nil, message: "Select Your Gender", preferredStyle: .actionSheet)

        for type in GenderType.allCases {
            let action = UIAlertAction(title: type.rawValue, style: type.rawValue == GenderType.none.rawValue ? .cancel : .default, handler: { (action: UIAlertAction) in
                guard let title = action.title, title != "Cancel" else {
                    return
                }
                RegistrationModel.sharedInstance.gender = title
                self.registrationView.handleRegistrationButton()
                self.registrationView.reloadData()
            })
            optionMenu.addAction(action)
        }

        self.present(optionMenu, animated: true, completion: nil)
    }

    func showGenderActionSheet() {
        showActionSheet()
    }
}

