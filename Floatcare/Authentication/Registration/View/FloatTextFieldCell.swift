//
//  FloatTextFieldswift
//  Floatcare
//
//  Created by BEAST on 13/05/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

enum TextFieldType: Int {
    case FirstName = 1
    case LastName
    case Gender
    case Birthday
}

enum FieldTypes: String {
    case FirstName = "First Name"
    case LastName = "Last Name"
    case Gender = "Gender"
    case Birthday = "Birthday"
}

enum IconName: String {
    case name = "ic_user_greyedout"
    case gender = "gender"
    case birthday = "ic_Birthday"
}


protocol TextFieldActionable: class {
    func showGenderActionSheet()
    func reloadData()
    func handleRegistrationButton()
}

class FloatTextFieldCell: UITableViewCell {

    @IBOutlet weak var textField: SkyFloatingLabelTextFieldWithIcon!
    internal typealias TextFieldTitles = FieldTypes
    weak var testFieldActionDelegate: TextFieldActionable!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension FloatTextFieldCell {
    fileprivate func rightImgeWithFormField(_ row: Int) {
        extractInitialSetUp(textField,
                            IconName.gender.rawValue,
                            row,
                            TextFieldTitles.Gender.rawValue)
        textField.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: -5, y: 10, width: 5, height: 5))
        imageView.image = #imageLiteral(resourceName: "down_arrow")
        imageView.tintColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        textField.rightView = imageView
        textField.text = RegistrationModel.sharedInstance.gender
    }

    fileprivate func birthdaySetUp(_ row: Int) {
        extractInitialSetUp(textField,
                            IconName.birthday.rawValue,
                            row,
                            TextFieldTitles.Birthday.rawValue)

        textField.iconMarginBottom = 13
        textField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        textField.text = RegistrationModel.sharedInstance.birthday
    }

    internal func initialSetUpBasedOn(_ row: Int) {
        switch TextFieldType(rawValue: row) {
        case .FirstName:
            extractInitialSetUp(textField,
                                IconName.name.rawValue,
                                row,
                                TextFieldTitles.FirstName.rawValue)
        case .LastName:
            extractInitialSetUp(textField,
                                IconName.name.rawValue,
                                row,
                                TextFieldTitles.LastName.rawValue)
        case .Gender:
            rightImgeWithFormField(row)
        case .Birthday:
            birthdaySetUp(row)
        case .none:
            break
        }
    }

    func extractInitialSetUp(
        _ textField: SkyFloatingLabelTextFieldWithIcon,
        _ icon: String,
        _ rowTag: Int,
        _ title: String) {

        textField.iconImage = UIImage(named: icon)
        textField.placeholder = title
        textField.title = title
        textField.selectedTitleColor = #colorLiteral(red: 0.4358979464, green: 0.4750115871, blue: 0.7274814248, alpha: 1)
        textField.delegate = self
        textField.tag = rowTag
    }
}

extension FloatTextFieldCell: UITextFieldDelegate  {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case TextFieldType.FirstName.rawValue:
            break
        case TextFieldType.LastName.rawValue:
            break
        case TextFieldType.Gender.rawValue:
            DispatchQueue.main.async {
                textField.resignFirstResponder()
            }
            testFieldActionDelegate?.showGenderActionSheet()
        case TextFieldType.Birthday.rawValue:
            break
        default:
            break
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
               case TextFieldType.FirstName.rawValue:
                RegistrationModel.sharedInstance.firstName = textField.text
               case TextFieldType.LastName.rawValue:
                RegistrationModel.sharedInstance.lastName = textField.text
               case TextFieldType.Gender.rawValue:
                    break
                case TextFieldType.Birthday.rawValue:
                   break
               default:
                   break
               }
        
        testFieldActionDelegate?.reloadData()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension FloatTextFieldCell {
    @objc func tapDone() {
        if let datePicker = textField?.inputView as? UIDatePicker {
            let dateformatter = DateFormatter()
            dateformatter.dateStyle = .medium
            dateformatter.dateFormat = "dd-MMM-yyyy"
            let str: String = dateformatter.string(from: datePicker.date)
            var date = Date()
            date = dateformatter.date(from: str)!
            dateformatter.dateFormat = "MM/dd/yyyy"
            let final = dateformatter.string(from: date)
        RegistrationModel.sharedInstance.birthday = final
        testFieldActionDelegate?.handleRegistrationButton()
        }
        textField.resignFirstResponder()
    }
}

