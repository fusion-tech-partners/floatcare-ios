//
//  RegistrationHeaderCell.swift
//  Floatcare
//
//  Created by BEAST on 13/05/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import CropViewController

class RegistrationHeaderCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var accountImageView: UIImageView!
    var imagePicker: ImagePicker!
    var parentVC : UIViewController?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.imagePicker = ImagePicker(presentationController: ReusableAlertController.getTopViewController(), delegate: self)

        titleLabel?.font = UIFont(name: "Athelas-Regular", size: 32)
        titleLabel?.textColor = UIColor(red: 0.43, green: 0.45, blue: 0.47, alpha: 1)
        titleLabel?.sizeToFit()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        accountImageView.addGestureRecognizer(tap)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.imagePicker.present(from: UIButton.init())
    }
    @IBAction func camClick(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        Utility.topViewController()?.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func camera()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = .camera
        parentVC = Utility.topViewController()
        Utility.topViewController()?.present(myPickerController, animated: true, completion: nil)
        
    }
    
    func photoLibrary()
    {
        
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = .photoLibrary
        parentVC = Utility.topViewController()
        Utility.topViewController()?.present(myPickerController, animated: true, completion: nil)
        
    }
}

extension RegistrationHeaderCell: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        accountImageView.image = image
    }
    
}


extension RegistrationHeaderCell: UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate {
    
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let pickedImage = info[.originalImage] as? UIImage {
            
//            self.accountImageView.image = pickedImage
//            RegistrationModel.sharedInstance.profileImage = pickedImage
            self.presentCropViewController(image: pickedImage)
        }
//        picker.dismiss(animated: true, completion: nil)
    }
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.accountImageView.image = image
        RegistrationModel.sharedInstance.profileImage = image
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
    func presentCropViewController(image :UIImage) {
      let cropViewController = CropViewController(image: image)
        cropViewController.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.presetSquare
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.aspectRatioLockDimensionSwapEnabled = true
        cropViewController.aspectRatioPickerButtonHidden = true
        cropViewController.delegate = self
        parentVC?.present(cropViewController, animated: true, completion: nil)
    }
}
