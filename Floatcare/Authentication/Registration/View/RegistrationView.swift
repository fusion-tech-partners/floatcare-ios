//
//  RegistrationView.swift
//  Floatcare
//
//  Created by BEAST on 13/05/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol ViewToControllerDeleagte: class {
    func showGenderActionSheet()
}

class RegistrationView: UIView {
    
    weak var delegate: ViewToControllerDeleagte!
    var viewModel = RegistrationViewModel()

    @IBOutlet weak var nextButton: FloatButton!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var rightImage: UIImageView!
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var registrationTable: UITableView!

    override func awakeFromNib() {
        super.awakeFromNib()

        leftImage.clipsToBounds = true
        rightImage.clipsToBounds = true

        registrationTable.separatorStyle = .none
        registrationTable.backgroundColor = .clear
        registrationTable.isOpaque = false
        registrationTable.backgroundView = nil
    }
}

extension RegistrationView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.row {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RegistrationHeaderCell.self), for: indexPath) as? RegistrationHeaderCell {
                cell.selectionStyle = .none
                return cell
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FloatTextFieldCell.self), for: indexPath) as? FloatTextFieldCell {
                cell.initialSetUpBasedOn(indexPath.row)
                cell.textField.text = RegistrationModel.sharedInstance.firstName ?? ""
                cell.testFieldActionDelegate = self
                return cell
            }
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FloatTextFieldCell.self), for: indexPath) as? FloatTextFieldCell {
                cell.textField.text = RegistrationModel.sharedInstance.lastName ?? ""
                cell.initialSetUpBasedOn(indexPath.row)
                cell.testFieldActionDelegate = self
                return cell
            }
        case 3:
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FloatTextFieldCell.self), for: indexPath) as? FloatTextFieldCell {
                cell.initialSetUpBasedOn(indexPath.row)
                cell.testFieldActionDelegate = self
                return cell
            }
        case 4:
            if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FloatTextFieldCell.self), for: indexPath) as? FloatTextFieldCell {
                cell.initialSetUpBasedOn(indexPath.row)
                cell.testFieldActionDelegate = self
                return cell
            }
        default:
            return UITableViewCell()
        }
        return UITableViewCell()

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 275 : 80
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


extension RegistrationView: TextFieldActionable {
    func showGenderActionSheet() {
        delegate?.showGenderActionSheet()
    }

    func reloadData() {
        handleRegistrationButton()
        registrationTable?.reloadData()
    }
}

extension RegistrationView {
     func handleRegistrationButton() {
        let status = viewModel.validateRegistrationInputs(RegistrationModel.sharedInstance)
        nextButton.isUserInteractionEnabled = status
        nextButton.alpha = status ? 1 : 0.3
    }
}
