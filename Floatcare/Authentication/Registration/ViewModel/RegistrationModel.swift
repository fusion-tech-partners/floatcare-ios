//
//  RegistrationModel.swift
//  Floatcare
//
//  Created by BEAST on 13/05/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import UIKit
class RegistrationModel {
    static let sharedInstance = RegistrationModel()
    private init() {

    }
    var firstName: String?
    var lastName: String?
    var gender: String?
    var birthday: String?
    var profileImage : UIImage?
    var staffUserCoreTypeId : Int?

    init(lastName:String?, firstName: String?, gender: String?, birthday: String?,img:UIImage?,type:Int?) {
        self.gender = gender
        self.birthday = birthday
        self.lastName = lastName
        self.firstName = firstName
        self.profileImage = img
        self.staffUserCoreTypeId = type
    }
}




