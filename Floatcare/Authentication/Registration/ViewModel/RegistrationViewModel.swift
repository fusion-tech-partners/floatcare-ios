//
//  RegistrationViewModel.swift
//  Floatcare
//
//  Created by BEAST on 28/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

class RegistrationViewModel: NSObject {
    func validateRegistrationInputs(_ model: RegistrationModel) -> Bool {
        if !String.isValid(input: model.firstName) || !String.isValid(input: model.lastName) || !String.isValid(input: model.birthday) || !String.isValid(input: model.gender) {
            return false
        }
        return true
    }
    
    func registerUser() {
        let model = RegistrationModel.sharedInstance
        
        guard let date = model.birthday else {
            return
        }
        
        
        let mutation = CreateUserBasicInformationMutation.init(firstName: model.firstName ?? "", organizationId: "98989898", lastName: model.lastName ?? "", email: "vrrr.gmail.com", phoneNumber: "918767851122", password: "2233113322", dateOfBirth: String.convertDateFormater(date), gender: model.gender ?? "", profilePhoto: "djshfdjsahfdjkhfdsakjfhdasf", userTypeId: "364723", staffUserCoreTypeId: "")
        
        ApolloManager.shared.client.perform(mutation: mutation) { (result) in
            guard let data = try? result.get().data else { return }
        }
    }
}




