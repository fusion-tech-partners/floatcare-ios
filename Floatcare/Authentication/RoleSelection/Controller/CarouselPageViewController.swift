//
//  CarouselPageViewController.swift
//  Floatcare
//
//  Created by BEAST on 23/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import UIKit

enum RoleImageNames: String {
    case nurse = "OB_nurse"
    case doctor = "OB_doc"
    case tech = "OB_tech"
    case assistant = "OB_assistant"
    case therapist = "OB_Therapist"
    case physician = "OB_Physician"
}

enum RoleTitleNames: String {
    case nurse = "Nurse"
    case doctor = "Doctor"
    case tech = "Technician"
    case assistant = "Assistant"
    case therapist = "Therapist"
    case physician = "Physician"
}

/*enum RoleDetails: String {
    case nurse = "Fancy short description describing this role goes on these two lines"
    case doctor = "Fancy short description describing this role goes on these two lines"
    case tech = "Fancy short description describing this role goes on these two lines"
    case assistant = "Fancy short description describing this role goes on these two lines"
    case therapist = "Fancy short description describing this role goes on these two lines"
}*/

struct RoleCollection {
    var image: String
    var roleTitle: String
    var roleDesicription: String
    var tag: Int
}

protocol HideAndSeekDelegate: class {
    func hideAndSeek(left: Bool, right: Bool)
}

class CarouselPageViewController: UIPageViewController {
    
    var items: [UIViewController] = []
    var currentPageIndex = 0
    var lastPendingViewControllerIndex = 0
    weak var hideAndSeekDelegate: HideAndSeekDelegate!
    var rolesCollection : [RoleCollection]?

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        decoratePageControl()
        _ = populateItems()
        if let firstViewController = items.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    fileprivate func decoratePageControl() {
        let pc = UIPageControl.appearance(whenContainedInInstancesOf: [CarouselPageViewController.self])
        pc.currentPageIndicatorTintColor = #colorLiteral(red: 0.4274509804, green: 0.4470588235, blue: 0.4705882353, alpha: 1)
        pc.pageIndicatorTintColor = #colorLiteral(red: 0.4274509804, green: 0.4470588235, blue: 0.4705882353, alpha: 0.3105743838)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for subView in view.subviews {
            if subView is  UIPageControl {
            subView.frame.origin.y = self.view.frame.size.height - 50
            subView.subviews.forEach {
                $0.transform = CGAffineTransform(scaleX: 1.4 , y: 1.4)
            }
            }
        }
    }
        
    func populateItems() {
        let detail = "Fancy short description describing this role goes on these two lines"
        
        let doctor = RoleCollection(image: RoleImageNames.doctor.rawValue, roleTitle: RoleTitleNames.doctor.rawValue, roleDesicription: detail, tag: 1)
        
        let nurse = RoleCollection(image: RoleImageNames.nurse.rawValue, roleTitle: RoleTitleNames.nurse.rawValue, roleDesicription: detail, tag: 2)
                

        let tech = RoleCollection(image: RoleImageNames.tech.rawValue, roleTitle: RoleTitleNames.tech.rawValue, roleDesicription: detail, tag: 3)
        
        let therapist = RoleCollection(image: RoleImageNames.therapist.rawValue, roleTitle: RoleTitleNames.therapist.rawValue, roleDesicription: detail, tag: 4)
        
        let assistant = RoleCollection(image: RoleImageNames.assistant.rawValue, roleTitle: RoleTitleNames.assistant.rawValue, roleDesicription: detail, tag: 5)
        
        let physician = RoleCollection(image: RoleImageNames.physician.rawValue, roleTitle: RoleTitleNames.physician.rawValue, roleDesicription: detail, tag: 6)


        rolesCollection = [doctor, nurse, tech, therapist, assistant, physician]
                
        for (index, item) in rolesCollection!.enumerated() {
            let role = createCarouselItemControler(item.image, item.roleTitle, roleDetails: item.roleDesicription, tag: index+1)
            items.append(role)
        }
        RegistrationModel.sharedInstance.staffUserCoreTypeId = rolesCollection![0].tag
    }
    
    fileprivate func createCarouselItemControler(_ image: String?,  _ roleTitle: String?, roleDetails: String?, tag: Int) -> UIViewController {
        let controller = UIViewController()
        
        controller.view = CarouselItem(image, roleTitle, roleDetails, tag: tag)
        
        return controller
    }
}

// MARK: - DataSource

extension CarouselPageViewController: UIPageViewControllerDataSource {
    
    // Back
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = items.firstIndex(of: viewController) else {
               return nil
           }
    
           let preIndex = vcIndex - 1
        
            hideAndSeekDelegate?.hideAndSeek(left: false, right: false)
        
            if vcIndex == 0 {
                hideAndSeekDelegate?.hideAndSeek(left: true, right: false)
                return nil
            }
        
           guard preIndex >= 0 else {
            return items.last // loops back to end
           }
           guard items.count > preIndex else {
               return nil
           }
        RegistrationModel.sharedInstance.staffUserCoreTypeId = rolesCollection?[preIndex].tag ?? 0
           return items[preIndex]
        
        //use this  For repeated pages
        /*guard let viewControllerIndex = items.firstIndex(of: viewController) else {
            return nil
        }

        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return items.last
        }
        
        guard items.count > previousIndex else {
            return nil
        }
        
        return items[previousIndex]*/
        
    }
    
    // Front
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = items.firstIndex(of: viewController) else {
               return nil
           }

           let nextIndex = vcIndex + 1
            hideAndSeekDelegate?.hideAndSeek(left: false, right: false)
        
        DispatchQueue.main.async {
            if self.items.count - 1 <=  nextIndex {
                self.hideAndSeekDelegate?.hideAndSeek(left: false, right: true)
            }
        }
     
        
            if items.count == nextIndex {
            return nil
            }

           guard items.count != nextIndex else {
               return items.first
           }
           guard items.count > nextIndex else {
               return nil
           }
        RegistrationModel.sharedInstance.staffUserCoreTypeId = rolesCollection?[nextIndex].tag ?? 0
           return items[nextIndex]
        
        
        //use this  For repeated pages
     /*   guard let viewControllerIndex = items.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        print("nextIndex === \(nextIndex)")

        guard items.count != nextIndex else {
            return items.first
        }
        
        guard items.count > nextIndex else {
            return nil
        }
        
        return items[nextIndex]*/
    }
    
    func presentationCount(for _: UIPageViewController) -> Int {
        return items.count
    }
    
    func presentationIndex(for _: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = items.firstIndex(of: firstViewController) else {
                return 0
        }
        return firstViewControllerIndex
    }
}

extension UIPageViewController {

    func goToNextPage(animated: Bool = true) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        setViewControllers([nextViewController], direction: .forward, animated: animated, completion: nil)
    }

    func goToPreviousPage(animated: Bool = true) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let previousViewController = dataSource?.pageViewController(self, viewControllerBefore: currentViewController) else { return }
        setViewControllers([previousViewController], direction: .reverse, animated: animated, completion: nil)
    }
}


// Remove parent buttons for first and last pages

extension CarouselPageViewController: UIPageViewControllerDelegate {
    private func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController]){
        
        if let items = pageViewController.viewControllers {
            for item in items {
                if let view = item.view {
                    self.lastPendingViewControllerIndex = view.tag
                }
        }
//        // 1
//        if let viewController = pendingViewControllers[0] as? UIViewController {
//            // 2
//            self.lastPendingViewControllerIndex = viewController
//        }
    }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
                if completed {
                    if let currentViewController = pageViewController.viewControllers?.first,
                        let index = items.firstIndex(of: currentViewController) {
                        self.currentPageIndex = index
                        
                        hideAndSeekDelegate?.hideAndSeek(left: self.currentPageIndex == 0 ? true : false, right: self.currentPageIndex == 4 ? true : false)
                    }
                }
    }
    
}
