//
//  RoleSelectionViewController.swift
//  Floatcare
//
//  Created by BEAST on 23/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class RoleSelectionViewController: UIViewController {
    
    @IBAction func roleSelected(_ sender: Any) {
        let registrationVC = UIStoryboard(name: "Verification", bundle: nil).instantiateViewController(withIdentifier: String(describing: MobileNumberViewController.self)) as! MobileNumberViewController
        self.navigationController?.pushViewController(registrationVC, animated: true)
    }
    @IBOutlet weak var backwordButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet var roleSelectionView: RoleSelectionView!
    
    var vc = CarouselPageViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNaigationBar()
        backwordButton.isHidden = true
        if let vd = self.children.first as? CarouselPageViewController {
            vc = vd
            vc.hideAndSeekDelegate = self
        }
    }
}
    
extension RoleSelectionViewController {

    fileprivate func setUpNaigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = UIColor.clear

        let button = UIButton(type:.custom)
        button.setImage(UIImage(named: "Back"), for: .normal)
        button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }

}

extension RoleSelectionViewController {
    
    @IBAction func swipeLegt(_ sender: UIButton) {
        vc.goToPreviousPage()
    }

    @IBAction func swipeRight(_ sender: UIButton) {
        vc.goToNextPage()
    }
}

extension RoleSelectionViewController : HideAndSeekDelegate {
    func hideAndSeek(left: Bool, right: Bool) {
        backwordButton.isHidden = left
        forwardButton.isHidden = right
    }
    
}

