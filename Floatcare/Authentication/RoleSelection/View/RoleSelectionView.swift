//
//  RoleSelectionView.swift
//  Floatcare
//
//  Created by BEAST on 23/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class RoleSelectionView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    @IBOutlet weak var container: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        setHeaderLabels()
    }

    func setHeaderLabels() {
        titleLabel?.text = """
                            Which Role best
                            describes you?
                            """
        titleLabel?.font = UIFont(name: "Athelas-Regular", size: 32)
        titleLabel?.textColor = UIColor(red: 0.42, green: 0.44, blue: 0.47, alpha: 1)
        titleLabel?.sizeToFit()
        
        subLabel.text = "(Swipe & Select Role)"
        subLabel?.font = UIFont(name: "Poppins-Regular", size: 15)
        subLabel?.textColor = UIColor(red: 0.66, green: 0.68, blue: 0.71, alpha: 1)
        subLabel?.sizeToFit()
    }
    
}
