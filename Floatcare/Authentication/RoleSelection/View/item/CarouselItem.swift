//
//  CarouselItem.swift
//  Floatcare
//
//  Created by BEAST on 23/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CarouselItem: UIView {
    static let CAROUSEL_ITEM_NIB = "CarouselItem"
    
    @IBOutlet var vwContent: UIView!
    @IBOutlet var vwBackground: UIView!
    @IBOutlet var container: RectangularDashedView!
    @IBOutlet weak var roleDescription: UILabel!
    @IBOutlet weak var roleTitle: UILabel!
    @IBOutlet weak var roleImageView: UIImageView!
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initWithNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initWithNib()
    }
    
    convenience init(_ image: String? = "", _ roleTitle: String? = "", _ roleDescription: String? = "", tag: Int) {
        self.init()
        self.roleDescription?.text = roleDescription
        self.roleTitle?.text = roleTitle
        self.roleImageView?.image = UIImage(named: image!)
        
        self.roleTitle?.sizeToFit()
        self.roleDescription?.sizeToFit()
        
        container.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.82)
        
//        container.backgroundColor = .red

        
        container.layer.shadowOffset = CGSize(width: 0, height: -5)
        container.layer.shadowColor = UIColor(red: 0.71, green: 0.71, blue: 0.75, alpha: 0.1).cgColor
        container.layer.shadowOpacity = 1
        container.layer.shadowRadius = 10
        container.layer.masksToBounds = false
    }
    
    fileprivate func initWithNib() {
        Bundle.main.loadNibNamed(CarouselItem.CAROUSEL_ITEM_NIB, owner: self, options: nil)
        vwContent.frame = bounds
        vwContent.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(vwContent)
    }
}

class RectangularDashedView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var dashWidth: CGFloat = 0
    @IBInspectable var dashColor: UIColor = .clear
    @IBInspectable var dashLength: CGFloat = 0
    @IBInspectable var betweenDashesSpace: CGFloat = 0
    
    var dashBorder: CAShapeLayer?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        dashBorder?.removeFromSuperlayer()
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = UIColor(red: 0.78, green: 0.81, blue: 0.93, alpha: 1).cgColor
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, betweenDashesSpace] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        if cornerRadius > 0 {
            dashBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        } else {
            dashBorder.path = UIBezierPath(rect: bounds).cgPath
        }
        layer.addSublayer(dashBorder)
        self.dashBorder = dashBorder
    }
}
