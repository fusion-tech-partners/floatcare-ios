//
//  ColorExtension.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 21/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

extension UIColor {
    
    var buttonViolet : UIColor{
        return  #colorLiteral(red: 0.3607843137, green: 0.3882352941, blue: 0.6705882353, alpha: 1)
    }
    var labelGray : UIColor{
        return UIColor(red: 169/255, green: 175/255, blue: 182/255, alpha: 1)
    }
    var labelGrayHeading : UIColor {
        return  #colorLiteral(red: 0.4274509804, green: 0.4470588235, blue: 0.4705882353, alpha: 1) //UIColor(red: 109/255, green: 114/255, blue: 120/255, alpha: 1)
    }
    var errorRed : UIColor {
          return UIColor(red: 255/255, green: 96/255, blue: 85/255, alpha: 1)
    }
    
    func rgb(r:CGFloat,g:CGFloat,b:CGFloat,k:CGFloat) -> UIColor {
        let color = UIColor.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: k)
        return color
    }

}
