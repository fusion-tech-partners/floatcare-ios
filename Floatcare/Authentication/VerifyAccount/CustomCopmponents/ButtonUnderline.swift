//
//  ButtonUnderline.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 21/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

class ButtonUnderline: UIButton {

override init(frame: CGRect) {
    super.init(frame: frame)
    buttonDefaults()
    
}
required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    buttonDefaults()
}

override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    
    self.buttonDefaults()
    backgroundColor = .red
}


func buttonDefaults() {
    setTitleColor(UIColor().buttonViolet, for: .normal)
    titleLabel!.font = UIFont.init(name: Fonts.poppinsRegular, size: 15)
    let buttonTitleStr = NSMutableAttributedString(string:self.titleLabel?.text ?? "", attributes:[NSAttributedString.Key.underlineStyle:1])
    let attributedStr  = NSMutableAttributedString(string:"")
    attributedStr.append(buttonTitleStr)
    self.setAttributedTitle(attributedStr, for: .normal)
}

}
