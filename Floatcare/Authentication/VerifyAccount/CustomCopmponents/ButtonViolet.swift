//
//  ButtonViolet.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 21/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit
import ObjectiveC


// Declare a global var to produce a unique address as the assoc object handle


@IBDesignable class ButtonViolet: UIButton {

override init(frame: CGRect) {
    super.init(frame: frame)
    buttonDefaults()
    
}
required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    buttonDefaults()
}

override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    
    self.buttonDefaults()
    backgroundColor = .red
}


func buttonDefaults() {
    backgroundColor = UIColor().buttonViolet
    layer.cornerRadius = 4
    setTitleColor(.white, for: .normal)
    titleLabel!.font = UIFont.init(name: Fonts.poppinsSemiBold, size: 18)
}

}
