//
//  GrayLabel.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 21/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

@IBDesignable class LabelGray: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        labelDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        labelDefaults()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        labelDefaults()
    }
    
    func labelDefaults() {
        textColor = UIColor().labelGray
        self.font = UIFont(name: Fonts.poppinsRegular, size: 15)
    }
    @IBInspectable var fontSize : CGFloat {
        get{
            return font.pointSize
        }set{
            self.font = UIFont(name: Fonts.poppinsRegular, size: newValue)
        }
    }
    
    

}



