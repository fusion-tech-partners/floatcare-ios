//
//  LabelHeader.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 21/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

class LabelHeader: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        labelDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        labelDefaults()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        labelDefaults()
    }
    
    func labelDefaults() {
        textColor = UIColor().labelGrayHeading
        self.font = UIFont(name: Fonts.athleasBold, size: 32)
    }
    
    @IBInspectable var fontSize : CGFloat {
        get{
            return font.pointSize
        }set{
            self.font = UIFont(name: Fonts.athleasRegular, size: newValue)
        }
    }
    
    

}
