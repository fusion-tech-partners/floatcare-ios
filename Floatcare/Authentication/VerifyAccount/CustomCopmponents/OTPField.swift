//
//  OTPField.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 21/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

@IBDesignable class OTPField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.textFieldDefaults()
    }
    
     required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.textFieldDefaults()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        textFieldDefaults()
    }
    
    func textFieldDefaults()  {
        layer.cornerRadius = 12
        backgroundColor = .white
        borderStyle = .none
        layer.borderColor = UIColor().buttonViolet.cgColor
        layer.borderWidth = 1
        textColor = UIColor().buttonViolet
//        font = UIFont(name: Fonts.poppinsBold, size: 25)
    }
    

}


