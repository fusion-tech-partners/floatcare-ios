//
//  ThemeHeaderLabel.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 26/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ThemeHeaderLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        labelDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        labelDefaults()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        labelDefaults()
    }
    
    func labelDefaults() {
        textColor = UIColor().buttonViolet
        self.font = UIFont(name: Fonts.athleasBold, size: 32)
    }
    @IBInspectable var fontSize : CGFloat {
        get{
            return font.pointSize
        }set{
            self.font = UIFont(name: Fonts.poppinsRegular, size: newValue)
        }
    }
    
    

}
