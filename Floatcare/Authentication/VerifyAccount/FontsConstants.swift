//
//  Fonts.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 27/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

class Fonts {
    
    static let poppinsBold = "Poppins-Bold"
    static let poppinsSemiBold = "Poppins-SemiBold"
    static let poppinsRegular = "Poppins-Regular"
    static let athleasRegular = "Athelas-Regular"
    static let athleasBold = "Athelas-Bold"
    static let sFProText = "SF Pro Text"
    static let sFProTextMedium = "SF Pro Text-Medium"
    

}
