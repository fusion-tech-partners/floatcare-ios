//
//  MobileNumberViewController.swift
//  Floatcare
//
//  Created by BEAST   on 28/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class MobileNumberViewController: KeyBoardNotifierViewController, UITextFieldDelegate {
    private let sessionProvider = URLSessionProvider()

    @IBOutlet weak var nextClick: ButtonViolet!
    @IBOutlet weak var mobileNumberTextField: SkyFloatingLabelTextFieldWithIcon!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        view.clipsToBounds = true
        setUpNaigationBar()
        mobileNumberTextField.keyboardType = .namePhonePad
        mobileNumberTextField.delegate = self
        mobileNumberTextField.title = "Phone Number*"
        mobileNumberTextField.placeholder = "Phone Number"
        mobileNumberTextField.selectedTitleColor = #colorLiteral(red: 0.4358979464, green: 0.4750115871, blue: 0.7274814248, alpha: 1)
        mobileNumberTextField.delegate = self

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            view.addGestureRecognizer(tap)
        }
        
        @objc func dismissKeyboard() {
            view.endEditing(true)
        }

    fileprivate func setUpNaigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = UIColor.clear

        let button = UIButton(type:.custom)
        button.setImage(UIImage(named: "Back"), for: .normal)
        button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }

    @objc func backTapped() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        if ConnectionCheck.isConnectedToNetwork() {
            verifyMobileNumber()
        } else {
            ReusableAlertController.showAlert(title: "Float Care", message: "Please check your internet connection")
        }
    }
    
   func showOtpVC() {
    let registrationVC = UIStoryboard(name: "Verification", bundle: nil).instantiateViewController(withIdentifier: String(describing: OTPViewController.self)) as! OTPViewController
            registrationVC.showKeyBoardDelegate = self
        registrationVC.mobileNUmber = mobileNumberTextField.text ?? ""
              self.navigationController?.pushViewController(registrationVC, animated: true)
    }
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX) XXX-XXXX"

        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text = formattedNumber(number: newString)
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


extension MobileNumberViewController {
    private func verifyMobileNumber() {
           
        guard let number = mobileNumberTextField.text, number.isValidPhoneNumber() else {
            ReusableAlertController.showAlert(title: "Float Care", message: "Please enter valid Phone Number")
        return
        }
        DispatchQueue.main.async {
            Progress.shared.showProgressView()
        }
        let code = Countries.init().countries.filter({$0.countryCode == Locale.current.regionCode})
        
        let actulNumber = "\(code.last?.phoneExtension ?? "1")\(number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())"

        sessionProvider.request(type: MobileNumber.self, service: MobileNumberService.number(number: actulNumber)) { response in
        switch response {
        case let .success(posts):
            DispatchQueue.main.async {
            Progress.shared.hideProgressView()
            }
            DispatchQueue.main.async {
                self.showAlert(posts.message, navigate: true)
            }
            
            // Fill OTP
            MobileNumberModel.sharedInstance.otp = posts.data.otp
            MobileNumberModel.sharedInstance.mobileNumber = posts.data.phoneNumber

        case let .failure(error):
            
            DispatchQueue.main.async {
            Progress.shared.hideProgressView()
            }
            
            if error == .irrelevent {
                DispatchQueue.main.async {
                    self.showAlert("Please enter valid Phone number.", navigate: false)
                }
            } else {
                self.showAlert("Somethig went wrong", navigate: false)
            }
        }
    }
}
    
    fileprivate func showAlert(_ message: String, navigate: Bool) {
        let alert = UIAlertController(title: "Float Care", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) {
               UIAlertAction in
            if navigate {
                self.showOtpVC()
            }
           }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension RangeReplaceableCollection where Self: StringProtocol {
    mutating func removeAllNonNumeric() {
        removeAll { !("0"..."9" ~= $0) }
    }
}

extension MobileNumberViewController: showKeyBoardDelegate {
    func showKeyBoard(_ resetStatus: Bool) {
        mobileNumberTextField.becomeFirstResponder()
        resetMobileNumberField(resetStatus)
    }
    
    func resetMobileNumberField(_ status: Bool) {
        if status {
            MobileNumberModel.sharedInstance.mobileNumber = nil
            mobileNumberTextField.text = nil
        }
    }
}
