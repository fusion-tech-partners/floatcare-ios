//
//  ViewController.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 21/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

protocol showKeyBoardDelegate: class {
    func showKeyBoard(_ resetStatus: Bool)
}

class OTPViewController: KeyBoardNotifierViewController {
    var mobileNUmber = ""
    @IBOutlet weak var mobileNumberLabel: LabelGray!
    private let sessionProvider = URLSessionProvider()
    weak var showKeyBoardDelegate: showKeyBoardDelegate!
    @IBOutlet weak var headingLabel: LabelGray!
    @IBOutlet weak var tfOne: OTPField!
    @IBOutlet weak var tfTwo: OTPField!
    @IBOutlet weak var tfThree: OTPField!
    @IBOutlet weak var tfFour: OTPField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var nextButton: ButtonViolet!
    
    var otpStringArray = Array.init(repeating: "*", count: 4)
    var tfsArray = [UITextField]()
    
    
    @IBAction func gotoMobileNumberVC(_ sender: Any) {
        showKeyBoardDelegate?.showKeyBoard(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendOTP(_ sender: Any) {
        otpStringArray = Array.init(repeating: "*", count: 4)
        tfsArray.forEach{
            $0.text = ""
        }
        
        if ConnectionCheck.isConnectedToNetwork() {
            verifyMobileNumber()
        } else {
            ReusableAlertController.showAlert(title: "Float Care", message: "Please check your internet connection")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.clipsToBounds = true
        if mobileNUmber != "" {
            mobileNumberLabel.text = mobileNUmber
        }
        
        // Do any additional setup after loading the view.
        tfsArray = [tfOne,tfTwo,tfThree,tfFour]
        tfsArray[0].becomeFirstResponder()
        tfsArray.forEach{
            $0.delegate = self
            $0.keyboardType = .numberPad
            $0.addTarget(self, action: #selector(didenterOtp(sender:)), for: .editingChanged)
            $0.addTarget(self, action: #selector(didenterOtp(sender:)), for: .valueChanged)
//            $0.tintColor = .clear
        }
        tfOne.becomeFirstResponder()
        setUpNaigationBar()
    }
    fileprivate func setUpNaigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = UIColor.clear
        
        let button = UIButton(type:.custom)
        button.setImage(UIImage(named: "Back"), for: .normal)
        button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func nextClick(_ sender: Any) {
        let otp = otpStringArray.joined(separator: "")
        if otp.contains("*") || otp != MobileNumberModel.sharedInstance.otp {
            errorLabel.isHidden = false
            tfsArray.forEach{
                $0.layer.borderColor = UIColor().errorRed.cgColor
            }
        } else {
            // Call GRAPHQL SERVICE
            if ConnectionCheck.isConnectedToNetwork() {
                registerUser()
            } else {
                ReusableAlertController.showAlert(title: "Float Care", message: "Please check your internet connection")
            }
        }
    }
    
    @objc func didenterOtp(sender:UITextField){
        if let str = sender.text,!(sender.text!.isEmpty){
            let currentIndex = tfsArray.firstIndex(of: sender)
            otpStringArray[currentIndex!] = str
            if currentIndex! < 3 {
                tfsArray[currentIndex! + 1].becomeFirstResponder()
            }else{
                tfsArray.last?.resignFirstResponder()
            }
        }else{
            let currentIndex = tfsArray.firstIndex(of: sender)
            otpStringArray[currentIndex!] = ""
            if currentIndex! > 0 {
                tfsArray[currentIndex! - 1].becomeFirstResponder()
            }
        }
    }
    
}

extension OTPViewController : UITextFieldDelegate{
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.text = ""
        let currentIndex = self.tfsArray.firstIndex(of: textField)
        self.otpStringArray[currentIndex!] = "*"
        tfsArray.forEach{$0.layer.borderColor = UIColor().buttonViolet.cgColor}
        errorLabel.isHidden = true
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

extension OTPViewController {
    
    func registerUser() {
        DispatchQueue.main.async {
            Progress.shared.showProgressView()
        }
        let registration = RegistrationModel.sharedInstance
        let login = LoginModel.sharedInstance
        let mobile = MobileNumberModel.sharedInstance
        
        guard  let email = login.email else {
            return
        }
        
        guard  let password = login.password else {
            return
        }
        
        guard let mobileNumber = mobile.mobileNumber else {
            return
        }
        
        let mutation = CreateUserBasicInformationMutation.init(firstName: registration.firstName ?? "", organizationId: "99999999", lastName: registration.lastName ?? "", email: email, phoneNumber: mobileNumber, password: password, dateOfBirth: String.convertDateFormater(registration.birthday ?? ""), gender: registration.gender ?? "", profilePhoto:registration.profileImage?.toStringCompressed() ?? "" ,userTypeId: "364723", staffUserCoreTypeId: String(describing: registration.staffUserCoreTypeId ?? 0))
        
        ApolloManager.shared.client.perform(mutation: mutation) { (result) in
            guard let data = try? result.get().data else {
                return
            }
            DispatchQueue.main.async {
                Progress.shared.hideProgressView()
            }
            if data.createUserBasicInformation != nil  {
                
                self.callLoginAPI()
                self.showAlert("Done with signup successfully", isFromRegistrationAPI: true)
            } else {
                ReusableAlertController.showAlert(title: "Float Care", message: "Something went wrong")
            }
        }
    }
    
    fileprivate func showAlert(_ message: String, isFromRegistrationAPI: Bool) {
        let alert = UIAlertController(title: "Float Care", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            if isFromRegistrationAPI {
                self.showBioAuthViewController()
            }
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showBioAuthViewController() {
        let registrationVC = UIStoryboard(name: "BioAuth", bundle: nil).instantiateViewController(withIdentifier: String(describing: BioAuthViewController.self)) as! BioAuthViewController
        registrationVC.bioVCDelegate = self
        self.present(registrationVC, animated: true, completion: nil)
    }
}

extension OTPViewController {
    
    private func verifyMobileNumber() {
        DispatchQueue.main.async {
            Progress.shared.showProgressView()
        }
        
        guard let number = MobileNumberModel.sharedInstance.mobileNumber else {
            return
        }
        
        let actulNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        sessionProvider.request(type: MobileNumber.self, service: MobileNumberService.number(number: actulNumber)) { response in
            switch response {
            case let .success(posts):
                DispatchQueue.main.async {
                    Progress.shared.hideProgressView()
                }
                DispatchQueue.main.async {
                    ReusableAlertController.showAlert(title: "Float Care", message: posts.message)
                }
                
                // Fill OTP
                MobileNumberModel.sharedInstance.otp = posts.data.otp
                MobileNumberModel.sharedInstance.mobileNumber = posts.data.phoneNumber
                
            case let .failure(error):
                DispatchQueue.main.async {
                    Progress.shared.hideProgressView()
                }
                self.showAlert("Somethig went wrong", isFromRegistrationAPI: false)
            }
        }
    }
}

extension OTPViewController {
    fileprivate func callLoginAPI() {
        Progress.shared.showProgressView()
        guard let email = LoginModel.sharedInstance.email else {
            return
        }
        
        guard let password = LoginModel.sharedInstance.password else {
            return
        }
        
        sessionProvider.request(type: LoginResponse.self, service: LoginService.login(email: email, password: password)) { response in
            switch response {
            case let .success(posts):
                Profile.shared.loginResponse = posts
                Progress.shared.hideProgressView()
                DispatchQueue.main.async {
                    if posts.status == 403 {
                        //                        self.showAlert(posts.message, navigation: .none)
                    } else {
                        //                        self.showAlert(posts.message, navigation: .login)
                        Profile.shared.loginResponse = posts
                    }
                }
            case let .failure(error):
                
                DispatchQueue.main.async {
                    Progress.shared.hideProgressView()
                }
                if error == .irrelevent {
                    DispatchQueue.main.async {
                        //                        self.showAlert("Please enter valid credentials.", navigation: .none)
                    }
                } else {
                    DispatchQueue.main.async {
                        //                        self.showAlert("Somethig went wrong", navigation: .none)
                    }
                }
                
            }
        }
    }
}

extension OTPViewController : BioVCDefinable {
    func showNextScreen() {
//        let registrationVC = UIStoryboard(name: "Verification", bundle: nil).instantiateViewController(withIdentifier: String(describing: WelcomeViewController.self)) as! WelcomeViewController
//        self.navigationController?.pushViewController(registrationVC, animated: true)
        let wsVC = UIStoryboard(name: "WorkSpace", bundle: nil).instantiateViewController(withIdentifier: String(describing: WorkSpaceInviteCodeVC.self)) as! WorkSpaceInviteCodeVC
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.pushViewController(wsVC, animated: true)
    }
}

