//
//  WelcomeViewController.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 22/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var floatButton: ButtonViolet!
    @IBOutlet weak var descriptionLabel: LabelGray!
    @IBOutlet weak var headingLabel: LabelHeader!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headingLabel.text = "Welcome \n to FloatCare"
        descriptionLabel.text = """
        Think of your FloatCare profile as
        a wayto manage your medical
        career & balance a healthy lifestyle.
        """
        
        view.clipsToBounds = true
        setUpNaigationBar()
    }
    
    fileprivate func setUpNaigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = UIColor.clear

        let button = UIButton(type:.custom)
        button.setImage(UIImage(named: "Back"), for: .normal)
        button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }

    @objc func backTapped() {
        self.navigationController?.popToRootViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func floatAction(_ sender: Any) {
          let schedulesViewController = UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: String(describing: SchedulesViewController.self)) as! SchedulesViewController
          self.navigationController?.setNavigationBarHidden(true, animated: true)
          self.navigationController?.pushViewController(schedulesViewController, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
