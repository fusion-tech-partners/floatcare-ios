//
//  AvailabilityHeaderCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 18/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class AvailabilityHeaderCell: UITableViewCell {

    @IBOutlet weak var availabilitySwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.availabilitySwitch.onTintColor = .primaryColor
    }

    
    @IBAction func availabilitySwitchAction(_ sender: UISwitch) {
        AvailabilityVCVM.shared.switchOpts = sender.isOn ? true : false
        AvailabilityVCVM.shared.parentVCRef?.availabilityTable.reloadData()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
