//
//  AvailaibilityTypeVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 18/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
class AvailabilityTypeVM {
    static let shared = AvailabilityTypeVM()
    var seletedType = "Availability Type 1"
}

class AvailaibilityTypeVC: UIViewController {
    var vmObj = AvailabilityTypeVM.shared
    @IBOutlet weak var availabilityTable: UITableView!
    @IBOutlet weak var titleLabel: LabelHeader!
    @IBOutlet weak var backgroundView: UIView!
    var currentType = ""
    var typeArray = ["Unavailable","Day Shift","Night Shift","Swing Shift"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    func setupUI() {
        backgroundView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        titleLabel.font = UIFont(name: Fonts.athleasRegular, size: 36)
        titleLabel.textColor = UIColor().rgb(r: 72, g: 93, b: 186, k: 1)//
        currentType = vmObj.seletedType
    }
    @IBAction func backClick(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
    }
    @IBAction func saveClick(_ sender: Any) {
        
        vmObj.seletedType = currentType
        self.navigationController?.popViewController(animated: true)
    }
    /*
     
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AvailaibilityTypeVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        typeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvailabilitytypeCell", for: indexPath) as! AvailabilitytypeCell
        if currentType == typeArray[indexPath.row]{
            cell.accessoryBtn.setImage(UIImage.init(named: "Tick"), for: .normal)
        }else{
            cell.accessoryBtn.setImage(nil, for: .normal)
        }
        cell.titleLbl?.text = typeArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentType = typeArray[indexPath.row]
        availabilityTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
}
