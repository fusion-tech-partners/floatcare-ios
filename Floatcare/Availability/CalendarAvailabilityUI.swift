//
//  CalendarAvailabilityUI.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 18/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
enum AvailabilitySections : CaseIterable{
    case sec1
    case sec2
    case sec3
}
enum SelectDatesUIEnum : String {
    case startD = "Start Date"
    case endD = "End Date"
    case startT = "Start Time"
    case endT = "End Time"
}

class CalendarAvailabilityUI  {
    static let shared = CalendarAvailabilityUI()
    private var tv : UITableView?
    private var index : IndexPath?
    let sections : [AvailabilitySections] = [.sec1,.sec2,.sec3]
    var datesRows : [SelectDatesUIEnum] = [.startD,.startT,.endD,.endT]
    func noOfrows(forSec:AvailabilitySections) -> Int {
        switch forSec {
        case .sec1 :
            return 1
        case .sec2 :
            return 1
        case .sec3 :
            if AvailabilityVCVM.shared.switchOpts == false{
              datesRows = [.startD,.startT,.endD,.endT]
            }else{
              datesRows = [.startD,.endD]
            }
            return datesRows.count
        }
    }
    func rowsHeight(forSec:AvailabilitySections) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func cellForRow(sec:AvailabilitySections,tableView:UITableView,indexPath:IndexPath) -> UITableViewCell {
        self.tv = tableView
        self.index = indexPath
        switch sec {
        case .sec1 ,.sec3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AvailabilityCell", for: indexPath) as! AvailabilityCell
            cell.imgVw.image = getImage(indexP: indexPath)
            cell.accessoryBtn.isHidden = (indexPath.section == 0 && indexPath.row == 0) ? false : true
            cell.nameTF.text = getTitleS(indexP: indexPath)
            cell.descriptionLabel.text = getDescription(indexP: indexPath)
            return cell
        case .sec2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AvailabilityHeaderCell", for: indexPath) as! AvailabilityHeaderCell
            return cell

        }
    }
    
    func getImage(indexP:IndexPath)-> UIImage  {
        if indexP.section == 0{
            switch indexP.row {
//            case 0: return #imageLiteral(resourceName: "Time")
            case 0: return UIImage.init(named: "myWork")!
            default: return UIImage()
            }
        }
        if indexP.section == 2{
            
            switch datesRows[indexP.row] {
            case .startD:return #imageLiteral(resourceName: "Schedule")
            case .startT:return #imageLiteral(resourceName: "Time")
            case .endD:return #imageLiteral(resourceName: "Schedule")
            case .endT:return #imageLiteral(resourceName: "Time")
            default: return UIImage()
            }
        }
        return UIImage()
    }
    
    func getTitleS(indexP:IndexPath)-> String  {
        if indexP.section == 0{
            switch indexP.row {
//                case 0: return AvailabilityTypeVM.shared.seletedType
                case 0: return Profile.shared.userWorkSpaceDetails?.getWorkspaceByUserId?.last?.organizationName ?? ""
                default: ""
            }
        }
        if indexP.section == 2{
            switch datesRows[indexP.row] {
            case .startD: return AvailabilityVCVM.shared.startDate.covertDateToReadableFormat
            case .startT: return AvailabilityVCVM.shared.startTime
            case .endD: return AvailabilityVCVM.shared.endDate.covertDateToReadableFormat
            case .endT: return AvailabilityVCVM.shared.endTime
            default: ""
            }
        }
        return ""
    }
    func getDescription(indexP:IndexPath)-> String  {
        if indexP.section == 0{
            switch indexP.row {
//                case 0: return "Availability Type"
                case 0: return "Worksite"
                default: ""
            }
        }
        if indexP.section == 2{
            return datesRows[indexP.row].rawValue
        }
        return ""
    }
    
    
}
