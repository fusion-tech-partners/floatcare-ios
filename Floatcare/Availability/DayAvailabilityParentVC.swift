//
//  DayAvailabilityParentVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 18/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit


class DayScreenVM {
    static let shared = DayScreenVM()
    var shiftType = ""
    var department = ""
    var time = ""
    var image : UIImage?
    var count = 0
    
    func fetchData(dayObj : DayScheduleModel)  {
        self.shiftType = dayObj.shiftTypeName ?? ""
        self.department = "\(dayObj.worksiteName ?? "") | \(dayObj.departmentName ?? "")"
        self.time = "\(dayObj.startTime ?? "") - \(dayObj.endTime ?? "")"
        if dayObj.shiftTypeName == "Day Shift"{
            self.image = #imageLiteral(resourceName: "ic_DayShift")
        }else if dayObj.shiftTypeName == "Night Shift"{
            self.image = #imageLiteral(resourceName: "ic_SwingShift")
        }
        self.count = 1
    }
        
}
class DayAvailabilityParentVC: UIViewController {

    @IBOutlet weak var titleLabel: LabelHeader!
    
    @IBOutlet weak var setAvalibilityBtn: ButtonViolet!
    @IBOutlet weak var emptyDescLbl: UILabel!
    @IBOutlet weak var emptyMsgLbl: LabelHeader!
    
    @IBOutlet weak var optionsBtn: UIButton!
    @IBOutlet weak var colleaugesBtn: UIButton!
    @IBOutlet weak var eventsBtn: UIButton!
    @IBOutlet weak var dayTV: UITableView!
    var todayDate = ""
    var arr = [UIButton]()
    var vmObj = DayScreenVM.shared
    var currentDayObj : DayScheduleModel?
    
    @IBOutlet weak var backgroundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arr = [eventsBtn,colleaugesBtn,optionsBtn]
        arr.forEach {
                   $0.layer.cornerRadius = 10
                   $0.backgroundColor = .clear
                   $0.addTarget(self, action: #selector(topOptionsClick(sender:)), for: .touchUpInside)
               }
        // Do any additional setup after loading the view.
        topOptionsClick(sender: eventsBtn)
        setupUI()
        fetchDayDetails()
        dayTV.isHidden = true
        
    }
    func fetchDayDetails()  {
        let query = FetchScheduleByDayQuery.init(userId: Profile.shared.userBasicInformation?.userId ?? "", selectedDate: todayDate.convertToBackendDateFormat)
        ApolloManager.shared.client.fetch(query: query) { (result) in
            print("\(result)")
            switch(result){
            case .success(let graphQLResult) :
                if let dat = graphQLResult.data?.resultMap{
                    let arr = dat["getScheduleByUserIdAndSelectedDay"] as! [Any]
                    if arr.count > 0{
                        let jsonData = try? JSONSerialization.data(withJSONObject: (arr.last!), options: .prettyPrinted)
                    let dayData = try? JSONDecoder().decode(DayScheduleModel.self, from: jsonData!)
                        self.currentDayObj = dayData
                    if dayData != nil{
                            self.vmObj.fetchData(dayObj: dayData!)
                            DispatchQueue.main.async {
                                self.dayTV.isHidden = false
                                self.dayTV.reloadData()
                                
                            }
                        }
                    }
                }
            case .failure(let error) :
                print(error.localizedDescription)
            
            }
        }
    }
    
    @objc func topOptionsClick(sender:UIButton)  {
        arr.forEach {
            $0.backgroundColor = .clear
        }
        sender.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9058823529, blue: 0.9490196078, alpha: 1)

    }
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setupUI() {
        backgroundView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        titleLabel.font = UIFont(name: Fonts.athleasRegular, size: 36)
        titleLabel.textColor = UIColor().rgb(r: 72, g: 93, b: 186, k: 1)//
        
        emptyMsgLbl.font = UIFont(name: Fonts.sFProText, size: 18)
        emptyMsgLbl.textColor = #colorLiteral(red: 0.3607843137, green: 0.3882352941, blue: 0.6705882353, alpha: 1)
        
        emptyDescLbl.font = UIFont(name: Fonts.sFProText, size: 16)
        emptyDescLbl.textColor = #colorLiteral(red: 0.5725490196, green: 0.5725490196, blue: 0.6156862745, alpha: 1)
        
        titleLabel.text = todayDate
        colleaugesBtn.isHidden = true
        optionsBtn.isHidden = true
    }
    @IBAction func availabilityClick(_ sender: Any) {
        
        self.setGeneralAvailabilty {
            
            DispatchQueue.main.async {
                
                ReusableAlertController.showAlertOk(title: "Success", message: "Now you are available for the day.") { (_) in
                    
                    self.navigationController?.popViewController(animated: true)
                }
             }
           

        }
 
    }
    
    
    func setGeneralAvailabilty(completion:(()->())?) {
        
        Progress.shared.showProgressView()
        ScheduleLayoutByUserId().GetAllScheduleLayoutByUserId(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)") { (schedules) in
            
            guard let scheduleLayoutId = schedules?.first?.scheduleLayoutId else {
                
                Progress.shared.hideProgressView()
                 completion?()
                return }
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let mutation = CreateGeneralAvailabilityMutation(userId: Profile.shared.userId, scheduleLayoutId: scheduleLayoutId, startDate: formatter.string(from: Date()), endDate: formatter.string(from: Date()), startTime: "00:00", endTime: "23:59")
             ApolloManager.shared.client.perform(mutation: mutation) { result in
                            
                Progress.shared.hideProgressView()
                            guard let user = try? result.get().data?.resultMap else {
                                
                                completion?()
                                return
                            }
                            
                            do {
                                let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                                
                                let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
                                print(convertedString)
                                
                                print(user)
                                completion?()
                            }catch {
                                
                                completion?()
                            }
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DayAvailabilityParentVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vmObj.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DayTVCell", for: indexPath) as! DayTVCell
        cell.typeLbl.text = vmObj.shiftType
        cell.deptLbl.text = vmObj.department
        cell.timeLbl.text = vmObj.time
        cell.accesoryImg.image = vmObj.image
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ShiftDetailViewController") as! ShiftDetailViewController
        vc.tvObj = self.currentDayObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
