//
//  DayScheduleModel.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 16/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

struct DayScheduleModel: Codable {
    let scheduleId : String?
    let worksiteId : String?
    let worksiteName : String?
    let departmentId : String?
    let departmentName : String?
    let userId : String?
    let shiftTypeId : String?
    let shiftTypeName : String?
    let shiftDate : String?
    let startTime : String?
    let endTime : String?
    let isOnCallRequest : Bool?
    let voluntaryLowCensus : Bool?
}
