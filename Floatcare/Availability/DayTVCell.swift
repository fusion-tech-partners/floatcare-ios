//
//  DayTVCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 16/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class DayTVCell: UITableViewCell {

    @IBOutlet weak var typeLbl: UILabel!
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var deptLbl: UILabel!
    
    @IBOutlet weak var accesoryImg: UIImageView!
    
    @IBOutlet weak var shadowView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        shadowView.layer.cornerRadius = 10
        shadowView.layer.shadowColor = #colorLiteral(red: 0.9652025261, green: 0.9577398896, blue: 1, alpha: 1)
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = .zero
        shadowView.layer.shadowRadius = 10
        shadowView.layer.shadowPath = UIBezierPath(rect: shadowView.bounds).cgPath
        shadowView.layer.shouldRasterize = true
        shadowView.layer.rasterizationScale = UIScreen.main.scale




    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
