//
//  SetAvailabilityVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 18/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit


class AvailabilityVCVM {
    static let shared = AvailabilityVCVM()
    var parentVCRef : SetAvailabilityVC?
    var switchOpts = false    
    var startDate = ""
    var startTime = ""
    var endDate = ""
    var endTime = ""
    func setDefaults()  {
        switchOpts = false
        startDate = ""
        startTime = ""
        endDate = ""
        endTime = ""
    }
}
class SetAvailabilityVC: UIViewController {
    @IBOutlet weak var availabilityTable: UITableView!
    var vmObj = AvailabilityVCVM.shared
    @IBOutlet weak var titleLabel: LabelHeader!
    @IBOutlet weak var backgroundView: UIView!
    var isStartTime = true
    override func viewDidLoad() {
        super.viewDidLoad()
        vmObj.parentVCRef = self
        // Do any additional setup after loading the view.
        setupUI()
        vmObj.setDefaults()
    }
    func openTimePicker(row:SelectDatesUIEnum) {
        isStartTime = (row == .startT) ? true : false
        let tf = UITextField.init(frame: CGRect.zero)
        self.view.addSubview(tf)
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePicker.Mode.time
        datePickerView.addTarget(self, action:#selector(setTime(sender:)), for: UIControl.Event.valueChanged)
        tf.inputView = datePickerView
        tf.becomeFirstResponder()
    }
    
    @objc func setTime(sender:UIDatePicker){
        let tf = DateFormatter.init()
        tf.dateFormat = "HH:MM"
        if isStartTime{
            vmObj.startTime = tf.string(from: sender.date)
        }else{
            vmObj.endTime = tf.string(from: sender.date)
        }
        availabilityTable.reloadData()
        
    }
    
    func setupUI() {
        backgroundView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        titleLabel.font = UIFont(name: Fonts.athleasRegular, size: 36)
        titleLabel.textColor = UIColor().rgb(r: 72, g: 93, b: 186, k: 1)//
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        availabilityTable.reloadData()
    }

    @IBAction func backClick(_ sender: Any) {
     gotoPreviousScreen()
    }
    func gotoPreviousScreen()  {
        if (self.navigationController != nil){
                 self.navigationController?.popViewController(animated: true)
             }else{
                 self.dismiss(animated: true, completion: nil)
             }
    }
    
    @IBAction func createAvailibility(_ sender: Any) {
        Progress.shared.showProgressView()
        let mutation = CreateAvailabilityMutation.init(userId: Profile.shared.userBasicInformation?.userId ?? "", startDate: vmObj.startDate, endDate: vmObj.endDate, startTime: vmObj.startTime, endTime: vmObj.endTime)
        
        ApolloManager.shared.client.perform(mutation: mutation){[weak self] (result) in
            print("\(result)")
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "FloatCare", message: "Availability Updated Successfully", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    self!.gotoPreviousScreen()
                }))
                self?.present(alert, animated: true, completion: nil)
            }
            Progress.shared.hideProgressView()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SetAvailabilityVC : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        CalendarAvailabilityUI.shared.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sec = CalendarAvailabilityUI.shared.sections[section]
        return CalendarAvailabilityUI.shared.noOfrows(forSec:sec )
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CalendarAvailabilityUI.shared.cellForRow(sec: CalendarAvailabilityUI.shared.sections[indexPath.section], tableView: tableView, indexPath: indexPath)
        
        return  cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CalendarAvailabilityUI.shared.rowsHeight(forSec: CalendarAvailabilityUI.shared.sections[indexPath.section])
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 && indexPath.section == 0{
            let vc = self.storyboard?.instantiateViewController(identifier: "AvailaibilityTypeVC") as! AvailaibilityTypeVC
            vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.section == 2 {
            switch  CalendarAvailabilityUI.shared.datesRows[indexPath.row] {
            case .startD:
                DispatchQueue.main.async {
                 let storyboard = UIStoryboard(name: "RequestTypeVC", bundle: nil)
                 let chooseDatesVC = storyboard.instantiateViewController(withIdentifier: "CRChooseDatesVC") as! CRChooseDatesVC
                              chooseDatesVC.modalPresentationStyle = .fullScreen
                            //  chooseDatesVC.requestType = self.requestType
                              chooseDatesVC.isFromEditReqScreen = false
                              chooseDatesVC.calendarNavigation = .fromSetAvailability
                        chooseDatesVC.isStartDateSelection = true
                              self.present(chooseDatesVC, animated: true, completion: nil)
                }
            case .endD:
                 DispatchQueue.main.async {
                  let storyboard = UIStoryboard(name: "RequestTypeVC", bundle: nil)
               let chooseDatesVC = storyboard.instantiateViewController(withIdentifier: "CRChooseDatesVC") as! CRChooseDatesVC
                              chooseDatesVC.modalPresentationStyle = .fullScreen
                            //  chooseDatesVC.requestType = self.requestType
                              chooseDatesVC.isFromEditReqScreen = false
                              chooseDatesVC.calendarNavigation = .fromSetAvailability
                                chooseDatesVC.isStartDateSelection = false
                              self.present(chooseDatesVC, animated: true, completion: nil)
                }
                
            case .startT,.endT:
                openTimePicker(row: CalendarAvailabilityUI.shared.datesRows[indexPath.row])
            default:
                print("")
            }
        }
    }
        
    
    
}
