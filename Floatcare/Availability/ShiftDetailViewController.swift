//
//  ShiftDetailViewController.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 16/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ShiftDetailViewController: UIViewController {
    @IBOutlet weak var hdrImg: UIImageView!
    
   
    @IBOutlet weak var hdrLabel: LabelHeader!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var optionsBtn: UIButton!
    @IBOutlet weak var dayTV: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    var tvObj : DayScheduleModel?
    var titlesArry = [String]()
    var descArray = [String]()
    var imagesArr = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setupUI() {
        backgroundView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        hdrLabel.font = UIFont(name: Fonts.athleasRegular, size: 36)
        hdrLabel.textColor = UIColor().rgb(r: 72, g: 93, b: 186, k: 1)//
        hdrLabel.textColor = #colorLiteral(red: 0.3607843137, green: 0.3882352941, blue: 0.6705882353, alpha: 1)
        
        infoBtn.layer.cornerRadius = 5
        hdrLabel.text = tvObj?.shiftTypeName ?? ""
        if tvObj?.shiftTypeName == "Day Shift"{
            hdrImg.image = #imageLiteral(resourceName: "ic_DayShift")
        }else if tvObj?.shiftTypeName == "Night Shift"{
            hdrImg.image = #imageLiteral(resourceName: "ic_SwingShift")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ShiftDetailViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShiftTVCell", for: indexPath) as! ShiftTVCell
        cell.swit.isHidden = true
        cell.descLbl.isHidden = true
        var txt = ""
        var img = UIImage()
        switch indexPath.row {
        case 0:
            txt = "\((tvObj?.startTime!)!) - \((tvObj?.endTime!)!)"
            img = #imageLiteral(resourceName: "Time")
        case 1:
            txt = "\((tvObj?.shiftDate?.covertDateToReadableFormat)!)"
            img = #imageLiteral(resourceName: "Schedule")
        case 2:
            txt = "\((tvObj?.departmentName)!)"
            cell.descLbl.isHidden = false
            cell.descLbl.text = "\((tvObj?.worksiteName)!)"
            img = #imageLiteral(resourceName: "myWork")
        case 3:
            txt = "\(Profile.shared.userBasicInformation?.staffUserCoreTypeName ?? "") "
            cell.descLbl.isHidden = false
            cell.descLbl.text = "Role"
            img = #imageLiteral(resourceName: "workExperience")
        case 4:
            txt = "\((tvObj?.shiftTypeName!)!)"
            cell.descLbl.isHidden = false
            cell.descLbl.text = "Shift Type"
            img = #imageLiteral(resourceName: "Full Time")
        case 5:
            txt = "Voluntary Low Census"
            cell.swit.setOn((tvObj?.voluntaryLowCensus!)!, animated: true)
            cell.swit.isHidden = false
            img = #imageLiteral(resourceName: "Time")
        case 6:
            txt = "Request Off Shift"
            img = #imageLiteral(resourceName: "Full Time")
        case 7:
            txt = "Swap Shift"
            img = #imageLiteral(resourceName: "swap")
        default:
            print("")
        }
        
        cell.imgVw.image = img
        cell.nameTF.text = txt
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     switch indexPath.row {
            
        case 6:
            let chooseDepartmentVC = UIStoryboard(name: "RequestTypeVC", bundle: nil).instantiateViewController(withIdentifier: "chooseDepartment") as! ChooseDepartmentVC
                chooseDepartmentVC.requestType = .callOff
                self.navigationController?.pushViewController(chooseDepartmentVC, animated: true)
                                   
                 
        case 7:
                
        let Swap = UIStoryboard(name: "RequestTypeVC", bundle: nil).instantiateViewController(withIdentifier: "SwapShiftVC") as! SwapShiftVC

                   self.navigationController?.pushViewController(Swap, animated: true)
//        let chooseDepartmentVC = UIStoryboard(name: "RequestTypeVC", bundle: nil).instantiateViewController(withIdentifier: "chooseDepartment") as! ChooseDepartmentVC
//        chooseDepartmentVC.requestType = .swap
//        self.navigationController?.pushViewController(chooseDepartmentVC, animated: true)
                        
                 
             default:
                 print("test")
                
            }
         
}
}
