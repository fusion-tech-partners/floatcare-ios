//
//  ShiftTVCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 16/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ShiftTVCell: UITableViewCell {
    @IBOutlet weak var swit: UISwitch!
    
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var nameTF: UILabel!
    @IBOutlet weak var thumbnailImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        thumbnailImg.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
