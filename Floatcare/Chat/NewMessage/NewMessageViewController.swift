//
//  NewMessageViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 27/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
protocol NewMessageViewControllerDelegate {
    
    func onSelectingUser(userId: String)
}
class NewMessageViewController: UIViewController {

    var delegate: NewMessageViewControllerDelegate?
    
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var headerContainterView : TopHeaderContainerView!
    @IBOutlet weak var tableview: UITableView!
    var searchStr: String = ""
    let viewModel: NewMessageViewModel = NewMessageViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            self.navigationController?.navigationBar.isHidden = false
//            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
        
         Progress.shared.showProgressView()
        viewModel.getStaffMembers(orgId: Profile.shared.organizationId()) { [weak self] (orgDetails) in
            
            self?.tableview.reloadData()
            
             Progress.shared.hideProgressView()
        }
        self.headerContainterView.dropDownActionBlock = {[weak self] index in
                   
                   self?.sortByIndex(index: index)
                }
                
        self.headerContainterView.searchTextChangeActionBlock = {[weak self] value in
                   
                    self?.filterContacts(text: value.lowercased())
                    //self!.getAllMessages()
            self?.viewModel.getStaffMembers(orgId: Profile.shared.organizationId()) { [weak self] (orgDetails) in
                        
                    }
            
        }
        }
        // Do any additional setup after loading the view.

    
func filterContacts(text: String) {
        
        self.searchStr = text
    
        guard text.count > 0 else {
            self.searchStr = ""
            
            self.tableview.reloadData()
            return
        }
        
    self.viewModel.collegues = self.viewModel.collegues.filter({ (message) -> Bool in
        
        return message.firstName.lowercased().contains(text.lowercased()) || message.lastName.lowercased().contains(text.lowercased())
        })
        
        self.tableview.reloadData()
        
    }

func sortByIndex(index: Int) {
    
    if index == 0 {
        self.viewModel.collegues = self.viewModel.collegues.sorted(by: {(message1, message2) -> Bool in
           // return message1.firstName > message2.firstName
            return  message1.firstName < message2.firstName
        })
    }

    self.tableview.reloadData()
    

    }
}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


extension NewMessageViewController: UITableViewDataSource ,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section != 0 else {
            
            return 1
        }
        
        return self.viewModel.collegues.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 0.0
        
           guard section != 0 else { return 0.0}
           return 66.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return nil
        
        guard section != 0 else { return nil}
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileInfoTableHeaderCell.rawValue) as! ProfileInfoTableHeaderCell
        cell.titleLabel.font = UIFont(font: .SFUIText, weight: .semibold, size: 14.0)
         cell.titleLabel.text = ""
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileSettingsTableCell.rawValue, for: indexPath) as! ProfileSettingsTableCell
            cell.isLastCell = indexPath.row == 1
            cell.isAccessoryButtonHidden = true
            switch indexPath.row {
                case 0:
                    cell.profileSettingNameLabel.text = "Find Contact"
                    cell.profileSettingDescriptionLabel.text = "Find somebody from your workspace"
                    cell.iconImageView.image = UIImage(named: "account")
//                case 1:
//                    cell.profileSettingNameLabel.text = "New Group"
//                    cell.profileSettingDescriptionLabel.text = "Create a new message group"
//                    cell.iconImageView.image = UIImage(named: "collegues")
                default:
                    break
            }
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NightColleaugeCell", for: indexPath) as! NightColleaugeCell
            let user = self.viewModel.collegues[indexPath.row]
            cell.userBasicInformation = user
            cell.empolyeeName.setTitle(user.firstName + " " + user.lastName , for: .normal)
            
            let imgUrl = amazonURL + (user.profilePhoto?.fillSpaces ?? "")
            
            if let imgStr = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                              
                cell.employeeProfilePic.sd_setImage(with: URL(string: imgStr), placeholderImage: UIImage(named: "placeholder.png"))
            }
           
            cell.onMessageButtonClicked = { [weak self] (tag)in
                
                self?.dismiss(animated: true, completion: {
                    self?.delegate?.onSelectingUser(userId: "\(cell.userBasicInformation?.userId ?? "0")")
                })
                
            }
            
            return cell
        }
        
     }
    
    
}

