//
//  NewMessageViewModel.swift
//  Floatcare
//
//  Created by OMNIADMIN on 27/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

 typealias OrgDetailsCompletion = (OragnizationDetails?) -> ()

class NewMessageViewModel {
    
    // var collegues:
    
    var collegues: [UserBasicInformation] = [UserBasicInformation]()
    
    func getStaffMembers(orgId: String, completion: @escaping OrgDetailsCompletion) {
        
        ApolloManager.shared.client.fetch(query: FindOrganizationByIdQuery(organizationId: orgId),
                                          cachePolicy: .fetchIgnoringCacheData,
                                          context: nil,
                                          queue: .main) {[weak self] result in
                                            
                                            guard let user = try? result.get().data?.resultMap else {
                                                
                                                completion(nil)
                                                return
                                            }
                                            
                                            do {
                                                let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                                                
                                                let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
//                                                print(convertedString)
                                                let decoder = JSONDecoder()
                                                let oragnizationDetails = try decoder.decode(findOrganizationById.self, from: jsonData)
                                                print(user)
                                                if let org = oragnizationDetails.findOrganizationById.first {
                                                    
                                                    self?.collegues = org.staffMembers
                                                }
                                                completion(oragnizationDetails.findOrganizationById.first)
                                            }catch  let error{
                                                
                                                print(error)
                                                completion(nil)
                                            }
        }
    }
    
}
