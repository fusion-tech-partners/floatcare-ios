//
//  BeastChat.swift
//  Floatcare
//
//  Created by BEAST on 13/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import InputBarAccessoryView

class BeastChat: UIViewController {
    var messageInputBar = InputBarAccessoryView()
    private var keyboardManager = KeyboardManager()
    @IBOutlet var tableView: UITableView!
    private var isTyping = false
    private var textTitle: String?

    private var heightKeyboard: CGFloat = 0
    private var keyboardWillShow = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)

        
//        ApolloManager.shared.getAllMessages()
        
//        configureMessageInputBar()
        // Do any additional setup after loading the view.
    }
    

   
    // MARK: - Message input bar methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func configureMessageInputBar() {

        view.addSubview(messageInputBar)

        keyboardManager.bind(inputAccessoryView: messageInputBar)
        keyboardManager.bind(to: tableView)

        messageInputBar.delegate = self

        let button = InputBarButtonItem()
        button.image = UIImage(named: "rcmessage_attach")
        button.setSize(CGSize(width: 36, height: 36), animated: false)

        button.onKeyboardSwipeGesture { item, gesture in
            if (gesture.direction == .left)     { item.inputBarAccessoryView?.setLeftStackViewWidthConstant(to: 0, animated: true)        }
            if (gesture.direction == .right) { item.inputBarAccessoryView?.setLeftStackViewWidthConstant(to: 36, animated: true)    }
        }

        button.onTouchUpInside { item in
            self.actionAttachMessage()
        }

        messageInputBar.setStackViewItems([button], forStack: .left, animated: false)

        messageInputBar.sendButton.title = nil
        messageInputBar.sendButton.image = UIImage(named: "rcmessage_send")
        messageInputBar.sendButton.setSize(CGSize(width: 36, height: 36), animated: false)

        messageInputBar.setLeftStackViewWidthConstant(to: 36, animated: false)
        messageInputBar.setRightStackViewWidthConstant(to: 36, animated: false)

        messageInputBar.inputTextView.isImagePasteEnabled = false
    }

}


extension BeastChat: UITableViewDelegate, UITableViewDataSource {

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ChatCell.self), for: indexPath) as? ChatCell {
            cell.selectionStyle = .none
            return cell
}
    return UITableViewCell()
    
}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = RCMessagesViewController(nibName: "RCMessagesView", bundle: nil)
        self.navigationController!.pushViewController(vc, animated: true)

    }
}


// MARK: - InputBarAccessoryViewDelegate
//-------------------------------------------------------------------------------------------------------------------------------------------------
extension BeastChat: InputBarAccessoryViewDelegate {

    //---------------------------------------------------------------------------------------------------------------------------------------------
    func inputBar(_ inputBar: InputBarAccessoryView, textViewTextDidChangeTo text: String) {

        if (text != "") {
            typingIndicatorUpdate()
        }
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    func inputBar(_ inputBar: InputBarAccessoryView, didChangeIntrinsicContentTo size: CGSize) {

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.scrollToBottom(animated: true)
        }
     
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {

        for component in inputBar.inputTextView.components {
            if let text = component as? String {
                actionSendMessage(text)
            }
        }
        messageInputBar.inputTextView.text = ""
        messageInputBar.invalidatePlugins()
    }
}


extension BeastChat {
    // MARK: - User actions (title)
    //---------------------------------------------------------------------------------------------------------------------------------------------
    @IBAction func actionTitle(_ sender: Any) {

        actionTitle()
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionTitle() {

    }

    // MARK: - User actions (load earlier)
    //---------------------------------------------------------------------------------------------------------------------------------------------
    @IBAction func actionLoadEarlier(_ sender: Any) {

        actionLoadEarlier()
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionLoadEarlier() {

    }

    // MARK: - User actions (bubble tap)
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionTapBubble(_ indexPath: IndexPath) {

    }

    // MARK: - User actions (avatar tap)
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionTapAvatar(_ indexPath: IndexPath) {

    }

    // MARK: - User actions (input panel)
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionAttachMessage() {

    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionSendMessage(_ text: String) {

    }
}

extension BeastChat {
    // MARK: - Typing indicator methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func typingIndicatorShow(_ typing: Bool, text: String = "typing...") {

        if (typing == true) && (isTyping == false) {
            //textTitle = labelTitle2.text
            //labelTitle2.text = text
        }
        if (typing == false) && (isTyping == true) {
            //labelTitle2.text = textTitle
        }
        isTyping = typing
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    func typingIndicatorUpdate() {

    }

}

extension BeastChat {
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func scrollToBottom(animated: Bool) {

        if (tableView.numberOfSections > 0) {
            let indexPath = IndexPath(row: 0, section: tableView.numberOfSections - 1)
            tableView.scrollToRow(at: indexPath, at: .top, animated: animated)
        }
    }

}

extension BeastChat {
    // MARK: - Keyboard methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    @objc func keyboardWillShow(_ notification: Notification?) {

        if (heightKeyboard != 0) { return }

        keyboardWillShow = true

        if let info = notification?.userInfo {
            if let duration = info[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double {
                if let keyboard = info[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + duration) {

                        if (self.keyboardWillShow) {
                            self.heightKeyboard = keyboard.size.height
                            self.layoutTableView()
                            self.scrollToBottom(animated: true)
                        }
                    }
                }
            }
        }

        UIMenuController.shared.menuItems = nil
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    @objc func keyboardWillHide(_ notification: Notification?) {

        heightKeyboard = 0
        keyboardWillShow = false

        layoutTableView()
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    func dismissKeyboard() {

        messageInputBar.inputTextView.resignFirstResponder()
    }
    
    // MARK: - Helper methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func layoutTableView() {

        let widthView    = view.frame.size.width
        let heightView    = view.frame.size.height

        let leftSafe    = view.safeAreaInsets.left
        let rightSafe    = view.safeAreaInsets.right

        let heightInput = messageInputBar.bounds.height

        let widthTable = widthView - leftSafe - rightSafe
        let heightTable = heightView - heightInput - heightKeyboard

        tableView.frame = CGRect(x: leftSafe, y: 0, width: widthTable, height: heightTable)
    }

}

