//
//  BeastChat.swift
//  Floatcare
//
//  Created by BEAST on 13/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

//-------------------------------------------------------------------------------------------------------------------------------------------------
class RCFooterLowerCell: UITableViewCell {

	private var indexPath: IndexPath!
	private var messagesView: RCMessagesViewController!

	private var labelText: UILabel!

	//---------------------------------------------------------------------------------------------------------------------------------------------
	func bindData(_ messagesView: RCMessagesViewController, at indexPath: IndexPath) {

		self.indexPath = indexPath
		self.messagesView = messagesView

		let rcmessage = messagesView.rcmessageAt(indexPath)

		backgroundColor = UIColor.clear

		if (labelText == nil) {
			labelText = UILabel()
			labelText.font = RCDefaults.footerLowerFont
			labelText.textColor = RCDefaults.footerLowerColor
			contentView.addSubview(labelText)
		}

		labelText.textAlignment = rcmessage.senderID != Profile.shared.userId ? .left : .right
		labelText.text = messagesView.textFooterLower(indexPath)
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	override func layoutSubviews() {

		super.layoutSubviews()

		let widthTable = messagesView.tableView.frame.size.width

		let width = widthTable - RCDefaults.footerLowerLeft - RCDefaults.footerLowerRight
		let height = (labelText.text != nil) ? RCDefaults.footerLowerHeight : 0

		labelText.frame = CGRect(x: RCDefaults.footerLowerLeft, y: 0, width: width, height: height)
	}

	// MARK: - Size methods
	//---------------------------------------------------------------------------------------------------------------------------------------------
	class func height(_ messagesView: RCMessagesViewController, at indexPath: IndexPath) -> CGFloat {

		return (messagesView.textFooterLower(indexPath) != nil) ? RCDefaults.footerLowerHeight : 0
	}
}
