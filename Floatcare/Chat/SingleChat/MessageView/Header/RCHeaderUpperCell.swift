//
//  BeastChat.swift
//  Floatcare
//
//  Created by BEAST on 13/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

//-------------------------------------------------------------------------------------------------------------------------------------------------
class RCHeaderUpperCell: UITableViewCell {

	private var indexPath: IndexPath!
	private var messagesView: RCMessagesViewController!

	private var labelText: UILabel!

	//---------------------------------------------------------------------------------------------------------------------------------------------
	func bindData(_ messagesView: RCMessagesViewController, at indexPath: IndexPath) {

		self.indexPath = indexPath
		self.messagesView = messagesView

		let rcmessage = messagesView.rcmessageAt(indexPath)

		backgroundColor = UIColor.clear

		if (labelText == nil) {
			labelText = UILabel()
			labelText.font = RCDefaults.headerUpperFont
			labelText.textColor = RCDefaults.headerUpperColor
			contentView.addSubview(labelText)
		}

        labelText.textAlignment = rcmessage.senderID != Profile.shared.userId ? .center : .center
		labelText.text = messagesView.textHeaderUpper(indexPath)
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	override func layoutSubviews() {

		super.layoutSubviews()

		let widthTable = messagesView.tableView.frame.size.width

		let width = widthTable - RCDefaults.headerUpperLeft - RCDefaults.headerUpperRight
		let height = (labelText.text != nil) ? RCDefaults.headerUpperHeight : 0

		labelText.frame = CGRect(x: RCDefaults.headerUpperLeft, y: 0, width: width, height: height)
	}

	// MARK: - Size methods
	//---------------------------------------------------------------------------------------------------------------------------------------------
	class func height(_ messagesView: RCMessagesViewController, at indexPath: IndexPath) -> CGFloat {

		return (messagesView.textHeaderUpper(indexPath) != nil) ? RCDefaults.headerUpperHeight : 0
	}
}
