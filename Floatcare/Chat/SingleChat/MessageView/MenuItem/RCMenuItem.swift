//
//  BeastChat.swift
//  Floatcare
//
//  Created by BEAST on 13/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

//-------------------------------------------------------------------------------------------------------------------------------------------------
class RCMenuItem: UIMenuItem {

	var indexPath: IndexPath?

	//---------------------------------------------------------------------------------------------------------------------------------------------
	class func indexPath(_ menuController: UIMenuController) -> IndexPath? {

		let menuItem = menuController.menuItems?.first as? RCMenuItem
		return menuItem?.indexPath
	}
}
