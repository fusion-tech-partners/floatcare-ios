//
//  MessageConstants.swift
//  Floatcare
//
//  Created by BEAST on 16/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

//-------------------------------------------------------------------------------------------------------------------------------------------------
let ONESIGNAL_APPID  =                       "277d0aab-5925-475f-ba99-4af140776900"
//-------------------------------------------------------------------------------------------------------------------------------------------------
let SINCH_HOST     =   "sandbox.sinch.com"
let SINCH_KEY      =                      "12eb4441-f90b-43f8-a0ed-3c3ff02e1c12"
let        SINCH_SECRET                        = "LwwW9qmV40q1jBrEldaemw=="
//-------------------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------------------
let        DEFAULT_TAB               =             0
let        DEFAULT_COUNTRY            =            188
//---------------------------------------------------------------------------------
let        VIDEO_LENGTH                =        5
//---------------------------------------------------------------------------------
let        MEDIA_PHOTO                  =          1
let        MEDIA_VIDEO                   =         2
let        MEDIA_AUDIO                   =        3
//---------------------------------------------------------------------------------
let        NETWORK_MANUAL                 =       1
let        NETWORK_WIFI                    =    2
let        NETWORK_ALL                      =      3
//---------------------------------------------------------------------------------
let        KEEPMEDIA_WEEK                    =    1
let        KEEPMEDIA_MONTH                    =    2
let        KEEPMEDIA_FOREVER                   = 3
//---------------------------------------------------------------------------------
let        MESSAGE_TEXT                        = "text"
let        MESSAGE_EMOJI                        = "emoji"
let        MESSAGE_PHOTO                        = "photo"
let        MESSAGE_VIDEO                        = "video"
let        MESSAGE_AUDIO                        = "audio"
let        MESSAGE_LOCATION                    = "location"
//---------------------------------------------------------------------------------
let        STATUS_QUEUED                        = "Queued"
let        STATUS_FAILED                        = "Failed"
let        STATUS_SENT                            = "Sent"
let        STATUS_READ                            = "Read"
//---------------------------------------------------------------------------------
let        MEDIASTATUS_UNKNOWN            =        0
let        MEDIASTATUS_LOADING             =       1
let        MEDIASTATUS_MANUAL               =     2
let        MEDIASTATUS_SUCCEED               =     3
//---------------------------------------------------------------------------------
let        AUDIOSTATUS_STOPPED                =    1
let        AUDIOSTATUS_PLAYING                 =   2
//---------------------------------------------------------------------------------
let        LOGIN_EMAIL                            = "Email"
let        LOGIN_PHONE                            = "Phone"
//---------------------------------------------------------------------------------
let        TEXT_SHARE_APP                        = "Check out https://related.chat"
//-------------------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------------------
let        NOTIFICATION_APP_STARTED            = "NotificationAppStarted"
let        NOTIFICATION_USER_LOGGED_IN            = "NotificationUserLoggedIn"
let        NOTIFICATION_USER_LOGGED_OUT        = "NotificationUserLoggedOut"
//---------------------------------------------------------------------------------
let        NOTIFICATION_CLEANUP_CHATVIEW        = "NotificationCleanupChatView"
//-------------------------------------------------------------------------------------------------------------------------------------------------
