//
//  BeastChat.swift
//  Floatcare
//
//  Created by BEAST on 13/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

//-------------------------------------------------------------------------------------------------------------------------------------------------
class RCMessageCell: UITableViewCell {

	var indexPath: IndexPath!
	var messagesView: RCMessagesViewController!

	var viewBubble: UIImageView!

    var imageAvatar: UIImageView!
    var labelAvatar: UILabel!

    var timeLabel: UILabel!

	//---------------------------------------------------------------------------------------------------------------------------------------------
	func bindData(_ messagesView: RCMessagesViewController, at indexPath: IndexPath) {

		self.indexPath = indexPath
		self.messagesView = messagesView

		backgroundColor = UIColor.clear

		if (viewBubble == nil) {
			viewBubble = UIImageView()
			viewBubble.layer.cornerRadius = RCDefaults.bubbleRadius
			contentView.addSubview(viewBubble)
			bubbleGestureRecognizer()
		}

		if (imageAvatar == nil) {
			imageAvatar = UIImageView()
			imageAvatar.layer.masksToBounds = true
			imageAvatar.layer.cornerRadius = RCDefaults.avatarDiameter / 2
			imageAvatar.backgroundColor = RCDefaults.avatarBackColor
			imageAvatar.isUserInteractionEnabled = true
			contentView.addSubview(imageAvatar)
			avatarGestureRecognizer()
		}
		imageAvatar.image = messagesView.avatarImage(indexPath)

		if (labelAvatar == nil) {
			labelAvatar = UILabel()
			labelAvatar.font = RCDefaults.avatarFont
			labelAvatar.textColor = RCDefaults.avatarTextColor
			labelAvatar.textAlignment = .center
			contentView.addSubview(labelAvatar)
		}
		labelAvatar.text = (imageAvatar.image == nil) ? messagesView.avatarInitials(indexPath) : nil
        
        
        if (timeLabel == nil) {
            timeLabel = UILabel()
            timeLabel.font = UIFont.systemFont(ofSize: 8)
            timeLabel.isUserInteractionEnabled = false
            timeLabel.backgroundColor = .clear
            contentView.addSubview(timeLabel)
        }
        
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	func layoutSubviews(_ size: CGSize) {

		super.layoutSubviews()

		let rcmessage = messagesView.rcmessageAt(indexPath)

		let widthTable = messagesView.tableView.frame.size.width

        let xBubble = rcmessage.senderID != Profile.shared.userId ? RCDefaults.bubbleMarginLeft : (widthTable - RCDefaults.bubbleMarginRight - size.width)
		viewBubble.frame = CGRect(x: xBubble, y: 0, width: size.width, height: size.height)

		let diameter = RCDefaults.avatarDiameter
		let xAvatar = rcmessage.senderID != Profile.shared.userId ? RCDefaults.avatarMarginLeft : (widthTable - RCDefaults.avatarMarginRight - diameter)
		imageAvatar.frame = CGRect(x: xAvatar, y: size.height - diameter, width: diameter, height: diameter)
		labelAvatar.frame = CGRect(x: xAvatar, y: size.height - diameter, width: diameter, height: diameter)
        
        let xTimeStampLable = rcmessage.senderID != Profile.shared.userId ? RCDefaults.bubbleMarginLeft : (widthTable - 80 )
        
        timeLabel.frame = CGRect(x: xTimeStampLable, y: viewBubble.frame.size.height - 10, width: size.width, height: size.height)
        timeLabel.text = rcmessage.getTimeStampValue()
        
    }

	// MARK: - Gesture recognizer methods
	//---------------------------------------------------------------------------------------------------------------------------------------------
	func bubbleGestureRecognizer() {

		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionTapBubble))
		viewBubble.addGestureRecognizer(tapGesture)
		tapGesture.cancelsTouchesInView = false

		let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(actionLongBubble(_:)))
		viewBubble.addGestureRecognizer(longGesture)
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	func avatarGestureRecognizer() {

		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionTapAvatar))
		imageAvatar.addGestureRecognizer(tapGesture)
		tapGesture.cancelsTouchesInView = false
	}

	// MARK: - User actions
	//---------------------------------------------------------------------------------------------------------------------------------------------
	@objc func actionTapBubble() {

		messagesView.dismissKeyboard()
		messagesView.actionTapBubble(indexPath)
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	@objc func actionTapAvatar() {

		messagesView.dismissKeyboard()
		messagesView.actionTapAvatar(indexPath)
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	@objc func actionLongBubble(_ gestureRecognizer: UILongPressGestureRecognizer) {

		switch gestureRecognizer.state {
			case .began:
				actionMenu()
			case .changed:
				break
			case .ended:
				break
			case .possible:
				break
			case .cancelled:
				break
			case .failed:
				break
			default:
				break
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	func actionMenu() {

//		if (messagesView.messageInputBar.inputTextView.isFirstResponder == false) {
//			let menuController = UIMenuController.shared
//			menuController.menuItems = messagesView.menuItems(indexPath)
//			menuController.showMenu(from: contentView, rect: viewBubble.frame)
//		} else {
//			messagesView.messageInputBar.inputTextView.resignFirstResponder()
//		}
	}
}
