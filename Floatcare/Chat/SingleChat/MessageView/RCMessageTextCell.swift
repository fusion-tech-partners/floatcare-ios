//
//  BeastChat.swift
//  Floatcare
//
//  Created by BEAST on 13/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

//-------------------------------------------------------------------------------------------------------------------------------------------------
class RCMessageTextCell: RCMessageCell {

	private var textView: UITextView!
    

	//---------------------------------------------------------------------------------------------------------------------------------------------
	override func bindData(_ messagesView: RCMessagesViewController, at indexPath: IndexPath) {

		super.bindData(messagesView, at: indexPath)

		let rcmessage = messagesView.rcmessageAt(indexPath)

		viewBubble.backgroundColor = rcmessage.senderID != Profile.shared.userId ? RCDefaults.textBubbleColorIncoming : RCDefaults.textBubbleColorOutgoing

		if (textView == nil) {
			textView = UITextView()
            textView.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
			textView.isEditable = false
			textView.isSelectable = false
			textView.isScrollEnabled = false
			textView.isUserInteractionEnabled = false
			textView.backgroundColor = UIColor.clear
			textView.textContainer.lineFragmentPadding = 0
			textView.textContainerInset = RCDefaults.textInset
			viewBubble.addSubview(textView)
            
            
		}
        
        rcmessage.senderID != Profile.shared.userId ? viewBubble.roundCorners([ .layerMinXMinYCorner,.layerMaxXMaxYCorner,.layerMaxXMinYCorner], radius: 17.0) : viewBubble.roundCorners([ .layerMinXMinYCorner,.layerMinXMaxYCorner,.layerMaxXMinYCorner], radius: 17.0)

        textView.textColor = rcmessage.senderID != Profile.shared.userId
        ? RCDefaults.textTextColorIncoming : RCDefaults.textTextColorOutgoing

		textView.text = rcmessage.text
        
        if let imgStr = (amazonURL + (rcmessage.profilePhoto.fillSpaces)).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                   
            super.imageAvatar.sd_setImage(with: URL(string: imgStr), placeholderImage: UIImage(named: "placeholder.png"))
        }
 	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	override func layoutSubviews() {

		let size = RCMessageTextCell.size(messagesView, at: indexPath)

		super.layoutSubviews(size)

		textView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
	}

	// MARK: - Size methods
	//---------------------------------------------------------------------------------------------------------------------------------------------
	class func height(_ messagesView: RCMessagesViewController, at indexPath: IndexPath) -> CGFloat {

		let size = self.size(messagesView, at: indexPath)
        return size.height + 30
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	class func size(_ messagesView: RCMessagesViewController, at indexPath: IndexPath) -> CGSize {

		let rcmessage = messagesView.rcmessageAt(indexPath)

		let widthTable = messagesView.tableView.frame.size.width

		let maxwidth = (0.6 * widthTable) - RCDefaults.textInsetLeft - RCDefaults.textInsetRight

		let rect = rcmessage.text.boundingRect(with: CGSize(width: maxwidth, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: RCDefaults.textFont], context: nil)

		let width = rect.size.width + RCDefaults.textInsetLeft + RCDefaults.textInsetRight
		let height = rect.size.height + RCDefaults.textInsetTop + RCDefaults.textInsetBottom

		return CGSize(width: CGFloat.maximum(width, RCDefaults.textBubbleWidthMin), height: CGFloat.maximum(height, RCDefaults.textBubbleHeightMin))
	}
}
