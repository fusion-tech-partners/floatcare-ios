//
//  BeastChat.swift
//  Floatcare
//
//  Created by BEAST on 13/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import QuartzCore
import GrowingTextView
import IQKeyboardManagerSwift

protocol RCMessagesViewControllerDelegate {
    
    func navigateToChatList()
}
//-------------------------------------------------------------------------------------------------------------------------------------------------
class RCMessagesViewController: UIViewController {
    
    var recverid: String! {
        
        didSet {
            
            self.allMessages.removeAll()
            self.tableView.reloadData()

            Progress.shared.showProgressView()
            FetchUserInfo().getUserInfo(userId: Int(self.recverid) ?? 0) { [weak self] (userInfo) in
                
                self?.updateuserDetails(userBasicInformation: userInfo)
                Progress.shared.hideProgressView()
            }
            
            self.reloadConversations()
            self.getReceiverDetails()
        }
    }
    var receiverFCMToken : String?
    var recverName: String?
    
    @IBOutlet weak var navigationBar: NavigationBar!
    
    @IBOutlet var sendBtn: UIButton!

    @IBOutlet var inputTextView: CornorShadowView!
    @IBOutlet var viewTitle: UIView!
    @IBOutlet var labelTitle1: UILabel!
    @IBOutlet var labelTitle2: UILabel!
    @IBOutlet var buttonTitle: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var viewLoadEarlier: UIView!
    var allMessages = [DisplayAllMessagesOfUser]()
    
    private var isTyping = false
    private var textTitle: String?
    
    private var heightKeyboard: CGFloat = 0
    private var keyboardWillShow = false
    
    var receiverUserInfo: UserBasicInformation?
    
    var delegate: RCMessagesViewControllerDelegate?
    
    var hasTopNotch: Bool {
        if #available(iOS 13.0,  *) {
            return UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets.top ?? 0 > 20
        }else{
         return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }

     }
    
    @IBOutlet weak var messageTextView: GrowingTextView!
    //---------------------------------------------------------------------------------------------------------------------------------------------
    convenience init() {
        
        self.init(nibName: "RCMessagesView", bundle: nil)
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.sendBtn.setImage(UIImage(named: "ic_swipe_next_active")?.withTintColor(.lightGray), for: .normal)
        
        if hasTopNotch {
            
             self.bottomConstraint.constant = 20.0
        } else {
            
            self.bottomConstraint.constant  = 5.0
        }
       
        
        self.inputTextView.layer.cornerRadius = 0.0
        
        self.messageTextView.layer.borderColor = UIColor.primaryColor.cgColor
        self.messageTextView.layer.borderWidth = 1.0
        self.messageTextView.layer.cornerRadius = 5.0
        
        self.messageTextView.delegate = self
        
        IQKeyboardManager.shared.enable = false
         IQKeyboardManager.shared.enableAutoToolbar = false
        
//        self.navigationBar.LeftButtonActionBlock = { sender in
//
//            self.delegate?.navigateToChatList()
//        }
        
        self.labelTitle1.text = self.recverName
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(RCHeaderUpperCell.self, forCellReuseIdentifier: "RCHeaderUpperCell")
        tableView.register(RCHeaderLowerCell.self, forCellReuseIdentifier: "RCHeaderLowerCell")
        tableView.register(RCMessageTextCell.self, forCellReuseIdentifier: "RCMessageTextCell")
        /*tableView.register(RCMessageEmojiCell.self, forCellReuseIdentifier: "RCMessageEmojiCell")
         tableView.register(RCMessagePhotoCell.self, forCellReuseIdentifier: "RCMessagePhotoCell")
         tableView.register(RCMessageVideoCell.self, forCellReuseIdentifier: "RCMessageVideoCell")
         tableView.register(RCMessageAudioCell.self, forCellReuseIdentifier: "RCMessageAudioCell")
         tableView.register(RCMessageLocationCell.self, forCellReuseIdentifier: "RCMessageLocationCell")*/
        
        tableView.register(RCFooterUpperCell.self, forCellReuseIdentifier: "RCFooterUpperCell")
        tableView.register(RCFooterLowerCell.self, forCellReuseIdentifier: "RCFooterLowerCell")
        
        //		tableView.tableHeaderView = viewLoadEarlier
        
        // configureMessageInputBar()
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showUserProfile))
        self.labelTitle1.addGestureRecognizer(gesture)
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(dismisskeyBoard))
        self.tableView.addGestureRecognizer(tapgesture)
        
        self.labelTitle1.isUserInteractionEnabled = true
       // self.navigationBar.rightButton.setImage(UIImage(named: "Block user"), for: .normal)
        
        Progress.shared.showProgressView()
        if let id = self.recverid {
            FetchUserInfo().getUserInfo(userId: Int(id) ?? 0) { [weak self] (userInfo) in
                
                self?.updateuserDetails(userBasicInformation: userInfo)
                Progress.shared.hideProgressView()
            }
        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(reloadConversations), name: NSNotification.Name(rawValue: "refreshChat"), object: nil)
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
             
        self.delegate?.navigateToChatList()
            //self.dismiss(animated: true, completion: nil)
    }
    
    @objc func reloadConversations() {
        
        guard let rId = self.recverid else{ return }
        
        ApolloManager.shared.getAllMessagesOfReceiever(receiverId: rId) { [weak self] (messages) in
                      
                      if messages.count > self?.allMessages.count ?? 0 {
                          DispatchQueue.main.async {
                              
                              self?.allMessages.removeAll()
                              self?.allMessages.append(contentsOf: messages)
                              self?.tableView.reloadData()
                              self?.scrollToBottom()
                          }
                         
                      }
                  }
    }
    
    func getReceiverDetails() {
        ApolloManager.shared.client.fetch(query: FindUsersByIdQuery(userId:self.recverid),
                     cachePolicy: .fetchIgnoringCacheData,
                     context: nil,
                     queue: .main) { result in
                        guard let user = try? result.get().data?.findUsersById?.resultMap else {
                            return
                        }
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            let decoder = JSONDecoder()
                            let userBasicInformation = try? decoder.decode(UserBasicInformation.self, from: jsonData)
                            self.receiverFCMToken = userBasicInformation?.fcmToken ?? ""
                        }catch {
                            
                        }
        }
    }
    
    @objc func dismisskeyBoard() {
        
        self.messageTextView.resignFirstResponder()
    }
    

    override func viewDidDisappear(_ animated: Bool) {
        
        IQKeyboardManager.shared.enable = true
    }
    
    
    @objc func showUserProfile() {
        
        if let id = self.recverid {
        let profileVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: String(describing: ProfileViewController.self)) as! ProfileViewController
        profileVC.viewModel = ProfileViewModel(userId: Int(id) ?? 0)
        self.navigationController?.pushViewController(profileVC, animated: true)
        }
    }
    
    func swipeToPop() {
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self;
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
            return false
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        IQKeyboardManager.shared.enable = false
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        swipeToPop()
    }
    
    
    private func updateuserDetails(userBasicInformation: UserBasicInformation?) {
        
        self.receiverUserInfo = userBasicInformation
        self.labelTitle1.text = (userBasicInformation?.firstName)! + " " + (userBasicInformation?.lastName)!
        // self.contactImage.sd_setImage(with: URL(string: self.userBasicInformation?.profilePhoto ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        
    }
    
    @IBAction func sendMessage() {
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        let timestring = df.string(from: Date())
        
        let newMessage = DisplayAllMessagesOfUser(text:  self.messageTextView.text, typename: "Messages", timeStamp: timestring, senderID: Profile.shared.userId, messageID: "123", receiverID: self.recverid, firstName: self.receiverUserInfo?.firstName ?? "", lastName: self.receiverUserInfo?.lastName ?? "",profilePhoto: Profile.shared.userBasicInformation?.profilePhoto ?? "")
        self.allMessages.append(newMessage)
        self.tableView.reloadData()
        self.scrollToBottom()
        
        ApolloManager.shared.sendMessage(text: self.messageTextView.text, senderId: Profile.shared.userId, receiverId: self.recverid, timeStamp: timestring) {
           
            //self.receiverFCMToken
            PushNotificationSender().sendPushNotification(to: Profile.shared.userBasicInformation!.fcmToken!, title: (Profile.shared.loginResponse?.data?.userBasicInformation?.firstName)!, body: self.messageTextView.text)
        }
        self.messageTextView.text = ""
        self.sendBtn.setImage(UIImage(named: "ic_swipe_next_active")?.withTintColor(.lightGray), for: .normal)
     }
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            if self.allMessages.count > 0 {
                let indexPath = IndexPath(row: self.allMessages.count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    
    // MARK: - Load earlier methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //	func loadEarlierShow(_ show: Bool) {
    //
    //		viewLoadEarlier.isHidden = !show
    //		var frame: CGRect = viewLoadEarlier.frame
    //		frame.size.height = show ? 50 : 0
    //		viewLoadEarlier.frame = frame
    //		tableView.reloadData()
    //	}
    
    // MARK: - Message methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func rcmessageAt(_ indexPath: IndexPath) -> DisplayAllMessagesOfUser {
        if self.allMessages.count > 0 {
            return (self.allMessages[indexPath.row])
        } else {
            return DisplayAllMessagesOfUser.init(text: "", typename: "", timeStamp: "", senderID: "", messageID: "", receiverID: "", firstName: "", lastName: "", profilePhoto: "")
        }
        
    }
    
    // MARK: - Avatar methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func avatarInitials(_ indexPath: IndexPath) -> String {
        
        return ""
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func avatarImage(_ indexPath: IndexPath) -> UIImage? {
        
        return nil
    }
    
    // MARK: - Header, Footer methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func textHeaderUpper(_ indexPath: IndexPath) -> String? {
        
        return nil
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func textHeaderLower(_ indexPath: IndexPath) -> String? {
        
        return nil
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func textFooterUpper(_ indexPath: IndexPath) -> String? {
        
        return nil
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func textFooterLower(_ indexPath: IndexPath) -> String? {
        
        return nil
    }
    
    // MARK: - Menu controller methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func menuItems(_ indexPath: IndexPath) -> [RCMenuItem]? {
        
        return nil
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        return false
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    
    
    // MARK: - Typing indicator methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func typingIndicatorShow(_ typing: Bool, text: String = "typing...") {
        
        if (typing == true) && (isTyping == false) {
            textTitle = labelTitle2.text
            labelTitle2.text = text
        }
        if (typing == false) && (isTyping == true) {
            labelTitle2.text = textTitle
        }
        isTyping = typing
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func typingIndicatorUpdate() {
        
    }
    
    // MARK: - Keyboard methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    @objc func keyboardWillShow(_ notification: Notification?) {
        
         if (heightKeyboard != 0) { return }
        
        keyboardWillShow = true
        
        if let info = notification?.userInfo {
            if let duration = info[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double {
                if let keyboard = info[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        if (self.keyboardWillShow) {
                            self.heightKeyboard = keyboard.size.height
                            
                            self.bottomConstraint.constant = self.heightKeyboard + 5.0
                            self.scrollToBottom()
                        }
                    }
                }
            }
        }
        
        UIMenuController.shared.menuItems = nil
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    @objc func keyboardWillHide(_ notification: Notification?) {
        
        heightKeyboard = 0
        keyboardWillShow = false
        
        if hasTopNotch {
            
             self.bottomConstraint.constant = 20.0
        } else {
            
            self.bottomConstraint.constant  = 5.0
        }
      }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func dismissKeyboard() {
        
        self.messageTextView.resignFirstResponder()
    }
    
    // MARK: - Message input bar methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    
    // MARK: - User actions (title)
    //---------------------------------------------------------------------------------------------------------------------------------------------
    @IBAction func actionTitle(_ sender: Any) {
        
        actionTitle()
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionTitle() {
        
    }
    
    // MARK: - User actions (load earlier)
    //---------------------------------------------------------------------------------------------------------------------------------------------
    @IBAction func actionLoadEarlier(_ sender: Any) {
        
        actionLoadEarlier()
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionLoadEarlier() {
        
    }
    
    // MARK: - User actions (bubble tap)
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionTapBubble(_ indexPath: IndexPath) {
        
    }
    
    // MARK: - User actions (avatar tap)
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionTapAvatar(_ indexPath: IndexPath) {
        
    }
    
    // MARK: - User actions (input panel)
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionAttachMessage() {
        
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func actionSendMessage(_ text: String) {
        
    }
    
    // MARK: - Helper methods
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func layoutTableView() {
        
        return
        let widthView	= view.frame.size.width
        let heightView	= view.frame.size.height
        
        let leftSafe	= view.safeAreaInsets.left
        let rightSafe	= view.safeAreaInsets.right
        
        let heightInput = self.inputTextView.frame.size.height
        
        let widthTable = widthView - leftSafe - rightSafe
        let heightTable = heightView - heightInput - heightKeyboard
        
        tableView.frame = CGRect(x: leftSafe, y: 0, width: widthTable, height: heightTable)
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func scrollToBottom(animated: Bool) {
        
        //		if (tableView.numberOfSections > 0) {
        //			let indexPath = IndexPath(row: 0, section: tableView.numberOfSections - 1)
        //			tableView.scrollToRow(at: indexPath, at: .top, animated: animated)
        //		}
    }
}

// MARK: - UITableViewDataSource
//-------------------------------------------------------------------------------------------------------------------------------------------------
extension RCMessagesViewController: UITableViewDataSource {
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allMessages.count
    }
    
    /*//---------------------------------------------------------------------------------------------------------------------------------------------
     func numberOfSections(in tableView: UITableView) -> Int {
     
     return 0
     }*/
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //		if (indexPath.row == 0) {
        //            return cellForHeaderUpper(tableView, at: indexPath)
        //        }
        //
        //		if (indexPath.row == 1) {
        //            return cellForHeaderLower(tableView, at: indexPath)
        //        }
        //
        //		if (indexPath.row == 2) {
        if allMessages.count > 0 {
            let rcmessage = rcmessageAt(indexPath)
            if (rcmessage.typename == "Messages") {
                return cellForMessageText(tableView, at: indexPath)
            }
            //            }
            
            /*	if (rcmessage.type == MESSAGE_EMOJI)	{ return cellForMessageEmoji(tableView, at: indexPath)		}
             if (rcmessage.type == MESSAGE_PHOTO)	{ return cellForMessagePhoto(tableView, at: indexPath)		}
             if (rcmessage.type == MESSAGE_VIDEO)	{ return cellForMessageVideo(tableView, at: indexPath)		}
             if (rcmessage.type == MESSAGE_AUDIO)	{ return cellForMessageAudio(tableView, at: indexPath)		}
             if (rcmessage.type == MESSAGE_LOCATION)	{ return cellForMessageLocation(tableView, at: indexPath)	}*/
        }
        
        //		if (indexPath.row == 3)						{ return cellForFooterUpper(tableView, at: indexPath)		}
        //		if (indexPath.row == 4)						{ return cellForFooterLower(tableView, at: indexPath)		}
        
        return UITableViewCell()
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func cellForHeaderUpper(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RCHeaderUpperCell", for: indexPath) as! RCHeaderUpperCell
        cell.bindData(self, at: indexPath)
        return cell
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func cellForHeaderLower(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RCHeaderLowerCell", for: indexPath) as! RCHeaderLowerCell
        cell.bindData(self, at: indexPath)
        return cell
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func cellForMessageText(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RCMessageTextCell", for: indexPath) as! RCMessageTextCell
        cell.bindData(self, at: indexPath)
        return cell
    }
    /*
     //---------------------------------------------------------------------------------------------------------------------------------------------
     func cellForMessageEmoji(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
     
     let cell = tableView.dequeueReusableCell(withIdentifier: "RCMessageEmojiCell", for: indexPath) as! RCMessageEmojiCell
     cell.bindData(self, at: indexPath)
     return cell
     }
     
     //---------------------------------------------------------------------------------------------------------------------------------------------
     func cellForMessagePhoto(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
     
     let cell = tableView.dequeueReusableCell(withIdentifier: "RCMessagePhotoCell", for: indexPath) as! RCMessagePhotoCell
     cell.bindData(self, at: indexPath)
     return cell
     }
     
     //---------------------------------------------------------------------------------------------------------------------------------------------
     func cellForMessageVideo(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
     
     let cell = tableView.dequeueReusableCell(withIdentifier: "RCMessageVideoCell", for: indexPath) as! RCMessageVideoCell
     cell.bindData(self, at: indexPath)
     return cell
     }
     
     //---------------------------------------------------------------------------------------------------------------------------------------------
     func cellForMessageAudio(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
     
     let cell = tableView.dequeueReusableCell(withIdentifier: "RCMessageAudioCell", for: indexPath) as! RCMessageAudioCell
     cell.bindData(self, at: indexPath)
     return cell
     }
     
     //---------------------------------------------------------------------------------------------------------------------------------------------
     func cellForMessageLocation(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
     
     let cell = tableView.dequeueReusableCell(withIdentifier: "RCMessageLocationCell", for: indexPath) as! RCMessageLocationCell
     cell.bindData(self, at: indexPath)
     return cell
     }
     */
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func cellForFooterUpper(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RCFooterUpperCell", for: indexPath) as! RCFooterUpperCell
        cell.bindData(self, at: indexPath)
        return cell
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func cellForFooterLower(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RCFooterLowerCell", for: indexPath) as! RCFooterLowerCell
        cell.bindData(self, at: indexPath)
        return cell
    }
}

// MARK: - UITableViewDelegate
//-------------------------------------------------------------------------------------------------------------------------------------------------
extension RCMessagesViewController: UITableViewDelegate {
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        view.tintColor = UIColor.clear
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        
        view.tintColor = UIColor.clear
    }
    
     //---------------------------------------------------------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //		if (indexPath.row == 0)
        //        {
        //            return RCHeaderUpperCell.height(self, at: indexPath)
        //        }
        //		if (indexPath.row == 1)
        //        {
        //            return RCHeaderLowerCell.height(self, at: indexPath)
        //        }
        
        //		if (indexPath.row == 2) {
        if allMessages.count > 0 {
            
            let rcmessage = rcmessageAt(indexPath)
            if (rcmessage.typename == "Messages")
            {
                return RCMessageTextCell.height(self, at: indexPath)
            }
        }
        /*if (rcmessage.type == MESSAGE_EMOJI)	{ return RCMessageEmojiCell.height(self, at: indexPath)		}
         if (rcmessage.type == MESSAGE_PHOTO)	{ return RCMessagePhotoCell.height(self, at: indexPath)		}
         if (rcmessage.type == MESSAGE_VIDEO)	{ return RCMessageVideoCell.height(self, at: indexPath)		}
         if (rcmessage.type == MESSAGE_AUDIO)	{ return RCMessageAudioCell.height(self, at: indexPath)		}
         if (rcmessage.type == MESSAGE_LOCATION)	{ return RCMessageLocationCell.height(self, at: indexPath)	}*/
        //		}
        //
        //		if (indexPath.row == 3)						{ return RCFooterUpperCell.height(self, at: indexPath)		}
        //		if (indexPath.row == 4)						{ return RCFooterLowerCell.height(self, at: indexPath)		}
        
        return 0
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //
    //		return RCDefaults.sectionHeaderMargin
    //	}
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    //
    //		return RCDefaults.sectionFooterMargin
    //	}
}

 

extension UIView {
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
}


extension RCMessagesViewController: UIGestureRecognizerDelegate {
    
}

extension RCMessagesViewController: GrowingTextViewDelegate {
    
}

extension RCMessagesViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text.count > 0 {
            
            self.sendBtn.setImage(UIImage(named: "ic_swipe_next_active"), for: .normal)
        }else {
            
            self.sendBtn.setImage(UIImage(named: "ic_swipe_next_active")?.withTintColor(.lightGray), for: .normal)
        }
    }
}


extension RCMessagesViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {

         self.messageTextView.resignFirstResponder()
    }
     
}


