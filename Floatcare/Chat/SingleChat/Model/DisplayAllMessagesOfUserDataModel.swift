
import Foundation


// MARK: - AllMessages
class AllMessages: Codable {
    let displayAllMessagesOfUser: [DisplayAllMessagesOfUser]

    init(displayAllMessagesOfUser: [DisplayAllMessagesOfUser]) {
        self.displayAllMessagesOfUser = displayAllMessagesOfUser
    }
}

// MARK: - DisplayAllMessagesOfUser
class DisplayAllMessagesOfUser: Codable {
    let text, typename, timeStamp, senderID: String
    let messageID, receiverID: String
    let firstName, lastName, profilePhoto: String

    // fileprivate var timer: Timer?
    
     
    enum CodingKeys: String, CodingKey {
        case text
        case typename = "__typename"
        case timeStamp
        case senderID = "senderId"
        case messageID = "messageId"
        case receiverID = "receiverId"
        case firstName
        case lastName
        case profilePhoto
        
    }
    
    init(text: String, typename: String, timeStamp: String, senderID: String, messageID: String, receiverID: String, firstName: String, lastName: String, profilePhoto: String) {
        self.text = text
        self.typename = typename
        self.timeStamp = timeStamp
        self.senderID = senderID
        self.messageID = messageID
        self.receiverID = receiverID
        self.firstName = firstName
        self.lastName = lastName
        self.profilePhoto = profilePhoto
    }
    
   
       
    func getTimeStampValue() -> String {
        
               
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        if let date = df.date(from: self.timeStamp) {
        df.dateFormat = "HH:mm a"//sr
        return df.string(from: date)
        }
      return ""

    }
}

       
