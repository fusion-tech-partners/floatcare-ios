//
//  ConversationsListViewController.swift
//  Floatcare
//
//  Created by BEAST on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import DropDown

protocol ConversationsListViewControllerDelegate {
    func navigateToChat(chatId: String)
}

class ConversationsListViewController: UIViewController {
    
    @IBOutlet weak var conversationsView: ConversationsView!
    @IBOutlet weak var navigationBar: NavigationBar!
    
    
    var delegate: ConversationsListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNaigationBar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadConversations), name: NSNotification.Name(rawValue: "refreshChat"), object: nil)
        self.conversationsView.getAllMessages()
    }
    
    @objc private func reloadConversations() {
        
        self.conversationsView.getAllMessages()
    }
    
    fileprivate func setUpNaigationBar() {
        
        self.navigationBar.LeftButtonActionBlock = { sender in
                   self.navigationController?.navigationBar.isHidden = false
                   self.navigationController?.popViewController(animated: true)
               }
        
        self.conversationsView.navigateToChat = { [weak self] receverId in
              
            self?.delegate?.navigateToChat(chatId: receverId)
//            let vc = self?.storyboard?.instantiateViewController(withIdentifier: "RCMessagesViewController") as! RCMessagesViewController
//            vc.recverid = receverId
//            self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        self.navigationBar.rightButton.setImage(#imageLiteral(resourceName: "ic_edit"), for: .normal)
        
        self.navigationBar.rightButtonActionBlock = { sender in
            
            self.editButtonTapped()
        }
        
    }
    
    @objc func editButtonTapped() {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewMessageViewController") as! NewMessageViewController
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension UINavigationController {

    func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }

    
}



extension ConversationsListViewController: NewMessageViewControllerDelegate {
    
    func onSelectingUser(userId: String) {
        
        self.delegate?.navigateToChat(chatId: userId)
     }
    
}
