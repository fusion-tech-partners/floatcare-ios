//
//  ConversationsTableViewCell.swift
//  Floatcare
//
//  Created by BEAST on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import SDWebImage

class ConversationsContactTableViewCell: UITableViewCell {
    
    var userBasicInformation: UserBasicInformation?
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var timeStampLabel: TTTAttributedLabel!
    @IBOutlet weak var messageLabel: TTTAttributedLabel!
    @IBOutlet weak var name: TTTAttributedLabel!
    @IBOutlet weak var contactImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layoutIfNeeded()
        
        self.name.text = ""
        
        contactImage?.layer.cornerRadius = (contactImage?.frame.height)! / 2.0
        contactImage?.layer.masksToBounds = true
        contactImage?.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.3882352941, blue: 0.6705882353, alpha: 1)
        
        container?.layer.cornerRadius = 10.58
       
        container?.layer.masksToBounds = true
    }

    func getUser(userId: String) {
        
        guard self.userBasicInformation == nil else {
            
            self.updateuserDetails()
            return
        }
        FetchUserInfo().getUserInfo(userId: Int(userId) ?? 0) { [weak self] (userInfo) in
            
            self?.userBasicInformation = userInfo
            self?.updateuserDetails()
            print(userInfo)
        }
    }
    
    private func updateuserDetails() {
        
        self.name.text = self.userBasicInformation!.firstName + " " + self.userBasicInformation!.lastName
        self.contactImage.sd_setImage(with: URL(string: self.userBasicInformation?.profilePhoto ?? ""), placeholderImage: UIImage(named: "placeholder.png"))

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
