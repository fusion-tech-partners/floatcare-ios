//
//  ConversationsView.swift
//  Floatcare
//
//  Created by BEAST on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

typealias NavigateToChat = (String) -> Void
class ConversationsView: UIView {
    
    @IBOutlet weak var conversationListTableView: UITableView!
    @IBOutlet weak var noMessagesView: UIView!
    @IBOutlet weak var headerContainterView : TopHeaderContainerView!
     var userBasicInformation: UserBasicInformation?
    var receiverUserInfo: UserBasicInformation?
    var rcMsgView : RCMessagesViewController?
    var indexpath : NSIndexPath?
    @IBOutlet weak var noMessagesTitle: UILabel!
    @IBOutlet weak var noMessagesSubTitle: UILabel!
    
    var allMessages = [DisplayAllMessagesOfUser]()
        
    var searchStr: String = ""
    var receiverFCMToken : String?
    
    var navigateToChat: NavigateToChat?
    
    override func awakeFromNib() {
        
       
        self.noMessagesTitle.textColor = .primaryColor
        self.noMessagesSubTitle.textColor = .leastLabelColor
        
        
        self.noMessagesTitle.font = UIFont(font: .SFUIText, weight: .bold, size: 16.0)
        self.noMessagesSubTitle.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
 
        conversationListTableView.delegate = self
        conversationListTableView.dataSource = self
        conversationListTableView.estimatedRowHeight = 80.0
        
        self.headerContainterView.dropDownActionBlock = {[weak self] index in
           
           self?.sortByIndex(index: index)
        }
        
        self.headerContainterView.searchTextChangeActionBlock = {[weak self] value in
           
            self?.filterChatList(text: value.lowercased())
            self!.getAllMessages()
        }
    }
       
        
//        func reloadConversations() {
//
//        guard let rId = self.recverid else{ return }
//
//        ApolloManager.shared.getAllMessagesOfReceiever(receiverId: rId) { [weak self] (messages) in
//
//                      if messages.count > self?.allMessages.count ?? 0 {
//                          DispatchQueue.main.async {
//
//                              self?.allMessages.removeAll()
//                              self?.allMessages.append(contentsOf: messages)
//                              self?.conversationListTableView.reloadData()
//                              //self?.scrollToBottom()
//                          }
//
//                      }
//                  }
//    }
   
    
    func filterChatList(text: String) {
        
        self.searchStr = text
        
        guard text.count > 0 else {
            self.searchStr = ""
            
            self.conversationListTableView.reloadData()
            return
        }
        
//       self.allMessages = self.allMessages.sorted(by: { (message1, message2) -> Bool in
//       // return message1.firstName > message2.firstName
//        return  message1.firstName < message2.firstName
        self.allMessages = self.allMessages.filter({ (message) -> Bool in
        
            return message.firstName.lowercased().contains(text.lowercased()) || message.lastName.lowercased().contains(text.lowercased()) || message.text.lowercased().contains(text.lowercased())
        })
        
        self.conversationListTableView.reloadData()
        
    }
    
    func sortByIndex(index: Int) {
        
        if index == 0 {
            self.allMessages = self.allMessages.sorted(by: { (message1, message2) -> Bool in
               // return message1.firstName > message2.firstName
                return  message1.firstName < message2.firstName
            })
        }else {
            self.allMessages = self.allMessages.sorted(by: { (message1, message2) -> Bool in
                return message1.timeStamp > message2.timeStamp
            })
        }
        
        self.conversationListTableView.reloadData()
        
    }
    
    func getAllMessages() {
        
        Progress.shared.showProgressView()
        ApolloManager.shared.getAllMessages(completion: { (items) in
            
            self.allMessages.removeAll()
            
            guard let totalItems = items else {
                
                self.noMessagesView.isHidden = false
                self.conversationListTableView.isHidden = true
                
                self.conversationListTableView.reloadData()
                Progress.shared.hideProgressView()
                return}
            
            self.allMessages.append(contentsOf: totalItems.sorted(by: { (message1, message2) -> Bool in
                
                return message1.messageID > message2.messageID
            }))
 
            if self.allMessages.count == 0 {
                self.noMessagesView.isHidden = false
                self.conversationListTableView.isHidden = true
            }else {
                self.noMessagesView.isHidden = true
                self.conversationListTableView.isHidden = false
            }
            
            self.filterChatList(text: self.searchStr)
            Progress.shared.hideProgressView()
        })
    }
}



extension ConversationsView: UITableViewDelegate, UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
         return self.allMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ConversationsContactTableViewCell.self), for: indexPath) as? ConversationsContactTableViewCell {
            
            
            let message = self.allMessages[indexPath.row]
            cell.messageLabel.text = message.text
            cell.name.text = message.firstName + " " + message.lastName
            if let imgStr = (amazonURL + (message.profilePhoto )).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                       
            cell.contactImage.sd_setImage(with: URL(string: imgStr), placeholderImage: UIImage(named: "placeholder.png"))
            }
            cell.timeStampLabel.text = message.getTimeStampValue()
           
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let cell = cell as? ConversationsContactTableViewCell  {
            if (indexPath.row % 2) != 0 {
                cell.container?.backgroundColor = .clear
            } else {
                cell.container?.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.9843137255, alpha: 1)
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    let message = self.allMessages[indexPath.row]
        
            self.navigateToChat?(message.receiverID)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
       if editingStyle == .delete
       {
        let message = self.allMessages[indexPath.row]
        let mutation = DeleteMessagesOfUserMutation.init(senderId: Profile.shared.userId, receiverId: message.receiverID )
        
        // let mutation = CreateARequestMutation.init(userId: Profile.shared.userId, worksiteId: worksiteDetails?.businessId! ?? "", departmentId: worksiteDetails?.workspaceId! ?? "", requestTypeId: requestTypeId, onDate: CreateReqParams.createReqParams.dates, shiftId: "0", notes: CreateReqParams.createReqParams.notes)
         Progress.shared.showProgressView()
        ApolloManager.shared.client.perform(mutation: mutation) {[weak self] (result) in
            
            print("\(result)")
            
            DispatchQueue.main.async {
                 Progress.shared.hideProgressView()
                let alert = UIAlertController(title: "FloatCare", message: "Request deleted successfully", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    //self?.navigationController.popViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification_Myrequest), object: nil, userInfo: nil)
                }))
                // self?.present(alert, animated: true, completion: nil)
            }
            
        }
       // deleteRequest()
        getAllMessages()
          Progress.shared.showProgressView()
        self.conversationListTableView.reloadData()
       }
    }
}
    
   // func deleteRequest() {
   
    
               
//        let mutation = DeleteMessagesOfUserMutation.init(senderId: Profile.shared.userId, receiverId: self.recverid )
//
//    // let mutation = CreateARequestMutation.init(userId: Profile.shared.userId, worksiteId: worksiteDetails?.businessId! ?? "", departmentId: worksiteDetails?.workspaceId! ?? "", requestTypeId: requestTypeId, onDate: CreateReqParams.createReqParams.dates, shiftId: "0", notes: CreateReqParams.createReqParams.notes)
//     Progress.shared.showProgressView()
//    ApolloManager.shared.client.perform(mutation: mutation) {[weak self] (result) in
//
//        print("\(result)")
//
//        DispatchQueue.main.async {
//             Progress.shared.hideProgressView()
//            let alert = UIAlertController(title: "FloatCare", message: "Request deleted successfully", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
//                //self?.navigationController.popViewController(animated: true)
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification_Myrequest), object: nil, userInfo: nil)
//            }))
//            // self?.present(alert, animated: true, completion: nil)
//        }
//
//    }

//    func getReceiverDetails() {
//           ApolloManager.shared.client.fetch(query: FindUsersByIdQuery(userId:self.recverid),
//                        cachePolicy: .fetchIgnoringCacheData,
//                        context: nil,
//                        queue: .main) { result in
//                           guard let user = try? result.get().data?.findUsersById?.resultMap else {
//                               return
//                           }
//                           do {
//                               let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
//                               let decoder = JSONDecoder()
//                               let userBasicInformation = try? decoder.decode(UserBasicInformation.self, from: jsonData)
//                               self.receiverFCMToken = userBasicInformation?.fcmToken ?? ""
//                           }catch {
//
//                           }
//           }
//       }



public extension Sequence {
    
    func categorise<U : Hashable>(_ key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var dict: [U:[Iterator.Element]] = [:]
        for el in self {
            let key = key(el)
            if case nil = dict[key]?.append(el) { dict[key] = [el] }
        }
        return dict
    }
}
