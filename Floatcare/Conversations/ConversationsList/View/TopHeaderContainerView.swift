//
//  HeaderContainerView.swift
//  Floatcare
//
//  Created by BEAST on 08/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import DropDown


typealias DropDownActionBlock = (_ index: Int) -> ()
typealias SearchTextChangeActionBlock = (_ search: String) -> ()
 
class HeaderContainerView: UIView {
    
     @IBOutlet weak var titleContainerView: UIImageView!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
         
        self.titleContainerView.backgroundColor = .profileBgColor
        
        self.titleContainerView.roundCorners(.layerMinXMaxYCorner, radius: 45.0)
 
    }
    
}

class TopHeaderContainerView: UIView {
    
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var searchViewContainer: UIView!
    @IBOutlet weak var searchSortContainer: UIView!
    var searchIconBtn: INSSearchBar!
    var searchField : UITextField!
    @IBOutlet weak var unreadCount: UILabel!
    @IBOutlet weak var titleContainerView: UIImageView!
    
    var dropDownActionBlock: DropDownActionBlock?
    var searchTextChangeActionBlock: SearchTextChangeActionBlock?
    
    let dropDown = DropDown()
       
    
    override func awakeFromNib() {
        super.awakeFromNib()
        unreadCount?.layer.cornerRadius = (unreadCount?.frame.height)! / 2.0
        unreadCount?.layer.masksToBounds = true
        unreadCount?.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.3882352941, blue: 0.6705882353, alpha: 1)

        
        self.titleContainerView.backgroundColor = .profileBgColor
        
        self.titleContainerView.roundCorners(.layerMinXMaxYCorner, radius: 45.0)
        self.configureDropDown()
        
        self.searchIconBtn = INSSearchBar(frame: CGRect(x: 0, y: 0, width: 44.0, height: 44.0))
        self.searchViewContainer.addSubview(self.searchIconBtn)
        self.searchIconBtn.delegate = self
        
//        self.searchField = UITextField(frame: CGRect(x: self.searchIconBtn.frame.width+5, y: self.searchIconBtn.frame.maxY, width: 44.0, height: 44.0))
//        self.searchViewContainer.addSubview(self.searchField)
//        self.searchField.placeholder = "search"
        
        self.searchViewContainer.backgroundColor = .clear
        self.searchSortContainer.layer.borderWidth = 2
        self.searchSortContainer.layer.borderColor = UIColor.profileBgColor.cgColor
        self.searchSortContainer.layer.cornerRadius = 20.0
        
        
        
    }
    
    
    private func configureDropDown() {
        
        dropDown.anchorView = self.sortButton
        dropDown.backgroundColor = .white
        dropDown.textColor = .primaryColor
        dropDown.textFont = UIFont(name: "SF Pro Text", size: 16.0)!
        dropDown.dataSource = ["Name", "Recent"]
        //dropDown.dataSource = ["Name", "Recent"]
        dropDown.bottomOffset = CGPoint(x: 0, y:40)
        
        dropDown.width = 150.0
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.dropDownActionBlock?(index)
            
        }
    }
    
    @IBAction func SortButtonTapped(_ sender: Any) {
        
        self.dropDown.show()
    }
    
    @IBAction func SearchButtonTapped(_ sender: Any) {
        
    }
    
}

extension TopHeaderContainerView: INSSearchBarDelegate {
    func destinationFrame(for searchBar: INSSearchBar!) -> CGRect {
        
        return CGRect(x: 0, y: 0, width: self.searchViewContainer.frame.size.width, height: self.searchViewContainer.frame.size.height)
    }
    
    func searchBar(_ searchBar: INSSearchBar!, willStartTransitioningTo destinationState: INSSearchBarState) {
        
    }
    
    func searchBar(_ searchBar: INSSearchBar!, didEndTransitioningFrom previousState: INSSearchBarState) {
        self.searchTextChangeActionBlock?(searchBar.searchField.text ?? "")
        
        
    }
    
    func searchBarDidTapReturn(_ searchBar: INSSearchBar!) {
        
    }
    
    func searchBarTextDidChange(_ searchBar: INSSearchBar!) {
        
        self.searchTextChangeActionBlock?(searchBar.searchField.text ?? "")
    }
    
    
}
