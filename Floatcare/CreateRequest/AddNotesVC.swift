//
//  AddNotesVC.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 08/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class AddNotesVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var notesTxtView: UITextView!
    @IBOutlet weak var screenBumLbl: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var backBtn: UIButton!
     var requestType : RequestTypes?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         topView.layer.cornerRadius = 40
         topView.layer.maskedCorners = [.layerMinXMaxYCorner]
        notesTxtView.delegate = self
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        notesTxtView.resignFirstResponder()
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func dismissKeyboard() {
               view.endEditing(true)
           }
    @IBAction func nextBtnTapped(_ sender: Any) {
        CreateReqParams.createReqParams.notes = self.notesTxtView.text
        switch self.requestType {
        case .callOff:
            let summaryVC = self.storyboard?.instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
            summaryVC.notes = self.notesTxtView.text
            summaryVC.requestType = .callOff
            self.navigationController?.pushViewController(summaryVC, animated: true)
        case .personal:
            let summaryVC = self.storyboard?.instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
            CreateReqParams.createReqParams.notes = self.notesTxtView.text
            summaryVC.notes = self.notesTxtView.text
            summaryVC.requestType = .personal
            self.navigationController?.pushViewController(summaryVC, animated: true)
        case .vacation:
            let summaryVC = self.storyboard?.instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
            CreateReqParams.createReqParams.notes = self.notesTxtView.text
            summaryVC.notes = self.notesTxtView.text
            summaryVC.requestType = .vacation
            self.navigationController?.pushViewController(summaryVC, animated: true)
        default:
            let notesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
            notesVC.requestType = .callOff
            self.navigationController?.pushViewController(notesVC, animated: true)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        notesTxtView.text = ""
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

