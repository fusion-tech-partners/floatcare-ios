//
//  CRChooseDatesVC.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 06/08/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
enum RequestTypes {
    case swap
    case callOff
    case personal
    case availability
    case giveAway
    case vacation
    
}
enum CalendarNavigation {
    case fromEditRequest
    case fromSetAvailability
    case createRequest
    case fromSwapCalender
    case fromVacation
   
}


class CRChooseDatesVC: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance  {
    @IBOutlet weak var monday: UILabel!
    
    @IBOutlet weak var satday: UILabel!
    @IBOutlet weak var friday: UILabel!
    @IBOutlet weak var thuday: UILabel!
    @IBOutlet weak var wedday: UILabel!
    @IBOutlet weak var tueday: UILabel!
    @IBOutlet weak var sunday: UILabel!
    @IBOutlet weak var stepCountLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var bottonView: UIView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var monthNameLbl: UILabel!
    var scheduleLayoutArr : [ShiftPlanningLayout]?
    @IBOutlet weak var topBgView: UIView!
    var selectedShiftType : SelectedShiftType = .noShiftSelected
     var requestType : RequestTypes?
     var isFromEditReqScreen : Bool = false
    var calendarNavigation: CalendarNavigation = .createRequest
    var isStartDateSelection : Bool = false
    //MARK: Calendar
    var selectedDate : Date?
    var selectedDatesArr = [String]()
    
      fileprivate let gregorian = Calendar(identifier: .gregorian)
      fileprivate let formatter: DateFormatter = {
          let formatter = DateFormatter()
          formatter.dateFormat = "yyyy-MM-dd"
          return formatter
      }()
    fileprivate weak var eventLabel: UILabel!
           
           private var currentPage: Date?
           
           private lazy var today: Date = {
               return Date()
           }()
           fileprivate lazy var dateFormatter1: DateFormatter = {
               let formatter = DateFormatter()
               formatter.dateFormat = "yyyy/MM/dd"
               return formatter
           }()
           fileprivate lazy var dateFormatter2: DateFormatter = {
               let formatter = DateFormatter()
               formatter.dateFormat = "yyyy-MM-dd"
               return formatter
           }()
    //       var navigationView = NavigationView()
    //       var headerView = UIView()
         //  var datesWithMultipleEvents = ["2020-08-10", "2020-08-16", "2020-08-22","2020-08-26"]
     var datesWithMultipleEvents = [""]
           var datesWithNoEvents = [""]
           var datesWithOnCall = [""]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        nextBtn.layer.cornerRadius = 6.0
        nextBtn.backgroundColor = .primaryColor
        bottonView.roundCorners([.layerMinXMinYCorner], radius: 40)
         topBgView.layer.cornerRadius = 40
         topBgView.layer.maskedCorners = [.layerMinXMaxYCorner]
        self.setUpCalendar()
               let dates = [
                   self.gregorian.date(byAdding: .day, value: -1, to: Date()),
                   Date(),
                   self.gregorian.date(byAdding: .day, value: 1, to: Date())
               ]
               dates.forEach { (date) in
                   self.calendar.select(date, scrollToDate: false)
               }
               // For UITest
               self.calendar.accessibilityIdentifier = "calendar"
               self.calendar.select(Date())
               self.calendar.deselect(Date())
            
             //  self.calendar.placeholderType = .fillHeadTail
    }
    override func viewWillAppear(_ animated: Bool) {
        switch self.calendarNavigation {
        case .fromEditRequest:
             nextBtn.setTitle("OK", for: .normal)
            calendar.allowsMultipleSelection = true
             stepCountLbl.isHidden = true

        case .fromSetAvailability:
            nextBtn.setTitle("OK", for: .normal)
            calendar.allowsMultipleSelection = false
            stepCountLbl.isHidden = true
        case .fromSwapCalender:
            nextBtn.setTitle("Next", for: .normal)
            calendar.allowsMultipleSelection = false
            stepCountLbl.isHidden = true
        case .fromVacation:
            nextBtn.setTitle("Next", for: .normal)
            calendar.allowsMultipleSelection = false
            stepCountLbl.isHidden = true
            
        default:
            nextBtn.setTitle("Next", for: .normal)
            calendar.allowsMultipleSelection = true
            stepCountLbl.isHidden = false

        }
        //if self.isFromEditReqScreen == true {
//           // nextBtn.isHidden = true
//            nextBtn.setTitle("OK", for: .normal)
//            // calendar.allowsMultipleSelection = false
//         }else {
//           //  nextBtn.isHidden = false
//            nextBtn.setTitle("Next", for: .normal)
//             //calendar.allowsMultipleSelection = true
//        }
        
    }
    func setAvailabilityStartDate() {
                let dateSelected = self.dateFormatter2.string(from: self.selectedDate ?? Date())
        AvailabilityVCVM.shared.startDate = dateSelected
    }
    func setAvailabilityEndDate() {
        let dateSelected = self.dateFormatter2.string(from: self.selectedDate ?? Date())
         AvailabilityVCVM.shared.endDate = dateSelected
    }
    @IBAction func backBtntapped(_ sender: Any) {
        switch self.calendarNavigation {
               case .fromEditRequest,.fromSetAvailability:
                   self.dismiss(animated: true, completion: nil)
                   
               default:
                    self.navigationController?.popViewController(animated: true)
               }

        
       
    }
    
  // MARK: - calendar Methods
    func setUpCalendar() {
    
            self.calendar.backgroundColor = UIColor.white
        
          calendar.rowHeight = 80
          calendar.pagingEnabled = false
        // self.calendar.weekdayHeight = 1
         self.calendar.calendarWeekdayView.frame = CGRect.zero
            self.calendar.calendarHeaderView.isHidden = true
            calendar.appearance.headerMinimumDissolvedAlpha = 0
            self.calendar.appearance.headerTitleFont = UIFont(name: "SF Pro Text", size: 0.0)
            self.calendar.headerHeight = 0.0
            calendar.appearance.caseOptions = [ .weekdayUsesUpperCase]
        self.calendar.calendarWeekdayView.isHidden = true
        self.calendar.calendarWeekdayView.removeFromSuperview()
            calendar.appearance.weekdayFont = UIFont(name: "SF Pro Text", size: 0.0)
            calendar.appearance.eventSelectionColor = UIColor.white
            calendar.appearance.eventOffset = CGPoint(x: 0, y: -7)
            calendar.today = nil // Hide the today circle
            calendar.register(CalendarCell.self, forCellReuseIdentifier: "cell")
            
            calendar.swipeToChooseGesture.isEnabled = false // Swipe-To-Choose
            calendar.appearance.headerTitleColor = UIColor.black
            calendar.appearance.weekdayTextColor = UIColor.black
            let scopeGesture = UIPanGestureRecognizer(target: calendar, action: #selector(calendar.handleScopeGesture(_:)));
            calendar.addGestureRecognizer(scopeGesture)
        calendar.placeholderType = .fillHeadTail
       
            let btnLeft = UIButton(frame:  CGRect(x: calendar.frame.minX + 10, y:  calendar.frame.minY + 10, width: 30, height: 30))
            btnLeft.addTarget(self, action: #selector(self.btnLeftAction(_:)), for: .touchUpInside)
            btnLeft.titleLabel?.textAlignment = .left
            btnLeft.setImage(UIImage(named: "prev"), for: .normal)
            btnLeft.setTitleColor(.black, for: .normal)
            calendar.calendarHeaderView.isUserInteractionEnabled = true
            // calendar.calendarHeaderView.isExclusiveTouch = true
          // self.view.addSubview(btnLeft)
            // calendar.calendarHeaderView.addSubview(btnLeft)
            
            let btnRight = UIButton(frame:  CGRect(x: calendar.frame.maxX - 60, y: calendar.frame.minY + 10, width: 30, height: 30))
            btnRight.addTarget(self, action: #selector(self.btnRightAction(_:)), for: .touchUpInside)
            btnRight.titleLabel?.textAlignment = .right
            btnRight.setImage(UIImage(named: "next"), for: .normal)
            btnRight.setTitleColor(.black, for: .normal)
          //  self.view.addSubview(btnRight)
        let month = Calendar.current.component(.month, from: Date())
           let year = Calendar.current.component(.year, from: Date())
        let monthName = self.monthName(month: month)
        self.monthNameLbl.text = "\(monthName)  \(year)"
        let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "EEEE"
               let dayInWeek = dateFormatter.string(from: Date())
                self.dayName(weekday: dayInWeek)
        self.getAllRequestForUser()
        }
    @objc  func btnLeftAction(_ sender: Any){
           self.moveCurrentPage(moveUp: false)
       }
       @objc  func btnRightAction(_ sender: Any){
           self.moveCurrentPage(moveUp: true)
       }
       private func moveCurrentPage(moveUp: Bool) {
           var dateComponents = DateComponents()
           if self.calendar.scope == .month {
               dateComponents.month = moveUp ? 1 : -1
           }else {
               dateComponents.weekOfYear = moveUp ? 1 : -1
           }
           self.currentPage = self.gregorian.date(byAdding: dateComponents, to: self.currentPage ?? self.today)
           self.calendar.setCurrentPage(self.currentPage!, animated: true)
       }
    func monthName(month : Int)->String {
        var name = ""
        switch month {
        case 1:
            name = "JANUARY"
            return name
        case 2:
            name = "FEBRUARY"
            return name
        case 3:
            name = "MARCH"
            return name
        case 4:
            name = "APRIL"
            return name
        case 5:
            name = "MAY"
            return name
        case 6:
            name = "JUNE"
            return name
        case 7:
            name = "JULY"
            return name
        case 8:
            name = "AUGUST"
            return name
        case 9:
            name = "SEPTEMBER"
            return name
        case 10:
            name = "OCTOBER"
            return name
        case 11:
            name = "NOVEMBER"
            return name
        case 12:
            name = "DECEMBER"
            return name
        default:
            return name
            
        }
    }
    func dayName(weekday:String){
        self.sunday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.monday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.tueday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.wedday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.thuday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.friday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.satday.font = UIFont(name: "SF Pro Text", size: 9.0)

        switch weekday {
        case "Sunday":
            self.sunday.textColor = .primaryColor
        case "Monday":
            self.monday.textColor = .primaryColor
        case "Tuesday":
             self.tueday.textColor = .primaryColor
        case "Wednesday":
             self.wedday.textColor = .primaryColor
        case "Thursday":
             self.thuday.textColor = .primaryColor
        case "Friday":
             self.friday.textColor = .primaryColor
        case "Saturday":
             self.satday.textColor = .primaryColor
        default:
             self.sunday.font = UIFont(name: "SF Pro Text", size: 9.0)
        }
    }
    // MARK:- FSCalendarDataSource
      func minimumDate(for calendar: FSCalendar) -> Date {
              
      //        let dateFormatter = DateFormatter()
      //        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"
              let date = Calendar.current.date(byAdding: .month, value: 0, to: Date()) ?? Date()
               let minimumDate = self.dateFormatter2.string(from: date)
              
                 return self.dateFormatter2.date(from: minimumDate)!
             }
                 
             func maximumDate(for calendar: FSCalendar) -> Date {
              let date = Calendar.current.date(byAdding: .month, value: 6, to: Date()) ?? Date()
              let maxDate = self.dateFormatter2.string(from: date)
                 return self.dateFormatter2.date(from: maxDate)!
             }
    
       func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
           let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
           return cell
       }
       
       func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
           self.configure(cell: cell, for: date, at: position)
       }
      func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
           let currentPageDate = calendar.currentPage

           let month = Calendar.current.component(.month, from: currentPageDate)
           let year = Calendar.current.component(.year, from: currentPageDate)
        let monthName = self.monthName(month: month)
        self.monthNameLbl.text = "\(monthName)  \(year)"
            print("did select date \(year) \(month)")
       
        self.calendar.setCurrentPage(currentPageDate, animated: false)
        
       }
       
       func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
           return 2
       }
       
       // MARK:- FSCalendarDelegate
       
       func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
           self.calendar.frame.size.height = bounds.height
           self.eventLabel.frame.origin.y = calendar.frame.maxY + 10
        
//        calendar.headerHeight = 1
//        calendar.appearance.headerTitleColor = UIColor.black
//        self.calendar.frame.size.height = bounds.height + 1
//        self.view.layoutIfNeeded()
       }
       
//       func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
//           return monthPosition == .current
//       }
       
//       func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
//          // return monthPosition == .current
//        if date .compare(Date()) == .orderedAscending {
//                         return false
//                     }
//                     else {
//                   return monthPosition == .current
//                         return true
//                     }
//       }
       
       func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.selectedDate = date
        let dateSelected = self.dateFormatter2.string(from: self.selectedDate ?? Date())
        print("did select date \(self.formatter.string(from: date))")
        
        if self.selectedShiftType == .userShiftstoSwap || self.selectedShiftType == .selectedShiftForSwap{
            handleDateSelectionForSwapShift(dateSelected: dateSelected)
        }else{
            let filter = self.scheduleLayoutArr?.filter({$0.onDate == dateSelected })
             if filter?.count ?? 0 == 0 {
                 let alert = UIAlertController(title: "FloatCare", message: "Please select a scheduled date", preferredStyle: .alert)
                               alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                  
                               }))
                                self.present(alert, animated: true, completion: nil)
             }else {
                  if self.calendarNavigation == .fromSetAvailability { // This is to select dates between start and end for setavailability
                            if self.isStartDateSelection == false {
                                if AvailabilityVCVM.shared.startDate.count == 0 {
                                     DispatchQueue.main.async {
                                       let alert = UIAlertController(title: "", message: "Please select start date first", preferredStyle: .alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        }))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }else {
                                    let startDate = self.dateFormatter2.date(from:  AvailabilityVCVM.shared.startDate)!
                                                   var datesBetweenArray = Date.dates(from: startDate , to: self.selectedDate!)
                                                   self.selectedDatesArr = datesBetweenArray
                                                   datesBetweenArray = []
                                }
                               
                            }else {
                                 self.selectedDatesArr.append(dateSelected)
                            }
                        }else {
                             self.selectedDatesArr.append(dateSelected)
                        }
             }

            self.calendar.setCurrentPage(self.selectedDate!, animated: true)
            self.configureVisibleCells()
        }
       }
    
    func handleDateSelectionForSwapShift(dateSelected:String)  {
        calendar.deselect(selectedDate!)
        if self.selectedShiftType == .userShiftstoSwap {
            if let filter = self.scheduleLayoutArr?.filter({$0.onDate == dateSelected }), filter.count > 0{
                self.selectedDatesArr.removeAll()
                self.selectedDatesArr.append(dateSelected)
        }
        else{
            
            ReusableAlertController.showAlert(title: "FloatCare", message: "Please select a scheduled date")
            }
        }else {//if selectedShiftForSwap
            self.selectedDatesArr.removeAll()
           self.selectedDatesArr.append(dateSelected)
        }
        self.calendar.setCurrentPage(self.selectedDate!, animated: true)
        self.configureVisibleCells()
    }
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
         print("did deselect date \(self.formatter.string(from: date))")
        let dateSelected = self.dateFormatter2.string(from: date )
        self.selectedDate = date
        if self.selectedShiftType == .userShiftstoSwap || self.selectedShiftType == .selectedShiftForSwap{
            handleDateSelectionForSwapShift(dateSelected: dateSelected)
        }else{
            let filter = self.scheduleLayoutArr?.filter({$0.onDate == dateSelected })
            if filter?.count ?? 0 == 0 {
                let alert = UIAlertController(title: "FloatCare", message: "Date already been selected", preferredStyle: .alert)
                              alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                 
                              }))
                               self.present(alert, animated: true, completion: nil)
            }else {
                self.selectedDatesArr = self.selectedDatesArr.filter() { $0 != dateSelected }
                self.configureVisibleCells()
            }
        }
              
    }
//       func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
//           print("did deselect date \(self.formatter.string(from: date))")
//        let dateSelected = self.dateFormatter2.string(from: date )
//        self.selectedDatesArr = self.selectedDatesArr.filter() { $0 != dateSelected }
//           self.configureVisibleCells()
//       }
       
       func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
           if self.gregorian.isDateInToday(date) {
               return [UIColor.orange]
           }
           return [appearance.eventDefaultColor]
       }
       
       // MARK: - Private functions
       
       private func configureVisibleCells() {
           calendar.visibleCells().forEach { (cell) in
               let date = calendar.date(for: cell)
               let position = calendar.monthPosition(for: cell)
               self.configure(cell: cell, for: date!, at: position)
           }
       }
       
       private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
           
           let diyCell = (cell as! CalendarCell)
        diyCell.eventIndicator.isHidden = true
            if position == .current { // Current month dates in current page
                         
                       if self.gregorian.isDateInToday(date) {
                            diyCell.titleLabel.textColor = UIColor.black
                                   diyCell.bgImgView.image = UIImage(named: "current_current_not")
                                  diyCell.shift1.isHidden = true
                                  diyCell.shift2.isHidden = true
                                  diyCell.shift3.isHidden = true
                                  diyCell.ws1.isHidden = true
                           diyCell.bgImgView.layer.cornerRadius = 0.0
                           diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                           diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                           diyCell.bgImgView.layer.shadowRadius = 0
                           diyCell.bgImgView.layer.shadowOpacity = 0
                       }else if date < Date() {
                            diyCell.titleLabel.textColor = UIColor.black
                           diyCell.bgImgView.image = UIImage(named: "")
                             diyCell.shift1.isHidden = true
                             diyCell.shift2.isHidden = true
                             diyCell.shift3.isHidden = true
                             diyCell.ws1.isHidden = true
                             
                             diyCell.bgImgView.backgroundColor = UIColor.white
                             diyCell.bgImgView.layer.cornerRadius = 10.0
                             diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                             diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                             diyCell.bgImgView.layer.shadowRadius = 2.0
                           diyCell.bgImgView.layer.shadowOpacity = 0.4

                       }else {
                            diyCell.titleLabel.textColor = UIColor.black
                           diyCell.bgImgView.image = UIImage(named: "")
                             diyCell.shift1.isHidden = true
                             diyCell.shift2.isHidden = true
                             diyCell.shift3.isHidden = true
                             diyCell.ws1.isHidden = true
                             
                             diyCell.bgImgView.backgroundColor = UIColor.white
                             diyCell.bgImgView.layer.cornerRadius = 10.0
                             diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                             diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                             diyCell.bgImgView.layer.shadowRadius = 2.0
                           diyCell.bgImgView.layer.shadowOpacity = 0.4
                       }
                     
                       
                       if self.scheduleLayoutArr != nil && self.scheduleLayoutArr?.count ?? 0 > 0  { // To apply layout in the current page
                           for scheduleLayout in (self.scheduleLayoutArr)! {
                               
                               let celldate = self.dateFormatter2.string(from: date)
                               let layoutDateString = scheduleLayout.onDate!
                               let shiftName = scheduleLayout.shiftTypeName!
                               let layoutDate = self.dateFormatter2.date(from: layoutDateString) as! Date
           //                    if celldate != layoutDateString {
           //                        return
           //                    }
                               let openShift = scheduleLayout.openShift!
                               let request = scheduleLayout.request!
                               let schedule = (scheduleLayout.assigned! && scheduleLayout.available!)
                               let available = scheduleLayout.available!
                               if schedule && celldate == layoutDateString {
                
                                   diyCell.shift1.isHidden = true
                                   diyCell.shift2.isHidden = true
//                                   diyCell.ws1.isHidden = false
                                   diyCell.shift3.isHidden = true
                                   diyCell.bgImgView.layer.cornerRadius = 0.0
                                   diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                                   diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                   diyCell.bgImgView.layer.shadowRadius = 0
                                   diyCell.bgImgView.layer.shadowOpacity = 0
                                   if celldate == layoutDateString && self.gregorian.isDateInToday(layoutDate) {
                                       //if dateVal >
                                       diyCell.bgImgView.image = UIImage(named: "current_current")
                                       
                                       
                                       if shiftName.lowercased() == "night shift" {
                                           diyCell.shift3.isHidden = false
                                           diyCell.shift3.setImage(UIImage(named: "night"), for: .normal)
                                       }else  if shiftName.lowercased() == "day shift" {
                                           diyCell.shift3.isHidden = false
                                           diyCell.shift3.setImage(UIImage(named: "dayshift"), for: .normal)
                                           
                                       }else  if shiftName.lowercased() == "swing shift" {
                                           diyCell.shift3.isHidden = false
                                           diyCell.shift3.setImage(UIImage(named: "swing"), for: .normal)
                                       }
                                       
                                       
                                   }else if Date() < layoutDate && celldate == layoutDateString  {
                                       diyCell.bgImgView.image = UIImage(named: "current_future")
           //                            diyCell.shift1.isHidden = true
           //                            diyCell.shift2.isHidden = true
           //                            diyCell.ws1.isHidden = false
           //                            diyCell.shift3.isHidden = true
           //                            diyCell.bgImgView.layer.cornerRadius = 0.0
           //                            diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
           //                            diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
           //                            diyCell.bgImgView.layer.shadowRadius = 0
           //                            diyCell.bgImgView.layer.shadowOpacity = 0
                                       
                                       if shiftName.lowercased() == "night shift" {
                                           diyCell.shift3.isHidden = false
                                           diyCell.shift3.setImage(UIImage(named: "night"), for: .normal)
                                       }else  if shiftName.lowercased() == "day shift" {
                                           diyCell.shift3.isHidden = false
                                           diyCell.shift3.setImage(UIImage(named: "dayshift"), for: .normal)
                                           
                                       }else  if shiftName.lowercased() == "swing shift" {
                                           diyCell.shift3.isHidden = false
                                           diyCell.shift3.setImage(UIImage(named: "swing"), for: .normal)
                                       }
                                       
                                       
                                   }else if Date() > layoutDate && celldate == layoutDateString {
                                       diyCell.bgImgView.image = UIImage(named: "current_past")
                                   
           //                            diyCell.shift1.isHidden = true
           //                            diyCell.shift2.isHidden = true
           //                            diyCell.ws1.isHidden = false
           //                            diyCell.shift3.isHidden = true
           //                            diyCell.bgImgView.layer.cornerRadius = 0.0
           //                            diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
           //                            diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
           //                            diyCell.bgImgView.layer.shadowRadius = 0
           //                            diyCell.bgImgView.layer.shadowOpacity = 0
                                       
                                       if shiftName.lowercased() == "night shift" {
                                           diyCell.shift3.isHidden = false
                                           diyCell.shift3.setImage(UIImage(named: "night"), for: .normal)
                                       }else if shiftName.lowercased() == "day shift" {
                                           diyCell.shift3.isHidden = false
                                           diyCell.shift3.setImage(UIImage(named: "dayshift"), for: .normal)
                                           
                                       }else if shiftName.lowercased() == "swing shift" {
                                           diyCell.shift3.isHidden = false
                                           diyCell.shift3.setImage(UIImage(named: "swing"), for: .normal)
                                       }
                                       
                                       
                                   }
                               }else if openShift  && celldate == layoutDateString {
                                   diyCell.bgImgView.image = UIImage(named: "select")
                                   diyCell.shift1.isHidden = true
                                   diyCell.shift2.isHidden = true
                                   diyCell.shift3.isHidden = true
                                   diyCell.ws1.isHidden = true
                                   
                                   diyCell.bgImgView.layer.cornerRadius = 0.0
                                   diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                                   diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                   diyCell.bgImgView.layer.shadowRadius = 0
                                   diyCell.bgImgView.layer.shadowOpacity = 0
                               }else if request && celldate == layoutDateString {
                                   diyCell.bgImgView.image = UIImage(named: "current_future")
                                  // diyCell.bgImgView.image = UIImage(named: "select")
                                   diyCell.shift1.isHidden = true
                                   diyCell.shift2.isHidden = true
                                   diyCell.shift3.isHidden = false
                                  // diyCell.shift3.isHidden = true
                                   diyCell.ws1.isHidden = true
//                                   diyCell.bgImgView.layer.cornerRadius = 0.0
//                                    diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
//                                    diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//                                    diyCell.bgImgView.layer.shadowRadius = 0
//                                    diyCell.bgImgView.layer.shadowOpacity = 0
                                diyCell.bgImgView.backgroundColor = UIColor.white
                                diyCell.bgImgView.layer.cornerRadius = 10.0
                                diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                                diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                diyCell.bgImgView.layer.shadowRadius = 2.0
                                diyCell.bgImgView.layer.shadowOpacity = 0.4
                                    
                                   
                               }else if available  && celldate == layoutDateString {
                                   diyCell.bgImgView.image = UIImage(named: "select")
                                   diyCell.shift1.isHidden = true
                                   diyCell.shift2.isHidden = true
                                   diyCell.shift3.isHidden = true
                                   diyCell.ws1.isHidden = true
                                
                                   diyCell.bgImgView.layer.cornerRadius = 0.0
                                   diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                                   diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                   diyCell.bgImgView.layer.shadowRadius = 0
                                   diyCell.bgImgView.layer.shadowOpacity = 0
                               }
                           }

                       }//else {
                           if self.calendarNavigation == .fromSetAvailability {
                               if self.isStartDateSelection == false {
                                   let key = self.dateFormatter2.string(from: date)
                                   if key ==  AvailabilityVCVM.shared.startDate {
                                       diyCell.bgImgView.image = UIImage(named: "select")
                                       diyCell.shift1.isHidden = true
                                       diyCell.shift2.isHidden = true
                                       diyCell.shift3.isHidden = true
                                       diyCell.ws1.isHidden = true
                                   }else {
                                    if self.scheduleLayoutArr != nil && self.scheduleLayoutArr?.count ?? 0 > 0  {
                                    }else {
                                       diyCell.bgImgView.image = UIImage(named: "")
                                       diyCell.shift1.isHidden = true
                                       diyCell.shift2.isHidden = true
                                       diyCell.shift3.isHidden = true
                                       diyCell.ws1.isHidden = true
                                       
                                       diyCell.bgImgView.backgroundColor = UIColor.white
                                       diyCell.bgImgView.layer.cornerRadius = 10.0
                                       diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                                       diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                       diyCell.bgImgView.layer.shadowRadius = 2.0
                                       diyCell.bgImgView.layer.shadowOpacity = 0.4
                                    }
                                   }
                               }else {
                                 if self.scheduleLayoutArr != nil && self.scheduleLayoutArr?.count ?? 0 > 0  {
                                 }else {
                                    diyCell.bgImgView.image = UIImage(named: "")
                                    diyCell.shift1.isHidden = true
                                    diyCell.shift2.isHidden = true
                                    diyCell.shift3.isHidden = true
                                    diyCell.ws1.isHidden = true
                                    
                                    diyCell.bgImgView.backgroundColor = UIColor.white
                                    diyCell.bgImgView.layer.cornerRadius = 10.0
                                    diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                                    diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                    diyCell.bgImgView.layer.shadowRadius = 2.0
                                    diyCell.bgImgView.layer.shadowOpacity = 0.4
                                }
                                   
                               }
                }
//                           }else {
//                           diyCell.bgImgView.image = UIImage(named: "")
//                           diyCell.shift1.isHidden = true
//                           diyCell.shift2.isHidden = true
//                           diyCell.shift3.isHidden = true
//                           diyCell.ws1.isHidden = true
//
//                           diyCell.bgImgView.backgroundColor = UIColor.white
//                           diyCell.bgImgView.layer.cornerRadius = 10.0
//                           diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
//                           diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//                           diyCell.bgImgView.layer.shadowRadius = 2.0
//                           diyCell.bgImgView.layer.shadowOpacity = 0.4
//                           }
                     //  }
                if self.selectedDatesArr.count > 0 {
                    let key = self.dateFormatter2.string(from: date)
                       if self.selectedDatesArr.contains(key) {
                            diyCell.bgImgView.image = UIImage(named: "select")
                            diyCell.shift1.isHidden = true
                            diyCell.shift2.isHidden = true
                            diyCell.shift3.isHidden = true
                            diyCell.ws1.isHidden = true
                        diyCell.bgImgView.layer.cornerRadius = 0.0
                        diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                        diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                        diyCell.bgImgView.layer.shadowRadius = 0
                        diyCell.bgImgView.layer.shadowOpacity = 0
                        }

                }

                      }else  if position == .previous { // previous month dates in current page
                          diyCell.titleLabel.textColor = UIColor.lightGray//sr
                          diyCell.bgImgView.image = UIImage(named: "")
                          diyCell.shift1.isHidden = true
                          diyCell.shift2.isHidden = true
                          diyCell.shift3.isHidden = true
                          diyCell.ws1.isHidden = true
                          
                          diyCell.bgImgView.backgroundColor = UIColor.white
                          diyCell.bgImgView.layer.cornerRadius = 10.0
                          diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                          diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                          diyCell.bgImgView.layer.shadowRadius = 2.0
                          diyCell.bgImgView.layer.shadowOpacity = 0.4
                      }
                      else  if position == .next { // next month dates in current page

                       diyCell.titleLabel.textColor = UIColor.clear
                                 // diyCell.bgImgView.isHidden = true
                                    diyCell.bgImgView.image = UIImage(named: "")
                                     diyCell.shift1.isHidden = true
                                     diyCell.shift2.isHidden = true
                                     diyCell.ws1.isHidden = true
                                     diyCell.shift3.isHidden = true
                                  diyCell.bgImgView.layer.cornerRadius = 0.0
                                                     diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                                                     diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                                     diyCell.bgImgView.layer.shadowRadius = 0
                                                     diyCell.bgImgView.layer.shadowOpacity = 0
                         
                      }

        self.setCalenderScheduleIcons(date: date, cell: cell)
       }
    
    private func setCalenderScheduleIcons(date: Date, cell: FSCalendarCell) {
           
           let dobFomat = DateFormatter()
           dobFomat.dateFormat = "yyyy-MM-dd"
            cell.numberoFworkSpaces(withCode: nil)
           if let schedules = self.scheduleLayoutArr {
               
               let filarr = schedules.filter({$0.onDate == dobFomat.string(from: date)})
               var colors = [UIColor]()
               for  obj in  filarr {
                   if let worksiteSettings = obj.worksiteSettings {
                       
                       if let color = worksiteSettings.color {
    
                           colors.append(color.hexStringToUIColor())
                       }
                   }
               }
               
                cell.numberoFworkSpaces(withCode: colors)
           }
       }
    func getAllRequestForUser() {
          Progress.shared.showProgressView()

          ScheduleAPI().getAllSchedulesForuser(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", successHandler: { (scheduleLayout) in
              DispatchQueue.main.async {
                  self.scheduleLayoutArr = scheduleLayout
                 // self.datesRange()
                Progress.shared.hideProgressView()
                   self.configureVisibleCells()
              }
              
          }) { (message) in
                DispatchQueue.main.async {
                  Progress.shared.hideProgressView()
                }
          }


    }
    @IBAction func nextBtnTapped(_ sender: Any) {
        switch self.calendarNavigation {
        case .fromEditRequest:
            if self.selectedDatesArr.count > 0 {
                CreateReqParams.createReqParams.dates = self.selectedDatesArr
                CreateReqParams.createReqParams.datesSelectedForEdit = true
            }
             self.dismiss(animated: true, completion: nil)
        case .fromSetAvailability:
            if self.isStartDateSelection {
                self.setAvailabilityStartDate()
            }else {
                self.setAvailabilityEndDate()
            }
             self.dismiss(animated: true, completion: nil)
        
        default:
            if self.selectedDatesArr.count > 0 {
                let date = self.dateFormatter2.string(from: self.selectedDate ?? Date())
                CreateReqParams.createReqParams.dates = self.selectedDatesArr
                switch self.requestType {
                case .swap:
                     let chooseShiftVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseShiftVC") as! ChooseShiftVC
                           chooseShiftVC.selectedShiftTye = self.selectedShiftType
                     
                            self.navigationController?.pushViewController(chooseShiftVC, animated: true)
                    case .callOff:
                                let notesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
                                   //   chooseShiftVC.selectedShiftTye = self.selectedShiftType
                               
                                notesVC.requestType = .callOff
                                       self.navigationController?.pushViewController(notesVC, animated: true)
                    case .personal:
                                           let notesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
                                             
                                           notesVC.requestType = .personal
                                                  self.navigationController?.pushViewController(notesVC, animated: true)
                    case .vacation:
                    let notesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
                    notesVC.requestType = .vacation
                           self.navigationController?.pushViewController(notesVC, animated: true)
                default:
//                     let chooseShiftVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseShiftVC") as! ChooseShiftVC
//                           chooseShiftVC.selectedShiftTye = self.selectedShiftType
//                            self.navigationController?.pushViewController(chooseShiftVC, animated: true)
                    if(self.selectedShiftType == .userShiftstoSwap){
                     let chooseShiftVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseShiftVC") as! ChooseShiftVC
                           chooseShiftVC.selectedShiftTye = self.selectedShiftType
                        chooseShiftVC.selectedDate = self.dateFormatter2.string(from: self.selectedDate ?? Date())
                        self.navigationController?.pushViewController(chooseShiftVC, animated: true)}
                    else{
                    let chooseShiftVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseShiftVC") as! ChooseShiftVC
                          chooseShiftVC.selectedShiftTye = self.selectedShiftType
                       chooseShiftVC.selectedDate = self.dateFormatter2.string(from: self.selectedDate ?? Date())
                       self.navigationController?.pushViewController(chooseShiftVC, animated: true)}
                    
                }
            }else {
                self.view.makeToast("Please choose dates", duration: 2.0, position: .center)
                return
            }
        }
//        if self.isFromEditReqScreen {
//            if self.selectedDatesArr.count > 0 {
//                CreateReqParams.createReqParams.dates = self.selectedDatesArr
//            }
//             self.dismiss(animated: true, completion: nil)
//        }else {
//            if self.selectedDatesArr.count > 0 {
//                       let date = self.dateFormatter2.string(from: self.selectedDate ?? Date())
//                       CreateReqParams.createReqParams.dates = self.selectedDatesArr
//                       switch self.requestType {
//                       case .swap:
//                            let chooseShiftVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseShiftVC") as! ChooseShiftVC
//                                  chooseShiftVC.selectedShiftTye = self.selectedShiftType
//                                   self.navigationController?.pushViewController(chooseShiftVC, animated: true)
//                           case .sick:
//                                       let notesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
//                                          //   chooseShiftVC.selectedShiftTye = self.selectedShiftType
//
//                                       notesVC.requestType = .sick
//                                              self.navigationController?.pushViewController(notesVC, animated: true)
//                           case .personal:
//                                                  let notesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
//
//                                                  notesVC.requestType = .personal
//                                                         self.navigationController?.pushViewController(notesVC, animated: true)
//                           case .vacation:
//                           let notesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNotesVC") as! AddNotesVC
//                           notesVC.requestType = .vacation
//                                  self.navigationController?.pushViewController(notesVC, animated: true)
//                       default:
//                            let chooseShiftVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseShiftVC") as! ChooseShiftVC
//                                  chooseShiftVC.selectedShiftTye = self.selectedShiftType
//                                   self.navigationController?.pushViewController(chooseShiftVC, animated: true)
//                       }
//                   }else {
//                       self.view.makeToast("Please choose dates", duration: 2.0, position: .center)
//                       return
//                   }
//        }
       
        
        
    }
    @IBAction func filterBtnTapped(_ sender: Any) {
    }
}
extension Date {
    
    static func dates(from fromDate: Date, to toDate: Date) -> [String] {
        var dateFormatter2: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
        var dates: [String] = []
        var date = fromDate
        
        while date <= toDate {
            let dateString = dateFormatter2.string(from: date)
            dates.append(dateString)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
}
