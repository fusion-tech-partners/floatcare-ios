//
//  chooseDepartmentCell.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/27/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

class ChooseDepartmentCell: UITableViewCell{
  
    typealias SwitchChanged = (_ sender : UISwitch) -> ()
    
    @IBOutlet weak var workspacesSwitch: UISwitch!
    @IBOutlet weak var titleLbl: UILabel!
    var switchChnged : SwitchChanged?
    override func awakeFromNib() {
    super.awakeFromNib()
         setupUi()
        titleLbl.text = "Choose all Workspaces"
       
    }
    @IBAction func chooseAllDepartments(_ sender: UISwitch) {
        switchChnged?(sender)
    }
    func setupUi(){
        titleLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 16.0)
        titleLbl.textColor = UIColor .primaryColor
    }
}
