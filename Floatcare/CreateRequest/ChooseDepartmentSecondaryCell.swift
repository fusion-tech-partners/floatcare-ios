//
//  ChooseDepartmentSecondaryCell.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/27/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
protocol SelectedDepartmentDelegate {
    func clickedonDepartment()
}
class ChooseDepartmentSecondaryCell: UITableViewCell{
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var colorView: UIImageView!
    @IBOutlet weak var branchNameLbl: UILabel!
    @IBOutlet weak var branchAdresLbl: UILabel!
    @IBOutlet weak var departmentTable: UITableView!
    
    var delegate : SelectedDepartmentDelegate?
    var workSiteId : String?
    var workSiteColorCode : UIColor?
    var deptsCount : [DepartMentWorkSites]?
    
    override func awakeFromNib() {
    super.awakeFromNib()
        setUpUi()
       
    }
        
    func setUpUi(){
        
        departmentTable.delegate = self;
        departmentTable.dataSource = self;
        colorView.layer.cornerRadius = 5.0
        cellBackgroundView.layer.cornerRadius = 5.0
        branchNameLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 16.0)
        branchAdresLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
 
        branchNameLbl.textColor = UIColor.primaryColor
        branchAdresLbl.textColor = UIColor.descriptionColor
        cellBackgroundView.layer.shadowColor = UIColor.gray.cgColor
        cellBackgroundView.layer.shadowOffset = CGSize(width: 3, height: 3)
        cellBackgroundView.layer.shadowOpacity = 0.8
        
    }
}

extension ChooseDepartmentSecondaryCell : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deptsCount?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DepartmentCell") as! DepartmentCell
        let dept = deptsCount?[indexPath.row]
        cell.headingTitleLbl.text = dept?.departmentTypeName
        cell.subTitleLbl.text = "\((dept?.staffRoleByUserId) ?? "") : \((dept?.fteStatusOfStaffUser) ?? "")"
        cell.descriptionLbl.text = dept?.description
        cell.workSiteId = self.workSiteId
        cell.workSiteColorCode = self.workSiteColorCode
        if let sel = ChooseDepartmentUtility.shared.arryOfselectedWS.filter({($0.department?.departmentId == dept?.departmentId) && ($0.worksiteId == dept?.businessId)}).last{
            if sel.isDepartmentSelected == true {
                cell.departmentButn.setTitle("Department Choosen", for: UIControl.State.normal)
                cell.departmentButn.backgroundColor = UIColor.lightGray
            }else{
                cell.departmentButn.setTitle("Choose Department", for: UIControl.State.normal)
                cell.departmentButn.backgroundColor = #colorLiteral(red: 0.3607843137, green: 0.4392156863, blue: 0.7764705882, alpha: 1)
            }
        }
        cell.addDetartmentClick = { sender in
            if let sel = ChooseDepartmentUtility.shared.arryOfselectedWS.filter({($0.department?.departmentId == dept?.departmentId) && ($0.worksiteId == dept?.businessId)}).last{
                if sel.isDepartmentSelected == true{
                     sel.isDepartmentSelected = false
                }else{
                     sel.isDepartmentSelected = true
                }
                self.departmentTable.reloadData()
            }
            self.delegate?.clickedonDepartment()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 1, alpha: 1)
        return vw;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
}
