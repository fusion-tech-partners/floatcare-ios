
//
//  ChooseDepartmentUtility.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 05/11/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ChooseDepartmentUtility {
    static let shared = ChooseDepartmentUtility()
    var arryOfselectedWS = [SelectedWSList]()
}

class SelectedWSList {
    var colorCode : UIColor?
    var worksiteId : String?
    var orgName : String?
    var department : DepartMentWorkSites?
    var isDepartmentSelected : Bool?
}
