//
//  ChooseDepartmentVC.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/27/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//


class WorksiteVM {
    var departmentsArr = [DepartMentWorkSites]()
    var workSiteDetails = WorkSiteSettings()
}
class OrgVM {
    var organisationDetails = OrganizationsWorkSites()
    var arrayOfWorkSites = [WorksiteVM]()
}

class ChooseDepartmentVM {
    var orgsArray : [OrgVM]?
    init() {
        self.orgsArray = [OrgVM]()
        let orgIDsarr = Array(NSSet.init(array: (Profile.shared.userWorksiteSettings?.getWorksiteSettingsByUserId?.map({$0.organizationId}))!)) as? Array<String>
        for orgId in orgIDsarr ?? [String]() {
            let filArray = Profile.shared.userWorksiteSettings?.getWorksiteSettingsByUserId?.filter({$0.organizationId == orgId})
            let orgObj = OrgVM()
            orgObj.organisationDetails = (filArray?.last?.organizationDetails)!
            
            for wsSettingObj in filArray!{
                let wsiteObj = WorksiteVM()
                wsiteObj.workSiteDetails = wsSettingObj
             
                wsiteObj.departmentsArr = wsSettingObj.departmentDetails!
                orgObj.arrayOfWorkSites.append(wsiteObj)
            }
            self.orgsArray?.append(orgObj)
        }
    }
}



import Foundation

class ChooseDepartmentVC: UIViewController, UITableViewDelegate,UITableViewDataSource{
   
    @IBOutlet weak var navigationBar: UIButton!
    
     var requestType : RequestTypes?
    @IBOutlet weak var headingTitleLbl: UILabel!
    @IBOutlet weak var departmentsTblView: UITableView!
    @IBOutlet weak var nextBtn: ButtonViolet!
    private var dateCellExpanded: Bool = false
    var selectedIndex : IndexPath?
    var isCollapse = false

    var vmObj = ChooseDepartmentVM.init()
    override func viewDidLoad() {
       super.viewDidLoad()
       setupUI()
       self.formAllDepartmentsArray()
        
    }
    func setupUI(){
      headingTitleLbl.font = UIFont(font: .Athelas, weight: .regular, size: 32)
     headingTitleLbl.textColor = UIColor.primaryColor
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1 + (vmObj.orgsArray?.count ?? 0)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return vmObj.orgsArray?[section-1].arrayOfWorkSites.count ?? 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0{
            return 40
        }
        if self.selectedIndex == indexPath && isCollapse == true{

            return CGFloat(120 + (180 * (vmObj.orgsArray?[indexPath.section-1].arrayOfWorkSites[indexPath.row].departmentsArr.count ?? 0) ))


//            return CGFloat(100 + (220 * (vmObj.orgsArray?[indexPath.section-1].arrayOfWorkSites[indexPath.row].departmentsArr.count ?? 0) ))
        }else{
            return 90
        }
    }
    
          
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "chooseDepartment", for: indexPath) as! ChooseDepartmentCell
            cell.switchChnged = { sender in
                self.allDepartmentsSelected(selectSwitch: sender)
            }
            var isOn = true
            if ChooseDepartmentUtility.shared.arryOfselectedWS.filter({$0.isDepartmentSelected == false}).last != nil {
                isOn = false
            }
//            if let ob = ChooseDepartmentUtility.shared.arryOfselectedWS.filter({$0.isDepartmentSelected == false}).last {
//                           isOn = false
//                       }
            cell.workspacesSwitch.setOn(isOn, animated: false)
            
            
            return cell
        }else{
           let cellSec = tableView.dequeueReusableCell(withIdentifier: "chooseDepartmentSecondaryCell", for: indexPath) as! ChooseDepartmentSecondaryCell
            let wsObj = vmObj.orgsArray?[indexPath.section-1].arrayOfWorkSites[indexPath.row]
            cellSec.branchNameLbl.text = vmObj.orgsArray?[indexPath.section-1].organisationDetails.city
            cellSec.branchAdresLbl.text = "\((vmObj.orgsArray?[indexPath.section-1].organisationDetails.address)!) \((vmObj.orgsArray?[indexPath.section-1].organisationDetails.state)!)"
            cellSec.deptsCount = wsObj?.departmentsArr
            cellSec.colorView.backgroundColor = wsObj?.workSiteDetails.color?.hexStringToUIColor()
            cellSec.workSiteId = wsObj?.workSiteDetails.worksiteId
            cellSec.workSiteColorCode = wsObj?.workSiteDetails.color?.hexStringToUIColor()
            cellSec.departmentTable.reloadData()
            cellSec.delegate = self
        return cellSec
        }
    }
    func formAllDepartmentsArray() {
        ChooseDepartmentUtility.shared.arryOfselectedWS.removeAll()
        for org in vmObj.orgsArray! {
            for wspace in org.arrayOfWorkSites {
                for dept in wspace.departmentsArr{
                    let obj = SelectedWSList()
                    obj.colorCode = wspace.workSiteDetails.color?.hexStringToUIColor()
                    obj.worksiteId = wspace.workSiteDetails.worksiteId
                    obj.department = dept
                    obj.orgName = org.organisationDetails.name
                    obj.isDepartmentSelected = false
                    ChooseDepartmentUtility.shared.arryOfselectedWS.append(obj)
                }
            }
        }
    }
    
    func allDepartmentsSelected(selectSwitch:UISwitch)  {
        for obj in ChooseDepartmentUtility.shared.arryOfselectedWS {
            if selectSwitch.isOn{
                obj.isDepartmentSelected = true
            }else{
                obj.isDepartmentSelected = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section > 0){
            let cell = tableView.cellForRow(at: indexPath) as! ChooseDepartmentSecondaryCell
//            let wsObj = vmObj.orgsArray?[indexPath.section-1].arrayOfWorkSites[indexPath.row]
//            cell.deptsCount = wsObj?.departmentsArr
//            cell.departmentTable.reloadData()
            if selectedIndex == indexPath
            {
                if self.isCollapse == false{
                    self.isCollapse = true
                }else{
                    self.isCollapse = false
                }
            }else{
                self.isCollapse = true
            }
            self.selectedIndex = indexPath
            departmentsTblView.reloadRows(at: [indexPath], with: .automatic)
            departmentsTblView.scrollToRow(at: indexPath, at: .top, animated: true)
//            departmentsTblView.reloadData()
        }

   }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
//    {
//        if(section == 0){
//            return UIView()
//        }else{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell
//            cell.headerTitle.text = vmObj.orgsArray?[section-1].organisationDetails.name
//            return cell.contentView
//        }
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(section == 0){
            return UIView()
       }else{
          let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell
          cell.headerTitle.text = vmObj.orgsArray?[section-1].organisationDetails.name
          return cell.contentView

        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 60
    }
    @IBAction func backButtonTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func nextBtnClick(_ sender: Any) {
        guard ChooseDepartmentUtility.shared.arryOfselectedWS.filter({$0.isDepartmentSelected == true}).last != nil else {
            ReusableAlertController.showAlert(title: "Floatcare", message: "Please select atlease one department")
            return
        }
        switch self.requestType {
        case .callOff:
        
        let crChooseDates = self.storyboard?.instantiateViewController(withIdentifier: "CRChooseDatesVC") as! CRChooseDatesVC
           crChooseDates.requestType = .callOff
          self.navigationController?.pushViewController(crChooseDates, animated: true)
        case .none: break
            
        case .swap:
            let swapShifts = self.storyboard?.instantiateViewController(withIdentifier: "SwapShiftVC") as! SwapShiftVC
            //swapShifts.requestType = .swap
                     self.navigationController?.pushViewController(swapShifts, animated: true)
            
        case .some(.personal): break
            
        case .some(.availability): break
            
        case .some(.giveAway): break
            
        case .vacation:
            let crChooseDates = self.storyboard?.instantiateViewController(withIdentifier: "CRChooseDatesVC") as! CRChooseDatesVC
             crChooseDates.requestType = .vacation
            crChooseDates.calendarNavigation = .fromVacation
            self.navigationController?.pushViewController(crChooseDates, animated: true)
            
        }
}
}


extension ChooseDepartmentVC : SelectedDepartmentDelegate{
    func clickedonDepartment() {
        DispatchQueue.main.async {
            self.departmentsTblView.reloadData()
        }
    }
    
    
}
