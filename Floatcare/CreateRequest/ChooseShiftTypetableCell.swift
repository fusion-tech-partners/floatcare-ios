//
//  ChooseShiftTypetableCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 08/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ChooseShiftTypetableCell: UITableViewCell {

    @IBOutlet weak var shiftTypeCV: UICollectionView!
    @IBOutlet weak var noOgColleguesLbl: UILabel!
    @IBOutlet weak var colleguesCV: UICollectionView!
    @IBOutlet weak var colleguesBgView: UIView!
    @IBOutlet weak var wsDetailLbl: UILabel!
    @IBOutlet weak var wsNameLbl: UILabel!
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var cornerBgView: CornorShadowView!
    var swapShiftObj : swapSchedules?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.colleguesCV.dataSource = self
        let cellSize = CGSize(width:28 , height:28)

               let layout = UICollectionViewFlowLayout()
               layout.scrollDirection = .vertical //.horizontal
               layout.itemSize = cellSize
               layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
               layout.minimumLineSpacing = 1.0
               layout.minimumInteritemSpacing = -10
               self.colleguesCV.setCollectionViewLayout(layout, animated: true)
        
         self.shiftTypeCV.dataSource = self
        self.shiftTypeCV.delegate = self
        let cellSize1 = CGSize(width:70 , height:28)
        self.shiftTypeCV.register(UINib.init(nibName:"ShiftCVCell", bundle: nil), forCellWithReuseIdentifier: "shiftCVCell")
               let flowLayout = UICollectionViewFlowLayout()
               flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = cellSize1
       // layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        flowLayout.minimumLineSpacing = 10.0
        flowLayout.minimumInteritemSpacing = 10
        self.shiftTypeCV.collectionViewLayout = flowLayout
        
    }
    func setupCell(swapShift: swapSchedules?) {
           // let dateOrg = ()!
//            guard let date = openshift?.onDate,
//                let startTime = openshift?.startTime,
//                let endTime = openshift?.endTime else { return}
        if SwapShiftUtility.shared.isFirstSwapShift {
            self.wsNameLbl.text = swapShift?.worksiteName ?? ""
            self.noOgColleguesLbl.text = "\(swapShift?.workingColleagues?.count ?? 0) collegues are working here"
            self.pinkView.backgroundColor = swapShift?.worksiteColor?.hexStringToUIColor()
            self.wsDetailLbl.text = swapShift?.worksiteDetails?.address ?? ""
        }else{
            self.wsNameLbl.text = "\(swapShift?.userDetails?.firstName ?? "") \(swapShift?.userDetails?.lastName ?? "")"
            self.noOgColleguesLbl.text = "\((swapShift?.worksiteName!)!)"
            self.pinkView.backgroundColor = swapShift?.worksiteColor?.hexStringToUIColor()
            self.wsDetailLbl.text = swapShift?.worksiteDetails?.address ?? ""
        }
//            self.shiftNameLabel.text = openshift?.staffUserName
//            self.timingsLabel.text = "\(date.covertDateToReadableFormat)\n\(startTime) - \(endTime)"
//            self.numberOfWorkersLabel.text = "Friends Worked here"
            
          //  if let imgStr = (amazonURL + (openshift?.organization?.logo ?? "")).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                       
    //                self.iconImageView?.sd_setImage(with: URL(string: imgStr), placeholderImage: UIImage(named: "placeholder.png"))
            self.swapShiftObj = swapShift
        }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }

}
extension ChooseShiftTypetableCell: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.colleguesCV:
            return self.swapShiftObj?.workingColleagues?.count ?? 0
            case self.shiftTypeCV:
            return 1
        default:
             return 4
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case self.colleguesCV:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkerCell", for: indexPath) as! WorkerCell
             return cell
            case self.shiftTypeCV:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shiftCVCell", for: indexPath as IndexPath) as! ShiftCVCell
             cell.bgView.layer.shadowRadius = 1
            cell.shiftNameLbl.text = "\(self.swapShiftObj?.startTime ?? "") - \(self.swapShiftObj?.endTime ?? "")"
            
            return cell
        default:
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkerCell", for: indexPath) as! WorkerCell
             return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 120, height: 30)
    }
        
}
