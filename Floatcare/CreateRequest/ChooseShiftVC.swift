//
//  ChooseShiftVC.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 08/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit


class ChooseShiftVC: UIViewController {
     private let viewModel: FetchSwapDetails = FetchSwapDetails()
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var reqShiftBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var shiftTypesTblView: UITableView!
    var selectedShiftTye : SelectedShiftType = .userShiftstoSwap
     var requestType : RequestTypes?
     var schedulesArr : [swapSchedules]?
    var selectedDate = ""

    override func viewDidLoad() {
        super.viewDidLoad()
       
          
        // Do any additional setup after loading the view.
        reqShiftBtn.layer.cornerRadius = 6.0
        reqShiftBtn.alpha = 0.5
               reqShiftBtn.backgroundColor = .primaryColor
               bottomView.roundCorners([.layerMinXMinYCorner], radius: 40)
         topView.layer.cornerRadius = 40
         topView.layer.maskedCorners = [.layerMinXMaxYCorner]
    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(true)
        
        if self.selectedShiftTye == .userShiftstoSwap{
            self.viewModel.findUserSchedulesOnDate(userId: Profile.shared.userId, onDate:selectedDate) { (shifts) in
                self.schedulesArr = shifts
                self.shiftTypesTblView.reloadData()
            }
        }else if self.selectedShiftTye == .selectedShiftForSwap{
            self.viewModel.findSchedulesByDepartmentIdAndUserId(userId: Profile.shared.userId, onDate:selectedDate , departmentId: SwapShiftUtility.shared.firstSwapShift!.departmentId!) { (shifts) in
                self.schedulesArr = shifts
                self.shiftTypesTblView.reloadData()
            }
        }
        
        
       }
    @IBAction func reqShiftBtnTapped(_ sender: Any) {
    }
    

    @IBAction func filterBtnTapped(_ sender: Any) {
    }
    @IBAction func searchBtnTapped(_ sender: Any) {
    }
    @IBAction func backBtnTapped(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
}
extension ChooseShiftVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 172
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        schedulesArr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cShiftTypeCell", for: indexPath) as! ChooseShiftTypetableCell
        
        cell.selectionStyle = .none
        cell.wsNameLbl.text = schedulesArr?[indexPath.row].worksiteName
        let swapShift = schedulesArr?[indexPath.row]
        cell.setupCell(swapShift: swapShift)
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //         let swapShiftVC = self.storyboard?.instantiateViewController(withIdentifier: "SwapShiftVC") as! SwapShiftVC
        //        swapShiftVC.shiftSelected = true
        //        self.navigationController?.popToViewController ((self.navigationController?.viewControllers[1]) as! SwapShiftVC, animated: true)
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SwapShiftVC.self) {
                let swapVC = controller as! SwapShiftVC
                swapVC.shiftToSwap = true
                swapVC.selectedShiftType = self.selectedShiftTye
                
                if SwapShiftUtility.shared.isFirstSwapShift == true{
                    SwapShiftUtility.shared.firstSwapShift = schedulesArr?[indexPath.row]
                }else{
                    SwapShiftUtility.shared.secondSwapShift = schedulesArr?[indexPath.row]
                    SwapShiftUtility.shared.seletcedSwapUserIndex = indexPath.row
                }
                _ =  self.navigationController!.popToViewController(swapVC, animated: true)
                break
            }
        }
        
        // self.navigationController?.popToViewController(swapShiftVC, animated: true)
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
