//
//  CreateRequest.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 30/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation


struct CreateARequestResponse: Codable {
    
    var createARequest: Requests?
}
struct UpdateARequestResponse: Codable {
    
    var updateARequest: Requests?
}
struct Requests : Codable {
    var requestId : String?
   var worksiteId: String?
   var worksiteName: String?
   var departmentId: String?
    var requestTypeId: String?
    var requestTypeName: String?
   var userId: String?
   var onDate:  [String]?
   var shiftId: String?
   var colleagueId: String?
   var colleagueName: String?
   var notes: String?
}

struct CreateReqParams  {
    static var createReqParams: CreateReqParams = CreateReqParams()
     var requestForUser : [Requests]?
    var notes: String = ""
     var dates = [String]()
    var request : Requests?
    var datesSelectedForEdit: Bool = false
}

class CreateRequestAPI {
    
   // To commit
    func createRequest(requestTypeId:String,wsIdBusId:String,deptID:String, completion: @escaping (Requests?) -> ()) {
        let mutation = CreateARequestMutation.init(userId: Profile.shared.userId, worksiteId: wsIdBusId/*worksiteDetails?.businessId! ?? ""*/, departmentId:deptID /*worksiteDetails?.workspaceId! ?? ""*/, requestTypeId: requestTypeId, onDate: CreateReqParams.createReqParams.dates, shiftId: "0", notes: CreateReqParams.createReqParams.notes)

        ApolloManager.shared.client.perform(mutation: mutation) { (result) in
            print("\(result)")

            guard let user = try? result.get().data?.resultMap else {
                completion(nil)
                    return
            }
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                
                let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
                print(convertedString)
                 let decoder = JSONDecoder()
                let createARequest = try decoder.decode(CreateARequestResponse.self, from: jsonData)
                print(createARequest.createARequest)
                 completion(createARequest.createARequest)
            }catch {
                completion(nil)
            }
    
        }

    }
}
class UpdateRequestAPI {
    
   // To commit
    func updateRequest(request:Requests, completion: @escaping (Requests?) -> ()) {
      //  let worksiteDetails = Profile.shared.userWorkSpaceDetails?.getWorkspaceByUserId?[0]
        let mutation = UpdateARequestMutation.init(requestId: request.requestId ?? "", userId: Profile.shared.userId, worksiteId: request.worksiteId ?? "", requestTypeId: request.requestTypeId ?? "", onDate: request.onDate ?? [""], shiftId: request.shiftId ?? "", notes: request.notes ?? "")
//        let mutation = CreateARequestMutation.init(userId: Profile.shared.userId, worksiteId: worksiteDetails?.businessId! ?? "", departmentId: worksiteDetails?.workspaceId! ?? "", requestTypeId: request.requestId, onDate: CreateReqParams.createReqParams.dates, shiftId: "0", notes: CreateReqParams.createReqParams.notes)

        ApolloManager.shared.client.perform(mutation: mutation) { (result) in
            print("\(result)")

            guard let user = try? result.get().data?.resultMap else {
                completion(nil)
                    return
            }
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                
                let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
                print(convertedString)
                 let decoder = JSONDecoder()
                let updateARequest = try decoder.decode(UpdateARequestResponse.self, from: jsonData)
                print(updateARequest.updateARequest)
                 completion(updateARequest.updateARequest)
            }catch {
                completion(nil)
            }
    
        }

    }
}
