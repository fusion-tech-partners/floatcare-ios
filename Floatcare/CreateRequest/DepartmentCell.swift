//
//  DepartmentCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 04/11/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class DepartmentCell: UITableViewCell {
    @IBOutlet weak var headingTitleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var departmentButn: UIButton!
    @IBOutlet weak var cellBackgroundView: UIView!
    
    var workSiteId : String?
    var workSiteColorCode : UIColor?
    var addDetartmentClick : actionBlock? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setUpUI()
        // Configure the view for the selected state
    }
    @IBAction func departmentButnTapped(_ sender: UIButton) {
//        departmentButn.setTitle("Department Choosen", for: UIControl.State.normal)
//        departmentButn.backgroundColor = UIColor.lightGray
        addDetartmentClick?(sender)
    }
    
    func setUpUI() {
        cellBackgroundView.layer.cornerRadius = 10.0
        cellBackgroundView.dropShadow(color: #colorLiteral(red: 0.8784313725, green: 0.8823529412, blue: 0.937254902, alpha: 1), opacity:1, offSet: .zero, radius: 10, scale: true)
        headingTitleLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 18.0)
        subTitleLbl.font = UIFont(font: .SFUIText, weight: .medium, size: 14.0)
        descriptionLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 16.0)
        
        headingTitleLbl.textColor = UIColor.primaryColor
        descriptionLbl.textColor = UIColor.appLightGray
        departmentButn.layer.cornerRadius = 10.0
    }

}

