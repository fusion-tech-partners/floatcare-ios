//
//  EditRequestVC.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 07/08/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class EditRequestVC: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var reqTblView: UITableView!
    @IBOutlet weak var notesLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var headerLbl: UILabel!
    var shiftNames  = ["Edit Request","Delete Request","Extended Sick Day"]
        var shiftDetails  = ["Update and modify your request","End your request",""]
       
       var imgArr = ["swap","give","availability","sickday","personal","vacation"]
    var requestType : RequestTypes?
    var selectedRequest : Requests?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         topView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        self.reqTblView.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setUpView()
    }

    func setUpView(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //dateFormatter.dateStyle = DateFormatter.Style.MediumStyle
        let dateObj = (dateFormatter.date(from:self.selectedRequest?.onDate?[0] ?? "") ?? nil)!
        dateFormatter.dateFormat = "MMM d,yyyy"
        let stringDate = dateFormatter.string(from:dateObj)
        self.dateLbl.text = stringDate
                //   selectedShiftView.dateLbl.text = stringDate
       // self.dateLbl.text = self.selectedRequest?.onDate?[0] ?? ""
        self.headerLbl.text = self.selectedRequest?.requestTypeName ?? ""
        self.notesLbl.text = self.selectedRequest?.notes ?? ""
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension EditRequestVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 65
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReqTypeCell", for: indexPath) as! ReqTypeTableCell
        
        
        switch indexPath.row {
        case 0,1:
            cell.imgView?.image = UIImage(named: "Basic Account")
        case 2:
            cell.imgView?.image = UIImage(named: "Add User")
        default:
            cell.imgView?.image = UIImage(named: "Basic Account")
        }
        cell.reqNameLbl.text = shiftNames[indexPath.row]
        cell.detailLbl.text = shiftDetails[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
             let storyboard = UIStoryboard(name: "RequestTypeVC", bundle: nil)
            let summaryVC = storyboard.instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
            summaryVC.notes = self.selectedRequest?.notes ?? ""
            if self.selectedRequest?.requestTypeName!.lowercased() == "call off" {
                summaryVC.requestType = .callOff
            }else if self.selectedRequest?.requestTypeName!.lowercased() == "vacation day" {
                summaryVC.requestType = .vacation
            }
            summaryVC.isFromEditReqScreen = true
             summaryVC.request = self.selectedRequest
            self.navigationController?.pushViewController(summaryVC, animated: true)
        case 1:
            self.deleteRequest()
        default:
            let summaryVC = self.storyboard?.instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
            summaryVC.notes = self.selectedRequest?.notes ?? ""
            summaryVC.requestType = .callOff
            self.navigationController?.pushViewController(summaryVC, animated: true)
        }
        
    }
    func deleteRequest() {
        let mutation = DeleteARequestMutation.init(requestId: self.selectedRequest!.requestId!)
        
        // let mutation = CreateARequestMutation.init(userId: Profile.shared.userId, worksiteId: worksiteDetails?.businessId! ?? "", departmentId: worksiteDetails?.workspaceId! ?? "", requestTypeId: requestTypeId, onDate: CreateReqParams.createReqParams.dates, shiftId: "0", notes: CreateReqParams.createReqParams.notes)
         Progress.shared.showProgressView()
        ApolloManager.shared.client.perform(mutation: mutation) {[weak self] (result) in
            print("\(result)")
            
            DispatchQueue.main.async {
                 Progress.shared.hideProgressView()
                let alert = UIAlertController(title: "FloatCare", message: "Request deleted successfully", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    self?.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification_Myrequest), object: nil, userInfo: nil)
                }))
                 self?.present(alert, animated: true, completion: nil)
            }
            
        }
        
        
        
    }
    
    
    
}
