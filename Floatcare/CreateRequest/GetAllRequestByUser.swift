//
//  GetAllRequestByUser.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 01/08/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class GetAllRequestByUserAPI {
//To commit
   func GetAllRequestByUser(userId: String = "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", completion: @escaping ([Requests]?) -> ()) {
   
       ApolloManager.shared.client.fetch(query:GetAllRequestsByUserIdQuery(userId: userId), cachePolicy: .fetchIgnoringCacheData, context: nil,queue: .main ) { result in
                       
                       guard let user = try? result.get().data?.resultMap else {
                           completion(nil)
                           return
                       }
                       
                       do {
                           let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                           let decoder = JSONDecoder()
                           let requests = try decoder.decode(AllRequestResponse.self, from: jsonData)
                        CreateReqParams.createReqParams.requestForUser = requests.getAllRequestsByUserId
                        completion(requests.getAllRequestsByUserId)

                       }catch {
                            completion(nil)
                       }
       }
   }
}
struct AllRequestResponse: Codable {
    
    var getAllRequestsByUserId: [Requests]?
}

