//
//  ReceivedSwapCell.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/29/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class ReceivedSwapCell: UITableViewCell{
    let receivedVc = ReceivedSwapVC()
    var swapShiftObj : SwapShiftRequests?
    @IBOutlet weak var imageviewPic: UIImageView!
     var swapClick: actionBlock?
    @IBOutlet weak var receivedCellBgView: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var secondView: UIView!
    
    @IBOutlet weak var profName: UILabel!
    
    @IBOutlet weak var daysLeftLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!
    
    @IBOutlet weak var departmentLbl: UILabel!
    @IBOutlet weak var profestnNameLbl: UILabel!
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var shiftTypeLbl: UILabel!
    
    @IBOutlet weak var sideColorImgView: UIImageView!
    
    @IBOutlet weak var secMonthLbl: UILabel!
    
    @IBOutlet weak var secDateLbl: UILabel!
    
    @IBOutlet weak var secDepartmentLbl: UILabel!
    
    @IBOutlet weak var secProfestnNameLbl: UILabel!
    
    @IBOutlet weak var secSideColorImgView: UIImageView!
    
    @IBOutlet weak var secShiftTypeLbl: UILabel!
    
    @IBOutlet weak var nextArrowBtn: UIButton!
    
    override func awakeFromNib() {
           super.awakeFromNib()
        
        setupUI()
           // Initialization code
       // self.nameLbl.text = "Margon Josh"
        //self.profName.text = "Nurse"
//        self.daysLeftLbl.text = "2 days left"
     //  self.monthLbl.text = "Nov"
     //   self.dateLbl.text = "15"
//        self.departmentLbl.text = "OBGYN"
//        self.profestnNameLbl.text = "Nurse"
//        self..text = "Night shift"
//
//        
     //   self.secMonthLbl.text = "Dec"
      //  self.secDateLbl.text = "15"
//        self.secDepartmentLbl.text = "OBGYN"
//        self.secProfestnNameLbl.text = "Nurse"
//        self.secShiftTypeLbl.text = "Night shift"
        let singleTap: UITapGestureRecognizer =  UITapGestureRecognizer(target: self, action: #selector(self.handleSingleTap(sender:)))
                             singleTap.numberOfTapsRequired = 1
                             self.nextArrowBtn.addGestureRecognizer(singleTap)

                             // Double Tap
                             let doubleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleDoubleTap))
                             doubleTap.numberOfTapsRequired = 2
                             self.nextArrowBtn.addGestureRecognizer(doubleTap)

                             singleTap.require(toFail: doubleTap)
                             singleTap.delaysTouchesBegan = true
                             doubleTap.delaysTouchesBegan = true

        
    }
   @objc func handleSingleTap(sender: AnyObject?) {

           self.daysLeftLbl.text = "Approved"
           print("Single Tap!")
    
    

          }
   @objc func handleDoubleTap() {
          self.daysLeftLbl.text = "Accepted"
       
           print("Double Tap!")
          }
          
    @IBAction func nextArrowBtn(_ sender: Any) {
        self.daysLeftLbl.text = "Approved"
        
    }
    func setupCell(swapShift: SwapShiftRequests?) {
    
                self.nameLbl.text = swapShift?.receiverInformation?.firstName ?? ""
        self.profName.text = swapShift?.sourceShiftInformation?.staffCoreTypeName ?? ""
        self.departmentLbl.text = swapShift?.requestedShiftInformation?.departmentName ?? ""
        self.shiftTypeLbl.text = swapShift?.requestedShiftInformation?.shiftTypeName ?? ""
        self.secShiftTypeLbl.text = swapShift?.sourceShiftInformation?.shiftTypeName ?? ""
        self.secDepartmentLbl.text = swapShift?.sourceShiftInformation?.departmentName ?? ""
        self.profName.text = swapShift?.sourceShiftInformation?.staffCoreTypeName ?? ""
        self.profestnNameLbl.text = swapShift?.sourceShiftInformation?.staffCoreTypeName ?? ""
        self.secProfestnNameLbl.text = swapShift?.sourceShiftInformation?.staffCoreTypeName ?? ""
        self.sideColorImgView.backgroundColor = swapShift?.requestedShiftInformation?.worksiteColor?.hexStringToUIColor()
        self.secSideColorImgView.backgroundColor = swapShift?.requestedShiftInformation?.worksiteColor?.hexStringToUIColor()
        

        self.dateLbl.text = getDate(string: swapShift?.createdOn ?? "")
        self.monthLbl.text = getMonthDate(string: swapShift?.createdOn ?? "")
        self.secDateLbl.text = getDate(string: swapShift?.createdOn ?? "")
        self.secMonthLbl.text = getMonthDate(string: swapShift?.createdOn ?? "")
        
      //  self.sideColorImgView.backgroundColor = sw
      // self.dateLbl.text = swapShift?.createdOn ?? ""
      // self.secDateLbl.text = swapShift?.createdOn ?? ""
        
                self.swapShiftObj = swapShift
            }
    
    func setupCell(swapSent: SwapShiftRequests?) {
    
        self.nameLbl.text = swapSent?.senderInformation?.firstName ?? ""
        self.profName.text = swapSent?.sourceShiftInformation?.staffCoreTypeName ?? ""
        self.departmentLbl.text = swapSent?.requestedShiftInformation?.departmentName ?? ""
        self.shiftTypeLbl.text = swapSent?.requestedShiftInformation?.shiftTypeName ?? ""
        self.secShiftTypeLbl.text = swapSent?.sourceShiftInformation?.shiftTypeName ?? ""
        self.secDepartmentLbl.text = swapSent?.sourceShiftInformation?.departmentName ?? ""
        self.profName.text = swapSent?.sourceShiftInformation?.staffCoreTypeName ?? ""
        self.profestnNameLbl.text = swapSent?.sourceShiftInformation?.staffCoreTypeName ?? ""
       // self.dateLbl.text = swapSent?.createdOn ?? ""
       // self.secDateLbl.text = swapSent?.createdOn ?? ""
        self.dateLbl.text = getDate(string: swapSent?.createdOn ?? "")
              self.monthLbl.text = getMonthDate(string: swapSent?.createdOn ?? "")
              self.secDateLbl.text = getDate(string: swapSent?.createdOn ?? "")
              self.secMonthLbl.text = getMonthDate(string: swapSent?.createdOn ?? "")
        self.sideColorImgView.backgroundColor = swapSent?.requestedShiftInformation?.worksiteColor?.hexStringToUIColor()
        self.secSideColorImgView.backgroundColor = swapSent?.requestedShiftInformation?.worksiteColor?.hexStringToUIColor()
         self.swapShiftObj = swapSent
        
        //self.dateLbl.text = getFormattedDate(string: swapSent?.createdOn ?? "", formatter: "d")
    }
    func getFormattedDate(string: String) -> String{
    let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
               //        dateFormatter.dateStyle = DateFormatter.Style.MediumStyle
               let dateObj = (dateFormatter.date(from:string) ?? nil)!
               dateFormatter.dateFormat = "MMM d, yyyy"
               let stringDate = dateFormatter.string(from:dateObj)
               return stringDate
    }
    func getMonthDate(string: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
           //        dateFormatter.dateStyle = DateFormatter.Style.MediumStyle
           let dateObj = (dateFormatter.date(from:string) ?? nil)!
           dateFormatter.dateFormat = "MMM"
           let stringDate = dateFormatter.string(from:dateObj)
           return stringDate
    }
    func getDate(string: String) -> String {
         let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "yyyy-mm-dd"
                  //        dateFormatter.dateStyle = DateFormatter.Style.MediumStyle
                  let dateObj = (dateFormatter.date(from:string) ?? nil)!
                  dateFormatter.dateFormat = "d"
                  let stringDate = dateFormatter.string(from:dateObj)
                  return stringDate
    }
        func setupUI(){
        self.receivedCellBgView.layer.cornerRadius = 10.0
            self.receivedCellBgView.layer.shadowColor = UIColor.gray.cgColor
        self.receivedCellBgView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.receivedCellBgView.layer.shadowOpacity = 0.8
        self.nameLbl.textColor = .primaryColor
        self.profName.textColor = .descriptionColor
        self.shiftTypeLbl.textColor = .descriptionColor
        self.secShiftTypeLbl.textColor = .descriptionColor
        
        self.nameLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 16.0)
        self.profName.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
        self.daysLeftLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
        self.monthLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 11.0)
        self.dateLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 12.0)
        self.departmentLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 14.0)
        self.profestnNameLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
        self.shiftTypeLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 12.0)
        
        self.secMonthLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 11.0)
        self.secDateLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 12.0)
        self.secDepartmentLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 14.0)
        self.secProfestnNameLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
        self.secShiftTypeLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 12.0)
        self.daysLeftLbl.layer.masksToBounds = true
        self.daysLeftLbl.layer.cornerRadius = 10.0
        self.firstView.layer.borderWidth = 2
        self.firstView.layer.borderColor = UIColor.lightGray.cgColor
        self.secondView.layer.borderWidth = 2
        self.secondView.layer.borderColor = UIColor.lightGray.cgColor
//        self.sideColorImgView.backgroundColor = UIColor.purple
//        self.secSideColorImgView.backgroundColor = UIColor.purple
        self.firstView.layer.cornerRadius = 5.0
        self.secondView.layer.cornerRadius = 5.0
        
//        self.firstView.layer.shadowColor = UIColor.gray.cgColor
//        self.firstView.layer.shadowOffset = CGSize(width: 3, height: 3)
//        self.firstView.layer.shadowOpacity = 0.8
//        self.secondView.layer.shadowColor = UIColor.gray.cgColor
//        self.secondView.layer.shadowOffset = CGSize(width: 3, height: 3)
//        self.secondView.layer.shadowOpacity = 0.8
        
        //self.profName.textColor 
        
    }

       override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           // Configure the view for the selected state
       }

}
