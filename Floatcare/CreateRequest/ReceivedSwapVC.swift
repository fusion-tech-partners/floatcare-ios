//
//  ReceivedSwapPageMenuVC.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/29/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class ReceivedSwapVC: UIViewController {
    private let viewModel: GetSwapDetails = GetSwapDetails()
    var swapRecievedArr : [SwapShiftRequests]?
    var swapSentArr : [SwapShiftRequests]?
    //var buttonClick : actionBlock?
    @IBOutlet weak var headerDateLbl: UILabel!
    @IBOutlet weak var headingTitleView: UIView!
     let button = UIButton()
           
           @IBOutlet weak var headingTitleLbl: UILabel!
           @IBOutlet weak var receivedBtn: UIButton!
    @IBOutlet weak var sentBtn: UIButton!
           @IBOutlet weak var receivedSwapTblView: UITableView!
           override func viewDidLoad() {
                  super.viewDidLoad()
            self.headerDateLbl.isHidden = true
           
            
        
                  self.setupView()
            receivedBtn.tag=1
                   receivedBtn.addTarget(self,action:#selector(buttonClicked),
                                     for:.touchUpInside)
                   sentBtn.tag=2
                   sentBtn.addTarget(self,action:#selector(buttonClicked),
                                     for:.touchUpInside)
             receivedBtn.isSelected = true
            sentBtn.isSelected = false
           
           // receivedBtn.backgroundColor =
           
            
//                if(receivedBtn.isTouchInside == true){
//                    self.viewModel.getSwapShiftRequestsRecievedByUserId(userId:SwapShiftUtility.shared.secondSwapShift!.userId!) {
//                    //SwapShiftUtility.shared.receivedSwapShift!.receiverId!
//                        (swaps) in
//                        self.swapRecievedArr = swaps
//                        self.receivedSwapTblView.reloadData()
//                  }
           
                  // Do any additional setup after loading the view.
              }
    

    @objc func buttonClicked(sender:UIButton)
    {
        switch sender.tag
        {
        case 1: self.viewModel.getSwapShiftRequestsRecievedByUserId(userId:Profile.shared.userId) {
        //SwapShiftUtility.shared.receivedSwapShift!.receiverId!
            (swaps) in
            self.swapRecievedArr = swaps
            self.receivedBtn.isSelected = true
            self.sentBtn.isSelected = false
            
            self.receivedSwapTblView.reloadData()
        }
            //when Button1 is clicked...
                break
            case 2:
                self.viewModel.getSwapShiftRequestsSentByUserId(userId:Profile.shared.userId) {
                    //SwapShiftUtility.shared.receivedSwapShift!.receiverId!
                        (swaps) in
                        self.swapSentArr = swaps
                    self.sentBtn.isSelected = true
                    self.receivedBtn.isSelected = false
                   
                        self.receivedSwapTblView.reloadData()
                }
                
                //when Button2 is clicked...
                break
        
            default:
            self.viewModel.getSwapShiftRequestsRecievedByUserId(userId:Profile.shared.userId) {
            //SwapShiftUtility.shared.receivedSwapShift!.receiverId!
                (swaps) in
                self.swapRecievedArr = swaps
                self.receivedSwapTblView.reloadData()
        }
    }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(true)
         self.viewModel.getSwapShiftRequestsRecievedByUserId(userId:Profile.shared.userId) {
          //SwapShiftUtility.shared.receivedSwapShift!.receiverId!
              (swaps) in
              self.swapRecievedArr = swaps
              self.receivedSwapTblView.reloadData()
        
    }
    
    }
    
           private func setupView() {
            
               self.headerDateLbl.layer.masksToBounds = true
               self.headerDateLbl.layer.cornerRadius = 10.0
               self.headingTitleView.roundCorners(.layerMinXMaxYCorner, radius: 30.0)
               self.headingTitleView.backgroundColor = .profileBgColor
               self.headingTitleLbl.font = UIFont(font: .Athelas, weight: .regular, size: 32)
               self.headingTitleLbl.textColor = .headingTitleColor
            
            receivedBtn.layer.cornerRadius = 10.0
            sentBtn.layer.cornerRadius = 10.0
            //receivedBtn.backgroundColor = UIColor.buttonBg
            sentBtn.backgroundColor = UIColor.clear
             self.receivedBtn.tintColor = UIColor.gray
            self.sentBtn.tintColor = UIColor.gray
               
               }
    
    @IBAction func backBtnAction(_ sender: Any) {
       let requestVc = self.storyboard?.instantiateViewController(withIdentifier: "RequestTypeVC") as! RequestTypeVC
        self.navigationController?.pushViewController(requestVc, animated: true)
       
    }
       }

       extension ReceivedSwapVC: UITableViewDataSource{
           
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 178
            return UITableView.automaticDimension
        }
        
           
           func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             //  extension PrivacySettingsViewController: UITableViewDataSource {
           if(sentBtn.isSelected == true){
                return swapSentArr?.count ?? 0
            }
            return swapRecievedArr?.count ?? 0
             //  return swapRecievedArr?.count ?? 0
        }
      
           func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell = tableView.dequeueReusableCell(withIdentifier: "ReceivedSwapCell", for: indexPath) as! ReceivedSwapCell
              
//           let swapShift = swapRecievedArr?[indexPath.row]
//           cell.setupCell(swapShift: swapShift)
        
            if(sentBtn.isSelected == true){
               // sentBtn
                self.receivedBtn.backgroundColor = UIColor.clear
                self.receivedBtn.tintColor = UIColor.gray
                self.sentBtn.tintColor = UIColor.gray
                receivedBtn.isSelected = false
                let swapSentShifts = swapSentArr?[indexPath.row]
                cell.setupCell(swapShift: swapSentShifts)
                //sentBtn.isSelected = true
            }else if(receivedBtn.isSelected == true)
            {
                sentBtn.isSelected = false
                self.sentBtn.backgroundColor = UIColor.clear
                self.sentBtn.tintColor = UIColor.gray
                let swapShift = swapRecievedArr?[indexPath.row]
                cell.setupCell(swapShift: swapShift)
                 
            }
             
//              cell.swapClick = { sender in
//              let vc = self.navigationController?.viewControllers
//              let fl = vc?.filter({$0.isKind(of: SwapShiftVC.self)})
//              self.navigationController?.popToViewController((fl?.last)!, animated: true)
//            }

               return cell
           }
        
}
       extension ReceivedSwapVC: UITableViewDelegate {
                   
           func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                       
                           
           let swapShift = self.storyboard?.instantiateViewController(withIdentifier: "SwapShiftVC") as! SwapShiftVC
            swapShift.selectedShiftType = .selectedShiftForSwap
            swapShift.selectedShiftType = .userShiftstoSwap
            swapShift.shiftToSwap = true
            swapShift.selectedShift = true
           self.navigationController?.pushViewController(swapShift, animated: true)
                   
                           }
                   }
            
    

                    
