//
//  ReqTypeTableCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 06/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ReqTypeTableCell: UITableViewCell {

    @IBOutlet weak var accImgView: UIImageView!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var reqNameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.reqNameLbl.textColor = .primaryColor
        self.detailLbl.textColor = UIColor(red: 146, green: 146, blue: 157)
        self.imgView.layer.cornerRadius = 10
        self.imgView.backgroundColor = UIColor(red: 224, green: 225, blue: 239)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
