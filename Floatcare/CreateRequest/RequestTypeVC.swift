//
//  RequestTypeVC.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 06/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class RequestUtility {
    static let shared = RequestUtility()
    var selectedWorkSiteArray = [WorkSiteSettings]()
}

class RequestTypeVC: UIViewController {

    @IBOutlet weak var topBgView: UIView!
    @IBOutlet weak var reqTblView: UITableView!
    var shiftNames  = ["Swap Shift","Give away shift","Set Availability","Call Off","Vacation Request"]
     var shiftDetails  = ["Swap your shift with someone else","Give your shift away to someone","Change your availability","Request a Call Off","Request a vactaion day"]
    
    var imgArr = ["swap","give","availability","sickday","personal","vacation"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        self.reqTblView.tableFooterView = UIView()
//         topBgView.roundCorners(corners: [.bottomLeft], radius: 40.0)
        topBgView.layer.cornerRadius = 40
        topBgView.layer.maskedCorners = [.layerMinXMaxYCorner]
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RequestTypeVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReqTypeCell", for: indexPath) as! ReqTypeTableCell
        cell.imgView?.image = UIImage(named: imgArr[indexPath.row])
        cell.reqNameLbl.text = shiftNames[indexPath.row]
         cell.detailLbl.text = shiftDetails[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
    
//            let chooseDepartmentVC = self.storyboard?.instantiateViewController(withIdentifier: "chooseDepartment") as! ChooseDepartmentVC
//            chooseDepartmentVC.requestType = .swap
//              self.navigationController?.pushViewController(chooseDepartmentVC, animated: true)
            
            let swapShifts = self.storyboard?.instantiateViewController(withIdentifier: "SwapShiftVC") as! SwapShiftVC
            //swapShifts.requestType = .swap
            self.navigationController?.pushViewController(swapShifts, animated: true)
            
        case 1:
            self.view.makeToast("Work is in-progress", duration: 2.0, position: .center)
                       return
//            let swapShiftVC = self.storyboard?.instantiateViewController(withIdentifier: "SwapShiftVC") as! SwapShiftVC
//            self.navigationController?.pushViewController(swapShiftVC, animated: true)
//        case 2:
////            let setAvailability = UIStoryboard.init(name: "Availability", bundle: nil).instantiateViewController(identifier: "SetAvailabilityVC") as! SetAvailabilityVC
////            self.navigationController?.pushViewController(setAvailability, animated: true)
//            self.view.makeToast("Work is in-progress", duration: 2.0, position: .center)
//                                  return
        case 2:
            DispatchQueue.main.async {
                let setAvailability = UIStoryboard.init(name: "Availability", bundle: nil).instantiateViewController(identifier: "SetAvailabilityVC") as! SetAvailabilityVC
                setAvailability.modalPresentationStyle = .fullScreen
                self.present(setAvailability, animated: true, completion: nil)
            }
            
        case 3:
            let chooseDepartmentVC = self.storyboard?.instantiateViewController(withIdentifier: "chooseDepartment") as! ChooseDepartmentVC
            chooseDepartmentVC.requestType = .callOff
              self.navigationController?.pushViewController(chooseDepartmentVC, animated: true)
            case 4:
            let chooseDatesVC = self.storyboard?.instantiateViewController(withIdentifier: "chooseDepartment") as! ChooseDepartmentVC
            chooseDatesVC.requestType = .vacation
              self.navigationController?.pushViewController(chooseDatesVC, animated: true)
            case 5:
            let chooseDatesVC = self.storyboard?.instantiateViewController(withIdentifier: "CRChooseDatesVC") as! CRChooseDatesVC
            chooseDatesVC.requestType = .vacation
              self.navigationController?.pushViewController(chooseDatesVC, animated: true)
        default:
            let swapShiftVC = self.storyboard?.instantiateViewController(withIdentifier: "SwapShiftVC") as! SwapShiftVC
            self.navigationController?.pushViewController(swapShiftVC, animated: true)
        }
       
           
       }
    
    
}
