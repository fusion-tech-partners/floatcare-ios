//
//  SelectSwapShiftVC.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/30/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class SelectSwapShiftVC: UIViewController, UITableViewDelegate,UITableViewDataSource{
       
    
     var requestType : RequestTypes?
    @IBOutlet weak var headingTitleLbl: UILabel!
    @IBOutlet weak var swapListTblView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    var selectedShiftTye : SelectedShiftType = .selectedShiftForSwap
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var topView: UIView!
    override func viewDidLoad() {
            super.viewDidLoad()
    
       setupUI()
            // Do any additional setup after loading the view.
          
        }
    func setupUI(){
        nextBtn.layer.cornerRadius = 6.0
        nextBtn.alpha = 0.5
               nextBtn.backgroundColor = .primaryColor
               bottomView.roundCorners([.layerMinXMinYCorner], radius: 40)
         topView.layer.cornerRadius = 40
         topView.layer.maskedCorners = [.layerMinXMaxYCorner]
      headingTitleLbl.font = UIFont(font: .Athelas, weight: .regular, size: 32)
     headingTitleLbl.textColor = UIColor.primaryColor
    }
    func numberOfSections(in tableView: UITableView) -> Int {
                       
                       return 1
                   }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 3
          }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
       return 145

    }
    
          
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
                    
                   let cellSec = tableView.dequeueReusableCell(withIdentifier: "selectSwapshiftCell", for: indexPath) as! selectSwapshiftCell
                
                         
                   return cellSec
           }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SwapShiftVC.self) {
                let swapVC = controller as! SwapShiftVC
                swapVC.selectedShift = true
                swapVC.selectedShiftType = self.selectedShiftTye
                _ =  self.navigationController!.popToViewController(swapVC, animated: true)
                break
            }
        }
   }
    
    @IBAction func backButtonTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func requestBtnClick(_ sender: Any) {
//        let swapShifts = self.storyboard?.instantiateViewController(withIdentifier: "SwapShiftVC") as! SwapShiftVC
//        //crChooseDates.requestType = .callOff
//          self.navigationController?.pushViewController(swapShifts, animated: true)
    }

}
