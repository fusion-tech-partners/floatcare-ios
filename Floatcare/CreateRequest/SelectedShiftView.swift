//
//  SelectedShiftView.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 08/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class SelectedShiftView: UIView {

    @IBOutlet weak var wsTypeCV: UICollectionView!
    @IBOutlet weak var empDesLbl: UILabel!
    @IBOutlet weak var wsNameLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var dateLbl: UILabel!
    
    var timeSlot = ""
    override init(frame: CGRect) {
            super.init(frame: frame)
            commonInit()
        }
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
             commonInit()
        }
        private func  commonInit()
        {
            Bundle.main.loadNibNamed("SelectedShiftView", owner: self, options: nil)
            addSubview(contentView)
            contentView.frame = self.bounds
            contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            contentView.backgroundColor = UIColor.clear
             self.wsTypeCV.dataSource = self
                   let cellSize1 = CGSize(width:70 , height:28)
                   self.wsTypeCV.register(UINib.init(nibName:"ShiftCVCell", bundle: nil), forCellWithReuseIdentifier: "shiftCVCell")
                          let flowLayout = UICollectionViewFlowLayout()
                          flowLayout.scrollDirection = .horizontal
                   flowLayout.itemSize = cellSize1
                  // layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
                   flowLayout.minimumLineSpacing = 10.0
                   flowLayout.minimumInteritemSpacing = 10
                   self.wsTypeCV.collectionViewLayout = flowLayout
            wsTypeCV.delegate = self
            
    }
    func cvData(sh:swapSchedules) {
        timeSlot = "\(sh.startTime!) - \(sh.endTime!)"
        wsTypeCV.reloadData()
    }

}
extension SelectedShiftView: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
             return 1
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shiftCVCell", for: indexPath as IndexPath) as! ShiftCVCell
        cell.bgView.layer.shadowRadius = 1
        cell.shiftNameLbl.text = timeSlot
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 120, height: 30)
    }
    
    
}
