//
//  SummaryVC.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 08/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class SummaryVC: UIViewController {
let textView = UITextView(frame: CGRect.zero)
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var sendReqBtn: UIButton!
    @IBOutlet weak var summaryTblView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var stepNumLbl: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    var requestType : RequestTypes?
    var sickDayArr  = ["Call Off","31st march, 2020","Note",""]
     var personalDayArr  = ["Personal day","31st march, 2020","Note",""]
     var vactionDayArr  = ["Vacation","31st march, 2020","Note",""]
     var shiftDetails1  = ["Request Type","Start Date","Notes"]
    var editshiftDetails  = ["Request Type","Modify Dates","Notes"]
     var shiftDetails2  = ["Request Type","Start Date","End Date","Notes"]
    
    var sickimgArr = ["sickday","availability","availability","personal"]
     var personalimgArr = ["personal","availability","availability","personal"]
    var vactionimgArr = ["vacation","availability","availability","personal"]
    var notes : String = ""
    var isFromEditReqScreen : Bool = false
    var request : Requests?
    var datesSelectedForEdit : Bool = false
    var finArr = ChooseDepartmentUtility.shared.arryOfselectedWS.filter({$0.isDepartmentSelected == true})
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         sendReqBtn.layer.cornerRadius = 6.0
        updateBtn.layer.cornerRadius = 6.0
        cancelBtn.layer.cornerRadius = 6.0
         topView.layer.cornerRadius = 40
         topView.layer.maskedCorners = [.layerMinXMaxYCorner]
        self.summaryTblView.tableFooterView = UIView()
        self.cancelBtn.backgroundColor = .profileBgColor
        self.updateBtn.setTitleColor(.white, for: .normal)
        self.cancelBtn.setTitleColor(.lightGreyTextColor, for: .normal)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if self.isFromEditReqScreen == true {
            self.sendReqBtn.isHidden = true
            self.updateBtn.isHidden = false
            self.cancelBtn.isHidden = false
            self.stackView.isHidden = false
            if CreateReqParams.createReqParams.datesSelectedForEdit {
                
            }else {
                 CreateReqParams.createReqParams.dates = self.request?.onDate ?? []
            }
           
            CreateReqParams.createReqParams.notes = self.request?.notes ?? ""
            self.summaryTblView.reloadData()
            
        }else {
            self.sendReqBtn.isHidden = false
            self.updateBtn.isHidden = true
            self.cancelBtn.isHidden = true
            self.stackView.isHidden = true
        }
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendReqBtnTapped(_ sender: UIButton) {
        if sender.titleLabel?.text != "CANCEL"{
            guard finArr.last != nil else {
                ReusableAlertController.showAlert(title: "Floatcare", message: "Please select atleast one department")
                return
            }
            Progress.shared.showProgressView()
            self.createRequest()
        }
    }
    func updateRequest() {
        CreateReqParams.createReqParams.request = Requests(requestId: self.request?.requestId ?? "", worksiteId: self.request?.worksiteId ?? "", worksiteName: self.request?.worksiteName ?? "", departmentId: self.request?.departmentId ?? "", requestTypeId: self.request?.requestTypeId ?? "", requestTypeName: self.request?.requestTypeName ?? ""
            , userId: Profile.shared.userId, onDate: CreateReqParams.createReqParams.dates, shiftId: self.request?.shiftId ?? "", colleagueId: self.request?.colleagueId ?? "", colleagueName: self.request?.colleagueName ?? "", notes: CreateReqParams.createReqParams.notes)
        UpdateRequestAPI().updateRequest(request:  CreateReqParams.createReqParams.request!, completion: { [weak self] (reuest) in
            if reuest != nil || reuest?.requestId?.count ?? 0 > 0 {
                 DispatchQueue.main.async {
                     Progress.shared.hideProgressView()
                let alert = UIAlertController(title: "FloatCare", message: "Request Updated successfully", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    self?.navigationController?.popViewController(animated: true)
                }))
                self?.present(alert, animated: true, completion: nil)
                    
                }
            }else {
                 DispatchQueue.main.async {
                     Progress.shared.hideProgressView()
                let alert = UIAlertController(title: "FloatCare", message: "Request updation failed. Please try after sometime", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
                self?.present(alert, animated: true, completion: nil)
                    
            }
            }
        })
    }
    func createRequest() {
        var reqTypeId = "3"
        switch self.requestType {
        case .callOff:
            print("Call Off")
            reqTypeId = "3"
            case .personal:
            print("Call Off")
            reqTypeId = "4"
            case .vacation:
                       print("Call Off")
                       reqTypeId = "5"
        default:
            print("test")
            return
        }
        
        for index in 0..<finArr.count {
            let dept = finArr[index]
            CreateRequestAPI().createRequest(requestTypeId: reqTypeId, wsIdBusId: dept.department?.businessId ?? "0", deptID: dept.department?.departmentId ?? "0",completion:{ [weak self] (reuest) in
                if reuest != nil || reuest?.requestId?.count ?? 0 > 0 {
                     DispatchQueue.main.async {
                         Progress.shared.hideProgressView()
                    let alert = UIAlertController(title: "FloatCare", message: "Request created successfully", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                       self?.navigationController?.popToRootViewController(animated: true)
                        
                        
                    }))
                        if index == self!.finArr.count - 1{
                            self?.present(alert, animated: true, completion: nil)
                        }                        
                    }
                }else {
                     DispatchQueue.main.async {
                         Progress.shared.hideProgressView()
                    let alert = UIAlertController(title: "FloatCare", message: "Request creation failed. Please try after sometime", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        
                    }))
                    self?.present(alert, animated: true, completion: nil)
                        
                }
                }
                
            } )
        }
        
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func updateBtnTapped(_ sender: Any) {
        self.updateRequest()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SummaryVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 70
        if indexPath.section == 0{
            return UITableView.automaticDimension
        }
        if indexPath.section == 1{
            return 60
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(section == 1){
           let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell
           cell.headerTitle.text = "Workspaces"
           return cell.contentView
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if self.isFromEditReqScreen {
                return 3
            }
            if CreateReqParams.createReqParams.dates.count > 1 {
                return 4
            }
            return 4
        }
        if(section == 1){
            return finArr.count
        }
        return 0
       
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return "Workspace \(section)"
//    }
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }
//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReqTypeCell", for: indexPath) as! ReqTypeTableCell
            if self.isFromEditReqScreen == false {
                switch self.requestType {
                case .callOff:
                    cell.reqNameLbl.text =  sickDayArr[indexPath.row]
                    cell.imgView?.image = UIImage(named: sickimgArr[indexPath.row])
                case .personal:
                    cell.reqNameLbl.text = personalDayArr[indexPath.row]
                     cell.imgView?.image = UIImage(named: personalimgArr[indexPath.row])
                case .vacation:
                    cell.reqNameLbl.text = vactionDayArr[indexPath.row]
                     cell.imgView?.image = UIImage(named: vactionimgArr[indexPath.row])
                default:
                    cell.reqNameLbl.text = "Personal Day"
                }
                if CreateReqParams.createReqParams.dates.count > 1 {
                               let sortedDates = CreateReqParams.createReqParams.dates.sorted { $0 < $1 }
                               let dateString = sortedDates[0]
                               let dateString1 = sortedDates.last
                               let requiredFormat = dateString.toDateString(inputDateFormat: "yyyy-MM-dd", ouputDateFormat: "MMM d, yyyy")
                               let requiredFormat1 = dateString1!.toDateString(inputDateFormat: "yyyy-MM-dd", ouputDateFormat: "MMM d, yyyy")
                               if indexPath.row == 1 { // start date
                                          cell.reqNameLbl.text = "\(requiredFormat)"

                                      }
                                     else if indexPath.row == 2 { //end date
                                       cell.reqNameLbl.text = "\(requiredFormat1)"
                               }else if indexPath.row == 3 {
                                   cell.reqNameLbl.text =  CreateReqParams.createReqParams.notes
                               }
                             //  else {
                                       cell.detailLbl.text = shiftDetails2[indexPath.row]
                                 //     }
                                      cell.accImgView.isHidden = true
                           }else {
                               if indexPath.row == 1 {
                                          let dateString = CreateReqParams.createReqParams.dates[0]
                                          let requiredFormat = dateString.toDateString(inputDateFormat: "yyyy-MM-dd", ouputDateFormat: "MMM d, yyyy")
                                    cell.reqNameLbl.text = "\(requiredFormat)"
                                    cell.detailLbl.text = shiftDetails1[indexPath.row]
                                      }
                                      if indexPath.row == 2 {
                                          cell.reqNameLbl.text = shiftDetails1[indexPath.row]
                                        cell.detailLbl.text = CreateReqParams.createReqParams.notes
                                      } //else {
                                     //  cell.detailLbl.text = shiftDetails1[indexPath.row]
                                    //  }
                                      cell.accImgView.isHidden = true
                           }
            }else {
                switch self.requestType {
                case .callOff:
                    cell.reqNameLbl.text =  sickDayArr[indexPath.row]
                    cell.imgView?.image = UIImage(named: sickimgArr[indexPath.row])
                case .personal:
                    cell.reqNameLbl.text = personalDayArr[indexPath.row]
                     cell.imgView?.image = UIImage(named: personalimgArr[indexPath.row])
                case .vacation:
                    cell.reqNameLbl.text = vactionDayArr[indexPath.row]
                     cell.imgView?.image = UIImage(named: vactionimgArr[indexPath.row])
                default:
                    cell.reqNameLbl.text = "Personal Day"
                }
                 cell.reqNameLbl.text = self.request?.requestTypeName ?? ""
                let sortedDates = CreateReqParams.createReqParams.dates.sorted { $0 < $1 }
                  let dateString = sortedDates[0]
                  let dateString1 = sortedDates.last
                  let requiredFormat = dateString.toDateString(inputDateFormat: "yyyy-MM-dd", ouputDateFormat: "MMM d, yyyy")
                  let requiredFormat1 = dateString1!.toDateString(inputDateFormat: "yyyy-MM-dd", ouputDateFormat: "MMM d, yyyy")
                if indexPath.row == 1 {
                    if sortedDates.count > 1 {
                                    cell.reqNameLbl.text = "\(requiredFormat) - \(requiredFormat1)"
                               }else {
                                    cell.reqNameLbl.text = "\(requiredFormat)"
                               }
                }
               
                    if indexPath.row == 2 {
                      cell.reqNameLbl.text =  CreateReqParams.createReqParams.notes
                  }
                          cell.detailLbl.text = editshiftDetails[indexPath.row]
                         cell.accImgView.isHidden = true
            }
            return cell
        }
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "summaryWorkspaceCell", for: indexPath) as! summaryWorkspaceCell
            let obj = finArr[indexPath.row]
            cell.titleLbl.text = obj.department?.departmentTypeName
            cell.subTitleLbl.text = "\((obj.orgName)!) : \((obj.department?.address)!)"
            cell.colorImageView.backgroundColor = obj.colorCode
            cell.buttonClick = { sender in
                self.finArr.remove(at: indexPath.row)
                self.summaryTblView.reloadData()
            }
             return cell
        }

    
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        if section == 1 {
            return 60
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isFromEditReqScreen {
            if indexPath.row == 1 {
                let chooseDatesVC = self.storyboard?.instantiateViewController(withIdentifier: "CRChooseDatesVC") as! CRChooseDatesVC
                chooseDatesVC.modalPresentationStyle = .fullScreen
                chooseDatesVC.requestType = self.requestType
                chooseDatesVC.isFromEditReqScreen = true
                chooseDatesVC.calendarNavigation = .fromEditRequest
                self.present(chooseDatesVC, animated: true, completion: nil)
              //  self.navigationController?.present(chooseDatesVC, animated: true, completion: nil)
            }else if indexPath.row == 2 {
                    self.displayAlertWithInputField()
                }
        }
      
        }
    func displayAlertWithInputField() {
        let alertController = UIAlertController(title: "Feedback \n\n\n\n\n", message: nil, preferredStyle: .alert)

        let cancelAction = UIAlertAction.init(title: "Cancel", style: .default) { (action) in
            alertController.view.removeObserver(self, forKeyPath: "bounds")
        }
        alertController.addAction(cancelAction)

        let saveAction = UIAlertAction(title: "Submit", style: .default) { (action) in
            CreateReqParams.createReqParams.notes = self.textView.text
            alertController.view.removeObserver(self, forKeyPath: "bounds")
             DispatchQueue.main.async {
                self.summaryTblView.reloadData()
            }
        }
        alertController.addAction(saveAction)

        alertController.view.addObserver(self, forKeyPath: "bounds", options: NSKeyValueObservingOptions.new, context: nil)
        textView.backgroundColor = UIColor.white
        textView.textContainerInset = UIEdgeInsets.init(top: 8, left: 5, bottom: 8, right: 5)
        alertController.view.addSubview(self.textView)

        self.present(alertController, animated: true, completion: nil)
        
    }
        // self.navigationController?.popToViewController(swapShiftVC, animated: true)
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func configureEditSummary(cell: ReqTypeTableCell, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    
        cell.reqNameLbl.text = self.request?.requestTypeName ?? ""
        switch self.requestType {
        case .callOff:
            cell.imgView?.image = UIImage(named: sickimgArr[indexPath.row])
        case .personal:
             cell.imgView?.image = UIImage(named: personalimgArr[indexPath.row])
        case .vacation:
             cell.imgView?.image = UIImage(named: vactionimgArr[indexPath.row])
        default:
            cell.reqNameLbl.text = "Personal Day"
        }
        
        return cell
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "bounds"{
            if let rect = (change?[NSKeyValueChangeKey.newKey] as? NSValue)?.cgRectValue {
                let margin: CGFloat = 8
                let xPos = rect.origin.x + margin
                let yPos = rect.origin.y + 54
                let width = rect.width - 2 * margin
                let height: CGFloat = 90

                textView.frame = CGRect.init(x: xPos, y: yPos, width: width, height: height)
            }
        }
    }
}
extension String
{
    func toDateString( inputDateFormat inputFormat  : String,  ouputDateFormat outputFormat  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: date!)
    }
}
