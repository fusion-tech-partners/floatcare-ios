//
//  SwapChooseDepartmentVC.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/30/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class SwapChooseDepartmentVC: UIViewController, UITableViewDelegate,UITableViewDataSource{
       @IBOutlet weak var navigationBar: UIButton!
    
     var requestType : RequestTypes?
    @IBOutlet weak var headingTitleLbl: UILabel!
    @IBOutlet weak var departmentsTblView: UITableView!
    @IBOutlet weak var nextBtn: ButtonViolet!
    private var dateCellExpanded: Bool = false
    var selectedIndex = -1
    var isCollapse = false
    
    override func viewDidLoad() {
            super.viewDidLoad()
        departmentsTblView.estimatedRowHeight = 322
        departmentsTblView.rowHeight = UITableView.automaticDimension
       setupUI()
            // Do any additional setup after loading the view.
          
        }
    func setupUI(){
      headingTitleLbl.font = UIFont(font: .Athelas, weight: .regular, size: 32)
     headingTitleLbl.textColor = UIColor.primaryColor
    }
    func numberOfSections(in tableView: UITableView) -> Int {
                       
                       return 1
                   }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 5
          }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0{
            return 70
        }
        if self.selectedIndex == indexPath.row && isCollapse == true{
            return 314
        }else{
            return 119
        }

    }
    
          
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if indexPath.row == 0{
         let cell = tableView.dequeueReusableCell(withIdentifier: "chooseDepartment", for: indexPath) as! SwapDepartmentCell
                       return cell
                       }else{
                   let cellSec = tableView.dequeueReusableCell(withIdentifier: "DepartmentSecondaryCell", for: indexPath) as! SwapDepartmentSecondaryCell
                cellSec.hospitalNameLbl.text = "Care Hospital"
                cellSec.branchNameLbl.text = "Banjara Hills"
                cellSec.branchAdresLbl.text = "123 Avenue, 4th street"
                
                cellSec.headingTitleLbl.text = "Emergency Services"
                cellSec.subTitleLbl.text = "Med-Surg Nurse • Fulltime"
                cellSec.descriptionLbl.text = "A little description about the department and what it does."
                
                         
                   return cellSec
           }
           }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex == indexPath.row
        {
            if self.isCollapse == false{
                self.isCollapse = true
            }else{
                self.isCollapse = false
            }
        }else{
            self.isCollapse = true
        }
        self.selectedIndex = indexPath.row
        departmentsTblView.reloadRows(at: [indexPath], with: .automatic)

   }
    
    @IBAction func backButtonTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func nextBtnClick(_ sender: Any) {
        let swapShifts = self.storyboard?.instantiateViewController(withIdentifier: "SwapShiftVC") as! SwapShiftVC
        //crChooseDates.requestType = .callOff
          self.navigationController?.pushViewController(swapShifts, animated: true)
    }

}
