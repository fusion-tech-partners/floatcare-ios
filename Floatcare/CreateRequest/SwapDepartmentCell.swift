//
//  SwapDepartmentCell.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/30/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class SwapDepartmentCell: UITableViewCell{
    @IBOutlet weak var workspacesSwitch: UISwitch!
    
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
    super.awakeFromNib()
         setupUi()
        titleLbl.text = "Choose all Workspaces"
       
    }
    func setupUi(){
        titleLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 16.0)
        titleLbl.textColor = UIColor .primaryColor
    }
}
