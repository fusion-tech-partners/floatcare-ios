//
//  SwapDepartmentSecondaryCell.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/30/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class SwapDepartmentSecondaryCell: UITableViewCell{
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var headingTitleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var departmentButn: UIButton!
    @IBOutlet weak var hospitalNameLbl: UILabel!
    @IBOutlet weak var colorView: UIImageView!
    @IBOutlet weak var branchNameLbl: UILabel!
    @IBOutlet weak var branchAdresLbl: UILabel!
    
    override func awakeFromNib() {
    super.awakeFromNib()
        setUpUi()
        
    }
    @IBAction func departmentButnTapped(_ sender: Any) {
        departmentButn.setTitle("Department Choosen", for: UIControl.State.normal)
        departmentButn.backgroundColor = UIColor.lightGray
    }
    
    func setUpUi(){
        colorView.layer.cornerRadius = 5.0
        cellBackgroundView.layer.cornerRadius = 5.0
        hospitalNameLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 14.0)
        branchNameLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 16.0)
        branchAdresLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
        
        headingTitleLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 18.0)
        subTitleLbl.font = UIFont(font: .SFUIText, weight: .medium, size: 14.0)
        descriptionLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 16.0)
        
        hospitalNameLbl.textColor = UIColor.descriptionColor
        branchNameLbl.textColor = UIColor.primaryColor
        branchAdresLbl.textColor = UIColor.descriptionColor
        
        headingTitleLbl.textColor = UIColor.primaryColor
        descriptionLbl.textColor = UIColor.appLightGray
        departmentButn.layer.cornerRadius = 10.0
        departmentButn.setTitle("Choose Department", for: UIControl.State.normal)
        
        
    }
}
