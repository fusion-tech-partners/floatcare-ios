import UIKit

 

class FetchSwapDetails {
    var swapShift : [swapSchedules]? = [swapSchedules]()
    func findSchedulesByDepartmentIdAndUserId(userId: String = "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)",onDate:String,departmentId: String, completion: @escaping ([swapSchedules]?) -> ()) {
        ApolloManager.shared.client.fetch(query:FindSchedulesByDepartmentIdAndUserIdAndSelectedDateQuery(userId: userId, departmentId: departmentId, onDate: onDate), cachePolicy: .fetchIgnoringCacheData, context: nil,queue: .main ) { result in
                        
                        guard let user = try? result.get().data?.resultMap else {
                            completion(nil)
                            return
                        }
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            let decoder = JSONDecoder()
                            let swapShifts = try decoder.decode(WSSwapModel.self, from: jsonData)
                         print("swapshifts issß",swapShifts)
                         completion(swapShifts.findSchedulesByDepartmentIdAndUserIdAndSelectedDate)

                        }catch {
                             completion(nil)
                        }
        }
    }
    
    func findUserSchedulesOnDate(userId: String = "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)",onDate:String, completion: @escaping ([swapSchedules]?) -> ()) {
        ApolloManager.shared.client.fetch(query:GetScheduleByUserIdAndSelectedDayQuery(userId: userId, selectedDate: onDate), cachePolicy: .fetchIgnoringCacheData, context: nil,queue: .main ) { result in
                        guard let user = try? result.get().data?.resultMap else {
                            completion(nil)
                            return
                        }
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            let decoder = JSONDecoder()
                            let swapShifts = try decoder.decode(WSUserScheduleSwapModel.self, from: jsonData)
                         print("swapshifts issß",swapShifts)
                         completion(swapShifts.getScheduleByUserIdAndSelectedDay)

                        }catch {
                             completion(nil)
                        }
        }
    }
        
}


struct WSSwapModel : Codable{
    
 var findSchedulesByDepartmentIdAndUserIdAndSelectedDate : [swapSchedules]?
}

struct WSUserScheduleSwapModel : Codable {
    var getScheduleByUserIdAndSelectedDay : [swapSchedules]?
}

struct swapSchedules : Codable {
     
    var scheduleId : String?
    var worksiteId: String?
    var worksiteName: String?
    var worksiteColor: String?
    var departmentId: String?
    var departmentName: String?
    var userId: String?
    var staffCoreTypeName: String?
    var shiftTypeId: String?
    var shiftTypeName: String?
    var shiftDate: String?
    var startTime: String?
    var endTime: String?
    var eventType: String?
    var isOnCallRequest: Bool?
   var voluntaryLowCensus: Bool?
    var userDetails : UserBasicInformation?
    var workingColleagues: [UserBasicInformation]?
    var worksiteDetails: BusinessesDetails?
}

struct BusinessesDetails : Codable{
    var businessId: String?
    var organizationId: String?
    var name: String?
    var businessTypeId: String?
    var businessType: String?
    var businessSubTypeId: String?
    var businessTypeCount: String?
    var businessSubType: String?
    var phoneNumber: String?
    //var extension: String?
    var email: String?
    var address: String?
    var city: String?
    var state: String?
    var postalCode: String?
    var country: String?
//    var department: [Department]?
    var numberOfDepartment: String?
    var status: String?
    var coverPhoto: String?
    var description: String?
    //var worksiteColor(...): String?
    //var departmentByUserId(...): [Department]?
}
//
//struct Department : Codable{
//
//    var departmentId: String?
//    var businessId: String?
//    var departmentTypeName: String?
//    var phoneNumber: String?
//    var `extension`: String?
//    var email: String?
//    var address: String?
//    var description: String?
//    var status: String?
//    var staffRoles: [DepartmentStaff]?
//    var shiftType: [DepartmentShifts]?
//    var colleagues: [UserBasicInformation]?
////    var fteStatusOfStaffUser;(...): String?
////    var staffRoleByUserId;(...): String?
//
//}
//
//    struct DepartmentStaff : codable {
//        var departmentId: String?
//        var staffUserCoreTypeId: String?
//        var staffCount: String?
//        var staffUserCoreTypeName: String?
//
//}
//    struct DepartmentShifts : codable {
//        var departmentShiftId: String?
//        var departmentId: String?
//        var color: String?
//        var icon: String?
//        var startTime: String?
//        var endTime: String?
//        var label: String?
//}
//
class GetSwapDetails{
func getSwapShiftRequestsRecievedByUserId(userId: String = "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", completion: @escaping ([SwapShiftRequests]?) -> ()) {
    ApolloManager.shared.client.fetch(query:GetSwapShiftRequestsRecievedByUserIdQuery(userId: userId), cachePolicy: .fetchIgnoringCacheData, context: nil,queue: .main ) { result in
                    
                    guard let user = try? result.get().data?.resultMap else {
                        completion(nil)
                        return
                    }
                    
                    do {
                        let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                        let decoder = JSONDecoder()
                        let swapShifts = try decoder.decode(WSSwapRecievedModel.self, from: jsonData)
                     print("swapshifts issß",swapShifts)
                     completion(swapShifts.getSwapShiftRequestsRecievedByUserId)

                    }catch {
                         completion(nil)
                    }
    }
}

func getSwapShiftRequestsSentByUserId(userId: String = "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", completion: @escaping ([SwapShiftRequests]?) -> ()) {
    ApolloManager.shared.client.fetch(query:GetSwapShiftRequestsSentByUserIdQuery(userId: userId), cachePolicy: .fetchIgnoringCacheData, context: nil,queue: .main ) { result in
                    
                    guard let user = try? result.get().data?.resultMap else {
                        completion(nil)
                        return
                    }
                    
                    do {
                        let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                        let decoder = JSONDecoder()
                        let swapShifts = try decoder.decode(WSSwapSentModel.self, from: jsonData)
                     print("swapshifts issß",swapShifts)
                     completion(swapShifts.getSwapShiftRequestsSentByUserId)

                    }catch {
                         completion(nil)
                    }
    }
}
}
struct WSSwapRecievedModel : Codable{
    
 var getSwapShiftRequestsRecievedByUserId : [SwapShiftRequests]?
}

struct WSSwapSentModel : Codable {
    var getSwapShiftRequestsSentByUserId : [SwapShiftRequests]?
}

struct SwapShiftRequests : Codable {
    var swapShiftRequestId: String?
    var senderId: String?
    var senderInformation: UserBasicInformation?
    var receiverId: String?
    var receiverInformation: UserBasicInformation?
    var sourceShiftId: String?
    var sourceShiftInformation: Schedules?
    var requestedShiftId: String?
    var requestedShiftInformation: Schedules?
    var isRecieverAccepted: Bool?
    var isSupervisorAccepted: Bool?
    var createdOn: String?
    var updatedOn: String?
    var status: String?
}

struct Schedules: Codable {
    var shiftTypeName : String?
    var staffCoreTypeName : String?
    var departmentName : String?
    var worksiteColor : String?
}

