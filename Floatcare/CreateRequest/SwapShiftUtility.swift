//
//  SwapShiftUtility.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 13/11/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class SwapShiftUtility  {
    static let shared = SwapShiftUtility()
    var firstSwapShift : swapSchedules?
    var secondSwapShift : swapSchedules?
    var receivedSwapShift : SwapShiftRequests?
    var isFirstSwapShift = true
    var seletcedSwapUserIndex = 0
    
    func resetTodefaults()  {
        firstSwapShift = nil
        secondSwapShift = nil
        //receivedSwapShift = nil
        isFirstSwapShift = true
    }
}
