//
//  SwapShiftVC.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 06/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

enum SelectedShiftType {
    case noShiftSelected
    case userShiftstoSwap
    case selectedShiftForSwap
}
class SwapShiftVC: UIViewController {

    @IBOutlet weak var selectedShiftView: SelectedShiftView!
    @IBOutlet weak var selectedShiftReturnView: SelectedShiftView!
    @IBOutlet weak var topBgView: UIView!
    @IBOutlet weak var sendReBtn: UIButton!
    @IBOutlet weak var shiftreturnSwapBtn: UIButton!
    @IBOutlet weak var shiftToSwapBtn: UIButton!
    var shiftToSwap: Bool = false
    var selectedShift : Bool = false
    var selectedShiftType : SelectedShiftType = .noShiftSelected
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        sendReBtn.backgroundColor = .primaryColor
        sendReBtn.layer.cornerRadius = 6.0
        sendReBtn.alpha = 0.5
         topBgView.roundCorners([ .layerMinXMaxYCorner], radius: 30.0)
        SwapShiftUtility.shared.resetTodefaults()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.configureShiftView()
        setupUI()
    }
    func setupUI() {
        if let fshift = SwapShiftUtility.shared.firstSwapShift{
//            let date = fshift.shiftDate
//            let formatingDate = getFormattedDate(string: date, format: "mmm d, yyyy")
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "yyyy-MM-dd"
            //        dateFormatter.dateStyle = DateFormatter.Style.MediumStyle
            let dateObj = (dateFormatter.date(from:fshift.shiftDate!) ?? nil)!
            dateFormatter.dateFormat = "MMM d, yyyy"
            let stringDate = dateFormatter.string(from:dateObj)
            selectedShiftView.dateLbl.text = stringDate
            selectedShiftView.wsNameLbl.text = fshift.worksiteName
            selectedShiftView.empDesLbl.text = fshift.staffCoreTypeName
            selectedShiftView.pinkView.backgroundColor = fshift.worksiteColor?.hexStringToUIColor()
            selectedShiftView.cvData(sh: fshift)
        }
        if let fshift = SwapShiftUtility.shared.secondSwapShift{
            selectedShiftReturnView.dateLbl.text = "\((fshift.userDetails?.firstName)!) \((fshift.userDetails?.lastName)!)"
            selectedShiftReturnView.wsNameLbl.text = fshift.worksiteName
            selectedShiftReturnView.empDesLbl.text = fshift.staffCoreTypeName
            selectedShiftReturnView.pinkView.backgroundColor = fshift.worksiteColor?.hexStringToUIColor()
            selectedShiftReturnView.cvData(sh: fshift)
        }
       
        
    }
//    func getFormattedDate(string: Date(), format: String) -> String {
////            let dateformat = DateFormatter()
////            dateformat.dateFormat = format
////            return dateformat.date(from: string)
//
//    }

   
    
    @IBAction func backBtnTapped(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendReqBtnTapped(_ sender: Any) {
        guard SwapShiftUtility.shared.firstSwapShift != nil && SwapShiftUtility.shared.secondSwapShift != nil else{
            self.view.makeToast("Please select shifts to swap")
            return
        }
        createSwapRequest()
    }
    func createSwapRequest()  {
        let mutation = CreateSwapShiftRequestMutation.init(senderId: Profile.shared.userId, receiverId: (SwapShiftUtility.shared.secondSwapShift?.userDetails!.userId)!, sourceShiftId: (SwapShiftUtility.shared.firstSwapShift?.scheduleId)!, requestedShiftId: (SwapShiftUtility.shared.secondSwapShift?.scheduleId)!)
       
        ApolloManager.shared.client.perform(mutation: mutation) { (result) in
        print("\(result)")
    
            guard let user = try? result.get().data?.resultMap else {
                return
            }
           
            ReusableAlertController.showAlertOk(title: "Floatcare", message: "Swapshift successfully created") { (stus) in
//                let receivedSwap = self.storyboard?.instantiateViewController(withIdentifier: "ReceivedSwapVC") as! ReceivedSwapVC
//                self.navigationController?.pushViewController(receivedSwap, animated: true)
                let receivedShift = self.storyboard?.instantiateViewController(withIdentifier: "ReceivedSwapVC") as! ReceivedSwapVC
                self.navigationController?.pushViewController(receivedShift, animated: true)
                //self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func shiftreturnSwapBtnTapped(_ sender: Any) {
        if(SwapShiftUtility.shared.firstSwapShift != nil){
            let chooseDatesVC = self.storyboard?.instantiateViewController(withIdentifier: "CRChooseDatesVC") as! CRChooseDatesVC
             chooseDatesVC.selectedShiftType = .selectedShiftForSwap
            SwapShiftUtility.shared.isFirstSwapShift = false
            chooseDatesVC.calendarNavigation = .fromSwapCalender
            self.navigationController?.pushViewController(chooseDatesVC, animated: true)
        }else{
            self.view.makeToast("Please select a shift to swap")
        }
    }
    @IBAction func shiftToSwapBtnTapped(_ sender: Any) {
        let chooseDatesVC = self.storyboard?.instantiateViewController(withIdentifier: "CRChooseDatesVC") as! CRChooseDatesVC
        SwapShiftUtility.shared.isFirstSwapShift = true
        chooseDatesVC.selectedShiftType = .userShiftstoSwap
        chooseDatesVC.calendarNavigation = .fromSwapCalender
        self.navigationController?.pushViewController(chooseDatesVC, animated: true)
    }
    func configureShiftView() {
        selectedShiftView.isHidden = SwapShiftUtility.shared.firstSwapShift != nil ? false : true
        selectedShiftReturnView.isHidden = SwapShiftUtility.shared.secondSwapShift != nil ? false : true
        if  SwapShiftUtility.shared.firstSwapShift != nil && SwapShiftUtility.shared.secondSwapShift != nil {
            sendReBtn.alpha = 1
            sendReBtn.isUserInteractionEnabled = true
        }else{
            sendReBtn.alpha = 0.5
            sendReBtn.isUserInteractionEnabled = true
        }
    
        guard  SwapShiftUtility.shared.firstSwapShift != nil, SwapShiftUtility.shared.secondSwapShift != nil else{
                return
        }
        sendReBtn.alpha = 1
        sendReBtn.isUserInteractionEnabled = true
//        switch self.selectedShiftType {
//        case .noShiftSelected:
//            sendReBtn.alpha = 0.5
//            selectedShiftView.isHidden = true
//             sendReBtn.isUserInteractionEnabled = false
//         case .shiftToSwap:
//            sendReBtn.alpha = 0.5
//            selectedShiftView.isHidden = false
//            sendReBtn.isUserInteractionEnabled = false
//        case .selectedShiftForSwap:
//            sendReBtn.alpha = 1.0
//            selectedShiftReturnView.isHidden = false
//             sendReBtn.isUserInteractionEnabled = false
//        default:
//            sendReBtn.alpha = 0.5
//            selectedShiftView.isHidden = true
//             sendReBtn.isUserInteractionEnabled = false
//        }
//        if self.shiftToSwap == true {
//            sendReBtn.alpha = 0.5
//            selectedShiftView.isHidden = false
//            sendReBtn.isUserInteractionEnabled = false
//
//        }
//        if self.selectedShift == true{
//            sendReBtn.alpha = 0.5
//            selectedShiftReturnView.isHidden = false
//            sendReBtn.isUserInteractionEnabled = false
//
//
//        }
//        if (self.shiftToSwap == true && self.selectedShift == true){
//            sendReBtn.alpha = 2.0
//            sendReBtn.isUserInteractionEnabled = true
//
//        }
    }
}
