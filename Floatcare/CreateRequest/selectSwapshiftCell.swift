//
//  selectSwapshiftCell.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/30/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class selectSwapshiftCell: UITableViewCell{
    
    @IBOutlet weak var cellBgView: UIView!
    @IBOutlet weak var sideColorView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var timeDisplayFirstLbl: UILabel!
    @IBOutlet weak var timeDisplaySecondLbl: UILabel!
    @IBOutlet weak var timeDiaplayThirdView: UILabel!
    override func awakeFromNib() {
    super.awakeFromNib()
         setupUI()
        titleLbl.text = "Sam Singh"
        subTitleLbl.text = "Lotus Hospital | Dentistry"
        timeDisplayFirstLbl.text = "10am - 3pm"
        timeDisplaySecondLbl.text = "3pm - 5pm"
        timeDiaplayThirdView.text = "5pm - 10pm"
    }
    
    func setupUI(){
        cellBgView.layer.cornerRadius = 10.0
        titleLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 18.0)
        subTitleLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
         timeDisplayFirstLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
         timeDisplaySecondLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
         timeDiaplayThirdView.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
        subTitleLbl.textColor = UIColor.descriptionColor
        timeDisplayFirstLbl.textColor = UIColor.descriptionColor
        timeDisplaySecondLbl.textColor = UIColor.descriptionColor
        timeDiaplayThirdView.textColor = UIColor.descriptionColor
        timeDisplayFirstLbl.layer.cornerRadius = 10.0
        timeDisplaySecondLbl.layer.cornerRadius = 10.0
        timeDiaplayThirdView.layer.cornerRadius = 10.0
        cellBgView.layer.shadowColor = UIColor.gray.cgColor
        cellBgView.layer.shadowOffset = CGSize(width: 3, height: 3)
        cellBgView.layer.shadowOpacity = 0.8
    }
}
