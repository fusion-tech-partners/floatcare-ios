//
//  summaryWorkspaceCell.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/28/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class summaryWorkspaceCell: UITableViewCell {
    
    @IBOutlet weak var colorImageView: UIImageView!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var clearButton: UIButton!
        
    var buttonClick : actionBlock?
    
    override func awakeFromNib() {
        super.awakeFromNib()
             setupUi()
           // titleLbl.text = "Choose all Workspaces"
           
        }
        func setupUi(){
            colorImageView?.layer.cornerRadius = 10.0

            titleLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 16.0)
            subTitleLbl.font = UIFont(font: .SFUIText, weight: .medium, size: 13.0)
            titleLbl.textColor = UIColor .primaryColor
            subTitleLbl.textColor = UIColor.descriptionColor
            
        }
    @IBAction func clearClick(_ sender: UIButton) {
        buttonClick?(sender)
    }
}
    
