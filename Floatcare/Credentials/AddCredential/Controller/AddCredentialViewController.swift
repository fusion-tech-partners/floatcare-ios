//
//  ChnagePrimaryNumberViewController.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 27/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import MRCountryPicker

class AddCredentialViewController: UIViewController {

    @IBOutlet weak var licenceview: UIView!
    @IBOutlet weak var licenceArrow: UIImageView!
    @IBOutlet weak var documentView: UIView!
    @IBOutlet weak var documentArrow: UIImageView!
    @IBOutlet weak var certificateView: UIView!
    @IBOutlet weak var certificateArrow: UIImageView!
     @IBOutlet weak var navigationBar: NavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNaigationBar()
        
        let backImage = UIImage(named: "ic_ChevronLeft")
              self.navigationController?.navigationBar.backIndicatorImage = backImage
              self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage

              self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                      
              
              self.navigationBar.LeftButtonActionBlock = { sender in
                  self.navigationController?.navigationBar.isHidden = false
                  self.navigationController?.popViewController(animated: true)
              }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let leftMirrored = UIImage(cgImage: licenceArrow.image!.cgImage!, scale: 1.0, orientation: .down)
        licenceArrow.image = leftMirrored
        documentArrow.image = leftMirrored
        certificateArrow.image = leftMirrored

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        licenceview.layer.cornerRadius = 15
        licenceview.layer.masksToBounds = false
        licenceview?.layer.shadowColor = UIColor.blue.cgColor
        licenceview?.layer.shadowOffset =  CGSize.init(width: 3, height: 5)
        licenceview?.layer.shadowOpacity = 0.18
        licenceview?.layer.shadowRadius = 10
        
        documentView.layer.cornerRadius = 15
        documentView.layer.masksToBounds = false
        documentView?.layer.shadowColor = UIColor.blue.cgColor
        documentView?.layer.shadowOffset =  CGSize.init(width: 3, height: 5)
        documentView?.layer.shadowOpacity = 0.18
        documentView?.layer.shadowRadius = 10
        
        certificateView.layer.cornerRadius = 15
        certificateView.layer.masksToBounds = false
        certificateView?.layer.shadowColor = UIColor.blue.cgColor
        certificateView?.layer.shadowOffset =  CGSize.init(width: 3, height: 5)
        certificateView?.layer.shadowOpacity = 0.18
        certificateView?.layer.shadowRadius = 10
    }
    
    fileprivate func setUpNaigationBar() {
           navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
           navigationController?.navigationBar.shadowImage = UIImage()

           let button = UIButton(type:.custom)
           button.setImage(UIImage(named: "Back"), for: .normal)
           button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
           button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           let barButton = UIBarButtonItem(customView: button)
           self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addShadow(effectView: UIView) {
        let shadowSize : CGFloat = 5.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: effectView.frame.size.width + shadowSize,
                                                   height: effectView.frame.size.height + shadowSize))
        effectView.layer.masksToBounds = false
        effectView.layer.shadowColor = UIColor.black.cgColor
        effectView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        effectView.layer.shadowOpacity = 0.5
        effectView.layer.shadowPath = shadowPath.cgPath
    }
    
    @IBAction func licenceAction(_ sender: Any) {
        
        if let controller = UIStoryboard.init(name: "ChangePhoneNumber", bundle: Bundle.main).instantiateViewController(identifier: "LicenceViewController") as? LicenceViewController {
            controller.type = .licence
            self.navigationController?.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    @IBAction func buttonAction(_ sender: Any) {
        
        if let controller = UIStoryboard.init(name: "ChangePhoneNumber", bundle: Bundle.main).instantiateViewController(identifier: "LicenceViewController") as? LicenceViewController {
            controller.type = .document
            self.navigationController?.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    @IBAction func certificateAction(_ sender: Any) {
        
        if let controller = UIStoryboard.init(name: "ChangePhoneNumber", bundle: Bundle.main).instantiateViewController(identifier: "LicenceViewController") as? LicenceViewController {
            controller.type = .certificate
            self.navigationController?.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
}
