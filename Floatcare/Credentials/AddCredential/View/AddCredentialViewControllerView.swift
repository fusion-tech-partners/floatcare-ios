//
//  ChangePhoneNumber.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 27/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
protocol AddCredentialViewControllerViewActionable: class {
    func savePhoneNumber()
}

class AddCredentialViewControllerView: UIView {

    @IBOutlet weak var mainView: UIView!

    override func awakeFromNib() {
        superview?.awakeFromNib()

        // Will set a custom width instead of the anchor view width
//        dropDownLeft.width = 200
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        mainView.roundCorners(corners: [.bottomLeft], radius: 30)


    }

}
