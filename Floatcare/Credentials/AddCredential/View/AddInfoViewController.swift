//
//  AddInfoViewController.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 03/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class AddInfoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AddInfoTableViewCellDelegate {
    
    var selectedType: String?
    var type = TypeOfCredential.licence
    @IBOutlet weak var mainView: UIView!
    var filterValue: FindAllUserCredential?
    
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var navigationBar: NavigationBar!
    var heightOfKeyboard = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNaigationBar()
        mainTableView.delegate = self
        mainTableView.dataSource = self
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        
        let backImage = UIImage(named: "ic_ChevronLeft")
              self.navigationController?.navigationBar.backIndicatorImage = backImage
              self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage

              self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                      
              
              self.navigationBar.LeftButtonActionBlock = { sender in
                  self.navigationController?.navigationBar.isHidden = false
                  self.navigationController?.popViewController(animated: true)
              }
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            heightOfKeyboard = Int(keyboardRectangle.height)
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mainView.roundCorners(corners: [.bottomLeft], radius: 30)
       
    }
    fileprivate func setUpNaigationBar() {
           navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
           navigationController?.navigationBar.shadowImage = UIImage()

           let button = UIButton(type:.custom)
           button.setImage(UIImage(named: "Back"), for: .normal)
           button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
           button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           let barButton = UIBarButtonItem(customView: button)
           self.navigationItem.leftBarButtonItem = barButton
        mainView.backgroundColor = .profileBgColor
    }
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddInfoTableViewCell", for: indexPath) as! AddInfoTableViewCell
        if let obj = filterValue {
            cell.isUpdateShouldBeCalled = true
            if let name = obj.credentialType {
                if name.localizedCaseInsensitiveContains("Certificate") {
                    cell.fillOutLabel.text = "Fill out certification information"
                } else if name.localizedCaseInsensitiveContains("Document") {
                    cell.fillOutLabel.text = "Fill out document information"
                } else if name.localizedCaseInsensitiveContains("license") {
                    cell.fillOutLabel.text = "Fill out license information"
                }
            }
            DispatchQueue.main.async {
                let frontImage = obj.frontFilePhoto?.toImage()
                let backImage = obj.backFilePhoto?.toImage()
                cell.frontCameraImageView.image = frontImage
                cell.backCameraImageView.image = backImage
            }
            cell.stateTextField.text = obj.state
            cell.subSearchTitle.text = obj.credentialSubTypeName
            cell.licenceViewTextField.text = obj.licenseNumber
            if let comLic = obj.isCompactLicense,
                comLic == "true" {
                cell.compactLicenceTextField.text = "Yes"
            } else {
                cell.compactLicenceTextField.text = "No"
            }
            if let expDate = obj.expirationDate {
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "yyyy-MM-dd"
                
                if let showDate = inputFormatter.date(from: expDate) {
                    inputFormatter.dateFormat = "dd/MM/yyyy"
                    let resultString = inputFormatter.string(from: showDate)
                    cell.expirateDateTextField.text = resultString
                }
            }
            
        } else {
            cell.subSearchTitle.text = selectedType
            switch type {
            
            case .licence:
                //Fill out license information
                cell.fillOutLabel.text = "Fill out license information"
                cell.licenceImage.image = UIImage.init(named: "licence")
            case .document:
                cell.fillOutLabel.text = "Fill out document information"
                cell.licenceImage.image = UIImage.init(named: "Document")
            case .certificate:
                cell.fillOutLabel.text = "Fill out certification information"
                cell.licenceImage.image = UIImage.init(named: "Certificate")
            }
            
        }
        
        cell.viewController = self
        cell.controllerDelegate = self
        return cell
    }
    func updateTableContentOffset(_ point: CGPoint) {
        let p = CGPoint.init(x: 0, y: Int(point.y - 50) )
        UIView.animate(withDuration: 0.2) {
            self.mainTableView.contentOffset = p
        }
        
    }

}
