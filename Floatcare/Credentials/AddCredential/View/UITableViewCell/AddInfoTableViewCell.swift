//
//  AddInfoTableViewCell.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 03/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

protocol AddInfoTableViewCellDelegate: class {
    func updateTableContentOffset(_ point: CGPoint)
}

class AddInfoTableViewCell: UITableViewCell,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CountryListDelegate {
    weak var controllerDelegate: AddInfoTableViewCellDelegate!
    @IBOutlet weak var fillOutLabel: UILabel!
    @IBOutlet weak var subSearchTitle: UILabel!
    
    @IBOutlet weak var backCameraImageView: UIImageView!
    @IBOutlet weak var frontCameraImageView: UIImageView!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var licenceNumberTextField: UILabel!
    @IBOutlet weak var backCameraButton: UIButton!
    @IBOutlet weak var frontCameraButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var licenceview: UIView!
    @IBOutlet weak var licenceArrow: UIImageView!
    @IBOutlet weak var licenceImage: UIImageView!
    @IBOutlet weak var issueTextField: UITextField!
    @IBOutlet weak var typeOfInfo: UIButton!
    @IBOutlet weak var expirateDateTextField: UITextField!
    @IBOutlet weak var licenceViewTextField: UITextField!
    @IBOutlet weak var compactLicenceTextField: UITextField!
    
    var picker = UIImagePickerController();
    var alert: UIAlertController!
    var pickImageCallback : ((UIImage) -> ())?;
    var viewController: UIViewController?
    let datePicker = UIDatePicker()
    var countryList = CountryList()
    var isUpdateShouldBeCalled: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addDoneButtonOnKeyboard()
        countryList.delegate = self
    }
    func selectedCountry(country: Country) {
        stateTextField.text = country.name
    }
    
    override func layoutSubviews() {
        superview?.layoutSubviews()
        bottomView.roundCorners(corners: [.topLeft], radius: 30)
        saveButton.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        saveButton.dropShadow(color: UIColor(displayP3Red: 92, green: 99, blue: 171, alpha: 1), opacity: 1, offSet: CGSize.init(width: 3, height: 3), radius: 3, scale: true)
        let leftMirrored = UIImage(cgImage: licenceArrow.image!.cgImage!, scale: 1.0, orientation: .left)
        licenceArrow.image = leftMirrored
        licenceview.layer.cornerRadius = 15
        licenceview.layer.masksToBounds = false
        licenceview?.layer.shadowColor = UIColor.blue.cgColor
        licenceview?.layer.shadowOffset =  CGSize.init(width: 3, height: 5)
        licenceview?.layer.shadowOpacity = 0.18
        licenceview?.layer.shadowRadius = 10
        
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        stateTextField.delegate = self
        expirateDateTextField.delegate = self
        issueTextField.delegate = self
        compactLicenceTextField.delegate = self
        licenceViewTextField.delegate = self
        
        stateTextField.inputAccessoryView = doneToolbar
        expirateDateTextField.inputAccessoryView = doneToolbar
        issueTextField.inputAccessoryView = doneToolbar
        compactLicenceTextField.inputAccessoryView = doneToolbar
        licenceViewTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        if issueTextField.isFirstResponder {
            issueTextField.resignFirstResponder()
        } else if expirateDateTextField.isFirstResponder {
            expirateDateTextField.resignFirstResponder()
        } else if licenceViewTextField.isFirstResponder {
            licenceViewTextField.resignFirstResponder()
        } else if stateTextField.isFirstResponder {
            stateTextField.resignFirstResponder()
        } else if compactLicenceTextField.isFirstResponder {
            compactLicenceTextField.resignFirstResponder()
        }
        controllerDelegate.updateTableContentOffset(CGPoint.zero)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func backCameraButtonTapped(_ sender: Any) {
        self.pickImage(viewController!) { (image) in
            self.backCameraImageView.image = image

        }
    }
    
    @IBAction func frontCameraButtonTapped(_ sender: Any) {
        self.pickImage(viewController!) { (image) in
            self.frontCameraImageView.image = image
        }
    }
    
    
    @IBAction func stateTextFieldTapped(_ sender: UITextField) {
        
    }
    @IBAction func compactLicenceTapped(_ sender: UITextField) {
        
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        saveButtonTapped()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if expirateDateTextField.isFirstResponder {
            showDatePicker(expirateDateTextField)
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if stateTextField.isFirstResponder {
            let navController = UINavigationController(rootViewController: countryList)
            navController.modalPresentationStyle = .popover
            textField.resignFirstResponder()
            self.viewController?.present(navController, animated: true, completion: nil)
        } else if compactLicenceTextField.isFirstResponder {
            compactLicenceTextField.resignFirstResponder()
            let alertView = UIAlertController(title: "Compact License", message: nil, preferredStyle: .actionSheet)
            let cameraAction = UIAlertAction(title: "Yes", style: .default){
                UIAlertAction in
                self.setValueForCompactText(true)
            }
            let galleryAction = UIAlertAction(title: "No", style: .default){
                UIAlertAction in
                self.setValueForCompactText(false)
            }
            alertView.addAction(cameraAction)
            alertView.addAction(galleryAction)
            alertView.popoverPresentationController?.sourceView = self.viewController!.view
            viewController?.present(alertView, animated: true, completion: nil)
        } else {
            let offset = textField.superview?.frame.origin ?? CGPoint.zero
            controllerDelegate.updateTableContentOffset(offset)
            if expirateDateTextField.isFirstResponder {
                
                datePicker.minimumDate = Date()
                datePicker.maximumDate = nil
                showDatePicker(expirateDateTextField)
            } else if issueTextField.isFirstResponder {
                datePicker.maximumDate = Date()
                datePicker.minimumDate = nil
                showDatePicker(issueTextField)
            }
        }
    }
    
    func setValueForCompactText(_ value: Bool) {
        if value {
            compactLicenceTextField.text = "Yes"
        } else {
            compactLicenceTextField.text = "No"
        }
    }
    
    func showDatePicker(_ txtDatePicker: UITextField){
    //Formate Date
       datePicker.datePickerMode = .date

      //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)

        txtDatePicker.inputAccessoryView = toolbar
        txtDatePicker.inputView = datePicker
    }

    @objc func donedatePicker(){

         let formatter = DateFormatter()
         formatter.dateFormat = "dd/MM/yyyy"
        if expirateDateTextField.isFirstResponder {
         expirateDateTextField.text = formatter.string(from: datePicker.date)
        } else if issueTextField.isFirstResponder {
            issueTextField.text = formatter.string(from: datePicker.date)
        }
        self.viewController?.view.endEditing(true)
    }

    @objc func cancelDatePicker(){
        self.viewController?.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        controllerDelegate.updateTableContentOffset(CGPoint.zero)
        return true
    }
    
    func saveButtonTapped() {
        if stateTextField.text?.isEmpty ?? true || compactLicenceTextField.text?.isEmpty ?? true || expirateDateTextField.text?.isEmpty ?? true || licenceViewTextField.text?.isEmpty ?? true {
            let alertVarning = UIAlertController.init(title: "Error", message: "Please fill all the details", preferredStyle: .alert)
            alertVarning.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.viewController?.present(alertVarning, animated: true, completion: nil)
        } else {
            DispatchQueue.main.async {
                Progress.shared.showProgressView()
            }
            
//            ["State License","BLS","ACLS","PALS","ATLS","Driver's License"]
//            ["Insurance Document","CV Document","I9 Document","Immunization records"]
//            ["Special Certificate","DEA Certificate"]
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "dd/MM/yyyy"
            let expirayData: String = expirateDateTextField.text ?? ""
            let showDate = inputFormatter.date(from: expirayData)
            inputFormatter.dateFormat = "yyyy-MM-dd"
            let resultString = inputFormatter.string(from: showDate!)
            var credentialTypeId = 0
            var credentialSubTypeId = 0
            let searchText = subSearchTitle.text ?? ""
//            [
//              "label": "State Nursing License" 1,1
//              "label": "National Nurse License" 1,2
//              "label": "ETC License" 1,3
//              "label": "BLS" 1,4
//              "label": "ACLS" 1,5
//              "label": "PALS" 1,6
//              "label": "ATLS" 1,7
//              "label": "Driver license" 1,8
//              "label": "State Nursing Certificate",2,9
//              "label": "National Nurse Certificate" 2,10
//              "label": "ETC Certificate" 2,11
//              "label": "DEA" 2,12
//              "label": "Spl Certificate"2,13
//              "label": "State Nurseing Document" 3,14
//              "label": "National Nurse Document" 3,15
//              "label": "ETC Document" 3,16
//              "label": "Insurance"3,17
//              "label": "1-9"3,18
//              "label": "Health Records" 3,19
//            ]
//
            switch searchText.lowercased() {
            case "State License":
                credentialTypeId = 1
                credentialSubTypeId = 1
            case "BLS":
                credentialTypeId = 1
                credentialSubTypeId = 4
            case "ACLS":
                credentialTypeId = 1
                credentialSubTypeId = 5
            case "PALS":
                credentialTypeId = 1
                credentialSubTypeId = 6
            case "ATLS":
                credentialTypeId = 1
                credentialSubTypeId = 7
            case "Driver's License":
                credentialTypeId = 1
                credentialSubTypeId = 8
            case "Insurance Document":
               credentialTypeId = 3
               credentialSubTypeId = 17
           case "CV Document":
               credentialTypeId = 3
               credentialSubTypeId = 16
           case "I9 Document":
               credentialTypeId = 3
               credentialSubTypeId = 18
           case "Immunization records":
               credentialTypeId = 3
               credentialSubTypeId = 19
           case "Special Certificate":
               credentialTypeId = 2
               credentialSubTypeId = 13
           case "DEA Certificate":
               credentialTypeId = 1
               credentialSubTypeId = 12
                
            default:
                break
            }

            let userId = Profile.shared.loginResponse?.data!.userBasicInformation?.userID ?? 0
            if isUpdateShouldBeCalled {
                let mutation = UpdateUserCredentialsMutation.init(userId: "\(userId)", credentialIcon: (licenceImage.image ?? UIImage()).toStringCompressed() ?? "", credentialName: searchText, credentialTypeId: "\(credentialTypeId)", credentialSubTypeId: "\(credentialSubTypeId)", expirationDate:  resultString, credentialStatusId: "4", licenseNumber: licenceViewTextField.text ?? "", frontFilePhoto: frontCameraImageView.image?.toStringCompressed() ?? "", backFilePhoto: backCameraImageView.image?.toStringCompressed() ?? "", state: stateTextField.text ?? "", isCompactLicense: compactLicenceTextField.text == "Yes" ? true : false)

                ApolloManager.shared.client.perform(mutation: mutation) { (result) in
                    DispatchQueue.main.async {
                       Progress.shared.hideProgressView()
                   }
                    switch result {
                        
                    case .success(let successData):
                        if let error = successData.errors {
                            DispatchQueue.main.async {
                                let alertVarning = UIAlertController.init(title: "Oops", message: "Something went wrong please try again.\n Error: \(error[0].localizedDescription)", preferredStyle: .alert)
                                alertVarning.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                                self.viewController?.present(alertVarning, animated: true, completion: nil)
                            }
                            print(error)
                        } else {
                            let alertView = UIAlertController(title: "Congratulation", message: "Data successfully uploaded and you can view your this in your credentials list screen.", preferredStyle: .alert)
                            let cameraAction = UIAlertAction(title: "Ok", style: .default){
                                UIAlertAction in
                                self.goToCredentialScreen()
                            }
                            alertView.addAction(cameraAction)
                            alertView.popoverPresentationController?.sourceView = self.viewController!.view
                            self.viewController?.present(alertView, animated: true, completion: nil)
                        }
                        
                    case .failure(let error):
                        DispatchQueue.main.async {
                            let alertVarning = UIAlertController.init(title: "Oops", message: "Something went wrong please try again.\n Error: \(error.localizedDescription)", preferredStyle: .alert)
                            alertVarning.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                            self.viewController?.present(alertVarning, animated: true, completion: nil)
                        }
                        print(error)
                    }
                }
            } else {
                let mutation = CreateUserCredentialsMutation.init(userId: "\(userId)", credentialIcon: (licenceImage.image ?? UIImage()).toStringCompressed() ?? "", credentialName: searchText, credentialTypeId: "\(credentialTypeId)", credentialSubTypeId: "\(credentialSubTypeId)", expirationDate:  resultString, credentialStatusId: "4", licenseNumber: licenceViewTextField.text ?? "", frontFilePhoto: frontCameraImageView.image?.toStringCompressed() ?? "", backFilePhoto: backCameraImageView.image?.toStringCompressed() ?? "", state: stateTextField.text ?? "", isCompactLicense: compactLicenceTextField.text == "Yes" ? true : false)

                ApolloManager.shared.client.perform(mutation: mutation) { (result) in
                    DispatchQueue.main.async {
                       Progress.shared.hideProgressView()
                   }
                    switch result {
                        
                    case .success(let successData):
                        if let error = successData.errors {
                            DispatchQueue.main.async {
                                let alertVarning = UIAlertController.init(title: "Oops", message: "Something went wrong please try again.\n Error: \(error[0].localizedDescription)", preferredStyle: .alert)
                                alertVarning.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                                self.viewController?.present(alertVarning, animated: true, completion: nil)
                            }
                            print(error)
                        } else {
                            let alertView = UIAlertController(title: "Congratulation", message: "Data successfully uploaded and you can view your this in your credentials list screen.", preferredStyle: .alert)
                            let cameraAction = UIAlertAction(title: "Ok", style: .default){
                                UIAlertAction in
                                self.goToCredentialScreen()
                            }
                            alertView.addAction(cameraAction)
                            alertView.popoverPresentationController?.sourceView = self.viewController!.view
                            self.viewController?.present(alertView, animated: true, completion: nil)
                        }
                        
                    case .failure(let error):
                        DispatchQueue.main.async {
                            let alertVarning = UIAlertController.init(title: "Oops", message: "Something went wrong please try again.\n Error: \(error.localizedDescription)", preferredStyle: .alert)
                            alertVarning.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                            self.viewController?.present(alertVarning, animated: true, completion: nil)
                        }
                        print(error)
                    }
                }
            }
            
            
            
        }
        
    }
    
    func goToCredentialScreen() {
        let vcs = self.viewController?.navigationController?.viewControllers
        let fil = vcs?.filter({$0.isKind(of: CredentialsViewController.self)})
        self.viewController?.navigationController?.popToViewController((fil?.last)!, animated: true)
        
    }
    
    func pickImage(_ viewController: UIViewController, _ callback: @escaping ((UIImage) -> ())) {
        pickImageCallback = callback;
        self.viewController = viewController;
        alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default){
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: .default){
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
            UIAlertAction in
        }

        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.viewController!.view
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        
        alert.dismiss(animated: true, completion: nil)
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            picker.sourceType = .camera
            self.viewController!.present(picker, animated: true, completion: nil)
        } else {
            let alertVarning = UIAlertController.init(title: "Warning", message: "You don't have camera access", preferredStyle: .alert)
            alertVarning.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.viewController?.present(alertVarning, animated: true, completion: nil)
        }
    }
    func openGallery(){
        alert.dismiss(animated: true, completion: nil)
        picker.sourceType = .photoLibrary
        self.viewController!.present(picker, animated: true, completion: nil)
    }


    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    // For Swift 4.2+
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        pickImageCallback?(image)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
    }
    
}



