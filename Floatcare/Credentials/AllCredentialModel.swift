//
//  AllCredentialModel.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 17/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let allCredentials = try? newJSONDecoder().decode(AllCredentials.self, from: jsonData)

import Foundation

// MARK: - AllCredentials
class AllCredentials: Codable {
    var findAllUserCredentials: [FindAllUserCredential] = []

    enum CodingKeys: String, CodingKey {
        case findAllUserCredentials
    }

    init(findAllUserCredentials: [FindAllUserCredential]?) {
        self.findAllUserCredentials = findAllUserCredentials ?? []
    }
    
    init(data: [[String: Any]]) {
        
        for credential in data {
            self.findAllUserCredentials.append(FindAllUserCredential.init(data: credential))
        }
    }
}

// MARK: - FindAllUserCredential
class FindAllUserCredential: Codable {
    let userCredentialId: String?
    let userId: String?
    let credentialIcon: String?
    let credentialTypeId: String?
    let credentialName: String?
    let credentialSubTypeName: String?
    let credentialSubTypeId: String?
    let credentialType: String?
    let credentialStatus: String?
    let expirationDate: String?
    let credentialStatusId: String?
    let licenseNumber: String?
    let frontFilePhoto: String?
    let backFilePhoto: String?
    let state: String?
    let isCompactLicense: String?

    enum CodingKeys: String, CodingKey {
        case userCredentialId
        case userId
        case credentialIcon
        case credentialTypeId
        case credentialName
        case credentialSubTypeName
        case credentialType
        case credentialStatus
        case expirationDate
        case credentialStatusId
        case licenseNumber
        case frontFilePhoto
        case backFilePhoto
        case state
        case isCompactLicense
        case credentialSubTypeId
    }

    init(userCredentialId: String?, userId: String?, credentialIcon: String?, credentialTypeId: String?, credentialName: String?, credentialSubTypeName: String?, credentialType: String?, credentialStatus: String?, expirationDate: String?, credentialStatusId: String?, licenseNumber: String?, frontFilePhoto: String?, backFilePhoto: String?, state: String?, isCompactLicense: String?, credentialSubTypeId: String?) {
        self.userCredentialId = userCredentialId
        self.userId = userId
        self.credentialIcon = credentialIcon
        self.credentialTypeId = credentialTypeId
        self.credentialName = credentialName
        self.credentialSubTypeName = credentialSubTypeName
        self.credentialType = credentialType
        self.credentialStatus = credentialStatus
        self.expirationDate = expirationDate
        self.credentialStatusId = credentialStatusId
        self.licenseNumber = licenseNumber
        self.frontFilePhoto = frontFilePhoto
        self.backFilePhoto = backFilePhoto
        self.state = state
        self.isCompactLicense = isCompactLicense
        self.credentialSubTypeId = credentialSubTypeId
    }
    
    init(data: [String: Any]) {
        self.userCredentialId = data["userCredentialId"] as? String
        self.userId = data["userId"] as? String
        self.credentialIcon = data["credentialIcon"] as? String
        self.credentialTypeId = data["credentialTypeId"] as? String
        self.credentialName = data["credentialName"] as? String
        self.credentialSubTypeName = data["credentialSubTypeName"] as? String
        self.credentialType = data["credentialType"] as? String
        self.credentialStatus = data["credentialStatus"] as? String
        self.expirationDate = data["expirationDate"] as? String
        self.credentialStatusId = data["credentialStatusId"] as? String
        
        self.licenseNumber = data["licenseNumber"] as? String
        self.frontFilePhoto = data["frontFilePhoto"] as? String
        self.backFilePhoto = data["backFilePhoto"] as? String
        self.state = data["state"] as? String
        self.isCompactLicense = data["isCompactLicense"] as? String
        self.credentialSubTypeId = data["credentialSubTypeId"] as? String
    }
}






