//
//  ChnagePrimaryNumberViewController.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 27/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import MRCountryPicker

class ChangePrimaryNumberViewController: UIViewController,UITextFieldDelegate, MRCountryPickerDelegate {


    @IBOutlet weak var countryPickerTextfield: UITextField!
    @IBOutlet weak var countryBut: UIButton!
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet var phoneNumberView: ChangePhoneNumber!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberView.controllerDelegate = self
        setUpNaigationBar()
        self.addDoneButtonOnKeyboard()
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPickerTextfield.inputView = countryPicker
        self.navigationController?.modalPresentationStyle = .fullScreen

    }
    @IBAction func countryButton(_ sender: Any) {
        
    }
    
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        countryBut.setImage(flag, for: .normal)
        countryPickerTextfield.text = countryCode
    }
    
    fileprivate func setUpNaigationBar() {
           navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
           navigationController?.navigationBar.shadowImage = UIImage()

           let button = UIButton(type:.custom)
           button.setImage(UIImage(named: "Back"), for: .normal)
           button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
           button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           let barButton = UIBarButtonItem(customView: button)
           self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)

    }
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        phoneNumberTextField.delegate = self
        phoneNumberTextField.inputAccessoryView = doneToolbar
        countryPickerTextfield.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction(){
        if phoneNumberTextField.isFirstResponder {
            phoneNumberTextField.resignFirstResponder()
        } else if countryPickerTextfield.isFirstResponder {
            countryPickerTextfield.resignFirstResponder()
        }
    }
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "(XXX) XXX-XXXX"

        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text = formattedNumber(number: newString)
        return false
    }

}

extension ChangePrimaryNumberViewController: ChangePrimaryNumberActionable {
    func savePhoneNumber() {
        if (phoneNumberTextField.text?.isEmpty ?? true) {
            let alert = UIAlertController(title: "Error", message: "Please enter phone number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)

        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
