//
//  ChangePhoneNumber.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 27/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
protocol ChangePrimaryNumberActionable: class {
    func savePhoneNumber()
}

class ChangePhoneNumber: UIView {

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var mainView: UIView!
    weak var controllerDelegate: ChangePrimaryNumberActionable!

    override func awakeFromNib() {
        superview?.awakeFromNib()

        // Will set a custom width instead of the anchor view width
//        dropDownLeft.width = 200
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        mainView.roundCorners(corners: [.bottomLeft], radius: 30)
        bottomView.roundCorners(corners: [.topLeft], radius: 30)
        saveButton.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
        saveButton.dropShadow(color: UIColor(displayP3Red: 92, green: 99, blue: 171, alpha: 1), opacity: 1, offSet: CGSize.init(width: 3, height: 3), radius: 3, scale: true)
        

    }
    @IBAction func saveButtonTapped(_ sender: Any) {
        controllerDelegate.savePhoneNumber()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
}

extension UIView {
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
      layer.masksToBounds = false
      layer.shadowColor = color.cgColor
      layer.shadowOpacity = opacity
      layer.shadowOffset = offSet
      layer.shadowRadius = radius

      layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
      layer.shouldRasterize = true
      layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
