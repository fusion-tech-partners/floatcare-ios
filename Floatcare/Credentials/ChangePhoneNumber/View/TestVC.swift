//
//  TestVC.swift
//  Floatcare
//
//  Created by BEAST on 04/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class TestVC: UIViewController {
    var backgroundImage: UIImage?
    var shadowImage: UIImage?
    var statusbarcolor: UIColor?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImage = navigationController?.navigationBar.backgroundImage(for: .default)
        shadowImage = navigationController?.navigationBar.shadowImage
        statusbarcolor = navigationController?.navigationBar.backgroundColor
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.setStatusBar(backgroundColor: UIColor(red: 241.0 / 255.0, green: 241.0 / 255.0, blue: 255.0 / 255.0, alpha: 1))
        navigationController?.navigationBar.backgroundColor = UIColor(red: 241.0 / 255.0, green: 241.0 / 255.0, blue: 255.0 / 255.0, alpha: 1)
        setUpNaigationBar()

    }
    
    fileprivate func setUpNaigationBar() {
           navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
           navigationController?.navigationBar.shadowImage = UIImage()

           let button = UIButton(type:.custom)
           button.setImage(UIImage(named: "Back"), for: .normal)
           button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
           button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           let barButton = UIBarButtonItem(customView: button)
           self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func backTapped() {
        navigationController?.navigationBar.setBackgroundImage(backgroundImage, for: .default)
               
        navigationController?.navigationBar.shadowImage = shadowImage
        navigationController?.setStatusBar(backgroundColor: statusbarcolor ?? UIColor.clear)
        navigationController?.navigationBar.backgroundColor = statusbarcolor
        self.navigationController?.popViewController(animated: true)
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
