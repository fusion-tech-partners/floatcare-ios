//
//  CredentialTableViewCell.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 17/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class CredentialTableViewCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var certificateImageView: UIImageView!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var activeStatus: UILabel!
    @IBOutlet weak var approvalStatus: UILabel!
    @IBOutlet weak var expires: UILabel!
    @IBOutlet weak var licenceview: UIView!
    @IBOutlet weak var statusColorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        superview?.layoutSubviews()
       
        mainView.layer.cornerRadius = 15
        mainView.layer.masksToBounds = false
        mainView?.layer.shadowColor = UIColor.blue.cgColor
        mainView?.layer.shadowOffset =  CGSize.init(width: 3, height: 5)
        mainView?.layer.shadowOpacity = 0.18
        mainView?.layer.shadowRadius = 10
        
    }

}
