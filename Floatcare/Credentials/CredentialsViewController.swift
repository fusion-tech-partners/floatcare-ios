//
//  CredentialsViewController.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class CredentialsViewController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var activeButton: UIButton!
    @IBOutlet weak var pendingButton: UIButton!
    @IBOutlet weak var inactiveButton: UIButton!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var addcredentialsBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var navigationBar: NavigationBar!
    var credentails = [FindAllUserCredential]()
    @IBOutlet weak var searchBarTextField: UITextField!
    var filterValues: [FindAllUserCredential]?
    var searchFilterValues: [FindAllUserCredential]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.roundCorners(corners: [.bottomLeft], radius: 30)
        setUpNaigationBar()
        mainTableView.dataSource = self
        mainTableView.delegate = self
        
        let backImage = UIImage(named: "ic_ChevronLeft")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpMainView()
        filterValues = nil
        searchFilterValues = nil
        getData()
        
    }
    func getData() {
        let query = FindAllUserCredentialsByUserIdQuery.init(userId: Profile.shared.userBasicInformation?.userId ?? "")
        DispatchQueue.main.async {
            Progress.shared.showProgressView()
        }
        ApolloManager.shared.client.clearCache()
        ApolloManager.shared.client.fetch(query: query) { (result) in
            Progress.shared.hideProgressView()
            switch result {
            case .success(let successData):
                if let error = successData.errors {
                    DispatchQueue.main.async {
                        let alertVarning = UIAlertController.init(title: "Oops", message: "Something went wrong please try again.\n Error: \(error[0].localizedDescription)", preferredStyle: .alert)
                        alertVarning.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alertVarning, animated: true, completion: nil)
                    }
                    print(error)
                } else {
                    guard let data = successData.data else { return }
                    if data.resultMap.count > 0,
                        let dic = data.resultMap["findAllUserCredentialsByUserId"] as? [[String: Any]] {
                        let allcred = AllCredentials.init(data: dic)
                        let arrayObj = allcred.findAllUserCredentials
                        if  arrayObj.count > 0 {
                            self.credentails = arrayObj
                            DispatchQueue.main.async {
                                self.filterValues = arrayObj //self.credentails.filter {$0.credentialStatusId == "1" }
                                self.mainTableView.reloadData()
                            }
                            return
                        }
                    }
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    let alertVarning = UIAlertController.init(title: "Oops", message: "Something went wrong please try again.\n Error: \(error.localizedDescription)", preferredStyle: .alert)
                    alertVarning.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alertVarning, animated: true, completion: nil)
                }
                print(error)
            }
        }
    }
    
    func setUpMainView() {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: searchBarTextField.frame.height - 1, width: searchBarTextField.frame.width, height: 1.0)
        bottomLine.backgroundColor = UIColor(red: 224.0 / 255.0, green: 230.0 / 255.0, blue: 255.0 / 255.0, alpha: 1).cgColor
        searchBarTextField.borderStyle = UITextField.BorderStyle.none
        searchBarTextField.layer.addSublayer(bottomLine)
        searchBarTextField.delegate = self
        searchBarTextField.isHidden = true
        addDoneButtonOnKeyboard()
        searchFilterValues = nil
        
//        bottomView.layer.borderWidth = 2
//        bottomView.layer.borderColor = UIColor(red: 224.0 / 255.0, green: 230.0 / 255.0, blue: 255.0 / 255.0, alpha: 1).cgColor
        activeButton.backgroundColor = UIColor.clear
        pendingButton.backgroundColor = UIColor.clear
        inactiveButton.backgroundColor = UIColor.clear
        activeButton.setTitleColor(UIColor(red: 109.0 / 255.0, green: 114.0 / 255.0, blue: 120.0 / 255.0, alpha: 1), for: .normal)
        pendingButton.setTitleColor(UIColor(red: 109.0 / 255.0, green: 114.0 / 255.0, blue: 120.0 / 255.0, alpha: 1), for: .normal)
        inactiveButton.setTitleColor(UIColor(red: 109.0 / 255.0, green: 114.0 / 255.0, blue: 120.0 / 255.0, alpha: 1), for: .normal)
        activeButtonTapped(activeButton!)
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        searchBarTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        searchBarTextField.resignFirstResponder()
        
        if searchBarTextField.text?.count ?? 0 == 0 {
            searchFilterValues = nil
            DispatchQueue.main.async {
                self.searchBarTextField.isHidden = true
            }
            return
        }
        searchFilterValues = filterValues?.filter {
            ($0.credentialType?.localizedCaseInsensitiveContains(searchBarTextField.text ?? "") ?? false)
        }
        DispatchQueue.main.async {
            self.mainTableView.reloadData()
        }
    }
    @IBAction func addcredentialsClick(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(identifier: "AddCredentialViewController") as! AddCredentialViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func activeButtonTapped(_ sender: Any) {
//        bottomView.layer.borderWidth = 2
//        bottomView.layer.borderColor = UIColor(red: 224.0 / 255.0, green: 230.0 / 255.0, blue: 255.0 / 255.0, alpha: 1).cgColor
        activeButton.backgroundColor = UIColor(red: 230.0 / 255.0, green: 231.0 / 255.0, blue: 242.0 / 255.0, alpha: 1)
        pendingButton.backgroundColor = UIColor.clear
        inactiveButton.backgroundColor = UIColor.clear
        activeButton.setTitleColor(UIColor(red: 72.0 / 255.0, green: 93.0 / 255.0, blue: 186.0 / 255.0, alpha: 1), for: .normal)
        pendingButton.setTitleColor(UIColor(red: 109.0 / 255.0, green: 114.0 / 255.0, blue: 120.0 / 255.0, alpha: 1), for: .normal)
        inactiveButton.setTitleColor(UIColor(red: 109.0 / 255.0, green: 114.0 / 255.0, blue: 120.0 / 255.0, alpha: 1), for: .normal)
        searchFilterValues = nil
        searchBarTextField.text = ""
//        filterValues = credentails.filter {$0.credentialStatusId == "1" }
        mainTableView.reloadData()
        
    }
    
    @IBAction func pendingButtonTapped(_ sender: Any) {
        pendingButton.backgroundColor = UIColor(red: 230.0 / 255.0, green: 231.0 / 255.0, blue: 242.0 / 255.0, alpha: 1)
        activeButton.backgroundColor = UIColor.clear
        inactiveButton.backgroundColor = UIColor.clear
        pendingButton.setTitleColor(UIColor(red: 72.0 / 255.0, green: 93.0 / 255.0, blue: 186.0 / 255.0, alpha: 1), for: .normal)
        activeButton.setTitleColor(UIColor(red: 109.0 / 255.0, green: 114.0 / 255.0, blue: 120.0 / 255.0, alpha: 1), for: .normal)
        inactiveButton.setTitleColor(UIColor(red: 109.0 / 255.0, green: 114.0 / 255.0, blue: 120.0 / 255.0, alpha: 1), for: .normal)
        searchFilterValues = nil
        searchBarTextField.text = ""
        filterValues = credentails.filter {$0.credentialStatusId == "4" }
        mainTableView.reloadData()
    }
    
    @IBAction func inactiveButtonTapped(_ sender: Any) {
        inactiveButton.backgroundColor = UIColor(red: 230.0 / 255.0, green: 231.0 / 255.0, blue: 242.0 / 255.0, alpha: 1)
        activeButton.backgroundColor = UIColor.clear
        pendingButton.backgroundColor = UIColor.clear
        inactiveButton.setTitleColor(UIColor(red: 72.0 / 255.0, green: 93.0 / 255.0, blue: 186.0 / 255.0, alpha: 1), for: .normal)
        pendingButton.setTitleColor(UIColor(red: 109.0 / 255.0, green: 114.0 / 255.0, blue: 120.0 / 255.0, alpha: 1), for: .normal)
        activeButton.setTitleColor(UIColor(red: 109.0 / 255.0, green: 114.0 / 255.0, blue: 120.0 / 255.0, alpha: 1), for: .normal)
        searchFilterValues = nil
        searchBarTextField.text = ""
        filterValues = credentails.filter {$0.credentialStatusId == "2" || $0.credentialStatusId == "3" }
        mainTableView.reloadData()
    }
    
    @IBAction func sortButtonTapped(_ sender: Any) {
    }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        searchBarTextField.isHidden = false
        searchBarTextField.becomeFirstResponder()
    }
    
    fileprivate func setUpNaigationBar() {
       navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
       navigationController?.navigationBar.shadowImage = UIImage()

       let button = UIButton(type:.custom)
       button.setImage(UIImage(named: "Back"), for: .normal)
       button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
       button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
       let barButton = UIBarButtonItem(customView: button)
       self.navigationItem.leftBarButtonItem = barButton
        
        let button1 = UIButton(type:.custom)
        button1.setImage(UIImage.init(systemName: "plus"), for: .normal)
        button1.tintColor = UIColor(red: 72.0 / 255.0, green: 92.0 / 255.0, blue: 186.0 / 255.0, alpha: 1)
        button1.addTarget(self, action:#selector(goToAddCredential), for: .touchUpInside)
        button1.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItem = barButton1
        mainView.backgroundColor = .profileBgColor
    }
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func goToAddCredential() {
        let addCredentialController = UIStoryboard.init(name: "ChangePhoneNumber", bundle: nil).instantiateViewController(identifier: "AddCredentialViewController")
        self.navigationController?.show(addCredentialController, sender: self)
    }
}

extension CredentialsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let searchCountr = searchFilterValues {
            return searchCountr.count
        }
        if let filterCount = filterValues {
            return filterCount.count
        }
        return credentails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CredentialTableViewCell", for: indexPath as IndexPath) as! CredentialTableViewCell
        var obj = FindAllUserCredential.init(data: [:])
        if let searchCountr = searchFilterValues {
            obj = searchCountr[indexPath.row]
        }else if let filterCount = filterValues {
            obj = filterCount[indexPath.row]
        } else {
            obj = credentails[indexPath.row]
        }
        cell.type.text = obj.credentialType ?? ""
        cell.title.text = obj.credentialName ?? ""
        cell.expires.text = ""
        if let expDate = obj.expirationDate {
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-MM-dd"
            
            if let showDate = inputFormatter.date(from: expDate) {
                inputFormatter.dateFormat = "dd/MM/yyyy"
                let resultString = inputFormatter.string(from: showDate)
                cell.expires.text = resultString
            }
        }
        cell.activeStatus.text = ""
        if let statusId = obj.credentialStatusId {
            switch statusId {
            case "1":
                cell.activeStatus.text = "Active"
                cell.approvalStatus.text = "Soft Approved"
            case "2":
                cell.activeStatus.text = "Missing"
                cell.approvalStatus.text = "Can't find details"
            case "3":
                cell.activeStatus.text = "Rejected"
                cell.approvalStatus.text = "Not Approved"
            case "4":
                cell.activeStatus.text = "Pending Review"
                cell.approvalStatus.text = "Review in process"
            default:
                cell.activeStatus.text = "Missing"
                cell.approvalStatus.text = "Can't find details"
            }
        } else {
            cell.activeStatus.text = "Missing"
            cell.approvalStatus.text = "Can't find details"
        }
        cell.certificateImageView?.image = (obj.credentialIcon ?? "").toImage()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        var obj = FindAllUserCredential.init(data: [:])
//           if let searchCountr = searchFilterValues {
//               obj = searchCountr[indexPath.row]
//           }else if let filterCount = filterValues {
//               obj = filterCount[indexPath.row]
//           } else {
//               obj = credentails[indexPath.row]
//           }
//        let addInfoPage = UIStoryboard.init(name: "ChangePhoneNumber", bundle: nil).instantiateViewController(identifier: "AddInfoViewController") as! AddInfoViewController
//        addInfoPage.filterValue = obj
//        self.navigationController?.show(addInfoPage, sender: self)
    }
}


extension CredentialsViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchFilterValues = nil
        DispatchQueue.main.async {
            self.mainTableView.reloadData()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if textField.text?.count ?? 0 == 0 {
            searchFilterValues = nil
            DispatchQueue.main.async {
                self.searchBarTextField.isHidden = true
            }
            return false
        }
        searchFilterValues = filterValues?.filter {
            ($0.credentialType?.localizedCaseInsensitiveContains(textField.text ?? "") ?? false)
        }
        DispatchQueue.main.async {
            self.mainTableView.reloadData()
        }
        return false
    }
}
