//
//  CustomSearchTextField.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import CoreData

protocol CustomSearchTextFieldProtocol: class {
    func keyBoardAppeared(_ appeared: Bool)
    func dismissView()
}

class CustomSearchTextField: UITextField{
    weak var customSearchTextFieldDelegate: CustomSearchTextFieldProtocol?
    var dataList : [String] = [String]()
    var showTableView = false
    var resultsList : [String] = [String]() {
        didSet {
            DispatchQueue.main.async {
                self.tableView?.reloadData()
            }
        }
    }
    var tableView: UITableView?
        
    // Connecting the new element to the parent view
    open override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        tableView?.removeFromSuperview()
    }
    
    override open func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        self.addTarget(self, action: #selector(CustomSearchTextField.textFieldDidChange), for: .editingChanged)
        self.addTarget(self, action: #selector(CustomSearchTextField.textFieldDidBeginEditing), for: .editingDidBegin)
        self.addTarget(self, action: #selector(CustomSearchTextField.textFieldDidEndEditing), for: .editingDidEnd)
        self.addTarget(self, action: #selector(CustomSearchTextField.textFieldDidEndEditingOnExit), for: .editingDidEndOnExit)
    }
    
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        self.attributedPlaceholder = NSAttributedString(string: "Search & Select Type",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 92.0 / 255.0, green: 99.0 / 255.0, blue: 171.0 / 255.0, alpha: 1)])
        buildSearchTableView()
        
    }
    
    
    //////////////////////////////////////////////////////////////////////////////
    // Text Field related methods
    //////////////////////////////////////////////////////////////////////////////
    
    @objc open func textFieldDidChange(){
        print("Text changed ...")
        filter()
        updateSearchTableView()
        tableView?.isHidden = false
    }
    
    @objc open func textFieldDidBeginEditing() {
        customSearchTextFieldDelegate?.keyBoardAppeared(true)
        print("Begin Editing")
    }
    
    @objc open func textFieldDidEndEditing() {
        customSearchTextFieldDelegate?.keyBoardAppeared(false)
        print("End editing")

    }
    
    @objc open func textFieldDidEndEditingOnExit() {
        customSearchTextFieldDelegate?.keyBoardAppeared(false)
        print("End on Exit")
    }

    // MARK: Filtering methods
    
    fileprivate func filter() {
        guard let searchString = self.text else {
            resultsList.removeAll()
            return
        }

        // Filter the data array and get only those countries that match the search text.
        resultsList = dataList.filter({ (country) -> Bool in
            return country.localizedCaseInsensitiveContains(searchString)

        })

        // Reload the tableview.
        tableView?.reloadData()
    }
    

}

extension CustomSearchTextField: UITableViewDelegate, UITableViewDataSource {
    

    //////////////////////////////////////////////////////////////////////////////
    // Table View related methods
    //////////////////////////////////////////////////////////////////////////////
    
    
    // MARK: TableView creation and updating
    
    // Create SearchTableview
    func buildSearchTableView() {

        if let tableView = tableView {
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CustomSearchTextFieldCell")
            tableView.delegate = self
            tableView.dataSource = self
            self.superview?.addSubview(tableView)

        } else {
            //addData()
            print("tableView created")
            tableView = UITableView(frame: CGRect.zero)
        }
        
        updateSearchTableView()
    }
    
    // Updating SearchtableView
    func updateSearchTableView() {
        
        if let tableView = tableView {
            superview?.bringSubviewToFront(tableView)
            var tableHeight: CGFloat = 0
            tableHeight = tableView.contentSize.height
            
            // Set a bottom margin of 10p
            if tableHeight < tableView.contentSize.height {
                tableHeight -= 10
            }
            
            // Set tableView frame
            var tableViewFrame = CGRect(x: 0, y: 0, width: frame.size.width, height: tableHeight)
            tableViewFrame.origin = self.convert(tableViewFrame.origin, to: nil)
            tableViewFrame.origin.x = frame.origin.x - 15
            tableViewFrame.origin.y = frame.origin.y + frame.size.height + 15
            UIView.animate(withDuration: 0.2, animations: { [weak self] in
                self?.tableView?.frame = tableViewFrame
            })
            
            //Setting tableView style
            tableView.layer.masksToBounds = true
            tableView.separatorInset = UIEdgeInsets.zero
            tableView.layer.cornerRadius = 5.0
            tableView.separatorColor = UIColor.lightGray
            tableView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
            
            if self.isFirstResponder {
                superview?.bringSubviewToFront(self)
            }
            tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
            tableView.reloadData()
            
            if !showTableView {
                self.tableView?.isHidden = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.showTableView = true
                    self.tableView?.isHidden = false
                }
            }
        }
    }
    
    
    
    // MARK: TableViewDataSource methods
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultsList.count > 0 || !(self.text?.isEmpty ?? true){
            return resultsList.count
        } else {
            return dataList.count
        }
    }
    
    // MARK: TableViewDelegate methods
    
    //Adding rows in the tableview with the data from dataList

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomSearchTextFieldCell", for: indexPath) as UITableViewCell
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor(red: 181.0 / 255.0, green: 181.0 / 255.0, blue: 181.0 / 255.0, alpha: 1)
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.font.withSize(14)
        if resultsList.count > 0  || !(self.text?.isEmpty ?? true){
            cell.textLabel?.text = resultsList[indexPath.row]

        } else {
            cell.textLabel?.text = dataList[indexPath.row]
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row")
        if resultsList.count > 0 {
            self.text = resultsList[indexPath.row]
        } else {
            self.text = dataList[indexPath.row]
        }
        tableView.isHidden = true
        self.endEditing(true)
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
