//
//  LicenceViewController.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 28/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

enum TypeOfCredential {
    case licence
    case document
    case certificate
}

class LicenceViewController: UIViewController, SubCategoryViewControllerDelegate {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var navigationBar: NavigationBar!
    var type = TypeOfCredential.licence
    private var subCategoryViewController = UIStoryboard.init(name: "ChangePhoneNumber", bundle: Bundle.main).instantiateViewController(identifier: "SubCategoryViewController") as? SubCategoryViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNaigationBar()
        let leftMirrored = UIImage(cgImage: arrowImage.image!.cgImage!, scale: 1.0, orientation: .left)
        arrowImage.image = leftMirrored
        
        let backImage = UIImage(named: "ic_ChevronLeft")
                     self.navigationController?.navigationBar.backIndicatorImage = backImage
                     self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage

                     self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                             
                     
                     self.navigationBar.LeftButtonActionBlock = { sender in
                         self.navigationController?.navigationBar.isHidden = false
                         self.navigationController?.popViewController(animated: true)
                     }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch type {
        case .licence:
            headerTitle.text = "Select a type of license"
            iconImageView.image = UIImage.init(named: "licence")
        case .document:
            headerTitle.text = "Select a type of document"
            iconImageView.image = UIImage.init(named: "Document")
        case .certificate:
            headerTitle.text = "Select a type of certificate"
            iconImageView.image = UIImage.init(named: "Certificate")
        }
        
    }
    
    fileprivate func setUpNaigationBar() {
           navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
           navigationController?.navigationBar.shadowImage = UIImage()

           let button = UIButton(type:.custom)
           button.setImage(UIImage(named: "Back"), for: .normal)
           button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
           button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           let barButton = UIBarButtonItem(customView: button)
           self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        titleView.layer.cornerRadius = 15
        titleView.layer.masksToBounds = false
        titleView?.layer.shadowColor = UIColor.blue.cgColor
        titleView?.layer.shadowOffset =  CGSize.init(width: 3, height: 5)
        titleView?.layer.shadowOpacity = 0.18
        titleView?.layer.shadowRadius = 10
    }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        
        subCategoryViewController = UIStoryboard.init(name: "ChangePhoneNumber", bundle: Bundle.main).instantiateViewController(identifier: "SubCategoryViewController") as? SubCategoryViewController
        subCategoryViewController?.subCatDelegate = self
        subCategoryViewController?.type = type
        self.navigationController?.pushViewController(subCategoryViewController!, animated: true)
//        if subCategoryViewController != nil {
//            subCategoryViewController?.type = type
//            subCategoryViewController?.subCatDelegate = self
//            self.navigationController?.addChild(subCategoryViewController!)
//            self.navigationController?.view.addSubview(subCategoryViewController!.view)
////            subCategoryViewController!.view.alpha = 0.9
//            subCategoryViewController!.didMove(toParent: self.navigationController)
//            subCategoryViewController!.willMove(toParent: self.navigationController)
//        }
    }

    @objc func thumbsUpButtonPressed() {
        subCategoryViewController?.view.removeFromSuperview()
        subCategoryViewController?.removeFromParent()
    }
    
    func closeView() {
        thumbsUpButtonPressed()
    }
    
    func goButtonTapped(_ searchText: String) {
//        thumbsUpButtonPressed()
        let controller = UIStoryboard.init(name: "ChangePhoneNumber", bundle: nil).instantiateViewController(identifier: "AddInfoViewController") as! AddInfoViewController
        controller.selectedType = searchText
        controller.type = type
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
