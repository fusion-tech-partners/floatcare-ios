//
//  SubCategoryViewController.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import RAMReel
import DropDown


protocol SubCategoryViewControllerDelegate: class {
    func closeView()
    func goButtonTapped(_ searchText: String)
}

class SubCategoryViewController: UIViewController, UITextFieldDelegate {
    
    fileprivate(set) public var words: [String] = []

    var dataSource: SimplePrefixQueryDataSource!
    var ramReel: RAMReel<RAMCell, RAMTextField, SimplePrefixQueryDataSource>!
    var finalData = [String]()
    var refTextField : UITextField!
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var tfSubcategory: UITextField!
    weak var subCatDelegate: SubCategoryViewControllerDelegate?
    @IBOutlet weak var searchTextField: CustomSearchTextField!
    @IBOutlet weak var goview: UIView!
    @IBOutlet weak var goButtonBottomConstraint: NSLayoutConstraint!
    var type = TypeOfCredential.licence
    var finalText = ""
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var navigationBar: NavigationBar!
    
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNaigationBar()
        var array = [String]()
        switch type {
        
        case .licence:
            //Fill out license information
            array = ["State License","BLS","ACLS","PALS","ATLS","Driver's License"]
        case .document:
            array = ["Insurance Document","CV Document","I9 Document","Immunization records"]
        case .certificate:
            array = ["Special Certificate","DEA Certificate"]
        }
        let joinedStrings = array.joined(separator: "\n")

        do {
            try joinedStrings.write(toFile: getFileURL().path, atomically: true, encoding: .utf8)
            getdata()
            } catch let error {
            // handle error
            print("Error on writing strings to file: \(error)")
        }
        
        print(self.finalData)
        dataSource = SimplePrefixQueryDataSource(finalData)
        
        ramReel = RAMReel(frame: searchView.bounds, dataSource: dataSource, placeholder: "Search & Select Type", attemptToDodgeKeyboard: false) {
            print("Plain:", $0)
        }
        var theme = RAMTheme.sharedTheme
        theme = theme.font(UIFont.systemFont(ofSize: 16))
        theme = theme.textColor(UIColor(red: 92.0 / 255.0, green: 99.0 / 255.0, blue: 171.0 / 255.0, alpha: 1))
        ramReel.theme = theme
        ramReel.hooks.append {
            let r = Array($0.reversed())
            let j = String(r)
            print("Reversed:", j)
        }
        ramReel.textFieldDelegate = self

        
        searchView.addSubview(ramReel.view)
        ramReel.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tfSubcategory.delegate = self
        
        let backImage = UIImage(named: "ic_ChevronLeft")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }
        tfSubcategory.font = (UIFont.systemFont(ofSize: 16))
        tfSubcategory.textColor = (UIColor(red: 92.0 / 255.0, green: 99.0 / 255.0, blue: 171.0 / 255.0, alpha: 1))
        
        dropDown.dataSource = array
        dropDown.backgroundColor = .white
        dropDown.textFont = (UIFont.systemFont(ofSize: 16))
        dropDown.textColor = (UIColor(red: 92.0 / 255.0, green: 99.0 / 255.0, blue: 171.0 / 255.0, alpha: 1))
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfSubcategory.text = item
            self.finalText = item
          print("Selected item: \(item) at index: \(index)")
        }
        addDoneButtonOnKeyboard()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mainView.roundCorners(corners: [.bottomLeft], radius: 30)
       
    }
    fileprivate func setUpNaigationBar() {
           navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
           navigationController?.navigationBar.shadowImage = UIImage()

           let button = UIButton(type:.custom)
           button.setImage(UIImage(named: "Back"), for: .normal)
           button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
           button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           let barButton = UIBarButtonItem(customView: button)
           self.navigationItem.leftBarButtonItem = barButton
        mainView.backgroundColor = .profileBgColor
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()

        tfSubcategory.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction(){
        tfSubcategory.resignFirstResponder()
    }
    
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)

    }
    func showPopOver(tf:UITextField)  {
        dropDown.show()
        dropDown.anchorView = tf // UIView or UIBarButtonIte
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        finalText = string
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.keyBoardAppeared(false)
        if textField.text?.isEmpty ?? true {
            
        } else {
            let data = dataSource.resultsForQuery(textField.text ?? "")
            let middleElement = data.count/2
            if data.count > 0{
                textField.text = data[middleElement].capitalized
            }
        }
        finalText = textField.text ?? ""
        print("End editing")
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        self.keyBoardAppeared(true)
//        refTextField = textField
        showPopOver(tf: textField)
//        print("Begin Editing")
    }
    
    func getdata() {
        let data: [String] = {
            
            do {
                let savedData = try Data(contentsOf: getFileURL())
             if let savedString = String(data: savedData, encoding: .utf8) {
                print(savedString)
                
                let contents = try String(contentsOfFile: getFileURL().path)
                       
                       guard !contents.isEmpty else {
                        return []
                    }
                
                let words = contents.split(separator: "\n")
                self.words = words.map(String.init)
                return self.words
             }
            } catch {
             print("Unable to read the file")
                            return []
            }
            
            return []
        }()
        
        finalData = data
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getFileURL() -> URL {
        return  getDocumentsDirectory().appendingPathComponent("SubCategoryViewControllerFloatCare").appendingPathExtension("txt")
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        subCatDelegate?.closeView()
        
    }
    @IBAction func goButtonTapped(_ sender: Any) {
        finalText = tfSubcategory.text ?? ""
        if !finalText.isEmpty {
//            let data = dataSource.resultsForQuery(finalText)
//            let middleElement = data.count/2
//            if data.count > 0{
//                finalText = data[middleElement].capitalized
//            }else{
//                finalText = refTextField.text ?? ""
//            }
            subCatDelegate?.goButtonTapped(self.finalText)
//            self.navigationController?.popViewController(animated: true)
        } else {
            let alert = UIAlertController(title: "Oops", message: "Please enter valid text", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func keyBoardAppeared(_ appeared: Bool) {
        if appeared {
            UIView.animate(withDuration: 0.5) {
                self.goButtonBottomConstraint.constant += 300
            }

        } else {
            UIView.animate(withDuration: 0.5) {
                self.goButtonBottomConstraint.constant = 100
            }
        }
    }
    func dismissView() {
        subCatDelegate?.closeView()
    }
}

