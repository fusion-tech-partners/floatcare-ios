//
//  DeepLinks.swift
//  Floatcare
//
//  Created by OMNIADMIN on 16/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

enum DeepLinksTypes: Int {
    
    case workSpacesInvitation = 1
    case newMessage = 2
    case startShiftPlanning = 3
    case remainderShiftPlanning = 4
    case missedShiftPlanning = 5
    case remainderShiftSchedule = 6
    case publishedSchedule = 7
    case approvedOpenShifts = 8
    case requestOpenShift = 9
}

extension UIViewController {
    
    func handleDeepLinks(deeplink: DeepLinksTypes){
        
        guard let topVC = self.getTopViewController() else { return }
        
        print(topVC.self)
//        if let topNVC = topVC as? UINavigationController,
//        topNVC.viewControllers.contains(<#T##element: UIViewController##UIViewController#>){
//
//        }
    }
    
    
    private func getTopViewController() -> UIViewController? {
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            return topController
         }
        
        return nil
    }
}
