//
//  StringExtenstion.swift
//  Floatcare
//
//  Created by Venkateswara Meda on 08/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

extension String {
    func toImage() -> UIImage? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters){
            return UIImage(data: data)
        }
        return nil
    }
}
