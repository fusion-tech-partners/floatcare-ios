//
//  CalendarActionsVC.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 01/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import Toast_Swift
let notification_Myrequest = "CallMyRequests"


class CalendarActionsVC: UIViewController {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var actionTblView: UITableView!
    @IBOutlet weak var closeBtn: UIButton!
    var  sectionNames = [ "Make Requests", "Set Availability", "Set Personal Events"];
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.closeBtn.layer.cornerRadius = CGFloat(roundf(Float(self.closeBtn.frame.size.width / 2.0)))
        
        let nib1 = UINib(nibName: "WorkSitesTableCell", bundle: nil)
              self.actionTblView.register(nib1, forCellReuseIdentifier: "workSiteCell")
              actionTblView.tableFooterView = UIView() //rgb(247, 247, 251)
        self.actionTblView.separatorColor = UIColor(red: 247, green: 247, blue: 251)
         self.bgView.roundCorners([ .layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 20.0)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func closeBtnTapped(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification_Myrequest), object: nil, userInfo: nil)
        self.dismiss(animated: true, completion: nil)
    }
}
extension CalendarActionsVC: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
       
            return 1
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return sectionNames.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 70
    }
    
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
          let cell = tableView.dequeueReusableCell(withIdentifier: "workSiteCell")
                      as! WorkSitesTableCell  //rgb(224, 225, 239)
          cell.wsBtn.backgroundColor = UIColor(red: 224, green: 225, blue: 239)
        cell.wsBtn.setImage(UIImage(named: "Basic Account"), for: .normal)
        cell.imgView.isHidden = false
        //  cell.wsBtn.tag = indexPath.row
        //  cell.wsBtn.addTarget(self, action: #selector(wsBtnTapped(sender:)), for: .touchUpInside)
        cell.siteNameLbl.text = sectionNames[indexPath.row]
          cell.selectionStyle = .none
          return cell
       
       
     
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            
            GetWorkSiteDetails().checkForWorkSpaceId(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", completion:{ [weak self] (userInfo) in
                
//                self?.userBasicInformation = userInfo
//                self?.updateuserDetails()
                print(userInfo)
                 DispatchQueue.main.async {
                    if userInfo?.count ?? 0 > 0 {
                        let sb = UIStoryboard.init(name: "RequestTypeVC", bundle: nil)
                        let requestVC = sb.instantiateViewController(identifier: "RequestTypeVC") as! RequestTypeVC
                        let nav = UINavigationController.init(rootViewController: requestVC)
                        nav.modalPresentationStyle = .fullScreen
                        self?.present(nav, animated: true, completion: nil)
                    }else {
                        let alert = UIAlertController(title: "", message: "You are not assigned to any workspace", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        }))
                        self?.present(alert, animated: true, completion: nil)
                    }
                }
            })
          
            
        }
        if indexPath.row == 1 {
//            moveToSetAvailability()
            self.view.makeToast("Feature Coming Soon")
        }

        if indexPath.row == 2 {
//            moveToSetAvailability()
            self.view.makeToast("Feature Coming Soon")
        }

    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    

 
}


extension CalendarActionsVC {
    func moveToSetAvailability() {
        DispatchQueue.main.async {
            let setAvailability = UIStoryboard.init(name: "Availability", bundle: nil).instantiateViewController(identifier: "SetAvailabilityVC") as! SetAvailabilityVC
            setAvailability.modalPresentationStyle = .fullScreen
            self.present(setAvailability, animated: true, completion: nil)
        }
    }
}
