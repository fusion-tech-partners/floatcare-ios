//
//  CalendarVC.swift
//  Float care
//
//  Created by Mounika.x.muduganti on 18/06/20.
//  Copyright © 2020 Mounika.x.muduganti. All rights reserved.
//

import UIKit
import SOPullUpView
enum ScheduleLayoutType {
    case myRequest
    case openShift
    case schedule
    case available
}

class CalendarVC: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance  {

    @IBOutlet weak var satday: UILabel!
    @IBOutlet weak var friday: UILabel!
    @IBOutlet weak var thursday: UILabel!
    @IBOutlet weak var wedday: UILabel!
    @IBOutlet weak var tuesday: UILabel!
    @IBOutlet weak var monday: UILabel!
    @IBOutlet weak var sunday: UILabel!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var headerView: UIView!
   // @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var filterBtn: UIButton!
    
    @IBOutlet weak var monthLbl: UILabel!
     let pullUpController = SOPullUpControl()
    var scheduleLayoutArr : [ShiftPlanningLayout]?
     var visualEffectView:UIVisualEffectView!
    var prevSelectedPage : Date?
    // used to return bottom Padding of safe area.
       var bottomPadding: CGFloat {
           let window = UIApplication.shared.keyWindow
           return window?.safeAreaInsets.top ?? 0.0
       }
    var datesWithSchedules : [Date] = []
    var calendarLayout : [CalendarLayout] = []
    //MARK: Calendar
     
       fileprivate let gregorian = Calendar(identifier: .gregorian)
       fileprivate let formatter: DateFormatter = {
           let formatter = DateFormatter()
           formatter.dateFormat = "yyyy-MM-dd"
           return formatter
       }()
       
    //   fileprivate weak var calendar: FSCalendar!
       fileprivate weak var eventLabel: UILabel!
       
       private var currentPage: Date?
       
       private lazy var today: Date = {
           return Date()
       }()
       fileprivate lazy var dateFormatter1: DateFormatter = {
           let formatter = DateFormatter()
           formatter.dateFormat = "yyyy/MM/dd"
           return formatter
       }()
       fileprivate lazy var dateFormatter2: DateFormatter = {
           let formatter = DateFormatter()
           formatter.dateFormat = "yyyy-MM-dd"
           return formatter
       }()
//       var navigationView = NavigationView()
//       var headerView = UIView()
    //   var datesWithMultipleEvents = ["2020-08-10", "2020-08-16", "2020-08-22","2020-08-26"]
     //  var datesWithNoEvents = ["2020-08-04", "2020-08-03", "2020-08-02", "2020-08-08", "2020-08-09"]
      // var datesWithOnCall = ["2020-08-15"]
    var datesWithMultipleEvents = [""]
    var datesWithNoEvents =  [""]
     var datesWithOnCall = [""]
    var requests : [Requests]?
    var dateswithRequests = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        self.filterBtn.tintColor = .appProfileEmployeCodeColor
        self.filterBtn.titleLabel?.font = UIFont(font: .SFUIText, weight: .semibold, size: 16.0)
        self.filterBtn.setImage(UIImage(named: "ic_Filters")?.withTintColor(.appProfileEmployeCodeColor), for: .normal)
        
        self.setUpCalendar()
        pullUpController.dataSource = self
        pullUpController.setupCard(from: view)
        let dates = [
            self.gregorian.date(byAdding: .day, value: -1, to: Date()),
            Date(),
            self.gregorian.date(byAdding: .day, value: 1, to: Date())
        ]
        dates.forEach { (date) in
            self.calendar.select(date, scrollToDate: false)
        }
        // For UITest
        self.calendar.accessibilityIdentifier = "calendar"
        self.calendar.select(Date())
        self.calendar.placeholderType = .fillHeadTail
       NotificationCenter.default.addObserver(self, selector: #selector(reloadAPIData), name: NSNotification.Name(rawValue: notification_Myrequest), object: nil)
    }
    @objc func reloadAPIData(){
       self.calendarLayout.removeAll()
        self.getAllRequestForUser()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.calendarLayout.removeAll()
         self.getAllRequestForUser()
        //self.visualEffectView.removeFromSuperview()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)        
    }
    
      func setUpCalendar() {
    
            self.calendar.backgroundColor = UIColor.white
          calendar.allowsMultipleSelection = false
          calendar.rowHeight = 80
          calendar.pagingEnabled = false
            self.calendar.calendarHeaderView.isHidden = true
            calendar.appearance.headerMinimumDissolvedAlpha = 0
            self.calendar.appearance.headerTitleFont = UIFont(name: "", size: 0)
            self.calendar.headerHeight = 0.0
            calendar.appearance.caseOptions = [ .weekdayUsesUpperCase]
        
       // let week_days_view = weekdayView.instanceFromNib() as! weekdayView
      //  week_days_view.frame = self.calendar.calendarWeekdayView.frame
        self.calendar.calendarWeekdayView.isHidden = true
        self.calendar.calendarWeekdayView.removeFromSuperview()

            calendar.appearance.weekdayFont = UIFont(name: "", size: 0.0)
            calendar.appearance.eventSelectionColor = UIColor.white
            calendar.appearance.eventOffset = CGPoint(x: 0, y: -7)
            calendar.today = nil // Hide the today circle
            calendar.register(CalendarCell.self, forCellReuseIdentifier: "cell")
            
            calendar.swipeToChooseGesture.isEnabled = false // Swipe-To-Choose
            calendar.appearance.headerTitleColor = UIColor.black
            calendar.appearance.weekdayTextColor = UIColor.black
//            let scopeGesture = UIPanGestureRecognizer(target: calendar, action: #selector(calendar.handleScopeGesture(_:)));
//            calendar.addGestureRecognizer(scopeGesture)
            
            
            let btnLeft = UIButton(frame:  CGRect(x: calendar.frame.minX + 10, y:  calendar.frame.minY + 10, width: 30, height: 30))
            btnLeft.addTarget(self, action: #selector(self.btnLeftAction(_:)), for: .touchUpInside)
            btnLeft.titleLabel?.textAlignment = .left
            btnLeft.setImage(UIImage(named: "prev"), for: .normal)
            btnLeft.setTitleColor(.black, for: .normal)
            calendar.calendarHeaderView.isUserInteractionEnabled = true
            // calendar.calendarHeaderView.isExclusiveTouch = true
          // self.view.addSubview(btnLeft)
            // calendar.calendarHeaderView.addSubview(btnLeft)
            
            let btnRight = UIButton(frame:  CGRect(x: calendar.frame.maxX - 60, y: calendar.frame.minY + 10, width: 30, height: 30))
            btnRight.addTarget(self, action: #selector(self.btnRightAction(_:)), for: .touchUpInside)
            btnRight.titleLabel?.textAlignment = .right
            btnRight.setImage(UIImage(named: "next"), for: .normal)
            btnRight.setTitleColor(.black, for: .normal)
          //  self.view.addSubview(btnRight)
        let month = Calendar.current.component(.month, from: Date())
        
           let year = Calendar.current.component(.year, from: Date())
        let monthName = self.monthName(month: month)
        
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(font: .SFUIText, weight: .bold, size: 16.0), NSAttributedString.Key.foregroundColor : UIColor.darkGray]

        let attrs2 = [NSAttributedString.Key.font : UIFont(font: .SFUIText, weight: .semibold, size: 16.0), NSAttributedString.Key.foregroundColor : UIColor.gray]

        let attributedString1 = NSMutableAttributedString(string: monthName.capitalized, attributes:attrs1 as [NSAttributedString.Key : Any])

        let attributedString2 = NSMutableAttributedString(string: " \(year)", attributes:attrs2 as [NSAttributedString.Key : Any])

        attributedString1.append(attributedString2)
        self.monthLbl.attributedText = attributedString1
 
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: Date())
         self.dayName(weekday: dayInWeek)
        }
    
    func moveToDayView(date:String)  {
        let filarr = self.scheduleLayoutArr?.filter({$0.onDate == date.convertToBackendDateFormat})
        if let obj = filarr?.last{
            if obj.request == true {
              ReusableAlertController.showAlert(title: "FloatCare", message: "Selected date is a sick/vacation day")
            }else{
             self.movetoDayVC(date: date)

            }
        }else{
            self.movetoDayVC(date: date)
        }
            
    }
    
    func movetoDayVC(date:String)  {
        
        let vc  = UIStoryboard.init(name: "Availability", bundle: nil).instantiateViewController(withIdentifier: "DayAvailabilityParentVC") as! DayAvailabilityParentVC
        vc.todayDate = date
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc  func btnLeftAction(_ sender: Any){
           self.moveCurrentPage(moveUp: false)
       }
       @objc  func btnRightAction(_ sender: Any){
           self.moveCurrentPage(moveUp: true)
       }
       private func moveCurrentPage(moveUp: Bool) {
           var dateComponents = DateComponents()
           if self.calendar.scope == .month {
               dateComponents.month = moveUp ? 1 : -1
           }else {
               dateComponents.weekOfYear = moveUp ? 1 : -1
           }
           self.currentPage = self.gregorian.date(byAdding: dateComponents, to: self.currentPage ?? self.today)
           self.calendar.setCurrentPage(self.currentPage!, animated: true)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: Date())
         self.dayName(weekday: dayInWeek)
       }
    
    func dayName(weekday:String){
        
        let weekfont = UIFont(font: .SFUIText, weight: .semibold, size: 10.0)
//        self.sunday.textColor = .primaryColor
        self.sunday.font = weekfont
        self.monday.font = weekfont
        self.tuesday.font = weekfont
        self.wedday.font = weekfont
        self.thursday.font = weekfont
        self.friday.font = weekfont
        self.satday.font = weekfont

        switch weekday {
        case "Sunday":
            self.sunday.textColor = .primaryColor

        case "Monday":
            self.monday.textColor = .primaryColor
        case "Tuesday":
             self.tuesday.textColor = .primaryColor
        case "Wednesday":
             self.wedday.textColor = .primaryColor
        case "Thursday":
             self.thursday.textColor = .primaryColor
        case "Friday":
             self.friday.textColor = .primaryColor
        case "Saturday":
             self.satday.textColor = .primaryColor
        default:
             self.sunday.font = UIFont(name: "SF Pro Text", size: 9.0)
        }
    }
    func monthName(month : Int)->String {
        var name = ""
        switch month {
        case 1:
            name = "JANUARY"
            return name
        case 2:
            name = "FEBRUARY"
            return name
        case 3:
            name = "MARCH"
            return name
        case 4:
            name = "APRIL"
            return name
        case 5:
            name = "MAY"
            return name
        case 6:
            name = "JUNE"
            return name
        case 7:
            name = "JULY"
            return name
        case 8:
            name = "AUGUST"
            return name
        case 9:
            name = "SEPTEMBER"
            return name
        case 10:
            name = "OCTOBER"
            return name
        case 11:
            name = "NOVEMBER"
            return name
        case 12:
            name = "DECEMBER"
            return name
        default:
            return name
            
        }
    }
    
    // MARK:- FSCalendarDataSource
       func minimumDate(for calendar: FSCalendar) -> Date {
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"
        let previous = Calendar.current.date(byAdding: .month, value: -1, to: Date()) ?? Date()
           let startDate = previous.startOfMonth()
        let date = Calendar.current.date(byAdding: .month, value: 0, to: Date()) ?? Date()
         let minimumDate = self.dateFormatter2.string(from: date)
        
           return startDate//self.dateFormatter2.date(from: startDate)!
       }
           
       func maximumDate(for calendar: FSCalendar) -> Date {
        let date = Calendar.current.date(byAdding: .month, value: 6, to: Date()) ?? Date()
        let maxDate = self.dateFormatter2.string(from: date)
           return self.dateFormatter2.date(from: maxDate)!
       }
   
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        
        return cell
    }
       
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        self.configure(cell: cell, for: date, at: position)
    }
    
    
    private func setCalenderScheduleIcons(date: Date, cell: FSCalendarCell,position: FSCalendarMonthPosition) {
        
        let dobFomat = DateFormatter()
        dobFomat.dateFormat = "yyyy-MM-dd"
         cell.numberoFworkSpaces(withCode: nil)
        if let schedules = self.scheduleLayoutArr {
            
            let filarr = schedules.filter({$0.onDate == dobFomat.string(from: date)})
            if filarr.count > 0{
                cell.bgImgView.image = UIImage(named: "current_current")
                self.setShfitImageForCVCell(shift: (schedules.last?.shiftTypeName)!, diyCell: cell)
            }
            var colors = [UIColor]()
            for  obj in  filarr {
                if let worksiteSettings = obj.worksiteSettings {
                    
                    if let color = worksiteSettings.color {
 
                        colors.append(color.hexStringToUIColor())
                        
                    }
                }
            }
            
             cell.numberoFworkSpaces(withCode: colors)
            
        }
    }
    func setShfitImageForCVCell(shift:String, diyCell:FSCalendarCell)  {
        diyCell.shift3.isHidden = false
        if shift == "night shift" {
               diyCell.shift3.setImage(UIImage(named: "night"), for: .normal)
           }else  if shift.lowercased() == "day shift" {
               diyCell.shift3.setImage(UIImage(named: "dayshift"), for: .normal)

           }else  if shift.lowercased() == "swing shift" {
               diyCell.shift3.setImage(UIImage(named: "swing"), for: .normal)
           }
    }
       func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
           let currentPageDate = calendar.currentPage

           let month = Calendar.current.component(.month, from: currentPageDate)
           let year = Calendar.current.component(.year, from: currentPageDate)
        let monthName = self.monthName(month: month)
        self.monthLbl.text = "\(monthName)  \(year)"
            print("did select date \(year) \(month)")
       
        self.calendar.setCurrentPage(currentPageDate, animated: false)
        
       }
       func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
           return 2
       }
       
       // MARK:- FSCalendarDelegate
       
       func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
           self.calendar.frame.size.height = bounds.height
           self.eventLabel.frame.origin.y = calendar.frame.maxY + 10
       }
       
       func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
//        if date .compare(Date()) == .orderedAscending {
//                   return true
//               }
//               else {
//             return monthPosition == .current
//                   return true
//               }
            return monthPosition == .current
       }
       
       func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
           return monthPosition == .current
       }
       
       func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let form = DateFormatter.init()
        form.dateFormat = "MMM dd, yyyy"
        let readDate = form.string(from: date)
        self.moveToDayView(date: readDate)

         if self.dateswithRequests.count > 0 {
            let key = self.dateFormatter2.string(from: date)
             if self.dateswithRequests.contains(key) {
                self.view.makeToast("Shift is assigned", duration: 2.0, position: .center)
                return
            }
        }
           self.configureVisibleCells()
       // self.calendarDateSelected(date: date)
       }
       
       func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
           print("did deselect date \(self.formatter.string(from: date))")
           self.configureVisibleCells()
       }
       
       func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
           if self.gregorian.isDateInToday(date) {
               return [UIColor.orange]
           }
           return [appearance.eventDefaultColor]
       }
       
       // MARK: - Private functions
       
       private func configureVisibleCells() {
           calendar.visibleCells().forEach { (cell) in
               let date = calendar.date(for: cell)
               let position = calendar.monthPosition(for: cell)
               self.configure(cell: cell, for: date!, at: position)
           }
       }
//    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition){
//        let dt = DateFormatter.init()
//        dt.dateFormat = "yyyy-MM-dd"
//        print(dt.string(from: date))
//        let diyCell = (cell as! CalendarCell)
//        diyCell.eventIndicator.isHidden = true
//        diyCell.titleLabel.font = UIFont(name: "SF Pro Text", size: 12.0)
//        diyCell.preferredTitleOffset = CGPoint(x: 0, y: 4)
//    }
       private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        let dt = DateFormatter.init()
        dt.dateFormat = "yyyy-MM-dd"
        print(dt.string(from: date))
        cell.numberoFworkSpaces(withCode: nil)
        let diyCell = (cell as! CalendarCell)
        diyCell.eventIndicator.isHidden = true
        diyCell.titleLabel.font = UIFont(name: "SF Pro Text", size: 12.0)
        diyCell.preferredTitleOffset = CGPoint(x: 0, y: 4)

           if position == .current { // Current month dates in current page

            if self.gregorian.isDateInToday(date) {
                 diyCell.titleLabel.textColor = UIColor.black
                        diyCell.bgImgView.image = UIImage(named: "current_current_not")
                       diyCell.shift1.isHidden = true
                       diyCell.shift2.isHidden = true
                       diyCell.shift3.isHidden = true
                       diyCell.ws1.isHidden = true
                diyCell.bgImgView.layer.cornerRadius = 0.0
                diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                diyCell.bgImgView.layer.shadowRadius = 0
                diyCell.bgImgView.layer.shadowOpacity = 0
            }else if date < Date() {
                // diyCell.titleLabel.textColor = UIColor.lightGray//previous dates

                diyCell.bgImgView.image = UIImage(named: "")
                  diyCell.shift1.isHidden = true
                  diyCell.shift2.isHidden = true
                  diyCell.shift3.isHidden = true
                  diyCell.ws1.isHidden = true

                  diyCell.bgImgView.backgroundColor = UIColor.white
                  diyCell.bgImgView.layer.cornerRadius = 10.0
                  diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                  diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                  diyCell.bgImgView.layer.shadowRadius = 2.0
                diyCell.bgImgView.layer.shadowOpacity = 0.4

            }else {
                 diyCell.titleLabel.textColor = UIColor.black//future dates only blocks
                diyCell.bgImgView.image = UIImage(named: "")
                  diyCell.shift1.isHidden = true
                  diyCell.shift2.isHidden = true
                  diyCell.shift3.isHidden = true
                  diyCell.ws1.isHidden = true

                  diyCell.bgImgView.backgroundColor = UIColor.white
                  diyCell.bgImgView.layer.cornerRadius = 10.0
                  diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                  diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                  diyCell.bgImgView.layer.shadowRadius = 2.0
                diyCell.bgImgView.layer.shadowOpacity = 0.4
            }


            if self.scheduleLayoutArr != nil && self.scheduleLayoutArr?.count ?? 0 > 0  { // To apply layout in the current page
//                for scheduleLayout in (self.scheduleLayoutArr)! {
                if let scheduleLayout = self.scheduleLayoutArr?.filter({$0.onDate == dt.string(from: date)}).last{
                    let celldate = self.dateFormatter2.string(from: date)
                    let layoutDateString = scheduleLayout.onDate!
                    let shiftName = scheduleLayout.shiftTypeName!
                    let layoutDate = self.dateFormatter2.date(from: layoutDateString)!
//                    if celldate != layoutDateString {
//                        return
//                    }
                    let openShift = scheduleLayout.openShift!
                    let request = scheduleLayout.request!
                    let schedule = (scheduleLayout.assigned! && scheduleLayout.available!)
                    let available = scheduleLayout.available!
                    if schedule && celldate == layoutDateString {
                        diyCell.shift1.isHidden = true
                        diyCell.shift2.isHidden = true
                        diyCell.ws1.isHidden = true
                        diyCell.shift3.isHidden = true
                        diyCell.bgImgView.layer.cornerRadius = 0.0
                        diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                        diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                        diyCell.bgImgView.layer.shadowRadius = 0
                        diyCell.bgImgView.layer.shadowOpacity = 0
                        if celldate == layoutDateString && self.gregorian.isDateInToday(layoutDate) {
                            //if dateVal >
                            diyCell.bgImgView.image = UIImage(named: "current_current")


                            if shiftName.lowercased() == "night shift" {
                                diyCell.shift3.isHidden = false
                                diyCell.shift3.setImage(UIImage(named: "night"), for: .normal)
                            }else  if shiftName.lowercased() == "day shift" {
                                diyCell.shift3.isHidden = false
                                diyCell.shift3.setImage(UIImage(named: "dayshift"), for: .normal)

                            }else  if shiftName.lowercased() == "swing shift" {
                                diyCell.shift3.isHidden = false
                                diyCell.shift3.setImage(UIImage(named: "swing"), for: .normal)
                            }


                        }else if Date() < layoutDate && celldate == layoutDateString  {
                            diyCell.bgImgView.image = UIImage(named: "current_future")
//                            diyCell.shift1.isHidden = true
//                            diyCell.shift2.isHidden = true
//                            diyCell.ws1.isHidden = false
//                            diyCell.shift3.isHidden = true
//                            diyCell.bgImgView.layer.cornerRadius = 0.0
//                            diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
//                            diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//                            diyCell.bgImgView.layer.shadowRadius = 0
//                            diyCell.bgImgView.layer.shadowOpacity = 0

                            if shiftName.lowercased() == "night shift" {
                                diyCell.shift3.isHidden = false
                                diyCell.shift3.setImage(UIImage(named: "night"), for: .normal)
                            }else  if shiftName.lowercased() == "day shift" {
                                diyCell.shift3.isHidden = false
                                diyCell.shift3.setImage(UIImage(named: "dayshift"), for: .normal)

                            }else  if shiftName.lowercased() == "swing shift" {
                                diyCell.shift3.isHidden = false
                                diyCell.shift3.setImage(UIImage(named: "swing"), for: .normal)
                            }


                        }else if Date() > layoutDate && celldate == layoutDateString {
                            diyCell.bgImgView.image = UIImage(named: "current_past")

//                            diyCell.shift1.isHidden = true
//                            diyCell.shift2.isHidden = true
//                            diyCell.ws1.isHidden = false
//                            diyCell.shift3.isHidden = true
//                            diyCell.bgImgView.layer.cornerRadius = 0.0
//                            diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
//                            diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//                            diyCell.bgImgView.layer.shadowRadius = 0
//                            diyCell.bgImgView.layer.shadowOpacity = 0

                            if shiftName.lowercased() == "night shift" {
                                diyCell.shift3.isHidden = false
                                diyCell.shift3.setImage(UIImage(named: "night"), for: .normal)
                            }else if shiftName.lowercased() == "day shift" {
                                diyCell.shift3.isHidden = false
                                diyCell.shift3.setImage(UIImage(named: "dayshift"), for: .normal)

                            }else if shiftName.lowercased() == "swing shift" {
                                diyCell.shift3.isHidden = false
                                diyCell.shift3.setImage(UIImage(named: "swing"), for: .normal)
                            }


                        }
                    }else if openShift  && celldate == layoutDateString {
                        diyCell.bgImgView.image = UIImage(named: "select")
                        diyCell.shift1.isHidden = true
                        diyCell.shift2.isHidden = true
                        diyCell.shift3.isHidden = true
                        diyCell.ws1.isHidden = true

                        diyCell.bgImgView.layer.cornerRadius = 0.0
                        diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                        diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                        diyCell.bgImgView.layer.shadowRadius = 0
                        diyCell.bgImgView.layer.shadowOpacity = 0
                    }else if request && celldate == layoutDateString {
//                        diyCell.bgImgView.image = UIImage(named: "select")
//                        diyCell.shift1.isHidden = true
//                        diyCell.shift2.isHidden = true
//                        diyCell.shift3.isHidden = true
//                        diyCell.ws1.isHidden = true
//                        diyCell.bgImgView.layer.cornerRadius = 0.0
//                                               diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
//                                               diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//                                               diyCell.bgImgView.layer.shadowRadius = 0
//                                               diyCell.bgImgView.layer.shadowOpacity = 0
//
                        diyCell.bgImgView.image = UIImage(named: "current_future")
                        diyCell.shift1.isHidden = true
                        diyCell.shift2.isHidden = true
                        diyCell.shift3.isHidden = false
                        diyCell.ws1.isHidden = true
                        diyCell.bgImgView.layer.cornerRadius = 0.0
                        diyCell.bgImgView.backgroundColor = UIColor.white
                        diyCell.bgImgView.layer.cornerRadius = 10.0
                        diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                        diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                        diyCell.bgImgView.layer.shadowRadius = 2.0
                        diyCell.bgImgView.layer.shadowOpacity = 0.4

                        }else if available  && celldate == layoutDateString {
                        diyCell.bgImgView.image = UIImage(named: "select")
                        diyCell.shift1.isHidden = true
                        diyCell.shift2.isHidden = true
                        diyCell.shift3.isHidden = true
                        diyCell.ws1.isHidden = true

                        diyCell.bgImgView.layer.cornerRadius = 0.0
                        diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                        diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                        diyCell.bgImgView.layer.shadowRadius = 0
                        diyCell.bgImgView.layer.shadowOpacity = 0
                    }
                }

            }else {
                diyCell.bgImgView.image = UIImage(named: "")
                            diyCell.shift1.isHidden = true
                            diyCell.shift2.isHidden = true
                            diyCell.shift3.isHidden = true
                            diyCell.ws1.isHidden = true

                            diyCell.bgImgView.backgroundColor = UIColor.white
                            diyCell.bgImgView.layer.cornerRadius = 10.0
                            diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                            diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                            diyCell.bgImgView.layer.shadowRadius = 2.0
                          diyCell.bgImgView.layer.shadowOpacity = 0.4
            }

            self.setCalenderScheduleIcons(date: date, cell: cell, position: position)

           }else  if position == .previous { // previous month dates in current page
               diyCell.titleLabel.textColor = UIColor.lightGray
               diyCell.bgImgView.image = UIImage(named: "")
               diyCell.shift1.isHidden = true
               diyCell.shift2.isHidden = true
               diyCell.shift3.isHidden = true
               diyCell.ws1.isHidden = true

               diyCell.bgImgView.backgroundColor = UIColor.white
               diyCell.bgImgView.layer.cornerRadius = 10.0
               diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
               diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
               diyCell.bgImgView.layer.shadowRadius = 2.0
               diyCell.bgImgView.layer.shadowOpacity = 0.4
           }
           else  if position == .next { // next month dates in current page

            diyCell.titleLabel.textColor = UIColor.clear
                      // diyCell.bgImgView.isHidden = true
                         diyCell.bgImgView.image = UIImage(named: "")
                          diyCell.shift1.isHidden = true
                          diyCell.shift2.isHidden = true
                          diyCell.ws1.isHidden = true
                          diyCell.shift3.isHidden = true
                       diyCell.bgImgView.layer.cornerRadius = 0.0
                                          diyCell.bgImgView.layer.shadowColor = UIColor.lightPurpleColor.cgColor
                                          diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                          diyCell.bgImgView.layer.shadowRadius = 0
                                          diyCell.bgImgView.layer.shadowOpacity = 0

           }




       }
    @IBAction func filterBtnTapped(_ sender: Any) {
        
       let storyboard = UIStoryboard(name: "CalendarVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: FilterViewController.self)) as! FilterViewController
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        
        self.overlayBlurredBackgroundView()
      //  vc.modalPresentationStyle = .custom
      //   vc.transitioningDelegate = self
        vc.view.backgroundColor = UIColor.clear
        self.present(vc, animated: true, completion: nil)
    }
    func overlayBlurredBackgroundView() {
        
        let blurredBackgroundView = UIVisualEffectView()
        
        blurredBackgroundView.frame = view.frame
        blurredBackgroundView.effect = UIBlurEffect(style: .dark)
        
        view.addSubview(blurredBackgroundView)
        
    }
    func calendarDateSelected(date:Date){
        
       let storyboard = UIStoryboard(name: "CalendarVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: CalendarActionsVC.self)) as! CalendarActionsVC
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        
        self.overlayBlurredBackgroundView()
      //  vc.modalPresentationStyle = .custom
      //   vc.transitioningDelegate = self
        vc.view.backgroundColor = UIColor.clear
        self.present(vc, animated: true, completion: nil)
    }
    
    func getAllRequestForUser() {
        Progress.shared.showProgressView()

        ScheduleAPI().getAllSchedulesForuser(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", successHandler: { (scheduleLayout) in
            DispatchQueue.main.async {
                self.scheduleLayoutArr = scheduleLayout
               // self.datesRange()
              Progress.shared.hideProgressView()
                 self.configureVisibleCells()
            }
            
            
            
            
        }) { (message) in
              DispatchQueue.main.async {
                Progress.shared.hideProgressView()
              }
        }

    }

    private func formattedDateFromString(dateString: String, format: String) -> String? {

              let inputFormatter = DateFormatter()
              inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

              if let date = inputFormatter.date(from: dateString) {
                // return date

                let outputFormatter = DateFormatter()
               outputFormatter.dateFormat = format
               // outputFormatter.locale = Locale(identifier: "en_US_POSIX")

                let endDateString = outputFormatter.string(from: date)
                
                return endDateString
                 // return outputFormatter.string(from: date)
              }

              return nil
          }
}
extension CalendarVC: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController:presented, presenting: presenting)
    }
}

// MARK: - SOPullUpViewDataSource

extension CalendarVC: SOPullUpViewDataSource {
    
    func pullUpViewCollapsedViewHeight() -> CGFloat {
         return bottomPadding + 245
    }
     
    func pullUpViewController() -> UIViewController {
        guard let vc = UIStoryboard(name: "PickedPull", bundle: nil).instantiateInitialViewController() as? PickedPullUpViewController else {return UIViewController()}
        vc.pullUpControl = self.pullUpController
        return vc
    }
    
    func pullUpViewExpandedViewHeight() -> CGFloat {
        return self.view.frame.height * 0.8
    }
}

class HalfSizePresentationController : UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        get {
            guard let theView = containerView else {
                return CGRect.zero
            }

            return CGRect(x: 0, y: theView.bounds.height*0.2, width: theView.bounds.width, height: theView.bounds.height * 0.8)
        }
    }
}
