//
//  ColleguesCVCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 24/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ColleguesCVCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgView.image = UIImage(named: "pic")
               self.imgView.layer.masksToBounds = true
    }
    override func layoutSubviews() {
        super.layoutSubviews()

        self.setCircularImageView()
    }

    func setCircularImageView() {
        
        self.imgView.backgroundColor = .profileBgColor
        self.imgView.layer.cornerRadius = CGFloat(roundf(Float(self.imgView.frame.size.width / 2.0)))
    }

}
