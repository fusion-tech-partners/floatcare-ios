//
//  FilterSectionHeader.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 30/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class FilterSectionHeader: UIView {

    @IBOutlet weak var expandBtn: UIButton!
    @IBOutlet weak var sectionName: UILabel!
    @IBOutlet var contentView: UIView!
     override init(frame: CGRect) {
               super.init(frame: frame)
               commonInit()
           }
           required init?(coder aDecoder: NSCoder) {
               super.init(coder: aDecoder)
                commonInit()
           }
           private func  commonInit()
           {
               Bundle.main.loadNibNamed("FilterSectionHeader", owner: self, options: nil)
               addSubview(contentView)
               contentView.frame = self.bounds
               contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
               contentView.backgroundColor = UIColor.white
               
       }

}
