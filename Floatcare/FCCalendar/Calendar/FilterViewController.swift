//
//  FilterViewController.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 29/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
 let RemoveFiltersNotificationName = "RemoveFiltersNotificationName"
class FilterViewController: UIViewController {
    
    @IBOutlet weak var filterCount: UILabel!
    @IBOutlet weak var filterTblView: UITableView!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var closrBtn: UIButton!
    @IBOutlet weak var bgView: UIView!
    //Mark: Variables
    var filtersCount = 0
    var wsSelectedIndexPaths : Array<IndexPath> = []
     var wsCount = 0
     var hiddenSections = Set<Int>()
      var sectionItems: Array<Any> = []
      var sectionNames: Array<Any> = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.closrBtn.layer.cornerRadius = CGFloat(roundf(Float(self.closrBtn.frame.size.width / 2.0)))
        self.filterCount.layer.cornerRadius = CGFloat(roundf(Float(self.filterCount.frame.size.width / 2.0)))
        self.filterCount.backgroundColor = .primaryColor
        self.filterCount.layer.borderColor = UIColor.primaryColor.cgColor
         self.filterCount.layer.borderWidth = 1
        self.bgView.roundCorners([ .layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 20.0)
        self.applyBtn.backgroundColor = .primaryColor
               self.clearBtn.backgroundColor = .profileBgColor
               
               self.applyBtn.setTitleColor(.white, for: .normal)
               self.clearBtn.setTitleColor(.lightGreyTextColor, for: .normal)
               
               self.clearBtn.titleLabel?.font = UIFont(name: Fonts.poppinsSemiBold, size: 14)
               self.applyBtn.titleLabel?.font = UIFont(name: Fonts.poppinsSemiBold, size: 14)
               
               let spacing: CGFloat = 5 // the amount of spacing to appear between image and title
               
               self.applyBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing);
               self.applyBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: 0);
               
               self.clearBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing);
               self.clearBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: 0);
               
               self.applyBtn.layer.cornerRadius = 11.0
               self.clearBtn.layer.cornerRadius = 11.0
        
        let nib = UINib(nibName: "ShiftTypeTableCell", bundle: nil)
               self.filterTblView.register(nib, forCellReuseIdentifier: "shiftCell")
        
        let nib1 = UINib(nibName: "WorkSitesTableCell", bundle: nil)
        self.filterTblView.register(nib1, forCellReuseIdentifier: "workSiteCell")
        filterTblView.tableFooterView = UIView()
        self.filterTblView.separatorColor = UIColor(red: 247, green: 247, blue: 251)
         sectionNames = [ "Shift Types", "Worksites"];
        sectionItems = [ ["iPhone 5"],
          ["iPad Mini", "iPad Air 2", "iPad Pro"]
        ];
        filterCount.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ReusableAlertController.showAlert(title: "Float Care", message: "Filter - Work In Progress")

    }
    
    @IBAction func applyBtnTapped(_ sender: Any) {
    }
    @IBAction func clearBtnTapped(_ sender: Any) {
        if self.filtersCount != 0 && self.wsCount != 0 {
            filterCount.isHidden = true
            self.filtersCount = 0
            self.wsCount = 0
             NotificationCenter.default.post(name:NSNotification.Name(rawValue: RemoveFiltersNotificationName), object: nil)
            self.filterTblView.reloadRows(at: wsSelectedIndexPaths, with: .automatic)
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func closeBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
// MARK: - TableView Methodas

extension FilterViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
       
            return sectionNames.count
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if self.hiddenSections.contains(section) {
            return 0
        }else {
            switch section {
            case 0:
                return 1
            case 1:
                return 3
            default:
                return 1
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 100
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.sectionNames.count != 0) {
            return self.sectionNames[section] as? String
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            let footer : UIView = UIView(frame: CGRect(x: 24, y: 0, width: self.view.frame.size.width - 48, height: 4))
            footer.backgroundColor = UIColor(red: 247, green: 247, blue: 251)
            return footer
        }else {
            let footer = UIView()
            return footer
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        if section == 0 {
            return 3
        }else {
           return 0
        }
       
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView : FilterSectionHeader = FilterSectionHeader(frame: CGRect(x: 24, y: 0, width: self.view.frame.size.width - 48, height: 60))
        sectionView.backgroundColor = .white
        sectionView.sectionName.text = sectionNames[section] as! String
        sectionView.expandBtn.setTitle("", for: .normal)
        sectionView.expandBtn.setTitleColor(.primaryColor, for: .normal)
        sectionView.expandBtn.setImage(UIImage(named: "filter_down"), for: .normal)
        sectionView.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(self.sectionHeaderWasTouched(_:)))
        sectionView.addGestureRecognizer(headerTapGesture)
        
        return sectionView
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        switch indexPath.section {
        case 0:
           let cell = tableView.dequeueReusableCell(withIdentifier: "shiftCell")
                       as! ShiftTypeTableCell
           cell.shiftsDelegate = self
            cell.selectionStyle = .none
           return cell
        case 1:
          let cell = tableView.dequeueReusableCell(withIdentifier: "workSiteCell")
                      as! WorkSitesTableCell
          cell.wsBtn.backgroundColor = UIColor(red: 102, green: 143, blue: 230)
          cell.wsBtn.tag = indexPath.row
          cell.wsBtn.addTarget(self, action: #selector(wsBtnTapped(sender:)), for: .touchUpInside)
          cell.siteNameLbl.text = "Apollo Hospital"
          cell.selectionStyle = .none
          return cell
       
        default:
           let cell = tableView.dequeueReusableCell(withIdentifier: "workSiteCell")
                       as! WorkSitesTableCell
           return cell
        }
     
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @objc func wsBtnTapped(sender : UIButton){
        let myIndexPath = IndexPath(row: sender.tag, section: 1)
        let cell = filterTblView.cellForRow(at: myIndexPath) as! WorkSitesTableCell
        
        if sender.isSelected == true {
            sender.isSelected = false
            cell.wsBtn.setImage(nil, for: .normal)
            self.wsCount = self.wsCount - 1
            self.wsSelectedIndexPaths.removeAll{$0 == myIndexPath}
        }else {
            sender.isSelected = true
            cell.wsBtn.setImage(UIImage(named: "tick"), for: .normal)
            self.wsCount = self.wsCount + 1
            self.wsSelectedIndexPaths.append(myIndexPath)
        }
        
        if (self.filtersCount + self.wsCount) > 0 {
                   filterCount.isHidden = false
                  filterCount.text = "\(self.filtersCount + self.wsCount)"
              }else {
                  filterCount.isHidden = true
              }
        // self.filterTblView.reloadItems(at: [myIndexPath])
    }
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! FilterSectionHeader
        let section = headerView.tag
        var sectionData :NSArray?
               switch section {
               case 0:
                sectionData = (sectionItems[0] as! NSArray)
               case 1:
                sectionData = (sectionItems[1] as! NSArray)
               
               default:
                   sectionData = sectionItems as NSArray
               }
        func indexPathsForSection() -> [IndexPath] {
                   var indexPaths = [IndexPath]()
                   
            for row in 0..<sectionData!.count {
                       indexPaths.append(IndexPath(row: row,
                                                   section: section))
                   }
                   
                   return indexPaths
               }
               
               if self.hiddenSections.contains(section) {
                headerView.expandBtn.setTitle("", for: .normal)
                headerView.expandBtn.setImage(UIImage(named: "filter_down"), for: .normal)
                   self.hiddenSections.remove(section)
                   self.filterTblView.insertRows(at: indexPathsForSection(),
                                             with: .fade)
               } else {
                
                headerView.expandBtn.setImage(UIImage(named: ""), for: .normal)
                headerView.expandBtn.setTitle(">", for: .normal)
                   self.hiddenSections.insert(section)
                   self.filterTblView.deleteRows(at: indexPathsForSection(),
                                             with: .fade)
               }
           }
 
}
extension FilterViewController: SelectedShiftNames {
    func selectedShifts(count: Int) {
        self.filtersCount = count
        if (self.filtersCount + self.wsCount) > 0 {
             filterCount.isHidden = false
            filterCount.text = "\(self.filtersCount + self.wsCount)"
        }else {
            filterCount.isHidden = true
        }
    }
    
   
}
