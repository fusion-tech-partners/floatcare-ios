//
//  MeetingTableCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 24/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class MeetingTableCell: UITableViewCell {

    @IBOutlet weak var contentBgView: CornorShadowView!
    @IBOutlet weak var noOfColleguesLbl: UILabel!
    @IBOutlet weak var colleguesCV: UICollectionView!
    @IBOutlet weak var shiftTimingsLbl: UILabel!
    @IBOutlet weak var wsNameLbl: UILabel!
    @IBOutlet weak var headerLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let cellSize = CGSize(width:28 , height:28)

               let layout = UICollectionViewFlowLayout()
               layout.scrollDirection = .vertical //.horizontal
               layout.itemSize = cellSize
               layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
               layout.minimumLineSpacing = 1.0
               layout.minimumInteritemSpacing = -10
               self.colleguesCV.setCollectionViewLayout(layout, animated: true) //ColleguesCVCell
        self.colleguesCV.register( UINib(nibName: "ColleguesCVCell", bundle: nil), forCellWithReuseIdentifier: "colleguesCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension MeetingTableCell: UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return 4
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colleguesCell", for: indexPath) as! ColleguesCVCell
           return cell
       }
}
