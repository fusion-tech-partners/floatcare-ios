//
//  PickedPullUpViewController.swift
//  Float care
//
//  Created by Mounika.x.muduganti on 18/06/20.
//  Copyright © 2020 Mounika.x.muduganti. All rights reserved.
//
import UIKit
import SOPullUpView

enum SelectedView {
    case filter
    case bottom
}
class PickedPullUpViewController: UIViewController {
    
    // MARK: - Outlet

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var upcomingLbl: UILabel!
    @IBOutlet weak var barView: UIView!
    @IBOutlet weak var pickedTable: UITableView!
    @IBOutlet weak var handleArea: UIView!
    
    // MARK: - Properties
   var selection : SelectedView = .bottom
    var pullUpControl: SOPullUpControl? {
        didSet {
            pullUpControl?.delegate = self
        }
    }
    
    // MARK: - Controller Life Cycle rgba(166, 164, 161, 0.1)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.barView.layer.cornerRadius = 4.5
        self.handleArea.roundCorners([ .layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 15.0)
        self.handleArea.layer.shadowColor = UIColor(red: 166, green: 161, blue: 164, alpha: 0.1).cgColor
           self.handleArea.layer.shadowOffset = CGSize(width: 10.0, height: 10.0)
           self.handleArea.layer.shadowRadius = 10.0
         self.handleArea.layer.shadowOpacity = 0.8
         let nib = UINib(nibName: "MeetingTableCell", bundle: nil)
         self.pickedTable.register(nib, forCellReuseIdentifier: "meetingCell")
        let nib1 = UINib(nibName: "RegularShiftTableCell", bundle: nil)
                self.pickedTable.register(nib1, forCellReuseIdentifier: "regularShiftCell")
         self.upcomingLbl.isHidden = true
         self.dateLbl.isHidden = true
         self.pickedTable.isHidden = true
    }
}

// MARK: - SOPullUpViewDelegate

extension PickedPullUpViewController: SOPullUpViewDelegate {

    func pullUpViewStatus(_ sender: UIViewController, didChangeTo status: PullUpStatus) {
        switch status {
        case .collapsed:
            UIView.animate(withDuration: 0.6)  { [weak self] in
               self?.upcomingLbl.isHidden = true
                self?.dateLbl.isHidden = true
               // self?.barView.isHidden = false
                self?.pickedTable.isHidden = true
                self?.handleArea.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
            }
        case .expanded:
            UIView.animate(withDuration: 0.6) { [weak self] in
                 self?.upcomingLbl.isHidden = false
                 self?.dateLbl.isHidden = false
                self?.handleArea.alpha = 1.0
                self?.handleArea.backgroundColor = UIColor.white
                self?.pickedTable.isHidden = false
            }
        }
    }
    
    func pullUpHandleArea(_ sender: UIViewController) -> UIView {
        return handleArea
    }
}

// MARK: - UITableViewDelegate , UITableViewDataSource

extension PickedPullUpViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "regularShiftCell")
                        as! RegularShiftTableCell
               // cell.wsNameLbl.text = "Apollo Hospital"
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "meetingCell")
                        as! MeetingTableCell
                cell.wsNameLbl.text = "Apollo Hospital"
            return cell
        }
         
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.makeToast("Work In Progress")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 115
        default:
            return 175
        }
    }
}
