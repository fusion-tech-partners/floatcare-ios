//
//  RegularShiftTableCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 24/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class RegularShiftTableCell: UITableViewCell {

    @IBOutlet weak var shiftBtn: UIButton!
    @IBOutlet weak var timingLbl: UILabel!
    @IBOutlet weak var wsnameLbl: UILabel!
    @IBOutlet weak var shiftNameLbl: UILabel!
    @IBOutlet weak var contentBgView: CornorShadowView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
