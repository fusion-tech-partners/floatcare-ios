//
//  ShiftCVCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 30/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ShiftCVCell: UICollectionViewCell {

    @IBOutlet weak var shiftNameLbl: UILabel!
    @IBOutlet weak var bgView: CornorShadowView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgView.layer.shadowRadius = 5.0
    }

}
