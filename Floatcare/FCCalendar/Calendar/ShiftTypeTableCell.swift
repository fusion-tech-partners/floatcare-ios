//
//  ShiftTypeTableCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 30/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
protocol SelectedShiftNames {
    func selectedShifts(count:Int)
    
}
class ShiftTypeTableCell: UITableViewCell {

    @IBOutlet weak var shiftNameCV: UICollectionView!
    var shiftsDelegate: SelectedShiftNames?
    var selectedShiftCount: Int = 0
    var shiftNamesArray : Array<IndexPath> = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.shiftNameCV.register(UINib.init(nibName:"ShiftCVCell", bundle: nil), forCellWithReuseIdentifier: "shiftCVCell")
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        self.shiftNameCV.collectionViewLayout = flowLayout
        self.shiftNameCV.allowsMultipleSelection = true
         NotificationCenter.default.addObserver(self, selector: #selector(refreshCell), name: NSNotification.Name(rawValue: RemoveFiltersNotificationName), object: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @objc func refreshCell(notification: Notification) {
        
      //  super.awakeFromNib()
         self.selectedShiftCount = 0
        if self.shiftNamesArray.count > 0 {
            self.shiftNameCV.reloadItems(at: self.shiftNamesArray)
            self.shiftNamesArray.removeAll()
        }
        
      }
}
extension ShiftTypeTableCell: UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shiftCVCell", for: indexPath as IndexPath) as! ShiftCVCell
        cell.shiftNameLbl.text = "Night"
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 10
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 10
       }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                return CGSize(width: 99, height: 46)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ShiftCVCell //rgba(92, 99, 171, 0.2)
        cell.shiftNameLbl.textColor = .primaryColor
        cell.bgView.backgroundColor = UIColor(red: 92, green: 99, blue: 171).withAlphaComponent(0.2)
        cell.bgView.layer.borderColor = UIColor.primaryColor.cgColor
        cell.bgView.layer.borderWidth = 1
        self.selectedShiftCount = self.selectedShiftCount + 1
        self.shiftNamesArray.append(indexPath)
        self.shiftsDelegate?.selectedShifts(count: self.selectedShiftCount)
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ShiftCVCell //rgb(72, 93, 186)
        cell.shiftNameLbl.textColor = .black
        cell.bgView.backgroundColor = .white
        cell.bgView.layer.borderColor = UIColor.clear.cgColor
        self.selectedShiftCount = self.selectedShiftCount - 1
        self.shiftNamesArray.removeAll{$0 == indexPath}
        self.shiftsDelegate?.selectedShifts(count: self.selectedShiftCount)
    }
}
