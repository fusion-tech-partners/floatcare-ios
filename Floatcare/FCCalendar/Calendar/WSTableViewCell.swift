//
//  WSTableViewCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 23/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class WSTableViewCell: UITableViewCell {

    @IBOutlet weak var wsNameLbl: UILabel!
    @IBOutlet weak var wsImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.wsImgView.layer.cornerRadius = 10.0
        self.wsImgView.layer.borderWidth = 0.5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
