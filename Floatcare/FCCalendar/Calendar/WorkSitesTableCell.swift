//
//  WorkSitesTableCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 30/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class WorkSitesTableCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var siteNameLbl: UILabel!
    @IBOutlet weak var wsBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.wsBtn.layer.cornerRadius = 10.0
        NotificationCenter.default.addObserver(self, selector: #selector(refreshCell), name: NSNotification.Name(rawValue: RemoveFiltersNotificationName), object: nil)
        imgView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @objc func refreshCell(notification: Notification) {
           
          
             
         }
}
