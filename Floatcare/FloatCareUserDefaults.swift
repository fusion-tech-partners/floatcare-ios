//
//  FloatCareConstants.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 22/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class FloatCareUserDefaults {
    static let shared = FloatCareUserDefaults()
    var isUserLogedIn : Bool{
        
        get {
//        return self.userId != nil && self.userId != "0"
            if let val = UserDefaults().value(forKey: "isUserLogedIn") as? Bool{
                return val
            }else{
                return false
            }
        }set{
            return UserDefaults().set(newValue, forKey: "isUserLogedIn")
        }
    }
    var userName : String{
        get {
            return UserDefaults().value(forKey: "userEmail") as! String
        }set{
            UserDefaults().set(newValue, forKey: "userEmail")
        }
    }
    var password : String{
          get {
            return UserDefaults().value(forKey: "userPassword") as! String
          }set{
              UserDefaults().set(newValue, forKey: "userPassword")
          }
      }
    
    var userId : String?{
        get {
          return UserDefaults().value(forKey: "userId") as? String
        }set{
            UserDefaults().set(newValue, forKey: "userId")
        }
    }
    
    var orgId : String?{
        get {
          return UserDefaults().value(forKey: "orgId") as? String
        }set{
            UserDefaults().set(newValue, forKey: "orgId")
        }
    }

    
    var faceID :Bool{
        get {
            return UserDefaults().bool(forKey: "isFaceIDEnabled")
        }set{
            UserDefaults().set(newValue, forKey: "isFaceIDEnabled")
        }
    }
    var worksiteId : String?{
        get {
          return UserDefaults().value(forKey: "worksiteId") as? String
        }set{
            UserDefaults().set(newValue, forKey: "worksiteId")
        }
    }
    
    var fcmToken : String?{
           get {
             return UserDefaults().value(forKey: "fcmToken") as? String
           }set{
               UserDefaults().set(newValue, forKey: "fcmToken")
           }
       }
    

}
