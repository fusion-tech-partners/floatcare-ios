//
//  Apollo.swift
//  Floatcare
//
//  Created by BEAST on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//


import Apollo
import SwiftyJSON


typealias DisplayAllMessagesOfUserCompletion = ([DisplayAllMessagesOfUser]?) -> ()

class ApolloManager {
    
    private var cancellable: Cancellable?
    static let shared = ApolloManager()
    var oncompletion: Completion?
    
    private init () {
        
    }
    
    func basicConfig() {
        client.cacheKeyForObject = { $0["id"] }
    }
    
    
    /// A web socket transport to use for subscriptions
    private lazy var webSocketTransport: WebSocketTransport = {
        let url = URL(string: WebSocketEndPoint)!
        let request = URLRequest(url: url)
        return WebSocketTransport(request: request)
    }()
    
    /// An HTTP transport to use for queries and mutations
    private lazy var httpTransport: HTTPNetworkTransport = {
        let url = URL(string: HttpEndPoint)!
        return HTTPNetworkTransport(url: url)
    }()
    
    /// A split network transport to allow the use of both of the above
    /// transports through a single `NetworkTransport` instance.
    private lazy var splitNetworkTransport = SplitNetworkTransport(
        httpNetworkTransport: self.httpTransport,
        webSocketNetworkTransport: self.webSocketTransport
    )
    
    /// Create a client using the `SplitNetworkTransport`.
    private(set) lazy var client = ApolloClient(networkTransport: self.splitNetworkTransport)
}

typealias OnSendingMessage = () -> ()
extension ApolloManager {
    func sendMessage(text: String, senderId: String, receiverId: String, timeStamp: String, onSendingMessage: OnSendingMessage?) {
        
        let receivedMessagesSubscription = CreateMessagesMutation.init(text: text, senderId: senderId, receiverId: receiverId, timeStamp: timeStamp, timeZone: "UTC")
        
        
        ApolloManager.shared.client.perform(mutation: receivedMessagesSubscription) { (result) in
            guard let data = try? result.get().data else { return }
            print(data)
            onSendingMessage?()
        }
    }
    
    func getAllMessagesOfReceiever(receiverId: String, completion: @escaping (_ meaasegesArry: [DisplayAllMessagesOfUser])->()) {
        
      //  let allmessages = DisplayAllMessageOfUserSubscription(senderId: Profile.shared.userId, receiverId: receiverId)
        
        let allmessages = FindMessagesOfSenderAndReceiverIdQuery(senderId: Profile.shared.userId, receiverId: receiverId)
        
        var finalArry: [DisplayAllMessagesOfUser] = [DisplayAllMessagesOfUser]()
        
        if let can = cancellable {
            
            can.cancel()
        }
        ApolloManager.shared.client.clearCache()
        cancellable = ApolloManager.shared.client.fetch(query: allmessages) { result in
            switch result {
            case .success(let graphQLResult):
                
                if (graphQLResult.data?.findMessagesOfSenderAndReceiverId) != nil {
                    guard let user = try? result.get().data?.resultMap else {
                        return
                    }
                                        
                    if let collection = JSON(user)["findMessagesOfSenderAndReceiverId"].array {
                        finalArry = [DisplayAllMessagesOfUser]()
                        
                        for item in collection {
                            let modelTemplate = DisplayAllMessagesOfUser.init(
                                text: item["text"].stringValue,
                                typename: item["__typename"].stringValue,
                                timeStamp: item["timeStamp"].stringValue,
                                senderID: item["senderId"].stringValue,
                                messageID: item["messageId"].stringValue,
                                receiverID: item["receiverId"].stringValue,
                                firstName: item["senderInformation"]["firstName"].stringValue,
                                lastName: item["senderInformation"]["lastName"].stringValue,
                                profilePhoto: item["senderInformation"]["profilePhoto"].stringValue)
                            
                            finalArry.append(modelTemplate)
                        }
                    }

                    completion(finalArry)
                }
                
            case .failure(let error):
                print("Failed to subscribe \(error)")
            }
        }
    }
    
    func getAllMessages(completion: @escaping (_ meaasegesArry: [DisplayAllMessagesOfUser]?)->()) {
        
      //  let allmessages = DisplayAllMessagesSubscription(senderId: Profile.shared.userId)
        
        let allmessages = FindMessagesOfUserQuery(senderId: Profile.shared.userId)
        var finalArry: [DisplayAllMessagesOfUser] = [DisplayAllMessagesOfUser]()
        
        ApolloManager.shared.client.clearCache()
        _ = ApolloManager.shared.client.fetch(query: allmessages) { result in
            switch result {
                case .success(let graphQLResult):
                    
                    if (graphQLResult.data?.findMessagesOfUser) != nil {
                        guard let user = try? result.get().data?.resultMap else {
                            return
                        }
                        
                        if let collection = JSON(user)["findMessagesOfUser"].array {
                            finalArry = [DisplayAllMessagesOfUser]()
                            
                            for item in collection {
                                
                                
                                if item["senderId"].stringValue == Profile.shared.userId {
                                    
                                    let modelTemplate = DisplayAllMessagesOfUser.init(
                                        text: item["text"].stringValue,
                                        typename: item["__typename"].stringValue,
                                        timeStamp: item["timeStamp"].stringValue,
                                        senderID: item["senderId"].stringValue,
                                        messageID: item["messageId"].stringValue,
                                        receiverID: item["receiverId"].stringValue,
                                        firstName: item["receiverInformation"]["firstName"].stringValue,
                                        lastName: item["receiverInformation"]["lastName"].stringValue,
                                        profilePhoto: item["receiverInformation"]["profilePhoto"].stringValue)
                                    finalArry.append(modelTemplate)
                                    
                                    
                                } else {
                                    
                                    let modelTemplate = DisplayAllMessagesOfUser.init(
                                        text: item["text"].stringValue,
                                        typename: item["__typename"].stringValue,
                                        timeStamp: item["timeStamp"].stringValue,
                                        senderID: item["receiverId"].stringValue,
                                        messageID: item["messageId"].stringValue,
                                        receiverID: item["senderId"].stringValue,
                                        firstName: item["senderInformation"]["firstName"].stringValue,
                                        lastName: item["senderInformation"]["lastName"].stringValue,
                                        profilePhoto: item["senderInformation"]["profilePhoto"].stringValue)
                                    finalArry.append(modelTemplate)
                                }
                                
                            }
                        }
                        
                        completion(finalArry)
                }
                
                case .failure(let error):
                     completion(nil)
                    print("Failed to subscribe \(error)")
            }
        }
    }
    
    func closeConnection() {
        self.webSocketTransport.closeConnection()
       }
}
