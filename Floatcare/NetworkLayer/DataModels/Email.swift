//
//  Email.swift
//  Floatcare
//
//  Created by BEAST   on 28/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

//struct Email: Codable {
//    let message: String
//    let status: Int
//    let success: Bool
//    
//}
struct Email: Codable {
    let message: String
    let status: Int
    let success: Bool
    let data: DataClassEmail?
}


// MARK: - DataClass
struct DataClassEmail: Codable {
    let otp, email: String
}

// Temp
class EmailModel {
static let sharedInstance = EmailModel()
    
    private init() {

    }
    var otp: String?
    var email: String?

    init(otp: String?, email: String?) {
        self.otp = otp
        self.email = email
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
