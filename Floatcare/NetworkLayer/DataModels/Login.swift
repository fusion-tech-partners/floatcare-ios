//
//  Login.swift
//  Floatcare
//
//  Created by BEAST on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation


//MARK: - LogoutResponse
struct LogoutResponse: Codable {
//    let message: String
//    let status: Int
    let success: Bool
   // var data: LoggedInData?
}
// MARK: - LoginResponse
struct LoginResponse: Codable {
    let message: String
    let status: Int
    let success: Bool
    var data: LoggedInData?
}

// MARK: - LoggedInData
struct LoggedInData: Codable {
    let token: String?
    var userBasicInformation: UserBasic?
    let responseType: String?
}

// MARK: - UserBasicInformation
struct UserBasic: Codable {
    var userID, organizationID: Int?
    var firstName, lastName: String?
    var middleName: String?
    var email, password: String?
    var preferredName, address1, address2, city: String?
    var state: String?
    var phone, profilePhoto: String?
    var title: String?
    var userTypeID: Int?
    var dateOfBirth: String?
    var staffUserCoreTypeID: Int?
    var linkedInProfile: String?
    var gender: String?
    var bioInformation, createdOn, secondaryPhoneNumber, secondaryEmail: String?
    var userBasicInformationLastSeen, userBasicInformationLastTyped, status, lastSeen: String?
    var lastTyped: String?
    var fcmToken : String?
    var staffUserCoreTypeName: String?
    
    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case organizationID = "organizationId"
        case firstName, lastName, middleName, email, password, preferredName, address1, address2, city, state, phone, profilePhoto, title
        case userTypeID = "userTypeId"
        case dateOfBirth
        case staffUserCoreTypeID = "staffUserCoreTypeId"
        case linkedInProfile, gender, bioInformation, createdOn, secondaryPhoneNumber, secondaryEmail
        case userBasicInformationLastSeen = "last_seen"
        case userBasicInformationLastTyped = "last_typed"
        case status
        case lastSeen = "last_Seen"
        case lastTyped = "last_Typed"
        case staffUserCoreTypeName = "staffUserCoreTypeID"
    }
}

