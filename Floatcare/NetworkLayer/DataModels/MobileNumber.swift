//
//  MobileNumber.swift
//  Floatcare
//
//  Created by BEAST   on 28/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct MobileNumber: Codable {
    let message: String
    let status: Int
    let success: Bool
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let otp, phoneNumber: String
}

// Temp
class MobileNumberModel {
static let sharedInstance = MobileNumberModel()
    
    private init() {

    }
    var otp: String?
    var mobileNumber: String?

    init(otp: String?, mobileNumber: String?) {
        self.otp = otp
        self.mobileNumber = mobileNumber
    }
}


