//
//  URLComponentsExtension.swift
//  NetWorkWrapper
//
//  Created by BEAST on 20/03/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

// This extension will merge baseURL with path and will add parameters to the url if parameters encoding is url

extension URLComponents {
    
    init(service: ServiceProtocol) {
        let url = service.baseURL.appendingPathComponent(service.path)
        self.init(url: url, resolvingAgainstBaseURL: false)!
        
        guard case let .requestParameters(parameters) = service.task, service.parametersEncoding == .url else { return }
        
        queryItems = parameters.map { key, value in
            return URLQueryItem(name: key, value: String(describing: value))
        }
    }
}
