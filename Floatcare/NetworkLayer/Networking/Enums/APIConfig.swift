//
//  APIConfig.swift
//  Floatcare
//
//  Created by BEAST on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

#if STAGING

enum APIConfig: String {
case baseURL = "https://stg-api.float.care"
}
let HttpEndPoint =  "https://stg-api.float.care/apis/graphql"
let WebSocketEndPoint = "http://3.128.8.37:8080/graphiql"
public let amazonURL = "https://floatcare.s3.us-east-2.amazonaws.com/"

#else

enum APIConfig: String {
case baseURL =  "https://api.float.care"
}
let HttpEndPoint =  "https://api.float.care/apis/graphql"
let WebSocketEndPoint = "http://3.20.173.129.8.37:8080/graphiql"

public let amazonURL = "https://floatcare-live-resources.s3.us-east-2.amazonaws.com/"

#endif
