//
//  HTTPMethod.swift
//  NetWorkWrapper
//
//  Created by BEAST on 20/03/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

//HTTPMethod is an enum responsible for setting HTTP method of requests. URLRequest has property .httpMethod to set method String type.

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
