//
//  NetworkError.swift
//  NetWorkWrapper
//
//  Created by BEAST on 20/03/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

enum NetworkError {
    case unknown
    case noJSONData
    case irrelevent
}
