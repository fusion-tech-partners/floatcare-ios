//
//  NetworkResponse.swift
//  NetWorkWrapper
//
//  Created by BEAST on 20/03/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

enum NetworkResponse<T> {
    case success(T)
    case failure(NetworkError)
}
