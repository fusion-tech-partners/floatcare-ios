//
//  ParametersEncoding.swift
//  NetWorkWrapper
//
//  Created by BEAST on 20/03/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

//ParametersEncoding is an enum responsible for setting encoding type.

enum ParametersEncoding {
    case url
    case json
}
