//
//  Task.swift
//  NetWorkWrapper
//
//  Created by BEAST on 20/03/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

typealias Parameters = [String: Any]

//Task is an enum responsible for configuring parameters for a specific service.

enum Task {
    case requestPlain
    case requestParameters(Parameters)
}
