//
//  ProviderProtocol.swift
//  NetWorkWrapper
//
//  Created by BEAST on 20/03/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

protocol ProviderProtocol {
    func request<T>(type: T.Type, service: ServiceProtocol, completion: @escaping (NetworkResponse<T>) -> ()) where T: Decodable
}
