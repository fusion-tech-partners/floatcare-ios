//
//  ServiceProtocol.swift
//  NetWorkWrapper
//
//  Created by BEAST on 20/03/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

// ServiceProtocol will be helper to create URLRequest. ServiceProtocol contains constituent components such as baseURL, path, method, headers, task and parametersEncoding.

typealias Headers = [String: String]

protocol ServiceProtocol {
    var baseURL: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var task: Task { get }
    var headers: Headers? { get }
    var parametersEncoding: ParametersEncoding { get }
}
