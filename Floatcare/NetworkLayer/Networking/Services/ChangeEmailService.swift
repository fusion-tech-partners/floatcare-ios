//
//  ChangeEmailService.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 19/08/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

import Foundation

enum ChangeEmailService: ServiceProtocol {
    case email(email: String)
    
    var baseURL: URL {
        return URL(string: APIConfig.baseURL.rawValue)!
    }
    
    var path: String {
        switch self {
        case .email:
            return "verify/changeEmail"
        }
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var task: Task {
        switch self {
        case let .email(email):
            let parameters = ["email": email]
            return .requestParameters(parameters)
        }
    }
    
    var headers: Headers? {
        return ["Content-Type" : "application/json"]
    }
    
    var parametersEncoding: ParametersEncoding {
        return .json
    }
}
