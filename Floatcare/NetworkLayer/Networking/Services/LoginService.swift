//
//  LoginService.swift
//  Floatcare
//
//  Created by BEAST on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

enum LoginService: ServiceProtocol {
    case login(email: String, password: String)
    
    var baseURL: URL {
        return URL(string: APIConfig.baseURL.rawValue)!
    }
    
    var path: String {
        switch self {
        case .login:
            return "auth/login"
        }
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var task: Task {
        switch self {
        case let .login(email, password):
            let parameters = ["email": email, "password": password]
            return .requestParameters(parameters)
        }
    }
    
    var headers: Headers? {
        return ["Content-Type" : "application/json"]
    }
    
    var parametersEncoding: ParametersEncoding {
        return .json
    }
}
enum LogoutService: ServiceProtocol {
    case logout
    
    var baseURL: URL {
        return URL(string: APIConfig.baseURL.rawValue)!
    }
    
    var path: String {
        switch self {
        case .logout:
            return "auth/logout"
        }
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var task: Task {
        switch self {
        case  .logout:
           // let parameters = ["email": email, "password": password]
            return .requestPlain
        }
    }
    
    var headers: Headers? {
        let token =  Profile.shared.loginResponse?.data?.token ?? ""
        return ["Content-Type" : "application/json","Authorization": "Bearer \(token)"]
    }
    
    var parametersEncoding: ParametersEncoding {
        return .json
    }
}


enum ChangePasswordService : ServiceProtocol {
    case changePassword(userId: Int, oldPassword: String, newPassword: String)
    
    var baseURL: URL {
        return URL(string: APIConfig.baseURL.rawValue)!
    }
    
    var path: String {
        switch self {
        case .changePassword:
            return "user/changePassword"
        }
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var task: Task {
        switch self {
        case let .changePassword(userId
            , oldPassword, newPassword):
            let parameters = ["userId": userId, "oldPassword": oldPassword, "newPassword": newPassword] as [String : Any] 
            return .requestParameters(parameters)
        }
    }
    
    var headers: Headers? {
        return ["Content-Type" : "application/json"]
    }
    
    var parametersEncoding: ParametersEncoding {
        return .json
    }
}
