//
//  MobileNumberService.swift
//  Floatcare
//
//  Created by BEAST   on 28/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

enum MobileNumberService: ServiceProtocol {
    case number(number: String)
    
    var baseURL: URL {
        return URL(string: APIConfig.baseURL.rawValue)!
    }
    
    var path: String {
        switch self {
        case .number:
            return "verify/verifyPhone"
        }
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var task: Task {
        switch self {
        case let .number(number):
            let parameters = ["phoneNumber": "+\(number)"]
            return .requestParameters(parameters)
        }
    }
    
    var headers: Headers? {
        return ["Content-Type" : "application/json"]
    }
    
    var parametersEncoding: ParametersEncoding {
        return .json
    }
}
