//
//  SchedulesAPI.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 22/08/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import Alamofire

struct ScheduleLayoutData : Decodable {
    var staffLayout : [StaffLayout]?
    let period: String?
    let year: String?
    let month: String?
    let week:String?
}

struct ScheduleData : Decodable {
    
    var staffLayout : [StaffLayout]?
}
struct StaffLayout :Decodable {
    var shiftPlanningLayout : [String: [ShiftPlanningLayout]]?
    
    
//    init( shiftPlanningLayout: [String:[ShiftPlanningLayout]]?) {
//
//        self.shiftPlanningLayout = shiftPlanningLayout
//    }
//
//    init(from decoder: Decoder) throws {
//
//        let container = try decoder.container(keyedBy: CodingKeys.self)
////        shiftPlanningLayout = try container.decode([ShiftPlanningLayout].self, forKey: .shiftPlanningLayout).filter{($0.assigned  == true && $0.available == true)}
//        shiftPlanningLayout = try container.decode([ShiftPlanningLayout].self, forKey: .shiftPlanningLayout)
//    }
//
//    /// CodingKeys specific for Availability
//    private enum CodingKeys: String, CodingKey {
//
//        case shiftPlanningLayout
//    }
    
}
struct ShiftPlanningLayout :Codable {
    let shiftTypeName: String?
    let startTime: String?
    let endTime:String?
    let departmentShiftId: Int?
    let assigned: Bool?
    let available: Bool?
    let onDate: String?
    let userId: Int?
    let openShift: Bool?
    let request: Bool?
    let worksiteSettings: WorksiteSettings?
}

struct WorksiteSettings :Codable {
    
    let color: String?
    let organizationId: Int?
    let organizationName:String?
    let userId: Int?
    let worksiteId: Int?
    let worksiteName: String?
    let worksiteSettingId: Int?
}


struct ShiftDate :Codable {

    let dayOfMonth: Int
}
struct CalendarLayout :Decodable {
    
    var shiftPlanningLayout: ShiftPlanningLayout
    let date: Date
    init( shiftPlanningLayout: ShiftPlanningLayout, date: Date) {
        
        self.shiftPlanningLayout = shiftPlanningLayout
        self.date = date
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        shiftPlanningLayout = try container.decode(ShiftPlanningLayout.self, forKey: .shiftPlanningLayout)
        date = try container.decode(Date.self, forKey: .date)
    }
    
    /// CodingKeys specific for Availability
    private enum CodingKeys: String, CodingKey {
        
        case shiftPlanningLayout
        case date
    }
}
class ScheduleAPI {
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
   func getAllSchedulesForuser(userId: String,successHandler: @escaping ([ShiftPlanningLayout]?) -> Void,
                               errorHandler: @escaping (String) -> Void) {
    
     guard let token =  Profile.shared.loginResponse?.data?.token else {
        errorHandler("No data found")
        return}
 
 
    
    let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: Date()) ?? Date()
   let previous = Calendar.current.date(byAdding: .month, value: -1, to: Date()) ?? Date()
 
    let startDate = previous.startOfMonth()
    let endDate = nextMonth.endOfMonth()
    let start = self.dateFormatter2.string(from: startDate)
    let end = self.dateFormatter2.string(from: endDate)
    let urlString = "\(APIConfig.baseURL.rawValue)/schedule/duration/user"
    guard let url = URL(string: urlString) else {return}
    var request        = URLRequest(url: url)
    request.httpMethod = "Post"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
//    {"userId": 81,
//    "startDate": "2020-08-05",
//    "endDate": "2020-10-12"}
    let postBody : [String : Any] = ["userId":userId,"startDate":start,"endDate":end]
    do {
                   request.httpBody   = try JSONSerialization.data(withJSONObject: postBody)
               } catch let error {
                   print("Error : \(error.localizedDescription)")
               }
    
    Alamofire.request(request).responseJSON{ (response) in
        print("response is po urls\(response)")
        if let status = response.response?.statusCode {
            switch(status){
            case 200:
                if let json = response.result.value as? [String: Any] {
                    print("JSON: \(json)") // serialized json response
                    do {
                        
                        if  let jsonDict = json["data"]   {
                            if jsonDict != nil {
                                let jsonData = try JSONSerialization.data(withJSONObject: jsonDict as Any,
                                                                          options: .prettyPrinted)
                                let scheduleLayout = try JSONDecoder().decode(ScheduleLayoutData?.self, from: jsonData)
                                if scheduleLayout?.staffLayout != nil && scheduleLayout?.staffLayout?.count ?? 0 > 0,
                                    let staffLayout = scheduleLayout?.staffLayout![0].shiftPlanningLayout{
                                    
                                    var shiftPlanningArr = [ShiftPlanningLayout]()
                                    staffLayout.forEach { (key, value) in
                                        shiftPlanningArr.append(contentsOf: value)
                                    }
                                    
                                    successHandler(shiftPlanningArr)
                                    
                                }else {
                                    successHandler(nil)
                                }
                               
                                
                            }else {
                                successHandler(nil)
                                
                            }
                            
                        } else {
                            errorHandler("No data found")
                            
                        }
                    }catch let error {
                        print("error description === \(error)")
                        errorHandler("No data found")
                    }
                }
            default:
                errorHandler("No data found")
            }
        }
    }
    }
}
extension Date {
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
}
