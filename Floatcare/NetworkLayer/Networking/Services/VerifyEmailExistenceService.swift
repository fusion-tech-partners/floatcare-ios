//
//  VerifyEmailExistenceService.swift
//  Floatcare
//
//  Created by BEAST   on 28/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

enum VerifyEmailExistenceService: ServiceProtocol {
    case email(email: String)
    
    var baseURL: URL {
        return URL(string: APIConfig.baseURL.rawValue)!
    }
    
    var path: String {
        switch self {
        case .email:
            return "verify/email"
        }
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var task: Task {
        switch self {
        case let .email(email):
            let parameters = ["email": email]
            return .requestParameters(parameters)
        }
    }
    
    var headers: Headers? {
        return ["Content-Type" : "application/json"]
    }
    
    var parametersEncoding: ParametersEncoding {
        return .json
    }
}
