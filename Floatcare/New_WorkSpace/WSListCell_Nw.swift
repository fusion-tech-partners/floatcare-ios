//
//  WSListCell_Nw.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 24/10/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class WSListCell_Nw: UITableViewCell {

    @IBOutlet weak var thumbnailImg: UIImageView!
      @IBOutlet weak var imgVw: UIImageView!
      @IBOutlet weak var nameTF: UILabel!
      @IBOutlet weak var accessoryBtn: UIButton!
      @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        thumbnailImg.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
}
    
}
