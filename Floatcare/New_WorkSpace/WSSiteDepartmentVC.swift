//
//  WSSiteDepartmentVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 25/10/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class WSSiteDepartmentVC: UIViewController {
@IBOutlet weak var tableView: UITableView!
    var depts : [DepartMentWorkSites]?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        depts = WorkSpaceSettingsUtility.shared.departments
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension WSSiteDepartmentVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 230.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return depts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WSsiteDepartmentCell", for: indexPath) as! WSsiteDepartmentCell
        let obj = depts![indexPath.row]
        cell.departmentSubTitleLbl.text = "\(String(describing: (obj.staffRoleByUserId ?? ""))) : \(String(describing: (obj.fteStatusOfStaffUser ?? "")))"
        cell.departmentTitleLbl.text = obj.departmentTypeName
        cell.addressLbl.text = obj.address
        cell.descriptnLbl.text = obj.description
        cell.ColleaugesClick = { sender in
            WorkSpaceSettingsUtility.shared.departmentColleauges = obj.colleagues
            WorkSpaceSettingsUtility.shared.pageMenuReference?.scrollToNext(animated: true, completion: nil)
        }
        cell.ScheduleClick = { sender in
            let vc = self.navigationController?.viewControllers
            let fl = vc?.filter({$0.isKind(of: SchedulesViewController.self)})
            self.navigationController?.popToViewController((fl?.last)!, animated: true)
            
        }
        return cell
    }
 
}
