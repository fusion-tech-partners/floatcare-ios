
//
//  WSSiteDetailsVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 25/10/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit



class WSSiteDetailsVC: UIViewController {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var topbarView: UIView!
    @IBOutlet weak var openShitDetailView: OpenShitDetailView!
    var orgDetails : OrganizationsWorkSites?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        orgDetails = WorkSpaceSettingsUtility.shared.orgDetails
        setupView()
        appendUI()

    }
    
    func appendUI() {
        openShitDetailView.workersArray = orgDetails?.staffMembers
        openShitDetailView.shiftTitleLabel.text = orgDetails?.name
        
        let arr = orgDetails?.businesses
        openShitDetailView.shiftNameLabel.text = orgDetails?.name
    
        openShitDetailView.addressLabel.text = (orgDetails?.address ?? "") + "," + (orgDetails?.state ?? "")
        openShitDetailView.numberOfWorkersLabel.text = "\(orgDetails?.staffMembers?.count ?? 0) colleagues have worked here"
    }
    
    @IBAction func settingsClick(_ sender: Any) {
        let vc  = self.storyboard?.instantiateViewController(identifier: "WSSiteSettingOptionsVC") as! WSSiteSettingOptionsVC
               vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func setupView() {
        
//        self.openShitDetailView.setupCell(openshift: self.viewModel.openshift)
        
        self.iconImageView.layer.cornerRadius = 10.0
        
        self.navigationBar.roundCorners([ .layerMinXMaxYCorner], radius: 30.0)
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          // Get the new view controller using segue.destination.
          // Pass the selected object to the new view controller.
          
          if segue.destination is WSSitePageMenuViewController {

            
          }
      }

}

extension WSSiteDetailsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkerCell", for: indexPath) as! WorkerCell
        return cell
    }
    
    
}

extension WSSiteDetailsVC : WorkSiteOptionsDelegate {
    func workSiteOptionsupdatedCAllback() {
        let vcArr = self.navigationController?.viewControllers
        let fila = vcArr?.filter({$0 .isKind(of: ProfileViewController.self)})
        if let vc = fila?.last {
            self.navigationController?.popToViewController(vc, animated: false)
        }
    }
    
    
}

