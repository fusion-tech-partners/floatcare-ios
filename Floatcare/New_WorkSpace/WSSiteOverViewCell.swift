//
//  WSSiteOverViewCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 25/10/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class WSSiteOverViewCell: UITableViewCell,UITextFieldDelegate{

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var addBioTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        addBioTextField.placeholder = "Add Some Bio"
        searchBar.backgroundColor = UIColor.white
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
}
