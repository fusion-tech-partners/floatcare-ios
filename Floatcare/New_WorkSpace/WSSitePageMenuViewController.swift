//
//  WSSitePageMenuViewController.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 25/10/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

import UIKit
import Swift_PageMenu

//class OpenShiftUtility{
//
//    static let shared = OpenShiftUtility()
//
//    var openshift: OpenShift!
//}

class WSSitePageMenuViewController: PageMenuController {
        
    required init?(coder: NSCoder) {
        super.init(coder: coder, options: OpenShiftsRoundRectPagerOption())
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabMenuView.backgroundColor = .clear
        
        self.dataSource = self
        WorkSpaceSettingsUtility.shared.pageMenuReference  = self
        
         // Do any additional setup after loading the view.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.destination is WSSiteColleaugesVC {
          
        }
    }
    
 

}

extension WSSitePageMenuViewController: PageMenuControllerDataSource {

    func viewControllers(forPageMenuController pageMenuController: PageMenuController) -> [UIViewController] {
        
        let openShiftsViewController1 = UIStoryboard(name: "NewWorkSpaceBoard", bundle: nil).instantiateViewController(withIdentifier: String(describing: WSSiteDepartmentVC.self)) as! WSSiteDepartmentVC
        let openShiftsViewController2 = UIStoryboard(name: "NewWorkSpaceBoard", bundle: nil).instantiateViewController(withIdentifier: String(describing: WSSiteColleaugesVC.self)) as! WSSiteColleaugesVC
        let openShiftsViewController3 = UIStoryboard(name: "NewWorkSpaceBoard", bundle: nil).instantiateViewController(withIdentifier: String(describing: WSSiteOverViewVC.self)) as! WSSiteOverViewVC
            
        return [openShiftsViewController1, openShiftsViewController2, openShiftsViewController3]
    }

    func menuTitles(forPageMenuController pageMenuController: PageMenuController) -> [String] {
        return ["Departments", "Colleagues", "Overview"]
    }

    func defaultPageIndex(forPageMenuController pageMenuController: PageMenuController) -> Int {
        return 0
    }
}





