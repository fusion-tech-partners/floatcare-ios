//
//  WSSiteSettingOptionsVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 25/10/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
protocol WorkSiteOptionsDelegate {
    func workSiteOptionsupdatedCAllback()
}

class WSSiteSettingOptionsVC: UIViewController {
        @IBOutlet weak var bottomView: UIView!
        @IBOutlet weak var optionsTableview: UITableView!
        var identifier = "ShiftOptionCell"
        var objVM = ["Change Color","Make Requests","Set Availability","Set Personal Events"]
        var delegate : WorkSiteOptionsDelegate?
    
        override func viewDidLoad() {
            super.viewDidLoad()

            // Do any additional setup after loading the view.
        }
        
        @IBAction func dismissClick(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
        }
        
        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */

    }

    extension WSSiteSettingOptionsVC : UITableViewDelegate, UITableViewDataSource{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return objVM.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier:identifier , for: indexPath) as! ShiftOptionCell
            cell.nameTF.text = objVM[indexPath.row]
            if indexPath.row == 0{
                cell.imgVw.isHidden = true
                cell.thumbnailImg.backgroundColor = WorkSpaceSettingsUtility.shared.wsColorCode?.hexStringToUIColor()
            }
            return cell
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            64
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if indexPath.row == 0 {
                let vc = UIStoryboard.init(name: "WorkSpace", bundle: nil).instantiateViewController(identifier: "WSColorVC") as! WSColorVC
                vc.modalPresentationStyle = .overFullScreen
                vc.currentColorCode = (WorkSpaceSettingsUtility.shared.wsColorCode?.hexStringToUIColor())!
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            }
        }
    }

extension WSSiteSettingOptionsVC : WorSiteColorDelegate{
    func workspaceColorUpdated() {
        self.delegate?.workSiteOptionsupdatedCAllback()
        self.dismiss(animated: false, completion: nil)
    }
}
