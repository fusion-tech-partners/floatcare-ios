//
//  WSsiteDepartmentCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 25/10/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class WSsiteDepartmentCell: UITableViewCell {

    
    
    @IBOutlet weak var departmentTitleLbl: UILabel!
    @IBOutlet weak var departmentSubTitleLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var descriptnLbl: UILabel!
    @IBOutlet weak var colleguesBtn: UIButton!
    @IBOutlet weak var scheduleBtn: UIButton!
    
    var ColleaugesClick: actionBlock?
    var ScheduleClick: actionBlock?
    @IBOutlet weak var departmentCellVew: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        colleguesBtn.layer.cornerRadius = 10.0
        scheduleBtn.layer.cornerRadius = 10.0
        departmentCellVew.layer.cornerRadius = 10.0
       // self.contentBgView.backgroundColor = UIColor().buttonViolet
        self.departmentTitleLbl.font = UIFont(name: Fonts.sFProText, size: 18)
        self.departmentSubTitleLbl.font = UIFont(name: Fonts.sFProTextMedium, size: 13)
        self.addressLbl.font = UIFont(name: Fonts.sFProText, size: 14)
        self.descriptnLbl.font = UIFont(name: Fonts.sFProText, size: 16)

        //departmentTitleLbl.text = "Emergency Services"
        departmentSubTitleLbl.text = "Med-Surg Nurse • Fulltime"
        addressLbl.text = "123 Avenue, 4TH Street"
        descriptnLbl.text = "A little description about the department and what it does."
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func colleaugesClick(_ sender: UIButton) {
        self.ColleaugesClick?(sender)
    }
    @IBAction func scheduleClick(_ sender: UIButton) {
        self.ScheduleClick?(sender)
    }
}
