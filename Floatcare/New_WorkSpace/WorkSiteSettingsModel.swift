//
//  WorkSiteSettingsModel.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 24/10/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
    
class FetchWorkSiteSettings {
    typealias FetchedWorksiteSetting = ([WorkSiteSettings]) -> ()
    func fetchWorksitesByUserId(userId: String = "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", completion: @escaping FetchedWorksiteSetting) {
        print("Fetching WorkSiteSettings")
        ApolloManager.shared.client.fetch(query:GetWorksiteSettingsByUserIdQuery(userId: userId), cachePolicy: .fetchIgnoringCacheData, context: nil,queue: .main ) { result in
                        
                        guard let user = try? result.get().data?.resultMap else {
                            completion([WorkSiteSettings]())
                            return
                        }
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
                            let decoder = JSONDecoder()
                            let worksiteSettings = try decoder.decode(WSSettingModel.self, from: jsonData)
                            Profile.shared.userWorksiteSettings = worksiteSettings
                            print(worksiteSettings)
                            completion(worksiteSettings.getWorksiteSettingsByUserId!)

                        }catch {
                            print(error.localizedDescription)
                             completion([WorkSiteSettings]())
                        }
        }
    }
}




struct WSSettingModel : Codable{
    var getWorksiteSettingsByUserId : [WorkSiteSettings]?
}

struct WorkSiteSettings : Codable {
    var worksiteSettingId: String?
    var userId: String?
    var userDetails: UserBasicInformation?
    var worksiteId: String?
    var worksiteDetails: BusinessWorkSites?
    var organizationId: String?
    var organizationDetails: OrganizationsWorkSites?
    var departmentCount: Int?
    var departmentDetails: [DepartMentWorkSites]?
    var color: String?
}


struct BusinessWorkSites : Codable {
    
    var businessId: String?
    var organizationId: String?
    var name: String?
    var businessTypeId: String?
    var businessType: String?
    var businessSubTypeId: String?
    var businessTypeCount: String?
    var businessSubType: String?
    var phoneNumber: String?
    var email: String?
    var address: String?
    var city: String?
    var state: String?
    var postalCode: String?
    var country: String?
    var department: [DepartMentWorkSites]?
    var numberOfDepartment: String?
    var status: String?
    var coverPhoto: String?
    var description: String?
    
}
struct OrganizationsWorkSites : Codable {
    var organizationId: String?
    var name: String?
    var address: String?
    var city: String?
    var state: String?
    var postalCode: String?
    var country: String?
    var status: String?
    var province: String?
    var license: String?
    var ownerId: String?
    var email: String?
    var phoneNumber: String?
    var logo: String?
    var businesses: [BusinessWorkSites]?
    var accountOwners: [UserBasicInformation]?
    var staffMembers: [UserBasicInformation]?
//    var businessTypes: [OrganizationBusinessTypeCounts]
    
}
struct DepartMentWorkSites : Codable {
    var fteStatusOfStaffUser : String?
    var staffRoleByUserId : String?
    var departmentId: String?
    var businessId: String?
    var departmentTypeName: String?
    var phoneNumber: String?
//    extension: Int
    var email: String?
    var address: String?
    var description: String?
    var status: String?
//    var staffRoles: [DepartmentStaff]
//    var shiftType: [DepartmentShifts]
    var colleagues: [UserBasicInformation]?

}

