//
//  WorkSpaceList_Nw.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 24/10/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
class ListRowData {
    var departmnentsCount = "0"
    var colorCode = ""
    var workSiteName = ""
    var workSiteSettingID = ""
    var workSiteId = ""
    var orgID = ""
}

class WSNewListVM {
    
    private var sections = Array(NSSet.init(array: (Profile.shared.userWorksiteSettings?.getWorksiteSettingsByUserId?.map({$0.organizationId}) ?? ["0"])))
    
    var sectionsHeader : [String]{
        var strArr = [String]()
        for sec in sections {
            if let filaA = Profile.shared.userWorksiteSettings?.getWorksiteSettingsByUserId?.filter({$0.organizationId == sec as? String}){
                strArr.append(filaA.last?.organizationDetails?.name ?? "")
            }
        }
        return strArr
    }
    var rowsData : [[ListRowData]]{
        var finalArray = [[ListRowData]]()
        for sec in sections {
            var dt = [ListRowData]()
            if let filaA = Profile.shared.userWorksiteSettings?.getWorksiteSettingsByUserId?.filter({$0.organizationId == sec as? String}){
                for obj in filaA {
                    let roData = ListRowData()
                    roData.colorCode = obj.color ?? ""
                    roData.departmnentsCount = "\(obj.departmentCount ?? 0)"
                    roData.workSiteName = obj.worksiteDetails?.name ?? ""
                    roData.workSiteSettingID = obj.worksiteSettingId ?? "0"
                    roData.orgID = obj.organizationId ?? "0"
                    roData.workSiteId = obj.worksiteId ?? "0"
                    dt.append(roData)
                }
            }
            finalArray.append(dt)
        }
        return finalArray
    }
}


class WorkSpaceList_Nw: UIViewController {
  @IBOutlet weak var infoTableView: UITableView!
  @IBOutlet weak var backgrounView: UIView!

    var businessworkSite: [BusinessWorkSites] = [BusinessWorkSites]()
    private let viewModel: WSSettingsVC = WSSettingsVC()
    var worksiteSettings: WorkSiteSettings?
    var organizationsWorkSites: OrganizationsWorkSites?
    var vmObj = WSNewListVM()
    
    var sections = [String]()
    var rows = [[ListRowData]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgrounView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sections = vmObj.sectionsHeader
        rows = vmObj.rowsData                      
    }
    @IBAction func getStartedClick(_ sender: Any) {
        
    }      
      @IBAction func backClick(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }
    @IBAction func addWSClick(_ sender: Any) {
        let wsVC = UIStoryboard.init(name: "WorkSpace", bundle: nil)
            .instantiateViewController(identifier: "WorkSpaceInviteCodeVC") as! WorkSpaceInviteCodeVC
        wsVC.modalPresentationStyle = .fullScreen
        wsVC.delegate = self
        self.present(wsVC, animated: true) {
            wsVC.skipBtn.isHidden = true
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension WorkSpaceList_Nw : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
       return rows[section].count
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WSListCell_Nw", for: indexPath) as! WSListCell_Nw

         
        //cell.businessWork = 
//        cell.descriptionLabel.text = (self.businessWorksites?.numberOfDepartment ?? "") + "departments"

       // let colorString = self.worksiteSettings?.color
       // cell.imageView?.backgroundColor = UIColor(hex: colorString!)

        cell.nameTF.text = rows[indexPath.section][indexPath.row].workSiteName
        cell.descriptionLabel.text = "\(rows[indexPath.section][indexPath.row].departmnentsCount) departments"
        cell.imgVw.isHidden = true
        let colorCode = rows[indexPath.section][indexPath.row].colorCode
        cell.thumbnailImg.backgroundColor = colorCode.hexStringToUIColor()

        return cell
    }
   
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell

        

        cell.headerTitle.text = sections[section]
        return cell.contentView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = rows[indexPath.section][indexPath.row]
        fetchAllColleauges(forSettingsID: obj.workSiteSettingID)
        fetchAllDepartments(organizationId: obj.orgID)
        WorkSpaceSettingsUtility.shared.wsColorCode = rows[indexPath.section][indexPath.row].colorCode
        WorkSpaceSettingsUtility.shared.orgId = obj.orgID
        WorkSpaceSettingsUtility.shared.wsSettingsId = obj.workSiteSettingID
        WorkSpaceSettingsUtility.shared.workSiteId = obj.workSiteId
        
        let vc = self.storyboard?.instantiateViewController(identifier: "WSSiteDetailsVC") as! WSSiteDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
        
//        switch vmObj.sections[indexPath.section] {
//        case .settings:
//            let vc = self.storyboard?.instantiateViewController(identifier: "WSSettingsVC") as! WSSettingsVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        default:
//            print("")
//        }
    }
    
    func fetchAllColleauges(forSettingsID : String)  {
        if let arr = Profile.shared.userWorksiteSettings?.getWorksiteSettingsByUserId?.filter({$0.worksiteSettingId == forSettingsID}){
            WorkSpaceSettingsUtility.shared.colleauges = (arr.first?.organizationDetails?.staffMembers)!
        }
    }
    func fetchAllDepartments(organizationId : String)  {
        _ = [DepartMentWorkSites]()
        if let arr = Profile.shared.userWorksiteSettings?.getWorksiteSettingsByUserId?.filter({$0.organizationId == organizationId}){
            WorkSpaceSettingsUtility.shared.departments = arr.first?.departmentDetails
            WorkSpaceSettingsUtility.shared.orgDetails = arr.first?.organizationDetails
        }
    }
    
    
    
}


extension WorkSpaceList_Nw : WorkSpaceInviteDelegate{
    func workspaceUpdated(wsInfoVC: WorkSpaceInviteCodeVC) {
        wsInfoVC.dismiss(animated: true, completion: nil)
        ReusableAlertController.showAlert(title: "FloatCare", message: "Worksapce updated successfully")
//        GetWorkSiteDetails().checkForWorkSpaceId(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)") { (success) in
//            self.infoTableView.reloadData()
//        }
    }
}

extension UIColor {
       public convenience init?(hex: String) {
           let r, g, b, a: CGFloat

           if hex.hasPrefix("#") {
               let start = hex.index(hex.startIndex, offsetBy: 1)
               let hexColor = String(hex[start...])

               if hexColor.count == 8 {
                   let scanner = Scanner(string: hexColor)
                   var hexNumber: UInt64 = 0

                   if scanner.scanHexInt64(&hexNumber) {
                       r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                       g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                       b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                       a = CGFloat(hexNumber & 0x000000ff) / 255

                       self.init(red: r, green: g, blue: b, alpha: a)
                       return
                   }
               }
           }

           return nil
       }
   }

extension String{
    func hexStringToUIColor() -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

}


