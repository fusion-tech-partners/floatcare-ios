//
//  NotificationCustomCell.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 9/24/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class MyNotificationsTableViewCell: UITableViewCell {
   
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var cellBgView: UIView!
    
    @IBOutlet weak var hrsDisplayLbl: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var displayImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellBgView.layer.cornerRadius = 15
        
        self.displayImgView.layer.cornerRadius = 10.0
        self.displayImgView.contentMode = UIView.ContentMode.center
        self.displayImgView.backgroundColor = .cellIconBgColor
    }
    var appNotification: AppNotification? {
            
            didSet {
                
                self.headerLbl.text = self.appNotification?.notificationType ?? "Invitation"
                self.msgLbl.text = self.appNotification?.content ?? ""
                self.hrsDisplayLbl.text = self.appNotification?.onDate?.covertDateToReadableFormat
            }
        }
        
    }

