//
//  NotificationsIconView.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 9/24/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

class NotificationsIcon: UIViewController {
    var userNotification: UserNotification!
    let notifyviewModel = NotificationSettingsViewModel()
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var topBgView: UIView!
    
    @IBOutlet weak var settingsBtn: UIButton!
   
    @IBOutlet weak var notifyTblView: UITableView!
   
    var allnotifications = [findUserNotificationByUserId]()
    
    override func viewDidLoad() {
     super.viewDidLoad()
        self.notifyviewModel.downloadAppNotifications { [weak self] in
            
            DispatchQueue.main.async {
                
                self?.notifyTblView.reloadData()
            }
        }
        
//       self.notifyviewModel.onDownloadingNotificaitons = { [weak self] in
//        
//        DispatchQueue.main.async {
//            
//            self?.notifyTblView.reloadData()
//        }
//        
//        }
    // setUpNaigationBar()
     
    self.navigationController?.isNavigationBarHidden = true
            topBgView.layer.cornerRadius = 40
            topBgView.layer.maskedCorners = [.layerMinXMaxYCorner]
        }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       
    }
}
extension NotificationsIcon: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       print("no.  notifications--------------",self.notifyviewModel.userNotifications.count)
       // guard section != 0 else { return 1}
       return  self.notifyviewModel.appNotifications.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyNotificationsTableViewCell", for: indexPath) as! MyNotificationsTableViewCell
        cell.layer.cornerRadius = 20.0
        cell.appNotification = self.notifyviewModel.appNotifications[indexPath.row]
        return cell
    }
    
}
