//
//  BasicAccountInfoViewController.swift
//  Fusion Tech
//
//  Created by OMNIADMIN on 27/05/20.
//  Copyright © 2020 OMNIADMIN. All rights reserved.
//

import UIKit

class BasicAccountInfoViewController: UIViewController {
    
    let sections: [BasicAccountInfoTabSections] = [.profilePic, .profileInfo, .contactInfo]
    
    @IBOutlet weak var accountInfoTableView: UITableView!
    
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var headingTitleView: UIView!
    @IBOutlet weak var headingTitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        accountInfoTableView.reloadData()
    }
    
    
    private func setupView() {
      
        self.navigationBar.isRightButtonHidden = true
        
        self.headingTitleView.roundCorners(.layerMinXMaxYCorner, radius: 30.0)
        self.headingTitleView.backgroundColor = .profileBgColor
        self.headingTitleLabel.font = UIFont(font: .Athelas, weight: .regular, size: 32)
        self.headingTitleLabel.textColor = .headingTitleColor
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func movetoProficeInfoVC() {
        let vc = self.storyboard?.instantiateViewController(identifier: "ProfileInformationVC") as! ProfileInformationVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc func movetoContactInfoVC() {
        let vc = self.storyboard?.instantiateViewController(identifier: "ContactInformationVC") as! ContactInformationVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}


extension BasicAccountInfoViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.sections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.sections[section].tabs().count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        guard self.sections[section] != .profilePic else { return nil}
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileEditTableHeaderCell.rawValue) as! ProfileEditTableHeaderCell
        let sectionType = self.sections[section]
        cell.titleLabel.text = sectionType.rawValue
        if section == 1{
            cell.editButton.addTarget(self, action: #selector(movetoProficeInfoVC), for: .touchUpInside)
        }
        if section == 2{
            cell.editButton.addTarget(self, action: #selector(movetoContactInfoVC), for: .touchUpInside)
        }
        return cell.contentView
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        guard self.sections[section] != .profilePic else { return nil}
        let footerView = UIView()
        footerView.backgroundColor = .footerBgColor
        return footerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = self.sections[indexPath.section]
        let row = section.tabs()[indexPath.row]
        
        switch  section {
            case .profilePic:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.EditProfilePicTableViewCell.rawValue, for: indexPath) as! EditProfilePicTableViewCell
                return cell
            case .profileInfo, .contactInfo:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileSettingsTableCell.rawValue, for: indexPath) as! ProfileSettingsTableCell
                cell.accountInfo = row
                cell.isAccessoryButtonHidden = true
                cell.setupAccountInfoCellContent()

                return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        guard self.sections[section] != .profilePic else { return 0.0}
        return 66.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        guard self.sections[section] != .profilePic else { return 0.0}
        return 5.0
    }
}


extension BasicAccountInfoViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let verifyMobileNumberViewController = self.storyboard?.instantiateViewController(identifier: "VerifyMobileNumberViewController") as! VerifyMobileNumberViewController
//        self.navigationController?.pushViewController(verifyMobileNumberViewController, animated: true)
    }
}
