//
//  ContactInformationVC.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 28/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit
enum ContactUpdateType: String {
    case primaryNum
    case secondaryNum
    case primaryEmail
    case secondaryEmail
}
fileprivate class VerificationTexts {

    func getUIvalues(type:IndexPath) -> EmailVerificationVM {
        let emailObj = EmailVerificationVM()
        switch type {
        case .init(row: 0, section: 0):
            emailObj.titleText = "Primary Number"
            emailObj.placeHolder = "Enter your number"
            emailObj.tfTitle = "Phone Number"
            emailObj.otpTitleText = "Verify Number"
            emailObj.updateType = .primaryNum
            
        
        case .init(row: 1, section: 0):
            emailObj.titleText = "Secondary Number"
            emailObj.placeHolder = "Enter Your number"
            emailObj.tfTitle = "Phone Number"
            emailObj.otpTitleText = "Secondary Number"
            emailObj.updateType = .secondaryNum
            
            
        case .init(row: 0, section: 1):
            emailObj.titleText = "Primary Email"
            emailObj.placeHolder = "Enter Your Email"
            emailObj.tfTitle = "Email Address"
            emailObj.otpTitleText = "Verify Email"
            emailObj.updateType = .primaryEmail
    
            
        case .init(row: 1, section: 1):
            emailObj.titleText = "Secondary Email"
            emailObj.placeHolder = "Enter Your Email"
            emailObj.tfTitle = "Email Address"
            emailObj.otpTitleText = "Secondary Email"
            emailObj.updateType = .secondaryEmail
            
        default:
            print("No item Found")
        }
        return emailObj
    }
}

class ContactEditModelVM {
     static let sharedInstance = ContactEditModelVM()
    fileprivate let labelTitles = [["Primary Phone","Secondary Phone"],["Primary Email","Secondary Email"]]
    fileprivate let vefifyScreenlabelTitles = [["Primary Number","Secondary Number"],["Primary Email","Secondary Email"]]
    fileprivate let otpScreenlabelTitles = [["Primary Number","Secondary Number"],["Primary Email","Secondary Email"]]

    fileprivate let headerTitles = ["Phone Number","Email Address"]
    var values: [[String]]{
        get{
            return [ [Profile.shared.userBasicInformation?.phone ?? "",Profile.shared.userBasicInformation?.secondaryPhoneNumber ?? ""],[Profile.shared.userBasicInformation?.email ?? "",Profile.shared.userBasicInformation?.secondaryEmail ?? ""]]
        }
    }
    
}

class ContactInformationVC: UIViewController {
   
    
    @IBOutlet weak var titleLabel: LabelHeader!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var contactTableView: UITableView!
    fileprivate var contactObj = ContactEditModelVM.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupInitialUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
           super.viewDidDisappear(animated)
           NotificationCenter.default.removeObserver(UIResponder.keyboardWillShowNotification)
           NotificationCenter.default.removeObserver(UIResponder.keyboardWillHideNotification)
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupInitialUI(){
        backgroundView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        backgroundView.backgroundColor = .profileBgColor
        titleLabel.font = UIFont(name: Fonts.athleasRegular, size: 36)
        titleLabel.textColor = UIColor().rgb(r: 72, g: 93, b: 186, k: 1)// #colorLiteral(red: 0.2823529412, green: 0.3647058824, blue: 0.7294117647, alpha: 1)
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                contactTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
           }
        }

        @objc func keyboardWillHide(notification: NSNotification) {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                contactTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            }
        }
    
    
}

extension ContactInformationVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return contactObj.headerTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactObj.labelTitles[section].count
       }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
        cell.floatLabel.text = contactObj.labelTitles[indexPath.section][indexPath.row]
        cell.nameTF.text = contactObj.values[indexPath.section][indexPath.row]
        if cell.nameTF.text?.isEmpty ?? true{
            cell.verifiedImg.isHidden = true
        }else{
            cell.verifiedImg.isHidden = false
        }
        return cell
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell
        cell.headerTitle.text = contactObj.headerTitles[section]
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if(section == 0){
            let view = UIView.init(frame: CGRect.zero)
            view.backgroundColor = .white
            let borderVw = UIView.init(frame: CGRect.init(x: 0, y: 14, width: self.view.frame.size.width, height: 4))
            borderVw.backgroundColor = UIColor().rgb(r: 247, g: 247, b: 251, k: 1)
            view.addSubview(borderVw)
            return view
        }else{
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0{
            return 16
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc  = self.storyboard?.instantiateViewController(identifier: "EmailVerificationVC") as! EmailVerificationVC
        vc.modelObj = VerificationTexts().getUIvalues(type: indexPath)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}


extension ContactInformationVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
}
