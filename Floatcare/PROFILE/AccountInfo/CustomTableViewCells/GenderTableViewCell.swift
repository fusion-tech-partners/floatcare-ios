//
//  GenderTableViewCell.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 27/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

internal class ColorCodes{
    static let btnSelected = #colorLiteral(red: 0.9019607843, green: 0.9058823529, blue: 0.9490196078, alpha: 1)
    static let btnUnSelected = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let butselectFont = #colorLiteral(red: 0.2823529412, green: 0.3607843137, blue: 0.7294117647, alpha: 1)
    static let butUnselectFont = #colorLiteral(red: 0.4274509804, green: 0.4470588235, blue: 0.4705882353, alpha: 1)
    static let floatLabelColor = #colorLiteral(red: 0.3607843137, green: 0.3882352941, blue: 0.6705882353, alpha: 1)
    
}

class GenderTableViewCell: UITableViewCell {
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    @IBOutlet weak var genderLabel: UILabel!
    
    private var arr = [UIButton]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        arr = [maleBtn,femaleBtn,otherBtn]
        setUpUI()
        genderButtonClick(maleBtn)
    }
    
    func setUpUI()  {
        genderLabel.textColor = ColorCodes.floatLabelColor
        handleOptionsUI()
    }
    func displySavedGender()  {
         if Profile.shared.userBasicInformation?.gender == "Male" {
                   genderButtonClick(maleBtn)
               }else if Profile.shared.userBasicInformation?.gender == "Female"{
                   genderButtonClick(femaleBtn)
               }else if Profile.shared.userBasicInformation?.gender == "Other"{
                   genderButtonClick(otherBtn)
               }
    }
    
    @IBAction func genderButtonClick(_ sender: UIButton) {
        handleOptionsUI()
        sender.setTitleColor(ColorCodes.butselectFont, for: .normal)
        sender.backgroundColor = ColorCodes.btnSelected
        ProfileInfoVM.sharedInstance.gender = (sender.titleLabel?.text!)!
    }
    func handleOptionsUI() {
        arr.forEach { $0.setTitleColor(ColorCodes.butUnselectFont, for: .normal)
            $0.backgroundColor = ColorCodes.btnUnSelected
            $0.layer.cornerRadius = 10
            $0.titleLabel?.font = UIFont.init(name: Fonts.sFProText, size: 14)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
                
        // Configure the view for the selected state
    }

}
