//
//  ProfileNameCell.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 27/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

class ProfileNameCell: UITableViewCell {
    
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var floatLabel: UILabel!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var bioTextView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func calendarClick(_ sender: Any) {
        nameTF.becomeFirstResponder()
    }
}
