//
//  EmailOTPVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 06/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class EmailOTPVC: UIViewController {
    
    @IBOutlet weak var titleLabel: LabelHeader!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var tfOne: OTPField!
    @IBOutlet weak var tfTwo: OTPField!
    @IBOutlet weak var tfThree: OTPField!
    @IBOutlet weak var tfFour: OTPField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var nextButton: ButtonViolet!
    
    var titleText = ""
    var otpStringArray = Array.init(repeating: "*", count: 4)
    var tfsArray = [UITextField]()
    var nmbrOrmail = ""
    var updateType : ContactUpdateType?
    private let sessionProvider = URLSessionProvider()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupInitialUI()
        titleLabel.text = titleText
        tfsArray = [tfOne,tfTwo,tfThree,tfFour]
        tfsArray[0].becomeFirstResponder()
        tfsArray.forEach{
            $0.delegate = self
            $0.keyboardType = .numberPad
            $0.addTarget(self, action: #selector(didenterOtp(sender:)), for: .editingChanged)

        }
        tfOne.becomeFirstResponder()

    }
    
    func setupInitialUI(){
        backgroundView.roundCorners(corners: [.bottomLeft,], radius: 60.0)
        bottomView.roundCorners(corners: [.topLeft,], radius: 60.0)
        backgroundView.backgroundColor = .profileBgColor
        bottomView.backgroundColor = .profileBgColor
        titleLabel.font = UIFont(name: Fonts.athleasRegular, size: 36)
        titleLabel.textColor = UIColor().rgb(r: 72, g: 93, b: 186, k: 1)// #colorLiteral(red: 0.2823529412, green: 0.3647058824, blue: 0.7294117647, alpha: 1)
    }
    
    @objc func didenterOtp(sender:UITextField){
        if let str = sender.text,!(sender.text!.isEmpty){
            let currentIndex = tfsArray.firstIndex(of: sender)
            otpStringArray[currentIndex!] = str
            if currentIndex! < 3 {
                tfsArray[currentIndex! + 1].becomeFirstResponder()
            }else{
                tfsArray.last?.resignFirstResponder()
            }
        }else{
            let currentIndex = tfsArray.firstIndex(of: sender)
            otpStringArray[currentIndex!] = ""
            if currentIndex! > 0 {
                tfsArray[currentIndex! - 1].becomeFirstResponder()
            }
        }
    }
    
    @IBAction func resendOTP(_ sender: Any) {
        switch updateType {
        case .primaryNum, .secondaryNum:
            verifyMobileNumber()
        case .primaryEmail, .secondaryEmail :
            updateEmail()
        default:
            ""
        }
        
    }
    
    @IBAction func backclick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextClick(_ sender: Any) {
        let otp = otpStringArray.joined(separator: "")
        if otp.contains("*") || otp != MobileNumberModel.sharedInstance.otp {
            errorLabel.isHidden = false
            tfsArray.forEach{
                $0.layer.borderColor = UIColor().errorRed.cgColor
            }
        } else {
            // Call GRAPHQL SERVICE
            if ConnectionCheck.isConnectedToNetwork() {
                ProfileUpdateAPI().updateContactInfo(type: updateType!, param: nmbrOrmail) { (msg, isCompleted) in
                    var msg = ""
                    switch self.updateType{
                    case .primaryNum :
                            msg = "Primary Contact updated successfully"
                    case .secondaryNum:
                            msg = "Secondary Contact updated successfully"
                    case .primaryEmail:
                            msg = "Primary Email updated successfully"
                    case .secondaryEmail:
                            msg = "Secondary Email updated successfully"
                    case .none:
                        print("")
                    }
                    ReusableAlertController.showAlertOk(title:"", message: msg) { (code) in
                        let vcsArr = self.navigationController?.viewControllers
                        self.navigationController?.popToViewController(vcsArr![vcsArr!.count - 4], animated: true)
                    }
                }
            } else {
                ReusableAlertController.showAlert(title: "Float Care", message: "Please check your internet connection")
            }
        }
    }
    
    /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension EmailOTPVC : UITextFieldDelegate{
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.text = ""
        let currentIndex = self.tfsArray.firstIndex(of: textField)
        self.otpStringArray[currentIndex!] = "*"
        tfsArray.forEach{$0.layer.borderColor = UIColor().buttonViolet.cgColor}
        errorLabel.isHidden = true
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
     }
}
// MARK: - Phone Update Extension
extension EmailOTPVC{
    private func verifyMobileNumber() {
            DispatchQueue.main.async {
                Progress.shared.showProgressView()
            }
                    
            sessionProvider.request(type: MobileNumber.self, service: MobileNumberService.number(number: nmbrOrmail)) { response in
            switch response {
            case let .success(posts):
                DispatchQueue.main.async {
                Progress.shared.hideProgressView()
                }
                DispatchQueue.main.async {
                    self.showAlert(posts.message, navigate: true)
                }
                
                // Fill OTP
                MobileNumberModel.sharedInstance.otp = posts.data.otp
                MobileNumberModel.sharedInstance.mobileNumber = posts.data.phoneNumber

            case let .failure(error):
                
                DispatchQueue.main.async {
                Progress.shared.hideProgressView()
                }
                
                if error == .irrelevent {
                    DispatchQueue.main.async {
                        self.showAlert("Please enter valid Phone number.", navigate: false)
                    }
                } else {
                    self.showAlert("Somethig went wrong", navigate: false)
                }
            }
        }
    }
    fileprivate func showAlert(_ message: String, navigate: Bool) {
        let alert = UIAlertController(title: "Float Care", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) {
               UIAlertAction in
            if navigate {
                
            }
           }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}
// MARK: - Email Update Extension
extension EmailOTPVC {
    
     func updateEmail() {
        view.endEditing(true)
        
        if nmbrOrmail.isValidEmail(){
            
            if ConnectionCheck.isConnectedToNetwork() {
                verifyEmailAndRegister()
            } else {
                ReusableAlertController.showAlert(title: "Float Care", message: "Please check your internet connection")
            }
        } else {
            ReusableAlertController.showAlert(title: "Float Care", message: "Enter Your Valid Email")
        }
    }
    
    func verifyEmailAndRegister() {
        Progress.shared.showProgressView()
        
        
        sessionProvider.request(type: Email.self, service: ChangeEmailService.email(email: nmbrOrmail)) { response in
            switch response {
            case let .success(posts):
                Progress.shared.hideProgressView()
                DispatchQueue.main.async {
                    ReusableAlertController.showAlert(title: "Float Care", message: posts.message)
                }
                EmailModel.sharedInstance.otp = posts.data?.otp
                EmailModel.sharedInstance.email = posts.data?.email
            case let .failure(error):
                DispatchQueue.main.async {
                    Progress.shared.hideProgressView()
                }
                DispatchQueue.main.async {
                    ReusableAlertController.showAlert(title: "Float Care", message: "Somethig went wrong")
                }
            }
        }
    }
}
