//
//  EmailVerificationVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 06/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class EmailVerificationVM  {
    var titleText = ""
    var  otpTitleText = ""
    var placeHolder = ""
    var tfTitle = ""
    var updateType : ContactUpdateType?
}

class EmailVerificationVC: UIViewController {
    
    @IBOutlet weak var saveButton: ButtonViolet!
    @IBOutlet weak var titleLabel: LabelHeader!
    
    @IBOutlet weak var emailTitleLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var emailTF: UITextField!
    var modelObj = EmailVerificationVM()
    private let sessionProvider = URLSessionProvider()
    var inputText = ""
    @IBOutlet weak var typeImage: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = modelObj.titleText
        emailTF.placeholder = modelObj.placeHolder
        emailTitleLabel.text = modelObj.tfTitle
        emailTF.delegate = self
        
        switch modelObj.updateType {
        case .primaryNum, .secondaryNum:
            typeImage.setBackgroundImage(#imageLiteral(resourceName: "ic_phone_inactive"), for: .normal)
            emailTF.keyboardType = .numberPad
        case .primaryEmail, .secondaryEmail:
            typeImage.setBackgroundImage(#imageLiteral(resourceName: "email"), for: .normal)
        case .none:
            print("")
        }
        
        // Do any additional setup after loading the view.
        setupInitialUI()
    }
    

    @IBAction func saveClick(_ sender: Any) {
        
        switch modelObj.updateType {
        case .primaryNum, .secondaryNum:
            verifyMobileNumber()
        case .primaryEmail, .secondaryEmail :
            updateEmail()
        default:
            print("In Progress")
        }
    }
    func moveToOTPVC()  {
        let vc = self.storyboard?.instantiateViewController(identifier: "EmailOTPVC") as! EmailOTPVC
        vc.titleText = self.modelObj.otpTitleText
        vc.nmbrOrmail = inputText
        vc.updateType = modelObj.updateType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setupInitialUI(){
        backgroundView.roundCorners(corners: [.bottomLeft,], radius: 60.0)
        bottomView.roundCorners(corners: [.topLeft,], radius: 60.0)
        titleLabel.font = UIFont(name: Fonts.athleasRegular, size: 36)
        titleLabel.textColor = UIColor().rgb(r: 72, g: 93, b: 186, k: 1)// #colorLiteral(red: 0.2823529412, green: 0.3647058824, blue: 0.7294117647, alpha: 1)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EmailVerificationVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch modelObj.updateType {
        case .primaryNum, .secondaryNum:
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = formattedNumber(number: newString)
            return false
        default:
            return true
        }
    }
    func formattedNumber(number: String) -> String {
            let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            let mask = "(XXX) XXX-XXXX"

            var result = ""
            var index = cleanPhoneNumber.startIndex
            for ch in mask where index < cleanPhoneNumber.endIndex {
                if ch == "X" {
                    result.append(cleanPhoneNumber[index])
                    index = cleanPhoneNumber.index(after: index)
                } else {
                    result.append(ch)
                }
            }
            return result
        }
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
             textField.resignFirstResponder()
         }
}
extension EmailVerificationVC{
    private func verifyMobileNumber() {
            DispatchQueue.main.async {
                Progress.shared.showProgressView()
            }
        
        guard let number = emailTF.text, !(number.isEmpty) else {
            return
        }
        let code = Countries.init().countries.filter({$0.countryCode == Locale.current.regionCode})
        let actulNumber = "\(code.last?.phoneExtension ?? "1")\(number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())"
            inputText = actulNumber
            sessionProvider.request(type: MobileNumber.self, service: MobileNumberService.number(number: actulNumber)) { response in
            switch response {
            case let .success(posts):
                DispatchQueue.main.async {
                Progress.shared.hideProgressView()
                }
                DispatchQueue.main.async {
                    self.showAlert(posts.message, navigate: true)
                }
                
                // Fill OTP
                MobileNumberModel.sharedInstance.otp = posts.data.otp
                MobileNumberModel.sharedInstance.mobileNumber = posts.data.phoneNumber

            case let .failure(error):
                
                DispatchQueue.main.async {
                Progress.shared.hideProgressView()
                }
                
                if error == .irrelevent {
                    DispatchQueue.main.async {
                        self.showAlert("Please enter valid Phone number.", navigate: false)
                    }
                } else {
                    self.showAlert("Somethig went wrong", navigate: false)
                }
            }
        }
    }
    fileprivate func showAlert(_ message: String, navigate: Bool) {
        let alert = UIAlertController(title: "Float Care", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) {
               UIAlertAction in
            if navigate {
                self.moveToOTPVC()
            }
           }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - Email Update Extension
extension EmailVerificationVC {
    
     func updateEmail() {
        view.endEditing(true)
        if let number = emailTF.text, !(number.isEmpty), number.isValidEmail(){
            
            if ConnectionCheck.isConnectedToNetwork() {
                verifyEmailAndRegister()
            } else {
                ReusableAlertController.showAlert(title: "Float Care", message: "Please check your internet connection")
            }
        }
         else {
            ReusableAlertController.showAlert(title: "Float Care", message: "Enter Your Valid Email")
        }
    }
    
    func verifyEmailAndRegister() {
        Progress.shared.showProgressView()
        
        
        sessionProvider.request(type: Email.self, service: ChangeEmailService.email(email: emailTF.text!)) { response in
            switch response {
            case let .success(posts):
                Progress.shared.hideProgressView()
                DispatchQueue.main.async {
                    ReusableAlertController.showAlert(title: "Float Care", message: posts.message)
                }
                EmailModel.sharedInstance.otp = posts.data?.otp
                EmailModel.sharedInstance.email = posts.data?.email
            case let .failure(error):
                DispatchQueue.main.async {
                    Progress.shared.hideProgressView()
                }
                DispatchQueue.main.async {
                    ReusableAlertController.showAlert(title: "Float Care", message: "Somethig went wrong")
                }
            }
        }
    }
}
