//
//  ProfileInformationVC.swift
//  FloatCare
//
//  Created by Dineshkumar kothuri on 26/05/20.
//  Copyright © 2020 Dineshkumar kothuri. All rights reserved.
//

import UIKit

class ProfileInfoVM {
    static let sharedInstance = ProfileInfoVM()
    var firstName = Profile.shared.userBasicInformation?.firstName ?? ""
    var lastName = Profile.shared.userBasicInformation?.lastName ?? ""
    var prefferedName = Profile.shared.userBasicInformation?.preferredName ?? ""
    var location = Profile.shared.userBasicInformation?.address1 ?? ""
    var gender = Profile.shared.userBasicInformation?.gender ?? ""
    var birthDay = Profile.shared.userBasicInformation?.dateOfBirth?.covertDateToReadableFormat ?? ""
    var bio = Profile.shared.userBasicInformation?.bioInformation ?? ""
    var titles = ["First Name","Last Name","Preferred Name","Location","Gender", "Birthday","Bio"]
}



class ProfileInformationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var saveButton: ButtonViolet!
    
    
    @IBOutlet weak var informationTable: UITableView!
    @IBOutlet weak var titleLabel: LabelHeader!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    var datePicker = UIDatePicker()
    let toolbar = UIToolbar();
    var profileInfoObj = ProfileInfoVM.sharedInstance
    var currentTextField : UITextField?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
                
        
        let fontFamilyNames = UIFont.familyNames
//        for familyName in fontFamilyNames {
//            print("------------------------------")
//            print("Font Family Name = [\(familyName)]")
//            let names = UIFont.fontNames(forFamilyName: familyName )
//            print("Font Names = [\(names)]")
//
//        }
        
//        Optional(<UICTFont: 0x7fee816077a0> font-family: "Athelas-Regular"; font-weight: normal; font-style: normal; font-size: 32.00pt)
        setupInitialUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    
    }
    
    func updateProfileInformation() {
        
        guard !(profileInfoObj.firstName.isEmpty) else{
            ReusableAlertController.showAlert(title:"Error!", message: "Please enter First Name")
            return
        }
        guard !(profileInfoObj.lastName.isEmpty) else{
            ReusableAlertController.showAlert(title:"Error!", message: "Please enter Last Name")
            return
        }
        Progress.shared.showProgressView()
        ProfileUpdateAPI.init().updateUserBasicDetails(name: profileInfoObj.firstName, prfName: profileInfoObj.prefferedName, lastName: profileInfoObj.lastName, location: profileInfoObj.location, gender: profileInfoObj.gender, birday: profileInfoObj.birthDay, bio: profileInfoObj.bio) { (message,issuccess) in
            Progress.shared.hideProgressView()
            if issuccess{
                self.navigationController?.popViewController(animated: true)
            }
            ReusableAlertController.showAlert(title: "", message: message)
        }

    }
        
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(UIResponder.keyboardWillShowNotification)
        NotificationCenter.default.removeObserver(UIResponder.keyboardWillHideNotification)
    }
    
    func showDatePicker() {
        let calendar = Calendar.current
        var com = DateComponents()
        com.calendar = calendar
        com.year = -10
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = #colorLiteral(red: 0.2823529412, green: 0.3607843137, blue: 0.7294117647, alpha: 1)
        datePicker.maximumDate = calendar.date(byAdding: com, to: Date())
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(dateDoneButton))

        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(dateCancelButton))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

    }
    
    @objc func dateDoneButton(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, yyyy"
        profileInfoObj.birthDay = formatter.string(from: datePicker.date)
        informationTable.reloadData()
    }
    @objc func dateCancelButton(){
        currentTextField!.resignFirstResponder()
    }
    
    func setupInitialUI(){
        backgroundView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        backgroundView.backgroundColor = .profileBgColor
        bottomView.roundCorners(corners: [.topLeft,], radius: 40.0)
        bottomView.backgroundColor = .profileBgColor
        titleLabel.font = UIFont(name: Fonts.athleasRegular, size: 36)
        titleLabel.textColor = UIColor().rgb(r: 72, g: 93, b: 186, k: 1)// #colorLiteral(red: 0.2823529412, green: 0.3647058824, blue: 0.7294117647, alpha: 1)
        showDatePicker()
    }
    
    @IBAction func saveClick(_ sender: Any) {
        updateProfileInformation()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
           if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
               informationTable.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
          }
       }

       @objc func keyboardWillHide(notification: NSNotification) {
           if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
               informationTable.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
           }
       }

//    MARK: - TableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileInfoObj.titles.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 4){
        return 103
        }
        if (indexPath.row == 6){
            return UITableView.automaticDimension
        }
        return 89
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
            if(indexPath.row == 4){
                let cell = tableView.dequeueReusableCell(withIdentifier: "GenderTableViewCell", for: indexPath) as! GenderTableViewCell
                cell.displySavedGender()
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileNameCell", for: indexPath) as! ProfileNameCell
                cell.nameTF.addTarget(self, action: #selector(inputFieldChanged(sender:)), for: .allEvents)
                cell.nameTF.delegate = self
                self.addDoneButtonOnKeyboard(tv: cell.bioTextView)
                cell.bioTextView.delegate = self
                cell.floatLabel.text = profileInfoObj.titles[indexPath.row]
                cell.nameTF.tag = 101 + indexPath.row
//                cell.nameTF.text = profileInfoObj.values[indexPath.row]
            if indexPath.row == 0{
                cell.nameTF.text = profileInfoObj.firstName
            }
            if indexPath.row == 1{
                  cell.nameTF.text = profileInfoObj.lastName
            }
            if indexPath.row == 2{
                  cell.nameTF.text = profileInfoObj.prefferedName
            }
            if indexPath.row == 3{
                  cell.nameTF.text = profileInfoObj.location
            }
            if indexPath.row == 5{
                cell.rightBtn.isHidden = false
                cell.nameTF.inputView = datePicker
                cell.nameTF.text = profileInfoObj.birthDay
                cell.nameTF.inputAccessoryView = toolbar
            }else{
                cell.rightBtn.isHidden = true
                cell.nameTF.inputView = nil
                cell.nameTF.inputAccessoryView = nil
                cell.nameTF.reloadInputViews()
            }
            
            if indexPath.row == 6{
                cell.bioTextView.isHidden = false
                cell.nameTF.isHidden = true
                cell.bioTextView.text = profileInfoObj.bio
            }else{
                cell.bioTextView.isHidden = true
                cell.nameTF.isHidden = false
            }
            
            return cell
        }
    }
    @objc func inputFieldChanged(sender:UITextField)  {
        currentTextField = sender
        
        switch sender.tag {
        case 101:
            profileInfoObj.firstName = sender.text!
        case 102:
            profileInfoObj.lastName = sender.text!
        case 103:
            profileInfoObj.prefferedName = sender.text!
        case 104:
            profileInfoObj.location = sender.text!
        default:
            print(sender.text!)
        }
        
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ProfileInformationVC : UITextViewDelegate, UITextFieldDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        informationTable.reloadRows(at: [IndexPath(item: 4, section: 0)], with: .none)
        if(text == "\n"){
        }else{
            profileInfoObj.bio = textView.text    
        }
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        profileInfoObj.bio = textView.text
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
    }
    func addDoneButtonOnKeyboard(tv:UITextView){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()

        tv.inputAccessoryView = doneToolbar
    }
    @objc func doneButtonAction(){
           view.endEditing(true)
    }
}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }    
}

