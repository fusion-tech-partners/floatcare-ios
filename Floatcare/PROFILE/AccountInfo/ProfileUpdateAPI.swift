//
//  ProfileUpdate.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 24/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import Alamofire
class ProfileUpdateAPI: NSObject {
    typealias completionBlock = ((String,Bool)->Void)
    var userObj : UserBasic?
    override init() {
        userObj = Profile.shared.loginResponse?.data?.userBasicInformation
    }
    func updateUserBasicDetails(name:String,
                                prfName:String,
                                lastName:String,
                                location:String,
                                gender:String,
                                birday:String,
                                bio:String,
                                comp:@escaping completionBlock) {
        userObj?.firstName = name
        userObj?.preferredName = prfName
        userObj?.lastName = lastName
        userObj?.address1 = location
        userObj?.gender = gender
        userObj?.dateOfBirth = birday
        userObj?.bioInformation = bio
        updateBasicInfoMutation(completion: comp)
    }
    
    func updateProfileImage(img : UIImage, comp:@escaping completionBlock) {
        userObj?.profilePhoto = img.toStringCompressed()
        userObj?.dateOfBirth = Profile.shared.loginResponse?.data?.userBasicInformation?.dateOfBirth?.covertDateToReadableFormat
        updateBasicInfoMutation(completion: comp)
        
    }
    
    func updateContactInfo(type:ContactUpdateType,param:String,comp:@escaping completionBlock)  {
        switch type {
        case .primaryNum:
            userObj?.phone = "+\(param)"
        case .secondaryNum:
            userObj?.secondaryPhoneNumber = "+\(param)"
        case .primaryEmail:
            userObj?.email = param
        case .secondaryEmail:
            userObj?.secondaryEmail = param
        }
        updateBasicInfoMutation(completion: comp)
    }
    func updatePassword(newPassword:String,comp:@escaping completionBlock)  {
        userObj?.password = newPassword
        updateBasicInfoMutation(completion: comp)
    }
    func updatePasswordRESTAPI(newPassword:String,comp:@escaping completionBlock){
        let urlString = "\(APIConfig.baseURL.rawValue)/user/changePassword"
        guard let url = URL(string: urlString) else {return}
        var request        = URLRequest(url: url)
        request.httpMethod = "Post"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let postBody : [String : Any] = ["userId":userObj?.userID ?? 0,"oldPassword":FloatCareUserDefaults().password,"newPassword":newPassword]
        do {
                       request.httpBody   = try JSONSerialization.data(withJSONObject: postBody)
                   } catch let error {
                       print("Error : \(error.localizedDescription)")
                   }
        
        Alamofire.request(request).responseJSON{ (response) in
            print("response is \(response)")
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                   comp("Password Updated Successfully",true)
                default:
                    comp("Password Update Failed",true)
                    
                }
            }
        }
    }
    
    func updateFCMToken(fcmToken: String)  {
        guard fcmToken.count > 0 else {
            return
        }
        userObj?.fcmToken = fcmToken
        let mutation = UpdateUserBasicInformationMutation.init(userId: "\(userObj?.userID ?? 0)", fcmToken: fcmToken, deviceType: "ios")
        ApolloManager.shared.client.perform(mutation: mutation){ (result) in
            switch(result){
            case .success(let graphQLResult):
                if let data = graphQLResult.data?.updateUserBasicInformation?.resultMap{
                    let jsonData = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    let userBasic = try? JSONDecoder().decode(UserBasicInformation.self, from: jsonData!)
                    Profile.shared.userBasicInformation = userBasic
                    self.callLoginApiToUpdateBasicObject()
                }
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
}

extension ProfileUpdateAPI {

    func updateBasicInfoMutation(completion: @escaping completionBlock) {
        
        let mutation = UpdateUserBasicInformationMutation.init(userId: "\(userObj?.userID ?? 0)" , firstName: userObj?.firstName ?? "", lastName: userObj!.lastName ?? "", email: userObj!.email ?? "", phone: userObj!.phone ?? "", preferredName: userObj?.preferredName ?? "", address1: userObj?.address1 ?? "", gender: userObj?.gender ?? "", dateOfBirth: userObj?.dateOfBirth?.convertToBackendDateFormat ?? "", bioInformation: userObj?.bioInformation ?? "", profilePhoto: userObj?.profilePhoto ?? "", secondaryPhoneNumber: userObj?.secondaryPhoneNumber ?? "", secondaryEmail: userObj?.secondaryEmail ?? "")
        ApolloManager.shared.client.perform(mutation: mutation){ (result) in
            switch result {
            case .success(let graphQLResult):
                if let data = graphQLResult.data?.updateUserBasicInformation?.resultMap{
                    let jsonData = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    let userBasic = try? JSONDecoder().decode(UserBasicInformation.self, from: jsonData!)
                    Profile.shared.userBasicInformation = userBasic
                    self.callLoginApiToUpdateBasicObject()
                    completion("Profile successfully updated",true)
                }
            case .failure(let error):
                completion(error.localizedDescription,false)
            }
        }

    }
    
    
    
        
    
}

extension ProfileUpdateAPI {
    func callLoginApiToUpdateBasicObject() {
        guard let email = LoginModel.sharedInstance.email else {
            return
        }
        
        guard let password = LoginModel.sharedInstance.password else {
            return
        }
        
        URLSessionProvider().request(type: LoginResponse.self, service: LoginService.login(email: email, password: password)) { response in
            switch response {
            case let .success(posts):
                Profile.shared.loginResponse = posts
            case let .failure(error):
                if error == .irrelevent {
                } else {
                }
            }
        }
    }
    
}



struct ChangePasswordObject : Codable {
    var message : String?
}

