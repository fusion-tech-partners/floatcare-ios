
//
//  FetchUserInfo.swift
//  Floatcare
//
//  Created by OMNIADMIN on 31/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import Apollo

struct UserNotifications {
    
}

struct findUserPrivacySettingsByUserId: Codable {
    
    var findUserPrivacySettingsByUserId : [UserPrivacySettings]!
}

struct UserPrivacySettings: Codable {
    
    var userId: String!
    var isVisibleToPublic: Bool! = false
    var isVisibleToWorkspace: Bool! = false
    var isVisibleToCollegues: Bool! = false
    var userPrivacySettingId: String!
}

struct UserBasicInformation: Codable {
    var userId: String!
    var firstName: String!
    var lastName: String!
    var middleName: String?
    var email: String!
    var password: String!
    var preferredName: String?
    var address1: String?
    var address2: String?
    var city: String?
    var state: String?
    var phone: String!
    var profilePhoto: String?
    var title: String?
    var userTypeId: String!
    var staffUserCoreTypeId: String?
    var dateOfBirth: String?
    var linkedInProfile: String?
    var organizationId: String!
    var gender: String!
    var bioInformation: String?
    var createdOn: String?
    var secondaryPhoneNumber: String?
    var secondaryEmail: String?
    var staffUserCoreTypeName: String?
    var fcmToken: String?
    var muteAllNotifications: Bool?
    
    func getDisplayedTextForProfile() -> NSMutableAttributedString {
       
       let attributedString = NSMutableAttributedString()
       let attrs1 = [NSAttributedString.Key.font : UIFont(font: .SFUIText, weight: .medium, size: 14.0), NSAttributedString.Key.foregroundColor : UIColor.appProfileEmployeCodeColor]
       let attrs2 = [NSAttributedString.Key.font : UIFont(font: .Athelas, weight: .bold, size: 23.0), NSAttributedString.Key.foregroundColor : UIColor.appProfileEmployeNameColor]
       let attrs3 = [NSAttributedString.Key.font : UIFont(font: .SFUIText, weight: .medium, size: 16.0), NSAttributedString.Key.foregroundColor : UIColor.appProfileEmployeAddressColor]
       let attrs4 = [NSAttributedString.Key.font :UIFont(font: .SFUIText, weight: .medium, size: 14.0), NSAttributedString.Key.foregroundColor : UIColor.descriptionColor]
       
       let attributedString1 = NSMutableAttributedString(string: "Nurse" , attributes: attrs1)
       let attributedString2 = NSMutableAttributedString(string:self.firstName, attributes: attrs2)
       let attributedString3 = NSMutableAttributedString(string: self.address1 ?? "" , attributes: attrs3)
       let attributedString4 = NSMutableAttributedString(string:"Member since 2009", attributes: attrs4)
       
       if attributedString1.string.count > 0 {
         attributedString.append(attributedString1)
         attributedString.append(NSMutableAttributedString(string: "\n"))
       }
       
       if attributedString2.string.count > 0 {
         attributedString.append(attributedString2)
         attributedString.append(NSMutableAttributedString(string: "\n"))
       }
       
       if attributedString3.string.count > 0 {
         attributedString.append(attributedString3)
         attributedString.append(NSMutableAttributedString(string: "\n"))
       }
       
       if attributedString4.string.count > 0 {
         attributedString.append(attributedString4)
         attributedString.append(NSMutableAttributedString(string: "\n"))
       }
       return attributedString
    }
}

class FetchUserInfo {
    
    typealias Completion = (UserBasicInformation?) -> ()
    
    func getUserInfo(userId: Int, completion: @escaping Completion) {
        
        ApolloManager.shared.client.fetch(query: FindUsersByIdQuery(userId: "\(userId)"),
                     cachePolicy: .fetchIgnoringCacheData,
                     context: nil,
                     queue: .main) { result in
                        
                        guard let user = try? result.get().data?.findUsersById?.resultMap else {
                            
                            completion(nil)
                            return
                        }
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            
                            let decoder = JSONDecoder()
                            let userBasicInformation = try? decoder.decode(UserBasicInformation.self, from: jsonData)
                            print(user)
                            completion(userBasicInformation)
                        }catch {
                            
                            completion(nil)
                        }
        }
    }
    
}

class FetchUserNotifications {
    
    typealias Completion = (findUserNotificationByUserId?) -> ()
    typealias NotificationCompletion = (AppUserNotifications?) -> ()
    
    func getUserNotifications(completion: @escaping Completion) {
        
        ApolloManager.shared.client.fetch(query: FindUserNotificationByUserIdQuery(userId: Profile.shared.userId),
                     cachePolicy: .fetchIgnoringCacheData,
                     context: nil,
                     queue: .main) { result in
                        
                        guard let user = try? result.get().data?.resultMap else {
                            
                            completion(nil)
                            return
                        }
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            
                            let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
                            print(convertedString)
                            let decoder = JSONDecoder()
                            let userBasicInformation = try! decoder.decode(findUserNotificationByUserId.self, from: jsonData)
                            print(user)
                            completion(userBasicInformation)
                        }catch {
                            
                            completion(nil)
                        }
        }
    }
    
    func getAppNotifications(completion: @escaping NotificationCompletion) {
        
        
        ApolloManager.shared.client.fetch(query: GetNotificationsByUserIdQuery(userId: Profile.shared.userId),
                                          cachePolicy: .fetchIgnoringCacheData,
                                          context: nil,
                                          queue: .main) { result in
                                            
                                            guard let user = try? result.get().data?.resultMap else {
                                                
                                                completion(nil)
                                                return
                                            }
                                            
                                            do {
                                                let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                                                
                                                let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
                                                print(convertedString)
                                                let decoder = JSONDecoder()
                                                let userBasicInformation = try decoder.decode(AppUserNotifications.self, from: jsonData)
                                                print(user)
                                                completion(userBasicInformation)
                                            }catch {
                                                
                                                completion(nil)
                                            }
        }
    }
    
}


class UpdateUserNotifications {
    
 
    typealias Completion = () -> ()
    
    func updateUserNotifications(userNotification: UserNotification, completion: @escaping Completion) {
        
        let mutation = CreateUserNotificationMutation(userId: Profile.shared.userId, notificationTypeId: userNotification.notificationTypeId, isMuteAllEnable: false, isEnablePush: userNotification.isEnablePush, isEnableSMS: userNotification.isEnableSMS, isEnableEmail: userNotification.isEnableEmail)
         ApolloManager.shared.client.perform(mutation: mutation) { result in
                        
                        guard let user = try? result.get().data?.resultMap else {
                            
                            completion()
                            return
                        }
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            
                            let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
                            print(convertedString)
                            
                            print(user)
                            completion()
                        }catch {
                            
                            completion()
                        }
        }
    }
    
    
}

class FetchNotificationsAll {
    
    typealias Completion = (findUserNotificationByUserId?) -> ()
    
    func getNotifications(completion: @escaping Completion) {
        
        ApolloManager.shared.client.fetch(query: FetchNotificationsFromWorkSpaceQueryQuery(userId: Profile.shared.userId),
                     cachePolicy: .fetchIgnoringCacheData,
                     context: nil,
                     queue: .main) { result in
                        
                        guard let user = try? result.get().data?.resultMap else {
                            
                            completion(nil)
                            return
                        }
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            
                            let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
                            print(convertedString)
                            let decoder = JSONDecoder()
                            let userBasicInformation = try! decoder.decode(findUserNotificationByUserId.self, from: jsonData)
                            print(user)
                            completion(userBasicInformation)
                        }catch {
                            
                            completion(nil)
                        }
        }
    }
    
}

