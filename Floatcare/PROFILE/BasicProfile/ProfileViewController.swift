
//
//  ProfileViewController.swift
//  Fusion Tech
//
//  Created by OMNIADMIN on 23/05/20.
//  Copyright © 2020 OMNIADMIN. All rights reserved.
//

import UIKit
import LGSideMenuController

class ProfileViewController: UIViewController {

    var viewModel: ProfileViewModel!
    
    @IBOutlet weak var profileTableView: UITableView!
    
     @IBOutlet weak var navigationBar: NavigationBar!
    var rcMessagesViewController: RCMessagesViewController?
    var lgSideMenuController: LGSideMenuController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        let backImage = UIImage(named: "ic_ChevronLeft")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }
        profileTableView.sectionHeaderHeight = CGFloat.leastNormalMagnitude
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpViewModel()
    }
    
    func setUpViewModel() {
        
        self.viewModel.fetchUserBasicInformation()
        self.viewModel.onFetchUser = {
            DispatchQueue.main.async {
                self.profileTableView.reloadData()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ProfileViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        guard section != 0 else { return 0.0}
        return 66.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return  viewModel.noOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let profileTab = viewModel.getSection(section: section)
        return viewModel.getSectionTabs(section: profileTab).count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard section != 0 else { return nil}
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileInfoTableHeaderCell.rawValue) as! ProfileInfoTableHeaderCell
        let sectionType = viewModel.getSection(section: section)
        cell.titleLabel.text = sectionType.getDisplayString()
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let profileTab = viewModel.getSection(section: indexPath.section)
        let sectionRows = profileTab.getRows()
        
        let row = sectionRows[indexPath.row]
        
        if indexPath.section == 0 {
            switch row {
                case .profileName:
                    let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileTableViewCell.rawValue, for: indexPath) as! ProfileTableViewCell
                    cell.setUpCellContent()
                    return cell
                case .profileDescription:
                    let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileInfoTableViewCell.rawValue, for: indexPath) as! ProfileInfoTableViewCell
                    cell.setUpCellContent()
                    return cell
                default:
                    break
            }
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileSettingsTableCell.rawValue, for: indexPath) as! ProfileSettingsTableCell
            cell.cellType = row
            cell.isLastCell = indexPath.row == (sectionRows.count-1)
            cell.setupCellContent()
             return cell
            
        }
        return UITableViewCell()
    }
    
}

extension ProfileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            
            if indexPath.row == 0 {
                
                let basicAccountInfoViewController = self.storyboard?.instantiateViewController(identifier: "BasicAccountInfoViewController") as! BasicAccountInfoViewController
                self.navigationController?.pushViewController(basicAccountInfoViewController, animated: true)
            } else if indexPath.row == 1 {
                
                let privacySettingsViewController = self.storyboard?.instantiateViewController(identifier: "PrivacySettingsViewController") as! PrivacySettingsViewController
                self.navigationController?.pushViewController(privacySettingsViewController, animated: true)
            } else if indexPath.row == 2{
                
                let notificationSettingsViewController = self.storyboard?.instantiateViewController(identifier: "NotificationSettingsViewController") as! NotificationSettingsViewController
                self.navigationController?.pushViewController(notificationSettingsViewController, animated: true)
            }
            
            else{
                let remaindersViewController = self.storyboard?.instantiateViewController(identifier: "RemaindersViewController") as! RemaindersViewController
                               self.navigationController?.pushViewController(remaindersViewController, animated: true)
            }
        }
        if indexPath.section == 2 {
            
            if indexPath.row == 0 {                
                let storyB = UIStoryboard.init(name: "ChangePhoneNumber", bundle: nil)
                let credentialsVC = storyB.instantiateViewController(identifier: "CredentialsViewController") as! CredentialsViewController
                self.navigationController?.pushViewController(credentialsVC, animated: true)
            } else if indexPath.row == 1 {
               let storyB = UIStoryboard.init(name: "Education", bundle: nil)
              let eduVC = storyB.instantiateViewController(identifier: "EducationListVC") as! EducationListVC
              self.navigationController?.pushViewController(eduVC, animated: true)
            } else {
                
              
            }
        }
        if indexPath.section == 3{
            if indexPath.row == 0 {
            let storyB = UIStoryboard.init(name: "NewWorkSpaceBoard", bundle: nil)
            let wsVC = storyB.instantiateViewController(identifier: "WorkSpaceList_Nw") as! WorkSpaceList_Nw
//                let wsVC = storyB.instantiateViewController(identifier: "WorkSpaceListViewController") as! WorkSpaceListViewController
            wsVC.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(wsVC, animated: true)
            }else if indexPath.row == 1 {
                self.movetoColleaugesList()
            }
        }
        
    }
    
    func movetoColleaugesList()  {
        
        let conversationsListViewController = UIStoryboard(name: "Shift", bundle: nil).instantiateViewController(withIdentifier: String(describing:
                   NighShiftColleaugesVC.self)) as! NighShiftColleaugesVC
               conversationsListViewController.delegate = self
        
        self.rcMessagesViewController = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "RCMessagesViewController") as? RCMessagesViewController
       self.rcMessagesViewController?.delegate = self
       self.lgSideMenuController = LGSideMenuController()
       lgSideMenuController.rootViewController = conversationsListViewController
       lgSideMenuController.rightViewController = self.rcMessagesViewController
       lgSideMenuController.rightViewWidth = UIScreen.main.bounds.size.width
       lgSideMenuController.rightViewPresentationStyle = .slideBelow
       self.navigationController?.pushViewController(lgSideMenuController, animated: true)
    }
    
}

extension ProfileViewController: ConversationsListViewControllerDelegate {
    func navigateToChat(chatId: String) {
        
        print("chat id is===\(chatId)")
        self.lgSideMenuController.showRightViewAnimated()
       self.rcMessagesViewController?.recverid = chatId
     }
}
extension ProfileViewController: RCMessagesViewControllerDelegate {
    func navigateToChatList() {
        self.lgSideMenuController.hideRightViewAnimated()
    }
}
