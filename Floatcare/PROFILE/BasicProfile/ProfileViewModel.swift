//
//  ProfileViewModel.swift
//  Floatcare
//
//  Created by OMNIADMIN on 31/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

typealias onFetchingUserInfo = () -> ()

class ProfileViewModel {
    
    var userId: Int?

    init(userId: Int? = Profile.shared.loginResponse?.data?.userBasicInformation?.userID) {
        
        self.userId  = userId
    }
    var fetchUserInfo = FetchUserInfo()
    
    var onFetchUser: onFetchingUserInfo?
    
   @objc func fetchUserBasicInformation() {
    
    guard let userid = self.userId else { return }
    self.fetchUserInfo.getUserInfo(userId: userid) { [weak self] user in
            
            guard let user = user else { return }
            Profile.shared.userBasicInformation = user
            self?.onFetchUser?()
          }
    }
    
    private let sections: [ProfileTabSections] = [.profile, .profileSettings, .professionalDetails, .workspace]
    
    func noOfSections() -> Int {
        
        return sections.count
    }
    
    func getSection(section: Int)-> ProfileTabSections {
        
        return sections[section]
    }
    
    func getSectionTabs(section: ProfileTabSections) -> [ProfileTabs] {
        
        return section.getRows()
    }
    
}
