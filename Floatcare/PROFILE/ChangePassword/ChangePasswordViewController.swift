//
//  ChangePasswordViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 08/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordViewController: SettingsViewController {

    @IBOutlet weak var nextButton: PrimaryButton!
    @IBOutlet weak var bottomBarView: UIView!
    var oldPwd = ""
    var newPwd = ""
    var confirmPwd = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bottomBarView.roundCorners([ .layerMinXMinYCorner], radius: 30.0)
        self.bottomBarView.backgroundColor = .profileBgColor
        
        self.nextButton.buttonTitle = "Save"
        // Do any additional setup after loading the view.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func nextClick(_ sender: Any) {
                
        guard oldPwd == FloatCareUserDefaults().password else{
            ReusableAlertController.showAlert(title: "FloatCare", message: "Incorrect old password")
            return
        }
        guard newPwd == confirmPwd, newPwd.trimmingCharacters(in: .whitespaces).count > 0  else{
            ReusableAlertController.showAlert(title: "FloatCare", message: "New password and confirm password are not matching")
            return
        }
        ProfileUpdateAPI().updatePasswordRESTAPI(newPassword: newPwd) { (msg, isUpdated) in
            FloatCareUserDefaults().password = self.newPwd
            if isUpdated{
              ReusableAlertController.showAlertOk(title: "FloatCare", message: "Password updated successfully") { (Int) in
                              self.navigationController?.popViewController(animated: true)
                              FloatCareUserDefaults().password = self.newPwd
                          }
                      }else{
                          ReusableAlertController.showAlert(title: "FloatCare", message: "Something wentt Wrong")
                      }
        }
//        ProfileUpdateAPI().updatePassword(newPassword: newPwd) { (msg, isUpdated) in
//            if isUpdated{
//                ReusableAlertController.showAlertOk(title: "FloatCare", message: "Password updated successfully") { (Int) in
//                    self.navigationController?.popViewController(animated: true)
//                    FloatCareUserDefaults().password = self.newPwd
//                }
//            }else{
//                ReusableAlertController.showAlert(title: "FloatCare", message: "Something wentt Wrong")
//            }
//        }
    }
    
}

extension ChangePasswordViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: "ChangePasswordTableCell"), for: indexPath) as? ChangePasswordTableCell {
            cell.textField.keyboardType = .emailAddress
            cell.textField.tag = indexPath.row
            cell.textFieldType = ChangePasswordTextFieldType(rawValue: indexPath.row)
          //  cell.testFieldActionDelegate = self
          //  cell.initialSetUpBasedOn(2)
            cell.textField.addTarget(self, action: #selector(readTFValues(tf:)), for: .allEvents)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func readTFValues(tf:UITextField){
        switch tf.tag {
        case ChangePasswordTextFieldType.oldPassowrd.rawValue:
            self.oldPwd = tf.text ?? ""
        case ChangePasswordTextFieldType.newPassword.rawValue:
            self.newPwd = tf.text ?? ""
        case ChangePasswordTextFieldType.confirmPassword.rawValue:
            self.confirmPwd = tf.text ?? ""
        default:
            print("")
        }
    }
}

enum ChangePasswordTextFieldType: Int {
    case oldPassowrd = 0
    case newPassword = 1
    case confirmPassword = 2
    
    func getPlaceHolder() -> String {
        
        switch self {
            case .oldPassowrd:
                return "Current"
            case .newPassword:
                return "New Password"
            case .confirmPassword:
                return "Confirm Password"
        }
    }
    
}
class ChangePasswordTableCell: UITableViewCell {
    
    @IBOutlet weak var textField: SkyFloatingLabelTextFieldWithIcon!
    var visibility: Bool = false {
        didSet{
            
            self.textField.isSecureTextEntry = !visibility
            self.textField.rightView = textFieldBtn
            self.textField.font = UIFont(font: .SFUIText, weight: .medium, size: 14.0)
        }
    }
    
    var textFieldType:ChangePasswordTextFieldType! {
        
        didSet{
            textField.placeholder = textFieldType.getPlaceHolder()
        }
    }
    
    override func awakeFromNib() {
        
        textField.rightView = textFieldBtn
        textField.rightViewMode = .always
        
        textField.selectedTitleColor = #colorLiteral(red: 0.4358979464, green: 0.4750115871, blue: 0.7274814248, alpha: 1)
        
        self.visibility = false
    }
}

extension ChangePasswordTableCell: LoginTextFieldActionable {
    
    @objc func refreshContent() {
        
        self.visibility = !self.visibility
    }
    
    func reloadData() {
        
    }
    
    func setButtonAsRightImage() -> UIButton {
        
        let button = UIButton(type: .custom)
        button.tintColor = self.visibility == true ? #colorLiteral(red: 0.4358979464, green: 0.4750115871, blue: 0.7274814248, alpha: 1) : #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(textField.frame.size.width - 40), y: CGFloat(5), width: CGFloat(40), height: CGFloat(30))
        button.backgroundColor = UIColor.clear
        button.addTarget(self, action: #selector(self.refreshContent), for: .touchUpInside)
        
        let imageName = visibility ? LoginIconName.visible.rawValue : LoginIconName.invisible.rawValue
        button.setImage(UIImage.init(named: imageName), for: .normal)
        
        return button
    }
    
    var textFieldBtn: UIButton {
        return setButtonAsRightImage()
    }
    
    
}
