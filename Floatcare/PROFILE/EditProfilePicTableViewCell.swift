//
//  EditProfilePicTableViewCell.swift
//  Fusion Tech
//
//  Created by OMNIADMIN on 27/05/20.
//  Copyright © 2020 OMNIADMIN. All rights reserved.
//

import UIKit
import CropViewController

class EditProfilePicTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profilePicImageView: UIImageView!
    @IBOutlet weak var captureBtn: UIButton!
    let myPickerController = UIImagePickerController()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.profilePicImageView.backgroundColor = .cellIconBgColor
        self.profilePicImageView.layer.cornerRadius = 15.0
        
        self.captureBtn.backgroundColor = .headingTitleColor
        self.captureBtn.imageView?.image = self.captureBtn.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.captureBtn.tintColor = .white
        // Initialization code
        let imgStr = amazonURL + (Profile.shared.userBasicInformation?.profilePhoto?.fillSpaces ?? "")
        profilePicImageView.sd_setImage(with: URL(string: imgStr), completed: nil)
    }
    
    @IBAction func captureAction(sender: UIButton) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        Utility.topViewController()?.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func camera()
    {
        myPickerController.delegate = self;
        myPickerController.sourceType = .camera
        
        Utility.topViewController()?.present(myPickerController, animated: true, completion: nil)
        
    }
    
    func photoLibrary()
    {
        myPickerController.delegate = self;
        myPickerController.sourceType = .photoLibrary
        Utility.topViewController()?.present(myPickerController, animated: true, completion: nil)
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


extension EditProfilePicTableViewCell: UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate {
    
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            self.presentCropViewController(image: pickedImage)
        }
    }
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.profilePicImageView.image = image
        Progress.shared.showProgressView()
        ProfileUpdateAPI.init().updateProfileImage(img: image) { (msg, issucces) in
            cropViewController.dismiss(animated: true, completion: nil)
            self.myPickerController.dismiss(animated: true, completion: nil)
            Progress.shared.hideProgressView()
            var mesg = ""
            if issucces{
                mesg = "Profile Image Successfully Updated"
            }else{
                mesg = "Profile Image Successfully Updated"
            }
            Utility.topViewController()?.view.makeToast(mesg)
        }
        
    }
    
    func presentCropViewController(image :UIImage) {
      let cropViewController = CropViewController(image: image)
        cropViewController.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.presetSquare
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.aspectRatioLockDimensionSwapEnabled = true
        cropViewController.aspectRatioPickerButtonHidden = true
        cropViewController.delegate = self
        Utility.topViewController()?.present(cropViewController, animated: true, completion: nil)
    }

}
