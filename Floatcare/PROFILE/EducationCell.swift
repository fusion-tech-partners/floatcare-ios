//
//  EducationCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 12/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class EducationCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var activeStatus: UILabel! 
    @IBOutlet weak var expires: UILabel!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        superview?.layoutSubviews()
       
        mainView.layer.cornerRadius = 15
        mainView.layer.masksToBounds = false
        mainView?.layer.shadowColor = UIColor.blue.cgColor
        mainView?.layer.shadowOffset =  CGSize.init(width: 3, height: 5)
        mainView?.layer.shadowOpacity = 0.18
        mainView?.layer.shadowRadius = 10
        
    }

}
