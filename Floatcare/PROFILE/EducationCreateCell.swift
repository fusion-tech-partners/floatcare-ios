//
//  EducationCreateCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 12/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import DropDown
class EducationCreateCell: UITableViewCell {
    
    @IBOutlet weak var accessoryBtnClick: UIButton!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var heading: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
