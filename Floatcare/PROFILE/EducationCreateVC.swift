//
//  EducationCreateVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 12/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import DropDown

class CreateEducationVM {
    static let shared = CreateEducationVM()
    let headings = ["School", "Business Type", "Location", "Degree of Study", "State", "Graduation"]
    let palceHolders = ["e.g. Texas university", "Select Type", "e.g. Houston, Texas", "e.g. Nursing", "e.g. Texas", "Select Month & Year"]
    
    var schoolName = ""
    var businessType = ""
    var location = ""
    var degreeStudy = ""
    var state = ""
    var graduation = ""
    var degreeTypeId = ""
    
}

class EducationCreateVC: UIViewController {
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bottomView: UIView!
    let vmObj = CreateEducationVM.shared
    let dropDown = DropDown()
    var datePicker = UIDatePicker()
    let toolbar = UIToolbar();
    var currentTextField : UITextField?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpNaigationBar()
        mainView.roundCorners(corners: [.bottomLeft], radius: 30)
        bottomView.roundCorners(corners: [.topLeft], radius: 30)
        mainTableView.dataSource = self
        mainTableView.delegate = self
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }
        
    
        
        dropDown.dataSource = ["Nursing", "Doctor", "Assistant","Physiotherapist"]
        dropDown.backgroundColor = .white
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.vmObj.businessType = item
            self.mainTableView.reloadData()
            self.vmObj.degreeTypeId = "\(index + 1)"
          print("Selected item: \(item) at index: \(index)")
        }

        showDatePicker()
        // Will set a custom width instead of the anchor view width
        
    }
    
    fileprivate func setUpNaigationBar() {
       navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
       navigationController?.navigationBar.shadowImage = UIImage()

       let button = UIButton(type:.custom)
       button.setImage(UIImage(named: "Back"), for: .normal)       
       button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
       let barButton = UIBarButtonItem(customView: button)
       self.navigationItem.leftBarButtonItem = barButton
            
        mainView.backgroundColor = .profileBgColor
    }
    @IBAction func addEducationClick(_ sender: Any) {
        createEducationDetails()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func showDatePicker() {
        let calendar = Calendar.current
        var com = DateComponents()
        com.calendar = calendar
        com.year = 0
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = #colorLiteral(red: 0.2823529412, green: 0.3607843137, blue: 0.7294117647, alpha: 1)
        datePicker.maximumDate = calendar.date(byAdding: com, to: Date())
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(dateDoneButton))

        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(dateCancelButton))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

    }
    @objc func dateDoneButton(){
           let formatter = DateFormatter()
           formatter.dateFormat = "MMM dd, yyyy"
           vmObj.graduation = formatter.string(from: datePicker.date)
           mainTableView.reloadData()
       }
       @objc func dateCancelButton(){
           currentTextField!.resignFirstResponder()
       }
    
    func createEducationDetails() {
        guard !(vmObj.schoolName.isEmpty), !(vmObj.businessType.isEmpty), !(vmObj.location.isEmpty), !(vmObj.degreeStudy.isEmpty),!(vmObj.state.isEmpty), !(vmObj.graduation.isEmpty) else {
            ReusableAlertController.showAlert(title: "Float Care", message: "Please fill all details")
            return
        }
        Progress.shared.showProgressView()
        let mutation = CreateEducationMutation.init(userId: Profile.shared.userBasicInformation!.userId, school: vmObj.schoolName, degreeTypeId: vmObj.degreeTypeId, degreeOfStudy: vmObj.degreeStudy, graduationDate: vmObj.graduation.convertToBackendDateFormat, location: vmObj.location, state: vmObj.state)
        ApolloManager.shared.client.perform(mutation: mutation) { (result) in
            print("\(result)")
            Progress.shared.hideProgressView()
           guard let user = try? result.get().data?.resultMap else {
            ReusableAlertController.showAlert(title: "Float Care", message: "Something went wrong please try again")
            return
           }
            ReusableAlertController.showAlertOk(title: "FloatCare", message: "Education added successfully") { (Int) in
                             self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
       

}

extension EducationCreateVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        vmObj.headings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EducationCreateCell", for: indexPath) as! EducationCreateCell
        cell.inputTextField.delegate = self
        cell.inputTextField.tag = indexPath.row
        cell.heading.text = vmObj.headings[indexPath.row]
        cell.inputTextField.placeholder = vmObj.palceHolders[indexPath.row]
        cell.inputTextField.addTarget(self, action: #selector(fetchInputFieldsData(tf:)), for: .allEvents)
        if indexPath.row == 1 {
            cell.accessoryBtnClick.setImage(UIImage(named:"down_arrow")!, for: .normal)
        }
        if indexPath.row == 5{
            cell.accessoryBtnClick.setImage(UIImage(named:"ic_Calendar")!, for: .normal)
            cell.inputTextField.inputView = datePicker
            cell.inputTextField.inputAccessoryView = toolbar
        }else{
            cell.inputTextField.isHidden = false
            cell.inputTextField.inputView = nil
            cell.inputTextField.inputAccessoryView = nil
            cell.inputTextField.reloadInputViews()
        }
        
        if indexPath.row == 1 || indexPath.row == 5{
            cell.accessoryBtnClick.isHidden = false
        }else{
            cell.accessoryBtnClick.isHidden = true
        }
        
        switch indexPath.row {
        case 0:
            cell.inputTextField.text = vmObj.schoolName
        case 1:
            cell.inputTextField.text = vmObj.businessType
        case 2:
        cell.inputTextField.text = vmObj.location
        case 3:
        cell.inputTextField.text = vmObj.degreeStudy
        case 4:
        cell.inputTextField.text = vmObj.state
        case 5:
        cell.inputTextField.text = vmObj.graduation
        default:
            print("Null")
        }
        
        return cell
    }
    
    @objc func fetchInputFieldsData(tf:UITextField){
        currentTextField = tf
        switch tf.tag {
        case 0:
            vmObj.schoolName = tf.text ?? ""
        case 2:
            vmObj.location = tf.text ?? ""
        case 3:
            vmObj.degreeStudy = tf.text ?? ""
        case 4:
            vmObj.state = tf.text ?? ""        
        default:
            print("Null")
        }
    }
}


extension EducationCreateVC : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1{                                    dropDown.show()
            dropDown.anchorView = textField // UIView or UIBarButtonItem
        }
        return true
    }
}

