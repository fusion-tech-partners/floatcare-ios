//
//  EducationListVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 12/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class EducationListVC: UIViewController {
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var mainView: UIView!
    var jsonList : [EduObject]?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpNaigationBar()
        mainView.roundCorners(corners: [.bottomLeft], radius: 30)
        mainTableView.dataSource = self
        mainTableView.delegate = self
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchUserCredentials()
    }
    func fetchUserCredentials() {
        let query = FetchEducationQuery.init(userId: (Profile.shared.userBasicInformation?.userId!)!)
        ApolloManager.shared.client.clearCache()
        ApolloManager.shared.client.fetch(query: query) { (result) in
            guard let educationDetails = try? result.get().data?.resultMap else{return}
            do {
                if (educationDetails["findAllUserEducationByUserId"] as? [Any]) != nil{
                let jsonData = try JSONSerialization.data(withJSONObject: educationDetails, options: .prettyPrinted)
                let decoder = JSONDecoder()
                let eduObj = try decoder.decode(EducationModel.self, from: jsonData)
                self.jsonList = eduObj.findAllUserEducationByUserId!
                    DispatchQueue.main.async {
                        self.mainTableView.reloadData()
                    }
                }
            }catch {
                
            }
            
        }
    }
    
    fileprivate func setUpNaigationBar() {
       navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
       navigationController?.navigationBar.shadowImage = UIImage()

       let button = UIButton(type:.custom)
       button.setImage(UIImage(named: "Back"), for: .normal)
       button.addTarget(self, action:#selector(backTapped), for: .touchUpInside)
       button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
       let barButton = UIBarButtonItem(customView: button)
       self.navigationItem.leftBarButtonItem = barButton
            
        mainView.backgroundColor = .profileBgColor
    }
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func addcredentialsClick(_ sender: Any) {
       let vc =  self.storyboard?.instantiateViewController(identifier: "EducationCreateVC") as! EducationCreateVC
       self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EducationListVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jsonList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EducationCell", for: indexPath) as! EducationCell
        let obj = jsonList?[indexPath.row]
        cell.type.text = "GRADUATED \(obj?.graduationDate?.covertDateToReadableFormat ?? "")"
        cell.title.text = "\(obj?.school ?? "")"
        cell.expires.text = "College/University"
        cell.activeStatus.text = "Degree: \(obj?.degreeOfStudy ?? "")"
        return cell
    }
    
    
}
