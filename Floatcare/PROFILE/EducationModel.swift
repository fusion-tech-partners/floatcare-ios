//
//  EducationObject.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 12/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

struct EducationModel: Codable {
    var findAllUserEducationByUserId : [EduObject]?
}

struct EduObject: Codable {
    var school: String?
    var degreeTypeId: String?
    var degreeOfStudy: String?
    var graduationDate: String?
    var location: String?
    var state: String?
}
