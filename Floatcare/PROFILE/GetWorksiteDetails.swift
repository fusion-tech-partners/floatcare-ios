//
//  GetWorksiteDetails.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 30/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import Apollo


class GetWorkSiteDetails {
    
    func checkForWorkSpaceId(userId: String = "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", completion: @escaping (String?) -> ()) {
        ApolloManager.shared.client.fetch(query:GetWorkspaceByUserIdQuery(userId: userId), cachePolicy: .fetchIgnoringCacheData, context: nil,queue: .main ) { result in
                        
                        guard let user = try? result.get().data?.resultMap else {
                            completion(nil)
                            return
                        }
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
                            let decoder = JSONDecoder()
                            let getWorkspaceDetailsByUserId = try decoder.decode(GetWorkspaceDetailsByUserId.self, from: jsonData)
                            Profile.shared.userWorkSpaceDetails = getWorkspaceDetailsByUserId
                            print(getWorkspaceDetailsByUserId)
                            completion(convertedString)

                        }catch {
                            print(error.localizedDescription)
                             completion(nil)
                        }
        }
    }
}
struct GetWorkspaceDetailsByUserId: Codable {
    
    var getWorkspaceByUserId: [WorkSiteDetails]?
    
}

struct WorkSiteDetails : Codable {
    var userWorkspaceId : String?
    var userId : String?
    var workspaceId : String?
    var workspaceType : String?
    var workspaceName : String?
    var fteStatus : String?
    var organizationId : String?
    var invitationCode : String?
    var joinedDate : String?
    var organizationName : String?
    var businessId : String?
    var staffUserCoreTypeId : String?
    var staffUserCoreTypeName : String?
    var departmentShiftId : String?
    var worksites : [WorkSite]?
}

struct WorkSite : Codable {
    var businessId : String?
    var name : String?
    var address : String?
    var postalCode : String?
    var state : String?
    var city : String?
}

