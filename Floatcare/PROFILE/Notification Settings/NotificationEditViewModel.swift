//
//  NotificationEditViewModel.swift
//  Floatcare
//
//  Created by OMNIADMIN on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

class NotificationEditViewModel {
    
    var userNotification: UserNotification!
    
    var updateUserNotifications: UpdateUserNotifications = UpdateUserNotifications()
    init(userNotification: UserNotification){
        
        self.userNotification = userNotification
    }
    
    func saveNotification(completion:(()->())?) {
        
        self.updateUserNotifications.updateUserNotifications(userNotification: self.userNotification) {
            
            print("updated")
            completion?()
        }
    }
    
    func updateNotification(type: Int, value: Bool) {
        
        switch type {
            case 0:
                self.userNotification.isEnablePush = value
            case 1:
                self.userNotification.isEnableSMS = value
            case 2:
                self.userNotification.isEnableEmail = value
            default:
                break
        }
        
        
    }
}
