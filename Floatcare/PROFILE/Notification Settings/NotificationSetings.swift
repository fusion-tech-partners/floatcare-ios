//
//  NotificationSetings.swift
//  Floatcare
//
//  Created by OMNIADMIN on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import UIKit

enum NotificationTypes: Int {
    
    case mentions = 1
    case updatesFromColleague = 2
    case reminders = 3
    case birthdays = 4
    case otherNotifications = 5
    case events = 6
     
}

struct findUserNotificationByUserId: Codable {
    
    let findUserNotificationByUserId:[UserNotification]!
}

struct AppUserNotifications: Codable {
    
    let getNotificationsByUserId:[AppNotification]!
}

struct findOrganizationById: Codable {
    
    let findOrganizationById: [OragnizationDetails]
}
struct OragnizationDetails: Codable {
    
    let name:String
    let address: String
    let logo: String
    let businesses: [Businesses]
    let staffMembers: [UserBasicInformation]
}

struct Businesses : Codable {
    
    let description: String
}

struct AppNotification : Codable {
    
    let notificationId: String?
    let notificationTypeId: String?
    let notificationType: String?
    let userId: String?
    let content: String?
     let isRead: Bool?
    let onDate: String?
    
    func notificationTypeName() -> String {
        
        switch Int(self.notificationTypeId ?? "0") {
            case 1:
                return "Mentions"
            case 2:
                return "Updates From Colleague"
            case 3:
                return "Reminders"
            case 4:
                return "Birthdays"
            case 5:
                return "Other Notifications"
            case 6:
                return "Events"
            default:
                return ""
            
        }
    }
}
struct UserNotification : Codable {
    
  let userNotificationId: String!
    //let notificationTypeName: String!
  let notificationTypeId: String!
  var isEnablePush: Bool!
  var isEnableSMS: Bool!
  var isEnableEmail: Bool!
    
    func notificationTypeName() -> String {
        
        switch Int(self.notificationTypeId){
            case 1:
                return "Mentions"
            case 2:
                return "Updates From Colleague"
            case 3:
                return "Reminders"
            case 4:
                return "Birthdays"
            case 5:
                return "Other Notifications"
            case 6:
                return "Events"
            default:
                return ""
            
        }
    }
    
    func enrolledNotifications() -> String {
        
        var enrolledNotifications:[String] = [String]()
        
        if isEnablePush {
            
            enrolledNotifications.append("Push")
        }
        if isEnableEmail{
            
            enrolledNotifications.append("Email")
        }
        if isEnableSMS{
            
            enrolledNotifications.append("SMS")
        }
        
        return enrolledNotifications.joined(separator: ", ")
    }
    
    func getIconImage() -> UIImage? {
        
        var iconImage = ""
        switch Int(self.notificationTypeId) {
            case 1:
                iconImage = "mentions"
            case 2:
                iconImage = "collegues"
            case 3:
                iconImage = "remainder"
            case 4:
                iconImage = "Birthday-1"
            case 5:
                iconImage = "ic_Notification"
            case 6:
                iconImage = "ic_Calendar"
            default:
                iconImage = "ic_Notification"
            
        }
        return UIImage(named: iconImage)
    }
}
