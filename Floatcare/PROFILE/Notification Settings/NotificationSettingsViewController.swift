//
//  NotificationSettingsViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class NotificationSettingsViewController: SettingsViewController {
    
    let viewModel = NotificationSettingsViewModel()
    
    override func viewWillAppear(_ animated: Bool) {
        
         Progress.shared.showProgressView()
         self.viewModel.downloadNotifications { [weak self] in
            
            DispatchQueue.main.async {
                
                 Progress.shared.hideProgressView()
                 self?.settingsTableView.reloadData()
            }
        }
    }
    
    override func setupView() {
        
        super.setupView()
    }
}

class SettingsViewController: UIViewController {
    
    
    
    @IBOutlet weak var settingsTableView: UITableView!
    
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var headingTitleView: UIView!
    @IBOutlet weak var headingTitleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        
     }
    
     func setupView() {
        
       
        
        self.navigationBar.isRightButtonHidden = true
        
        self.headingTitleView.roundCorners(.layerMinXMaxYCorner, radius: 30.0)
        self.headingTitleView.backgroundColor = .profileBgColor
        self.headingTitleLabel.font = UIFont(font: .Athelas, weight: .regular, size: 32)
        self.headingTitleLabel.textColor = .headingTitleColor
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}



extension NotificationSettingsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard section != 0 else { return 1}
        return self.viewModel.userNotifications.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        guard section != 0 else { return nil}
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileInfoTableHeaderCell.rawValue) as! ProfileInfoTableHeaderCell
         cell.titleLabel.text = "What notifications you will receieve"
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch  indexPath.section {
            case 0:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.MainNotificationSettingTableCell.rawValue, for: indexPath) as! MainNotificationSettingTableCell
               // cell.notificationSwitch.isOn = 
                cell.isLastCell = true
                return cell
            default :
                
                let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.NotificationSettingTableCell.rawValue, for: indexPath) as! NotificationSettingTableCell
                    cell.userNotification = self.viewModel.userNotifications[indexPath.row]
                print("notification is.............", cell.userNotification ?? "")
                  return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        guard section != 0 else { return 0.0}
        return 66.0
    }
    
    
}


extension NotificationSettingsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard indexPath.section != 0 else { return }
        
        let notificationsEditViewController = self.storyboard?.instantiateViewController(identifier: "NotificationsEditViewController") as! NotificationsEditViewController
        notificationsEditViewController.viewModel = NotificationEditViewModel(userNotification: self.viewModel.userNotifications[indexPath.row])
        self.navigationController?.pushViewController(notificationsEditViewController, animated: true)
    }
}
