
//
//  NotificationSettingsViewModel.swift
//  Floatcare
//
//  Created by OMNIADMIN on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

typealias actionCompletionBlock = () -> ()

class NotificationSettingsViewModel {
    
    var onDownloadingNotificaitons: actionCompletionBlock?
    
    let fetchUserNotifications = FetchUserNotifications()
    var userNotifications: [UserNotification] = [UserNotification]()
    
    var appNotifications: [AppNotification] = [AppNotification]()
    
    func downloadNotifications(onDownloadingNotificaitons: actionCompletionBlock? = nil) {
        
         fetchUserNotifications.getUserNotifications { [weak self] notifications in
            
            guard let tNotifications = notifications?.findUserNotificationByUserId else { return}
            self?.userNotifications = tNotifications
            onDownloadingNotificaitons?()
          }
    }
    
    
    func downloadAppNotifications(onDownloadingNotificaitons: actionCompletionBlock? = nil) {
           
            fetchUserNotifications.getAppNotifications { [weak self] notifications in
               
               guard let tNotifications = notifications?.getNotificationsByUserId else { return}
               self?.appNotifications = tNotifications
               onDownloadingNotificaitons?()
             }
       }
    
    
}
