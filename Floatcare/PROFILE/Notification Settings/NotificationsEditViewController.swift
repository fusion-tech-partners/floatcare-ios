//
//  NotificationsEditViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class NotificationsEditViewController: UIViewController {

    @IBOutlet weak var settingsTableView: UITableView!
    
    var viewModel: NotificationEditViewModel!
    
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var headingTitleView: UIView!
    @IBOutlet weak var headingTitleLabel: UILabel!
    
    @IBOutlet weak var nextButton: PrimaryButton!
    @IBOutlet weak var bottomBarView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        
     }
    
    private func setupView() {
        
        self.headingTitleLabel.text = self.viewModel.userNotification.notificationTypeName()
        self.navigationBar.isRightButtonHidden = true
        
        self.headingTitleView.roundCorners(.layerMinXMaxYCorner, radius: 30.0)
        self.headingTitleView.backgroundColor = .profileBgColor
        self.headingTitleLabel.font = UIFont(font: .Athelas, weight: .regular, size: 32)
        self.headingTitleLabel.textColor = .headingTitleColor
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            
            self.navigationController?.popViewController(animated: true)
        }
        
        self.bottomBarView.roundCorners([ .layerMinXMinYCorner], radius: 30.0)
        self.bottomBarView.backgroundColor = .profileBgColor
    
        self.nextButton.buttonTitle = "Save"
    }
    
    @IBAction func save() {
        
        Progress.shared.showProgressView()
        self.viewModel.saveNotification {
            
            Progress.shared.hideProgressView()
        }
    }
    
    
}


extension NotificationsEditViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.MainNotificationSettingTableCell.rawValue, for: indexPath) as! MainNotificationSettingTableCell
        
        cell.notificationSwitch.tag = indexPath.row
        switch indexPath.row {
            case 0:
                cell.profileSettingNameLabel.text = "Push Notifications"
                cell.profileSettingDescriptionLabel.text = "Cell Popups"
                cell.notificationSwitch.isOn = self.viewModel.userNotification.isEnablePush
                
            case 1:
                cell.profileSettingNameLabel.text = "Text Messages"
                cell.profileSettingDescriptionLabel.text = "People you work with"
                cell.notificationSwitch.isOn = self.viewModel.userNotification.isEnableSMS
            case 2:
                cell.profileSettingNameLabel.text = "Your email"
                cell.profileSettingDescriptionLabel.text = "people you know"
                cell.notificationSwitch.isOn = self.viewModel.userNotification.isEnableEmail
            default:
                break
        }
        
        cell.onValueChange = {[weak self] notificationSwitch in
            
            self?.viewModel.updateNotification(type: notificationSwitch.tag, value: notificationSwitch.isOn)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
}
