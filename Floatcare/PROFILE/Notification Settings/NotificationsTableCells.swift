//
//  NotificationsTableCells.swift
//  Floatcare
//
//  Created by OMNIADMIN on 02/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import UIKit

typealias OnValueBlock = (UISwitch) -> ()

class MainNotificationSettingTableCell: ProfileSettingsTableCell {
    
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    var onValueChange: OnValueBlock?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.iconImageView.image = UIImage(named: "education")
        self.notificationSwitch.onTintColor = .primaryColor
        self.isAccessoryButtonHidden = true
        self.profileSettingNameLabel.text = "Mute all Notifications"
        self.profileSettingDescriptionLabel.text = "Stop all of them"
        self.iconImageView.image = UIImage(named: "mute")
        
        self.notificationSwitch.addTarget(self, action: #selector(onValueChanged), for: .valueChanged)
    }
    
    @objc func onValueChanged() {
        
        self.onValueChange?(self.notificationSwitch)
    }
}

class NotificationSettingTableCell: ProfileSettingsTableCell {
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.iconImageView.image = UIImage(named: "education")
        self.isLastCell = false
         self.isAccessoryButtonHidden = false
    }
    
    var userNotification: UserNotification? {
        
        didSet {
            
            self.profileSettingNameLabel.text = self.userNotification?.notificationTypeName()
            self.profileSettingDescriptionLabel.text = self.userNotification?.enrolledNotifications()
            self.iconImageView.image = self.userNotification?.getIconImage()
        }
    }
    
}
