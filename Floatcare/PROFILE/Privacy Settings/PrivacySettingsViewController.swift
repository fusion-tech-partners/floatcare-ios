//
//  PrivacySettingsViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 07/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class PrivacySettingsViewController: SettingsViewController {

    let viewModel = PrivacySettingsViewModel()
    
    @IBOutlet weak var nextButton: PrimaryButton!
    @IBOutlet weak var bottomBarView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func setupView() {
        
        super.setupView()
        
        self.bottomBarView.roundCorners([ .layerMinXMinYCorner], radius: 30.0)
        self.bottomBarView.backgroundColor = .profileBgColor
        
        self.nextButton.buttonTitle = "Save"
        
        self.viewModel.fetchPrivacySettings { (findUserPrivacySettingsByUserId) in
            DispatchQueue.main.async {
                self.settingsTableView.reloadData()
            }
         }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func save() {
        
        self.viewModel.updateUserPrivacySettings()
     }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension PrivacySettingsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return section == 0 ? 3 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.MainNotificationSettingTableCell.rawValue, for: indexPath) as! MainNotificationSettingTableCell
            
            cell.notificationSwitch.tag = indexPath.row
            switch indexPath.row {
                case 0:
                    cell.profileSettingNameLabel.text = "Public"
                    cell.profileSettingDescriptionLabel.text = "Visible to everyone"
                    cell.notificationSwitch.isOn = self.viewModel.userPrivacySettings?.isVisibleToPublic == true
                    cell.isLastCell = false
                    
                case 1:
                    cell.profileSettingNameLabel.text = "Workspace & Colleagues"
                    cell.profileSettingDescriptionLabel.text = "People you work with"
                    cell.notificationSwitch.isOn = self.viewModel.userPrivacySettings?.isVisibleToCollegues == true
                    cell.isLastCell = false
                
                case 2:
                    cell.profileSettingNameLabel.text = "Connected Professionals"
                    cell.profileSettingDescriptionLabel.text = "people you know"
                    cell.notificationSwitch.isOn = self.viewModel.userPrivacySettings?.isVisibleToWorkspace == true
                    cell.isLastCell = true
                
                default:
                    break
            }
            
            cell.onValueChange = {[weak self] notificationSwitch in
                
                switch notificationSwitch.tag {
                    case 0:
                        self?.viewModel.userPrivacySettings?.isVisibleToPublic = notificationSwitch.isOn
                    case 1:
                        self?.viewModel.userPrivacySettings?.isVisibleToCollegues = notificationSwitch.isOn
                    case 2:
                        self?.viewModel.userPrivacySettings?.isVisibleToWorkspace = notificationSwitch.isOn
                    default:
                    break
                }
            }
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.NotificationSettingTableCell.rawValue, for: indexPath) as! NotificationSettingTableCell
            cell.profileSettingNameLabel.text = "Password"
            cell.profileSettingDescriptionLabel.text = "Manage your password"
            return cell
        }
        
    }
    
    
}


extension PrivacySettingsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            
            let changePasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            self.navigationController?.pushViewController(changePasswordViewController, animated: true)
        }
    }
}


