//
//  PrivacySettingsViewModel.swift
//  Floatcare
//
//  Created by OMNIADMIN on 07/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

 typealias Completion = (UserPrivacySettings?) -> ()

class PrivacySettingsViewModel {
    
    var userPrivacySettings : UserPrivacySettings?
    func fetchPrivacySettings(completion: @escaping Completion) {
        
        ApolloManager.shared.client.fetch(query: FindUserPrivacySettingsByUserIdQuery(userId: "\((Profile.shared.loginResponse?.data?.userBasicInformation?.userID)!)"),
        cachePolicy: .fetchIgnoringCacheData,
        context: nil,
        queue: .main) {[weak self] result in
            
            print(result)
            guard let user = try? result.get().data?.resultMap else {
                self!.userPrivacySettings = UserPrivacySettings()
                completion(UserPrivacySettings())
                return
            }
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                
                let convertedString = String(data: jsonData, encoding: String.Encoding.utf8) // the data will be converted to the string
                print(convertedString)
                
                let decoder = JSONDecoder()
                let userPrivacySettingsByUserId = try! decoder.decode(findUserPrivacySettingsByUserId.self, from: jsonData)
                
                self?.userPrivacySettings = userPrivacySettingsByUserId.findUserPrivacySettingsByUserId.first
                completion(userPrivacySettingsByUserId.findUserPrivacySettingsByUserId.first)
            }catch {
                
                completion(UserPrivacySettings())
            }
        }
        
    }
    
    func updateUserPrivacySettings() {
        let mutation = CreateUserPrivacySettingsMutation(userId: Profile.shared.userId, isVisibleToPublic: self.userPrivacySettings?.isVisibleToPublic, isVisibleToWorkspace: self.userPrivacySettings?.isVisibleToWorkspace, isVisibleToCollegues: self.userPrivacySettings?.isVisibleToCollegues)
        ApolloManager.shared.client.perform(mutation: mutation) { (result) in
            print("\(result)")

            guard let user = try? result.get().data?.resultMap else {
                return
            }
        }
        
         ReusableAlertController.showAlert(title: "Privacy Settings", message: "Updated Successfully")
    }
}
