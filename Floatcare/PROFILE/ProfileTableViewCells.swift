//
//  ProfileTableViewCell.swift
//  Fusion Tech
//
//  Created by OMNIADMIN on 23/05/20.
//  Copyright © 2020 OMNIADMIN. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImageLbl: UILabel!
    
    @IBOutlet weak var profilePicImageView: UIImageView!
    @IBOutlet weak var profileInfoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profileImageLbl.isHidden = true
        
        self.contentView.backgroundColor = .profileBgColor
        
        self.profilePicImageView.contentMode = .scaleToFill
        self.profilePicImageView.layer.cornerRadius = 15.0
        self.profilePicImageView.backgroundColor = .cellIconBgColor
        
        self.profileImageLbl.contentMode = .scaleToFill
        self.profileImageLbl.layer.cornerRadius = 15.0
        self.profileImageLbl.backgroundColor = .cellIconBgColor
         // Initialization code
    }
    
    func setUpCellContent() {
        let imgStr = amazonURL + (Profile.shared.userBasicInformation?.profilePhoto?.fillSpaces ?? "")
        self.profilePicImageView.sd_setImage(with: URL(string: imgStr), completed: nil)
        if(self.profilePicImageView == nil){
            profileImageLbl.isHidden = false
            profileImageLbl.backgroundColor = UIColor.white
            let firstLetter = Profile.shared.userBasicInformation?.firstName.prefix(1)
            let secondLetter = Profile.shared.userBasicInformation?.firstName.prefix(1)
            profileImageLbl.text = "\(firstLetter)\(secondLetter)"
        }
        self.profileInfoLabel.attributedText = Profile.shared.userBasicInformation?.getDisplayedTextForProfile()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class ProfileInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var profileInfoHeadingLabel: UILabel!
    @IBOutlet weak var profileInfoDescriptionLabel: UILabel!

    @IBOutlet weak var contentBgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentBgView.backgroundColor = .profileBgColor
        
        self.profileInfoHeadingLabel.textColor = .appLightGray
        self.profileInfoDescriptionLabel.textColor = .appDarkGray
        
        self.profileInfoHeadingLabel.font = UIFont(font: .SFUIText, weight: .semibold, size: 14.0)
        self.profileInfoDescriptionLabel.font = UIFont(font: .SFUIText, weight: .light, size: 15.0)
     }
    
    func setUpCellContent() {
        
        self.profileInfoHeadingLabel.text = "My Bio"
        self.profileInfoDescriptionLabel.text = Profile.shared.userBasicInformation?.bioInformation
        self.profileInfoDescriptionLabel.sizeToFit()
        
        self.contentBgView.roundCorners(.layerMinXMaxYCorner, radius: 30.0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}


class ProfileInfoTableHeaderCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.titleLabel.textColor = .descriptionColor
        self.titleLabel.font = UIFont(font: .SFUIText, weight: .bold, size: 14.0)
    }
}


class ProfileEditTableHeaderCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.contentView.backgroundColor = .white
        
        self.titleLabel.textColor = .descriptionColor
        self.titleLabel.font = UIFont(font: .SFUIText, weight: .medium, size: 14.0)
        
        self.editButton.titleLabel?.font = UIFont(font: .SFUIText, weight: .medium, size: 14.0)
        self.editButton.setTitle("Edit >", for: .normal)
        self.editButton.setTitleColor(.profileArrowColor, for: .normal)
    }
}

class ProfileSettingsTableCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var profileSettingNameLabel: UILabel!
    @IBOutlet weak var profileSettingDescriptionLabel: UILabel!
    
    @IBOutlet weak var lineSeperator: UIImageView!
    
    @IBOutlet weak var lastCellLineSeperator: UIImageView?
    @IBOutlet weak var lastCellLineHeight: NSLayoutConstraint?
    
    
    var isLastCell: Bool = false {
        
        didSet {
            
            self.lastCellLineHeight?.constant = isLastCell ? 4.0 : 0.0
        }
    }
    var isAccessoryButtonHidden: Bool = false {
        
        didSet {
            
            self.accessoryView = self.isAccessoryButtonHidden ? nil : UIImageView(image: UIImage(named: "accessory"))
        }
    }
    
    var cellType: ProfileTabs?
    
    var accountInfo: BasicAccountInfoTabs?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.iconImageView.layer.cornerRadius = 10.0
        self.iconImageView.contentMode = UIView.ContentMode.center
        self.iconImageView.backgroundColor = .cellIconBgColor
        
        self.profileSettingNameLabel.textColor = .primaryColor
        self.profileSettingDescriptionLabel.textColor = .descriptionColor
        
        self.profileSettingNameLabel.font = UIFont(font: .SFUIText, weight: .semibold, size: 16.0)
        self.profileSettingDescriptionLabel.font = UIFont(font: .SFUIText, weight: .medium, size: 13.0)
        
        self.lineSeperator.backgroundColor = .footerBgColor
        self.lastCellLineSeperator?.backgroundColor = .footerBgColor
    }
    
    func setupCellContent() {
        
        self.iconImageView.image = self.cellType?.getImage()
        self.profileSettingNameLabel.text = self.cellType?.getHeaderText()
        self.profileSettingDescriptionLabel.text = self.cellType?.getDescription()
    }
    
    func setupAccountInfoCellContent() {
    
        self.iconImageView.image = self.accountInfo?.getImage()
        self.profileSettingNameLabel.text = self.accountInfo?.getValue()
        self.profileSettingDescriptionLabel.text = self.accountInfo?.rawValue
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}

