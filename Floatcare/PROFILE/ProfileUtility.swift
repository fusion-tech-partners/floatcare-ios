//
//  ProfileUtility.swift
//  Fusion Tech
//
//  Created by OMNIADMIN on 23/05/20.
//  Copyright © 2020 OMNIADMIN. All rights reserved.
//

import Foundation
import UIKit

enum BasicAccountInfoTabSections: String {
    
    case profilePic = "Profile Picture"
    case profileInfo = "Profile Information"
    case contactInfo = "Contact Information"
    
    func tabs()-> [BasicAccountInfoTabs]{
        
        switch self {
            case .profilePic: return [.profilePic]
            case .profileInfo: return [.fullName, .preferedName, .location, .gender, .birthday, .bio]
            case .contactInfo: return [.primaryPhoneNumber, .secondayPhoneNumber, .primaryEmail, .secondaryEmail]
        }
    }
}
enum BasicAccountInfoTabs: String {
    
    case profilePic = "Profile Pic"
    case fullName = "Full Name"
    case preferedName = "Prefered Name"
    case location = "Location"
    case gender = "Gender"
    case birthday = "Birthday"
    case bio = "Bio"
    case primaryPhoneNumber = "Primary phone number"
    case secondayPhoneNumber = "Secondary phone number"
    case primaryEmail = "Primary email"
    case secondaryEmail = "Secondary email"
    
    func getValue() -> String {
        
        guard let profile = Profile.shared.userBasicInformation else { return ""}
        
        switch self {
        case .fullName: return (Profile.shared.userBasicInformation?.firstName ?? "") + " " + (Profile.shared.userBasicInformation?.lastName ?? "")
        case .preferedName: return (Profile.shared.userBasicInformation?.preferredName ?? "")
            case .location: return profile.address1 ?? ""
        case .gender: return (Profile.shared.userBasicInformation?.gender ?? "")
        case .birthday: return (Profile.shared.userBasicInformation?.dateOfBirth?.covertDateToReadableFormat ?? "")
        case .bio: return (Profile.shared.userBasicInformation?.bioInformation ?? "")
        case .primaryPhoneNumber: return (Profile.shared.userBasicInformation?.phone ?? "")
        case .secondayPhoneNumber: return (Profile.shared.userBasicInformation?.secondaryPhoneNumber ?? "")
        case .primaryEmail: return (Profile.shared.userBasicInformation?.email ?? "")
        case .secondaryEmail: return (Profile.shared.userBasicInformation?.secondaryEmail ?? "")
            case.profilePic: return ""
        }
    }
    
    func getImage() -> UIImage? {
        
        var imageName: String = ""
        switch self {
            case .fullName:
                imageName = "Account-1"
            case .preferedName:
                imageName = "Preferred name"
            case .location:
                imageName = "State"
            case .gender:
                imageName = "account"
            case .birthday:
                imageName = "Birthday-1"
            case .bio:
                imageName = "Bio"
            case .primaryPhoneNumber:
                imageName = "Phone"
            case .secondayPhoneNumber:
                imageName = "Phone"
            case .primaryEmail:
                imageName = "Email-1"
            case .secondaryEmail:
                imageName = "Email-1"
            case .profilePic:
                imageName = "myWork"
             
        }
        
        return UIImage(named: imageName)
    }
}

enum ProfileTabs: Int {
    
    case profileName
    case profileDescription
    case basicInfo
    case privacy
    case notificationSettings
    case remainders
    case credientials
    case education
    case workExperience
    case workspaces
    case collegues
    case communities
    
    func getImage() -> UIImage? {
        
        var imageName: String = ""
        switch self {
            case .basicInfo:
                imageName = "account"
            case .privacy:
                imageName = "spam"
            case .notificationSettings:
                imageName = "Notifications"
            case .remainders:
                imageName = "ic_Calendar"
            case .credientials:
                imageName = "credientials"
            case .education:
                imageName = "education"
            case .workExperience:
                imageName = "workExperience"
            case .workspaces:
                imageName = "myWork"
            case .collegues:
                imageName = "myCollegues"
            case .communities:
                imageName = "myWork"
            default:
                imageName = ""
            
        }
        
        return UIImage(named: imageName)
    }
    
    func getHeaderText() -> String {
        
        switch self {
            case .basicInfo:
                return "Basic Account Info"
            case .privacy:
                return "Privacy"
            case .notificationSettings:
                return "Notification Settings"
            case .remainders:
            return "Reminders"
            case .credientials:
                return "Credientails"
            case .education:
                return "Education"
            case .workExperience:
                return "Work Experience"
            case .workspaces:
                return "My Workspaces"
            case .collegues:
                return "My Collegues"
            case .communities:
                return "My Communities"
            default:
            return ""
            
        }
    }
    
    func getDescription() -> String {
        
        switch self {
            case .basicInfo:
                return "Update and modify your profile"
            case .privacy:
                return "Change email, password or number"
            case .notificationSettings:
                return "Update and modify notification settings"
            case.remainders:
                return "Configure your reminders"
            case .credientials:
                return "Update and modify your credientials"
            case .education:
                return "Add your education details here"
            case .workExperience:
                return "Add your work experience here"
            case .workspaces:
                return "Manage your workspace settings"
            case .collegues:
                return "Manage and Andd Collegues"
            case .communities:
                return "Description"
            default:
            return ""
            
        }
    }
}

enum ProfileTabSections: String {
    
    case profile
    case profileSettings
    case professionalDetails
    case workspace
    
    func getDisplayString()-> String {
        
        switch self {
            case .profile: return ""
            case .profileSettings: return "Profile Settings"
            case .professionalDetails: return "Professional Details & Information"
            case .workspace: return "My Workspaces, Collegues & Community"
        }
    }
    func getRows() -> [ProfileTabs] {
        
        switch self {
            case .profile: return [.profileName, .profileDescription]
        case .profileSettings: return [.basicInfo, .privacy, .notificationSettings, .remainders]
            case .professionalDetails: return [.credientials, .education, .workExperience]
            case .workspace: return [.workspaces, .collegues, .communities]
        }
    }
}




extension ProfileTabs {
    
    func title() -> String {
        
        return "Basic Account Info"
    }
    
    func subTitle() -> String {
        
        return "Basic Account Info"
    }
    
    func image() -> UIImage? {
         return nil
    }
}


enum ProfileCellIdentifiers: String{
    
    case ProfileTableViewCell = "ProfileTableViewCell"
    case ProfileInfoTableViewCell = "ProfileInfoTableViewCell"
    case ProfileSettingsTableCell = "ProfileSettingsTableCell"
    case ProfileInfoTableHeaderCell = "ProfileInfoTableHeaderCell"
    case EditProfilePicTableViewCell = "EditProfilePicTableViewCell"
    case ProfileEditTableHeaderCell = "ProfileEditTableHeaderCell"
    case NotificationSettingTableCell = "NotificationSettingTableCell"
    case MainNotificationSettingTableCell = "MainNotificationSettingTableCell"
    case NightColleaugeCell = "NightColleaugeCell"
}

typealias NavigateToChatOnUserSelection = (Int)->()
class Utility {
    
    static let shared = Utility()
    
    var navigateToChatOnUserSelection :NavigateToChatOnUserSelection?
    
    static func topViewController() -> UIViewController? {
        
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
    
    static func getDateInFormat(inputDate: String, inputFormat: String,outputFormat: String) -> String {
        
        let df = DateFormatter()
        df.dateFormat = inputFormat
        guard let date = df.date(from: inputDate) else {return ""}
        df.dateFormat = outputFormat
        return df.string(from: date)
    }
}


extension Date {
    
    func getYear() -> String{
        
        let df = DateFormatter()
        df.dateFormat = "YYYY"
        return df.string(from: self)
    }
}


class Profile{
    
    static let shared = Profile()
    
    var userId = {
        
        return FloatCareUserDefaults.shared.userId ?? "0"

    }()
    
    
    var worksiteId = {
       
        return FloatCareUserDefaults.shared.worksiteId ?? "0"

    }()
    
    func organizationId() -> String {
        
         return FloatCareUserDefaults.shared.orgId ?? "0"
        
//        if let workspace = Profile.shared.userWorkSpaceDetails?.getWorkspaceByUserId?.first {
//
//            return workspace.organizationId ?? "0"
//        }
//        return "0"

    }
    var userWorkSpaceDetails: GetWorkspaceDetailsByUserId? = nil
   var userWorksiteSettings: WSSettingModel? = nil
    var userSwapDetails : WSSwapModel? = nil
    var userBasicInformation: UserBasicInformation? = nil
    var loginResponse : LoginResponse? = nil {
        
        didSet {
            
            FloatCareUserDefaults.shared.userId = "\(String(describing: loginResponse?.data?.userBasicInformation?.userID ?? 0))"
            FloatCareUserDefaults.shared.orgId = "\(String(describing: loginResponse?.data?.userBasicInformation?.organizationID ?? 0))"
            
        }
    }
}


