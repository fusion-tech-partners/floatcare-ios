//
//  EventRemaindersViewController.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/23/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

class EventRemaindersViewController: UIViewController{
    
    @IBOutlet weak var navigationBar: NavigationBar!
    
    @IBOutlet weak var headingTitleLbl: UILabel!
    @IBOutlet weak var headingTitleView: UIView!
    @IBOutlet weak var eventTableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var saveBtn: ButtonViolet!
     override func viewDidLoad() {
               super.viewDidLoad()
               
               self.setupView()
               // Do any additional setup after loading the view.
           }
        private func setupView() {
          
            self.navigationBar.isRightButtonHidden = true
            
            self.headingTitleView.roundCorners(.layerMinXMaxYCorner, radius: 30.0)
            self.headingTitleView.backgroundColor = .profileBgColor
            self.headingTitleLbl.font = UIFont(font: .Athelas, weight: .regular, size: 32)
            self.headingTitleLbl.textColor = .headingTitleColor
            
            self.navigationBar.LeftButtonActionBlock = { sender in
                
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    extension EventRemaindersViewController: UITableViewDataSource{
        
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          //  extension PrivacySettingsViewController: UITableViewDataSource {
                
         return 5
        }
            
        func numberOfSections(in tableView: UITableView) -> Int {
                    
                    return 1
                }
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //
    //                return section == 0 ? 3 : 1
    //            }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "eventRemainderCell", for: indexPath) as! EventRemainderTableViewCell
            return cell
            }else{
        let cellSec = tableView.dequeueReusableCell(withIdentifier: "eventRemainderSecond", for: indexPath) as! eventRemainderSecondaryCell
                if indexPath.row == 1 {
                    cellSec.titleLabel.text = "Minutes Before"
                }
                if indexPath.row == 2 {
                    cellSec.titleLabel.text = "Hours"
                }
                if indexPath.row == 3 {
                    cellSec.titleLabel.text = "Days"
                }
                if indexPath.row == 4 {
                    cellSec.titleLabel.text = "Weeks"
                }
        return cellSec
}
}

}
    extension EventRemaindersViewController: UITableViewDelegate {
                
 func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
                    if let cell = tableView.cellForRow(at: indexPath) {
                           cell.accessoryType = .checkmark
                        if(indexPath.row == 0){
                            cell.accessoryType = .none
                        }
                    }
                   }
        
 func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
            if let cell = tableView.cellForRow(at: indexPath) {
                cell.accessoryType = .none
            }
        }
}


