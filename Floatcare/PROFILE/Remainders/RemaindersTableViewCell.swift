//
//  RemaindersTableViewCell.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/22/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

class RemaindersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var remaindersTitleLbl: UILabel!
    
    @IBOutlet weak var remaindersDetailLbl: UILabel!

    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.remaindersTitleLbl.textColor = .primaryColor
        self.remaindersDetailLbl.textColor = .descriptionColor
        self.remaindersTitleLbl.font = UIFont(font: .SFUIText, weight: .semibold, size: 16.0)
        self.remaindersDetailLbl.font = UIFont(font: .SFUIText, weight: .medium, size: 13.0)
         self.isAccessoryButtonHidden = false
    }

    var isAccessoryButtonHidden: Bool = false {
        
        didSet {
            
            self.accessoryView = self.isAccessoryButtonHidden ? nil : UIImageView(image: UIImage(named: "accessory"))
        }
    }
}
