//
//  RemaindersViewController.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/22/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class RemaindersViewController: UIViewController {
    
    @IBOutlet weak var navigationBar: NavigationBar!
    
    @IBOutlet weak var headingTitleView: UIView!
    
    @IBOutlet weak var headingTitleLbl: UILabel!
    
    @IBOutlet weak var remaindersTblView: UITableView!
    override func viewDidLoad() {
           super.viewDidLoad()
           
           self.setupView()
           // Do any additional setup after loading the view.
       }
    private func setupView() {
      
        self.navigationBar.isRightButtonHidden = true
        
        self.headingTitleView.roundCorners(.layerMinXMaxYCorner, radius: 30.0)
        self.headingTitleView.backgroundColor = .profileBgColor
        self.headingTitleLbl.font = UIFont(font: .Athelas, weight: .regular, size: 32)
        self.headingTitleLbl.textColor = .headingTitleColor
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension RemaindersViewController: UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      //  extension PrivacySettingsViewController: UITableViewDataSource {
            
     return 6
    }
        
    func numberOfSections(in tableView: UITableView) -> Int {
                
                return 1
            }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//                return section == 0 ? 3 : 1
//            }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RemaindersTableViewCell", for: indexPath) as! RemaindersTableViewCell
        
        if indexPath.row == 0{
                    cell.remaindersTitleLbl.text = "Event Reminders"
                    cell.remaindersDetailLbl.text = "15 min before"
       
        }
        if indexPath.row == 1{
               cell.remaindersTitleLbl.text = "Assigned Workshifts"
               cell.remaindersDetailLbl.text = "None"
        }
        if indexPath.row == 2{
            cell.remaindersTitleLbl.text = "On Call Shifts"
            cell.remaindersDetailLbl.text = "A Start"
        }
        if indexPath.row == 3{
            cell.remaindersTitleLbl.text = "Meetings"
            cell.remaindersDetailLbl.text = "15min,30min"
        }
        if indexPath.row == 4{
                cell.remaindersTitleLbl.text = "Education"
                cell.remaindersDetailLbl.text = "30min"
        }
          if indexPath.row == 5{
                cell.remaindersTitleLbl.text = "Events"
                cell.remaindersDetailLbl.text = "1 hr before"
        }
        return cell
    }
}

extension RemaindersViewController: UITableViewDelegate {
            
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
                if indexPath.section == 0 {
                    if indexPath.row == 0 {
                    
    let eventRemaindersViewController = self.storyboard?.instantiateViewController(withIdentifier: "eventRemaindersView") as! EventRemaindersViewController
    self.navigationController?.pushViewController(eventRemaindersViewController, animated: true)
            
                    }
            }
        }
}

