//
//  eventRemainderSecondaryCell.swift
//  Floatcare
//
//  Created by sramika mangalapurapu on 10/23/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

class eventRemainderSecondaryCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
    
    super.awakeFromNib()
    self.titleLabel.textColor = .primaryColor
    self.titleLabel.font = UIFont(font: .SFUIText, weight: .semibold, size: 16.0)
    }
}
