//
//  VerifyMobileNumberViewController.swift
//  Fusion Tech
//
//  Created by OMNIADMIN on 30/05/20.
//  Copyright © 2020 OMNIADMIN. All rights reserved.
//

import UIKit
import SGCodeTextField

class VerifyMobileNumberViewController: UIViewController {
    
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var headingTitleView: UIView!
    @IBOutlet weak var headingTitleLabel: UILabel!
    
    @IBOutlet weak var disclamirLabel: UILabel!
    
    @IBOutlet weak var nextButton: PrimaryButton!
    @IBOutlet weak var bottomBarView: UIView!
    
    @IBOutlet weak var otpView: SGCodeTextField!
    
    @IBOutlet weak var resendCodeLabel: UnderlinedLabel!
    @IBOutlet weak var resendCodeDesLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        
        // Do any additional setup after loading the view.
    }
    
    
    private func setupView() {
        
        self.navigationBar.isRightButtonHidden = true
        
        self.headingTitleView.roundCorners(.layerMinXMaxYCorner, radius: 30.0)
        self.headingTitleView.backgroundColor = .profileBgColor
        self.headingTitleLabel.font = UIFont(font: .Athelas, weight: .regular, size: 32)
        self.headingTitleLabel.textColor = .headingTitleColor
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            
            self.navigationController?.popViewController(animated: true)
        }
        
        
        self.bottomBarView.roundCorners([ .layerMinXMinYCorner], radius: 30.0)
        self.bottomBarView.backgroundColor = .profileBgColor
        
        self.disclamirLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 12.0)
        self.disclamirLabel.textColor = .secondayLabelColor
        self.nextButton.buttonTitle = "Next"
        
        self.otpView.count = 4
        self.otpView.placeholder = ""
        self.otpView.digitBorderColor = .secondayLabelColor
        self.otpView.textColor = .primaryColor
        self.otpView.digitBackgroundColor = .secondayLabelColorWithAlpha
        self.otpView.digitBackgroundColorEmpty = .secondayLabelColorWithAlpha
        self.otpView.digitSpacing = 10.0
        self.otpView.digitCornerRadius = 12.0
        self.otpView.digitBorderColorFocused = .primaryColor
        self.otpView.textColorFocused = .primaryColor
        self.otpView.digitBorderColorEmpty = .primaryColor
        self.otpView.digitBackgroundColorFocused = .secondayLabelColorWithAlpha
        
        
        self.resendCodeLabel.textAlignment = .right
        self.resendCodeLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 15.0)
        self.resendCodeLabel.text = "Resend Code"
        
        
        self.resendCodeDesLabel.textAlignment = .right
        self.resendCodeDesLabel.textColor = .leastLabelColor
        self.resendCodeDesLabel.font = UIFont(font: .SFUIText, weight: .bold, size: 15.0)
        self.resendCodeDesLabel.text = "I didn't get code."
     }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
