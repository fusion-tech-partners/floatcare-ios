import Foundation
import UIKit

typealias actionBlock = (_ sender: UIButton) -> ()

final class NavigationBar: UIView {
    
    private static let NIB_NAME = "NavigationBar"
    
    @IBOutlet private var view: UIView!
    @IBOutlet private weak var leftButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var rightButton: UIButton!
 
    var LeftButtonActionBlock: actionBlock?
    var rightButtonActionBlock: actionBlock?
    
    var isRightButtonHidden: Bool = false {
        
        didSet {
            rightButton.isHidden = isRightButtonHidden
        }
    }
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var isLeftButtonHidden: Bool {
        set {
            leftButton.isHidden = newValue
        }
        get {
            return leftButton.isHidden
        }
    }
    
    var isRightFirstButtonEnabled: Bool {
        set {
            rightButton.isEnabled = newValue
        }
        get {
            return rightButton.isEnabled
        }
    }
    
    override func awakeFromNib() {
        initWithNib()
        
    }
    
    @IBAction func backAction(sender: UIButton) {
      
        self.LeftButtonActionBlock?(sender)
    }
    
    
    @IBAction func rightBtnAction(sender: UIButton) {
         
           self.rightButtonActionBlock?(sender)
       }
    
    private func initWithNib() {
        Bundle.main.loadNibNamed(NavigationBar.NIB_NAME, owner: self, options: nil)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .profileBgColor
        addSubview(view)
        setupLayout()
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate(
            [
                view.topAnchor.constraint(equalTo: topAnchor),
                view.leadingAnchor.constraint(equalTo: leadingAnchor),
                view.bottomAnchor.constraint(equalTo: bottomAnchor),
                view.trailingAnchor.constraint(equalTo: trailingAnchor),
            ]
        )
    }
}
