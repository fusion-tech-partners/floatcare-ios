//
//  PushNotificationSender.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 05/09/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class PushNotificationSender {
    func sendPushNotification(to token: String, title: String, body: String) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["to" : "cDw8eHznJUVHjeoxAtosr4:APA91bFoPJBWp2AZVcPmViINYnF_FSEKYf7fyeQm8OLg6XKviUJ2QlNOSAdTZyit4ckWNyukwPoJwobg1m_iKO9juroCMS8j7Lri9lpIaa4Wk15BCVbYE1uu3Nfb1DA8XJwJ-_dqNKCc",
                                           "notification" : ["title" : title, "body": body, "detail" : body,"url": Profile.shared.userBasicInformation?.profilePhoto,"userId":Profile.shared.userBasicInformation?.userId]
                                           
        ]
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAAgUfjxM4:APA91bG-AVYicYvEashII-zq7g8R4bkYQhysYBFxBxdPe4LdkSZiRfehia59HUxwSfgEKMnm_lYkbD5xFSZFPr2DxP9gSS6TSPAGfWaZ-MBtmde7d5WGvBAf11bb7yBfSZBYxeY7ZytG", forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
}
