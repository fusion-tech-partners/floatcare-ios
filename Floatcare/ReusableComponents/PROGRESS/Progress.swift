//
//  Progress.swift
//  FeedtheNeed
//
//  Created by BEAST on 17/02/2020.
//  Copyright © 2020 BEAST. All rights reserved.
//

import UIKit
class Progress: ProgressProtocol {



    // MARK:- Propperties

    var containerView = UIView()

    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)

    var loadingView: UIView = UIView()



    // MARK:- Singleton

    static let shared = Progress()



    func showProgressView(onView: UIView? = nil) {

        DispatchQueue.main.async {
            
        
        if  let _ = UIApplication.shared.delegate as? AppDelegate,

            let window = UIApplication.shared.windows.first {            

            var container = onView

            if  onView == nil {

                container = window

            }

            guard let view = container else {

                return

            }

            

            self.containerView.frame = window.frame

            self.containerView.center = window.center

            self.containerView.backgroundColor = UIColor.clear

            self.containerView.tag = 789465

            self.loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)

            self.loadingView.center = view.center

//            loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.3)

            self.loadingView.backgroundColor = .clear

            self.loadingView.clipsToBounds = true

            self.loadingView.layer.masksToBounds = true

            self.loadingView.layer.cornerRadius = 10

            self.loadingView.accessibilityIdentifier = "id_view_busy_indicator"

            self.activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)

            self.activityIndicator.style = .large

            self.activityIndicator.color = #colorLiteral(red: 0.3607843137, green: 0.3882352941, blue: 0.6705882353, alpha: 1)

            self.activityIndicator.center = CGPoint(x: self.loadingView.frame.size.width / 2,

                                               y: self.loadingView.frame.size.height / 2)

            self.loadingView.addSubview(self.activityIndicator)

            self.containerView.addSubview(self.loadingView)

            view.addSubview(self.containerView)

            self.activityIndicator.accessibilityIdentifier = "id_progress_bar"

            self.activityIndicator.startAnimating()

        }
        }

    }



    func hideProgressView() {

        DispatchQueue.main.async {

            self.loadingView.removeFromSuperview()

            self.activityIndicator.stopAnimating()

            self.containerView.removeFromSuperview()

        }

    }

}

