//
//  ProgressProtocol.swift
//  FeedtheNeed
//
//  Created by BEAST on 17/02/2020.
//  Copyright © 2020 BEAST. All rights reserved.
//

import Foundation
import UIKit

protocol ProgressProtocol {    

    var containerView: UIView { get set }

    var activityIndicator: UIActivityIndicatorView { get set }

    var loadingView: UIView { get set }

    

    func showProgressView(onView: UIView?)

    

    func hideProgressView()

}

