//
//  UIButton+Extensions.swift
//  Fusion Tech
//
//  Created by OMNIADMIN on 30/05/20.
//  Copyright © 2020 OMNIADMIN. All rights reserved.
//

import Foundation
import UIKit

class PrimaryButton: UIButton {
    
    override func awakeFromNib() {
        
        self.backgroundColor = .primaryColor
        self.titleLabel?.font = UIFont(font: .SFUIText, weight: .semibold, size: 18)
        self.setTitleColor(.white, for: .normal)
        self.layer.cornerRadius = 5.0
    }
    
    var buttonTitle: String = "" {
        
        didSet {
            
            self.setTitle(buttonTitle, for: .normal)
        }
    }
}
