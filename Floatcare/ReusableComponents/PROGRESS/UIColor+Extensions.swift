//
//  UIColor+Extensions.swift
//  Fusion Tech
//
//  Created by OMNIADMIN on 23/05/20.
//  Copyright © 2020 OMNIADMIN. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    public static var appLightGray: UIColor = UIColor(rgb: 0xB5B5BE)
    public static var appDarkGray: UIColor = UIColor(rgb: 0x474752)

    @objc public static var primaryColor: UIColor = UIColor(rgb: 0x5C63AB)
    public static var descriptionColor: UIColor = UIColor(rgb: 0x92929D)
    
    public static var lightPurpleColor: UIColor = UIColor(rgb: 0x9875E3)
    
    public static var profileBgColor: UIColor = UIColor(rgb: 0xF1F1FF)
    public static var profileArrowColor: UIColor = UIColor(rgb: 0x4394AD)
    
    public static var appProfileEmployeCodeColor: UIColor = UIColor(rgb: 0x4394AD)
    public static var appProfileEmployeNameColor: UIColor = UIColor(rgb: 0x353535)
    public static var appProfileEmployeAddressColor: UIColor = UIColor(rgb: 0x44444F)
    public static var cellIconBgColor: UIColor = UIColor(rgb: 0xE0E1EF)
    public static var footerBgColor: UIColor = UIColor(rgb: 0xF7F7FB)
    public static var headingTitleColor: UIColor = UIColor(rgb: 0x485DBA)
    public static var secondayLabelColor: UIColor = UIColor(rgb: 0xA9AFB6)
    public static var secondayLabelColorWithAlpha: UIColor = UIColor(rgb: 0xA9AFB6, alpha: 0.1)
    public static var leastLabelColor: UIColor = UIColor(rgb: 0x6D7278, alpha: 0.7)
    
    public static var lightGreyTextColor: UIColor = UIColor(rgb: 0x696974)
    
     public static var pinkColor: UIColor = UIColor(rgb: 0xFF73A4)
    public static var orangeColor: UIColor = UIColor(rgb: 0xFA6400)
    public static var redColor: UIColor = UIColor(rgb: 0xFB0000)
    public static var yellowColor: UIColor = UIColor(rgb: 0xF7B500)
    public static var orchidColor: UIColor = UIColor(rgb: 0xDC73E6)
    public static var steelBlueColor: UIColor = UIColor(rgb: 0x4394AD)
    public static var persianBlueColor: UIColor = UIColor(rgb: 0x2736D2)
    public static var cornflowerBlue: UIColor = UIColor(rgb: 0x668FE6)
    public static var turquoise: UIColor = UIColor(rgb: 0x4DCFF1)
    public static var tiffanyBlue: UIColor = UIColor(rgb: 0x00C6B0)
    public static var buttonBg : UIColor = UIColor(rgb: 0xE6E7F2)

 }

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, alpha:CGFloat = 1.0) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
    
    convenience init(rgb: Int, alpha:CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            alpha: alpha
        )
    }
    var htmlRGBColor:String {
           return String(format: "#%02x%02x%02x", Int(rgbComponents.red * 255), Int(rgbComponents.green * 255),Int(rgbComponents.blue * 255))
    }
    var rgbComponents:(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        if getRed(&r, green: &g, blue: &b, alpha: &a) {
            return (r,g,b,a)
        }
        return (0,0,0,0)
    }
    
}
