//
//  UIFont+Extensions.swift
//  Fusion Tech
//
//  Created by OMNIADMIN on 23/05/20.
//  Copyright © 2020 OMNIADMIN. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    
    enum Font: String {
        case SFUIText = "SFProText"
        case SFUIDisplay = "SFUIDisplay"
        case Athelas = "Athelas"
    }

    private static func name(of weight: UIFont.Weight) -> String? {
        switch weight {
            case .ultraLight: return "UltraLight"
            case .thin: return "Thin"
            case .light: return "Light"
            case .regular: return "Regular"
            case .medium: return "Medium"
            case .semibold: return "Semibold"
            case .bold: return "Bold"
            case .heavy: return "Heavy"
            case .black: return "Black"
            default: return nil
        }
    }

    convenience init?(font: Font, weight: UIFont.Weight, size: CGFloat) {
        var fontName = "\(font.rawValue)"
        if let weightName = UIFont.name(of: weight) { fontName += "-\(weightName)" }
        self.init(name: fontName, size: size)
    }
}

