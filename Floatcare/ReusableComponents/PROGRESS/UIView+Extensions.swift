//
//  UIView+Extensions.swift
//  Fusion Tech
//
//  Created by OMNIADMIN on 23/05/20.
//  Copyright © 2020 OMNIADMIN. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
        
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = corners
    }
}
