//
//  ReusableAlertController.swift
//  Floatcare
//
//  Created by BEAST   on 28/05/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import UIKit

class ReusableAlertController {
    typealias alertCompletion = (Int) -> Void
    
    public static func getTopViewController() -> UIViewController {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        
        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return UIViewController()
    }

    //Show alert
    public static func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        alert.addAction(defaultAction)
        DispatchQueue.main.async(execute: {
            getTopViewController().present(alert, animated: true)
        })
    }
    public static func showAlertCompletion(title: String, message: String, compl: @escaping alertCompletion) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { action in
            compl(200)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alert.addAction(cancel)
        alert.addAction(defaultAction)        
        DispatchQueue.main.async(execute: {
            getTopViewController().present(alert, animated: true)
        })
    }
    
    public static func showAlertOk(title: String, message: String, compl: @escaping alertCompletion) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { action in
            compl(200)
        })
        alert.addAction(defaultAction)
        DispatchQueue.main.async(execute: {
            getTopViewController().present(alert, animated: true)
        })
    }
}


