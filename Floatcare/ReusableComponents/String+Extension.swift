//
//  String+htmlAttributedString.swift
//  TMTE
//
//  Created by BEAST on 4/10/17.
//  Copyright © 2018 Floatcare. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    public func toPhoneNumber() -> String {
        return self.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "$1-$2-$3", options: .regularExpression, range: nil)
    }
    
    func isValidEmail() -> Bool {
        if String.isValid(input: self) == false {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        return result
    }
    
    static func isValid(input: String?) -> Bool {
        if let result = input?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
            if result.count > 0 {
                return true
            }
        }
        return false
    }
    
    func isValidZIPCode() -> Bool {
        
        if String.isValid(input: self) == false {
            return false
        }
        
        if count != 5 || self == "00000" {
            return false
        }
    
        return true
    }
    
    func isValidPhoneNumber() -> Bool {
        
        if String.isValid(input: self) == false {
            return false
        }
        var tempVar = replacingOccurrences(of: "-", with: "")
        tempVar = tempVar.replacingOccurrences(of: "(", with: "")
        tempVar = tempVar.replacingOccurrences(of: ")", with: "")
        tempVar = tempVar.replacingOccurrences(of: " ", with: "")
        if tempVar.count != 10 || tempVar == "0000000000" {
            return false
        }
        return true
    }
    
    func convertToDate(_ dateStr: String) -> Date {
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
     return dateFormatter.date(from: dateStr)!
    }
    var convertToBackendDateFormat: String {
     let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        let dt = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: dt ?? Date())
    }
    var covertDateToReadableFormat: String{
        if(self.count>0){
            let dobFomat = DateFormatter()
            dobFomat.dateFormat = "yyyy-MM-dd"
            guard let dateServer = dobFomat.date(from: self) else { return ""}
            
            let locForm = DateFormatter()
            locForm.dateFormat = "MMM dd,yyyy"
            return locForm.string(from: dateServer)
        }else{
            return ""
        }
    }
        
    var fillSpaces : String {
        if self.count > 0 {
            let st = self.replacingOccurrences(of: " ", with: "%20")
            return st
        }
        return ""
    }
}

