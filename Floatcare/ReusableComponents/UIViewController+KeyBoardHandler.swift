//
//  UIViewController+KeyBoardHandler.swift
//  Floatcare
//
//  Created by BEAST on 14/05/2020.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import UIKit

class KeyBoardNotifierViewController: UIViewController {

    var tableView: UITableView? = nil

    func enrollTableForKeyBoardNotification(table: UITableView) {
        tableView = table
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWillShow(_ notification:Notification) {

            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
            }
    }

    @objc func keyboardWillHide(_ notification:Notification) {

            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            }
    }

}
