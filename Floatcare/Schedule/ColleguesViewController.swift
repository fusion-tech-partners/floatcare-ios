//
//  ColleguesViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 18/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ColleguesViewController: UIViewController {
    

    var viewModel = OrganizationViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension ColleguesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 66.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return OpenShiftUtility.shared.openshift.invitees?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.NightColleaugeCell.rawValue, for: indexPath) as! NightColleaugeCell
        
        cell.messageButton.tag = indexPath.row
        if let staff  = OpenShiftUtility.shared.openshift.invitees?[indexPath.row] {
            cell.empolyeeName.setTitle((staff.firstName ?? "") + " " + (staff.lastName ?? ""), for: .normal)
            // cell.profileSettingDescriptionLabel.text = "Nurse"
            
            if let pic = staff.profilePhoto,
                let imgStr = (amazonURL + pic).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                
                cell.employeeProfilePic?.sd_setImage(with: URL(string: imgStr), placeholderImage: UIImage(named: "placeholder.png"))
            }
        }
        cell.onMessageButtonClicked = { (tag) in
            
            let staff  = OpenShiftUtility.shared.openshift.invitees?[tag]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "chat"), object: staff)
         }
        return cell
    }
    
    
    
}
