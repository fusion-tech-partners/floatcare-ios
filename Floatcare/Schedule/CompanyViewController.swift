//
//  CompanyViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 18/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class CompanyViewController: UIViewController {

    var viewModel = OrganizationViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

         viewModel.getStaffMembers(orgId: OpenShiftUtility.shared.openshift.organization?.organizationId ?? "0") { (organization) in
                   
                   self.tableView.reloadData()
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension CompanyViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyTableCell") as! CompanyTableCell
        cell.companyNameLabel?.text = "Hospital Description"
        cell.detailLabel?.text = self.viewModel.business?.description
        
        return cell
    }
    
    
    
}
