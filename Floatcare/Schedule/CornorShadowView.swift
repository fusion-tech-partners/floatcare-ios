//
//  CornorShadowView.swift
//  Floatcare
//
//  Created by OMNIADMIN on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class CornorShadowView: UIView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override func awakeFromNib() {
        
        self.layer.cornerRadius = 12
        
        self.layer.shadowColor = UIColor.primaryColor.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 5.0
    }
    
}
