//
//  MyRequestsTableViewCell.swift
//  Floatcare
//
//  Created by OMNIADMIN on 04/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class MyRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var shiftNameLbl: UILabel!
    @IBOutlet weak var shiftDescriptionLbl: UILabel!
    @IBOutlet weak var shiftStatusLbl: UILabel!
    
    @IBOutlet weak var contentBgView: CornorShadowView!
    
    @IBOutlet weak var arrowRight: UIImageView!
    
    @IBOutlet weak var imageRight: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.shiftNameLbl.font = UIFont(name: Fonts.poppinsSemiBold, size: 18)
        self.shiftDescriptionLbl.font = UIFont(name: Fonts.poppinsRegular, size: 14)
        self.shiftStatusLbl.font = UIFont(name: Fonts.sFProText, size: 13)
        
        self.shiftDescriptionLbl.numberOfLines = 0
        self.shiftDescriptionLbl.text = String(format: "%@\n%@", "3rd Nov - 9th Nov","Apollo Hospital")
        // Initialization code
    }
    func setTheme(indexPath: IndexPath) {
        
        self.setBgColor(indexPath: indexPath)
        if indexPath.section == 0 {
            
            self.setColorsForWhiteTheme()
        } else {
            
            self.setColorsForColorTheme()
        }
    }
    
    private func setColorsForColorTheme() {
        
        self.shiftNameLbl.textColor = UIColor.black
        self.shiftDescriptionLbl.textColor = UIColor.appProfileEmployeAddressColor
        self.shiftStatusLbl.textColor = UIColor.descriptionColor
        self.arrowRight.tintColor = UIColor().buttonViolet
        self.imageRight.isHidden = true
    }
    
    private func setColorsForWhiteTheme() {
        
        self.shiftNameLbl.textColor = UIColor.white
        self.shiftDescriptionLbl.textColor = UIColor.white
        self.shiftStatusLbl.textColor = UIColor.clear
        self.arrowRight.tintColor = UIColor.white
        self.imageRight.isHidden = false
    }
    
    private func setBgColor(indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            switch indexPath.row {
                case 0:
                    self.contentBgView.backgroundColor = UIColor().buttonViolet
                case 1:
                    self.contentBgView.backgroundColor = UIColor.lightPurpleColor
                default:
                break
            }
        } else {
            
            self.contentBgView.backgroundColor = .white
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
