//
//  MyRequestsViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 03/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import LMJDropdownMenu

class MyRequestsViewController: UIViewController {
    
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var shiftsHeading: UILabel!
    
    @IBOutlet weak var shiftsTableview: UITableView!
    
    @IBOutlet weak var dropdown: LMJDropdownMenu!
    var schedulesArr : [ScheduleLayout]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shiftsTableview.reloadData()
        self.setupDropdown()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(reloadAPIData), name: NSNotification.Name(rawValue: notification_Myrequest), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         self.getAllRequestForUser()
        self.getSchedulesForUser()
        
        //self.visualEffectView.removeFromSuperview()
    }
    @objc func reloadAPIData(){
        self.getAllRequestForUser()
        self.getSchedulesForUser()
    }
    func getAllRequestForUser() {
          Progress.shared.showProgressView()
        GetAllRequestByUserAPI().GetAllRequestByUser(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)") { (requests) in
            DispatchQueue.main.async {
                self.shiftsTableview.reloadData()
                  Progress.shared.hideProgressView()
                }
            }
        }
    @objc func getSchedulesForUser() {
         Progress.shared.showProgressView()
        ScheduleLayoutByUserId().GetAllScheduleLayoutByUserId(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)") { (schedules) in
            self.schedulesArr = schedules
             DispatchQueue.main.async {
                          print("\(schedules)")
                self.shiftsTableview.reloadData()
                             Progress.shared.hideProgressView()
                           }
        }
    }
    private func setupDropdown() {
        
        self.dropdown.delegate = self
        self.dropdown.dataSource = self
        self.dropdown.titleColor = .primaryColor
        self.dropdown.title = "Status"
        self.dropdown.titleAlignment = .center
        
        self.dropdown.titleFont = UIFont(font: .SFUIText, weight: .semibold, size: 14.0)!
        self.dropdown.rotateIcon = UIImage(named:"down_arrow")!
        self.dropdown.rotateIconSize = CGSize(width: 13.0, height: 8.0)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension MyRequestsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        guard section != 0 else { return 0.0}
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard section != 0 else { return nil}
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileInfoTableHeaderCell.rawValue) as! ProfileInfoTableHeaderCell
        cell.titleLabel.font = UIFont(font: .SFUIText, weight: .semibold, size: 14.0)
        cell.titleLabel.text = "New"
        cell.contentView.backgroundColor = .white
        return cell.contentView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       // guard section != 0 else { return Profile.shared.userWorkSpaceDetails?.getWorkspaceByUserId?.count ?? 0 + 1}
        guard section != 0 else { return self.schedulesArr?.count ?? 0}
        return CreateReqParams.createReqParams.requestForUser?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        if indexPath.section != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyRequestsTableViewCell", for: indexPath) as! MyRequestsTableViewCell
                   cell.setTheme(indexPath: indexPath)
            let request =  CreateReqParams.createReqParams.requestForUser![indexPath.row]
            cell.shiftNameLbl.text = request.requestTypeName
            cell.shiftDescriptionLbl.text = request.notes ?? ""
            cell.shiftStatusLbl.text = request.worksiteName ?? ""
             return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShiftPlanningTableViewCell", for: indexPath) as! ShiftPlanningTableViewCell
                  // cell.setTheme(indexPath: indexPath)
            let schedule = self.schedulesArr![indexPath.row]
            cell.headerLbl.text = "Shift Planning"
            let shiftStartDate = self.formattedDateFromString(dateString: schedule.scheduleStartDate ?? "", format: "MMM d, yyyy")
             let shiftEndDate = self.formattedDateFromString(dateString: schedule.scheduleEndDate ?? "", format: "MMM d, yyyy")
            cell.durationLbl.text = "\(shiftStartDate ?? "")" + " - " + "\(shiftEndDate ?? "")"
          //  let mySubstring = str.prefix(5)
            let duration = schedule.shiftPlanningPhaseLength ?? ""
            
            cell.deadlineLbl.text =  "Deadline: " + "\(duration.prefix(1))" + " Week"
            cell.departmentLbl.text = schedule.departmentName ?? ""
            cell.workspaceLbl.text = "Floatcare"
                  return cell
        }
       
    }
    func formattedDateFromString(dateString: String, format: String) -> String? {

           let inputFormatter = DateFormatter()
           inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

           if let date = inputFormatter.date(from: dateString) {

               let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format

               return outputFormatter.string(from: date)
           }

           return nil
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != 0 {
             let request =  CreateReqParams.createReqParams.requestForUser![indexPath.row]
            let editVC = UIStoryboard(name: "EditRequestVC", bundle: nil).instantiateViewController(withIdentifier: String(describing: EditRequestVC.self)) as! EditRequestVC
            editVC.selectedRequest = request
            self.navigationController?.pushViewController(editVC, animated: true)
            
        }else {
            let schedule = self.schedulesArr![indexPath.row]
            let shiftStartDate = self.formattedDateFromString(dateString: schedule.scheduleStartDate ?? "", format: "yyyy-MM-dd")
             let shiftEndDate = self.formattedDateFromString(dateString: schedule.scheduleEndDate ?? "", format: "yyyy-MM-dd")
            ScheduleLayout.shiftPlanningDetails.shiftPlanningStartDate =  shiftStartDate
             ScheduleLayout.shiftPlanningDetails.shiftPlanningEndDate =  shiftEndDate
            ScheduleLayout.shiftPlanningDetails.scheduleLayoutId = schedule.scheduleLayoutId ?? ""
           // let request =  CreateReqParams.createReqParams.requestForUser![indexPath.row]
            let editVC = UIStoryboard(name: "ShiftPalanningVC", bundle: nil).instantiateViewController(withIdentifier: String(describing: ShiftPalanningVC.self)) as! ShiftPalanningVC
           // editVC.selectedRequest = request
            self.navigationController?.pushViewController(editVC, animated: true)
            
        }
//        let requestDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "RequestDetailViewController") as! RequestDetailViewController
//        self.navigationController?.pushViewController(requestDetailViewController, animated: true)
    }
}


extension MyRequestsViewController: LMJDropdownMenuDataSource {
    func numberOfOptions(in menu: LMJDropdownMenu) -> UInt {
        3
    }
    
    func dropdownMenu(_ menu: LMJDropdownMenu, heightForOptionAt index: UInt) -> CGFloat {
        
        return 32.0
    }
    
    func dropdownMenu(_ menu: LMJDropdownMenu, titleForOptionAt index: UInt) -> String {
        switch index {
            case 0:
             return "Status"
            case 1:
            return "Name"
            case 2:
            return "Date"
            default:
            return ""
        }
    }
    
    
}

extension MyRequestsViewController: LMJDropdownMenuDelegate {
    
}
