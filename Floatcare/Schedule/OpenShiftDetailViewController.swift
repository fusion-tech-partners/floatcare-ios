//
//  OpenShiftsViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class OpenShiftDetailViewModel: OrganizationViewModel {
    
    var openshift: OpenShift!
    
    init (openshift: OpenShift) {
        
        self.openshift = openshift
    }
}

class OpenShiftDetailViewController: UIViewController {
    
 
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var topbarView: UIView!
    
    @IBOutlet weak var openShitDetailView: OpenShitDetailView!
    
    @IBOutlet weak var nextButton: PrimaryButton!
    @IBOutlet weak var bottomBarView: UIView!
    
    var viewModel: OpenShiftDetailViewModel!
    
//    @IBOutlet weak var shiftsHeading: UILabel!
//
//    @IBOutlet weak var shiftsTableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
        
        viewModel.getStaffMembers(orgId: OpenShiftUtility.shared.openshift.organization?.organizationId ?? "0") { (organization) in
            
            self.openShitDetailView.addressLabel.text = organization?.address ?? ""
         }
        
        if let imgStr = (amazonURL + (self.viewModel.openshift?.organization?.logo ?? "")).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                   
                self.iconImageView?.sd_setImage(with: URL(string: imgStr), placeholderImage: UIImage(named: "placeholder.png"))
        }
        // Do any additional setup after loading the view.
    }
    

    func setupView() {
        
        self.openShitDetailView.setupCell(openshift: self.viewModel.openshift)
        
        self.iconImageView.layer.cornerRadius = 10.0
        
        self.navigationBar.roundCorners([ .layerMinXMaxYCorner], radius: 30.0)
        
        self.bottomBarView.roundCorners([ .layerMinXMinYCorner], radius: 30.0)
        self.bottomBarView.backgroundColor = .profileBgColor
        
        self.nextButton.buttonTitle = "Request Shift"
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func acceptShiftInfo() {
        
        let updateOpenShiftsMutation = UpdateOpenShiftsMutation(userId: Profile.shared.userId, openShiftId: OpenShiftUtility.shared.openshift.openShiftId, isAccepted: true, isApproved: false, status: "")
        
        
        ApolloManager.shared.client.perform(mutation: updateOpenShiftsMutation) { (result) in
            guard let data = try? result.get().data else { return }
            print(data)
            ReusableAlertController.showAlert(title: "Open Shifts", message: "Open shift accepted by you Successfully")
         }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.destination is OpenShiftsPageMenuControllerViewController {
            
            OpenShiftUtility.shared.openshift = self.viewModel.openshift
        }
    }
    

}


class OpenShitDetailView: CornorShadowView {
    
    @IBOutlet weak var shiftNameLabel: UILabel!
    @IBOutlet weak var shiftTitleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var colleguesCollectionView: UICollectionView!
    @IBOutlet weak var numberOfWorkersLabel: UILabel!
    var workersArray : [UserBasicInformation]?
    override func awakeFromNib() {
        
        
        super.awakeFromNib()
        self.colleguesCollectionView.dataSource = self
        
        self.addressLabel.text = ""
        
        self.shiftNameLabel.font = UIFont(font: .Athelas, weight: .bold, size: 23.0)
        self.shiftTitleLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 12.0)
        self.addressLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 16.0)
        
        self.addressLabel.numberOfLines = 2
        
 
        self.shiftNameLabel.textColor = .primaryColor
        self.shiftTitleLabel.textColor = .lightGreyTextColor
        self.addressLabel.textColor = .leastLabelColor
        
        self.numberOfWorkersLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 12.0)
        self.numberOfWorkersLabel.textColor = .descriptionColor
        
        let cellSize = CGSize(width:21 , height:21)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.minimumLineSpacing = 1.0
        layout.minimumInteritemSpacing = -10
        self.colleguesCollectionView.setCollectionViewLayout(layout, animated: true)
        
     }
    
    func setupCell(openshift: OpenShift) {
        
        self.shiftTitleLabel.text = "HOSPITAL"
        self.shiftNameLabel.text = openshift.staffUserName ?? ""
        self.addressLabel.text = openshift.organization?.address ?? ""
        self.numberOfWorkersLabel.text = "collegues have worked here"
    }
    
}

extension OpenShitDetailView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkerCell", for: indexPath) as! WorkerCell
        if let worker = workersArray?[indexPath.row]{
           if let pic = worker.profilePhoto,
                let imgStr = (amazonURL + pic).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                cell.imageView?.sd_setImage(with: URL(string: imgStr), placeholderImage: UIImage(named: "placeholder.png"))
            }
        }
        return cell
    }
    
    
}
