//
//  OpenShiftsPageMenuControllerViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 18/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import Swift_PageMenu

class OpenShiftUtility{
    
    static let shared = OpenShiftUtility()
    
    var openshift: OpenShift!
}

class OpenShiftsPageMenuControllerViewController: PageMenuController {
        
    required init?(coder: NSCoder) {
        super.init(coder: coder, options: OpenShiftsRoundRectPagerOption())
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabMenuView.backgroundColor = .clear
        
        self.dataSource = self
        
        
         // Do any additional setup after loading the view.
    }
    
 

}

extension OpenShiftsPageMenuControllerViewController: PageMenuControllerDataSource {

    func viewControllers(forPageMenuController pageMenuController: PageMenuController) -> [UIViewController] {
        
        let openShiftsViewController1 = UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: String(describing: ShiftMenuViewController.self)) as! ShiftMenuViewController
        let openShiftsViewController2 = UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: String(describing: ColleguesViewController.self)) as! ColleguesViewController
        let openShiftsViewController3 = UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: String(describing: CompanyViewController.self)) as! CompanyViewController
            
        return [openShiftsViewController1, openShiftsViewController2, openShiftsViewController3]
    }

    func menuTitles(forPageMenuController pageMenuController: PageMenuController) -> [String] {
        return ["Shift Info", "Colleagues", "Company"]
    }

    func defaultPageIndex(forPageMenuController pageMenuController: PageMenuController) -> Int {
        return 0
    }
}


struct OpenShiftsRoundRectPagerOption: PageMenuOptions {

    var isInfinite: Bool = false

    var tabMenuPosition: TabMenuPosition = .top

    var menuItemSize: PageMenuItemSize {
        return .fixed(width: UIScreen.main.bounds.size.width/3.0, height: 44.0)
    }

    var menuTitleColor: UIColor {
        return .leastLabelColor
    }

    var menuTitleSelectedColor: UIColor {
        
        return .primaryColor
    }

    var menuCursor: PageMenuCursor {
         
        return .underline(barColor: .primaryColor, height: 4.0)
    }

    var font: UIFont {
        
        return UIFont(font: .SFUIText, weight: .medium, size: 15.0)!
    }

    var menuItemMargin: CGFloat {
        return 0
    }

    var tabMenuBackgroundColor: UIColor {
        return .profileBgColor
    }

    var tabMenuContentInset: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 4)
    }

    public init(isInfinite: Bool = false, tabMenuPosition: TabMenuPosition = .top) {
        self.isInfinite = isInfinite
        self.tabMenuPosition = tabMenuPosition
    }
}
