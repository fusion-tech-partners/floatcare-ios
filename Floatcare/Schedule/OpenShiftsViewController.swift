//
//  OpenShiftsViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class OpenShiftsViewController: UIViewController {
    
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var shiftsHeading: UILabel!
    
    @IBOutlet weak var shiftsTableview: UITableView!
    
    private let viewModel: OpenShiftsViewModel = OpenShiftsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
 
        self.viewModel.fetchOpenShifts { (shifts) in
            self.shiftsTableview.reloadData()
        }
               // Do any additional setup after loading the view.
    }
    

    func setupView() {
        
        self.shiftsHeading.text = ""
        self.shiftsHeading.font = UIFont(font: .SFUIText, weight: .semibold, size: 18.0)
        
        self.sortButton.setImage(UIImage(named: "ic_Filter")!.withRenderingMode(.alwaysTemplate), for: .normal)
        self.sortButton.titleLabel?.font = UIFont(font: .SFUIText, weight: .semibold, size: 14.0)
        self.sortButton.tintColor = .profileArrowColor
        self.sortButton.setTitleColor(.profileArrowColor, for: .normal)
        self.sortButton.alignImageRight()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension OpenShiftsViewController: UITableViewDataSource, UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        
//        return 66.0
//    }
//    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        
//        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileInfoTableHeaderCell.rawValue) as! ProfileInfoTableHeaderCell
//        cell.contentView.backgroundColor = .white
//        cell.titleLabel.text = "20th May, 2020"
//        return cell.contentView
//    }
//    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        
//        return 2
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.viewModel.openShifts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OpenShitsTableCell", for: indexPath) as! OpenShitsTableCell
        
        let openshift = self.viewModel.openShifts?[indexPath.row]
        cell.setupCell(openshift: openshift)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let openshift = self.viewModel.openShifts?[indexPath.row] else { return }
        let openShiftDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "OpenShiftDetailViewController") as! OpenShiftDetailViewController
        openShiftDetailViewController.viewModel = OpenShiftDetailViewModel(openshift: openshift)
        self.navigationController?.pushViewController(openShiftDetailViewController, animated: true)
    }
}
