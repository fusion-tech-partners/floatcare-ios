//
//  OpenShiftsViewModel.swift
//  Floatcare
//
//  Created by OMNIADMIN on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import Apollo

typealias OpenShiftsCompletion = ([OpenShift]?) -> ()
typealias UserOpenShiftsCompletion = ([UserOpenShifts]?) -> ()

struct GetOpenShiftDetailsByWorksiteId: Codable {
    
    var getOpenShiftsByUserId: [OpenShift]?
}
struct OpenShift: Codable {
        
    var openShiftId: String?
    var worksiteId: String?
    var departmentId: String?
    var departmentName: String?
    var shiftTypeId: String?
    var onDate: String?
    var startTime: String?
    var endTime: String?
    var staffUserCoreTypeId: String?
    var staffUserName: String?
    var quota: String?
    var worksiteName: String?
    var worksiteAddress: String?
//    isSendNotifications: Boolean
//    isAutoApprove: Boolean
//    isAutoClose: Boolean
//    isAllowPartial: Boolean
//    note: String
//    status: String
    var invitees: [OpenShiftInvitees]?
    var organization: Organization?
}

struct OpenShiftInvitees: Codable {
    
    let firstName: String?
    let lastName: String?
    let profilePhoto: String?
    let isApproved: Bool
    let isAccepted: Bool
    let userId: String?
}
struct Organization : Codable {
    var organizationId: String?
    var name: String?
    var address: String?
    var city: String?
    var state: String?
    var postalCode: String?
    var logo: String?
    
}
struct GetListOfShiftsByUserId: Codable {
    
    var getListOfShiftsByUserId: [UserOpenShifts]?
}
struct UserOpenShifts: Codable {
    
    var departmentName: String?
    var scheduleId: String?
    var worksiteId: String?
    var worksiteName: String?
    var departmentId: String?
    var userId: String?
    var shiftType: String?
    var shiftDate: String?
    var startTime: String?
    var endTime: String?
    var isOnCallRequest: String?
}

class OpenShiftsViewModel {
    
    var openShifts : [OpenShift]? = [OpenShift]()
    var userOpenShifts : [UserOpenShifts]? = [UserOpenShifts]()
    
    func fetchOpenShifts(completion: @escaping OpenShiftsCompletion) {
        
        let openshifts = GetOpenShiftsByUserIdQuery(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)")
        ApolloManager.shared.client.fetch(query: openshifts,
        cachePolicy: .fetchIgnoringCacheData,
        context: nil,
        queue: .main) { result in
            
            guard let user = try? result.get().data?.resultMap else {
                
                completion(nil)
                return
            }
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                
                let decoder = JSONDecoder()
                let oragnizationDetails = try decoder.decode(GetOpenShiftDetailsByWorksiteId.self, from: jsonData)
                print(user)
                
                self.openShifts = oragnizationDetails.getOpenShiftsByUserId
                completion(oragnizationDetails.getOpenShiftsByUserId)
            }catch  let error{
                
                print(error)
                completion(nil)
            }
         }
    }
    
    func fetchUserOpenShifts(completion: @escaping UserOpenShiftsCompletion) {
        
        let openshifts = GetOpenShiftDetailsByWorksiteIdQuery(worksiteId: "141")
        ApolloManager.shared.client.fetch(query: openshifts,
        cachePolicy: .fetchIgnoringCacheData,
        context: nil,
        queue: .main) { result in
            
            guard let user = try? result.get().data?.resultMap else {
                
                completion(nil)
                return
            }
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                
                let decoder = JSONDecoder()
                let oragnizationDetails = try decoder.decode(GetListOfShiftsByUserId.self, from: jsonData)
                print(user)
                
                self.userOpenShifts = oragnizationDetails.getListOfShiftsByUserId
                completion(oragnizationDetails.getListOfShiftsByUserId)
            }catch  let error{
                
                print(error)
                completion(nil)
            }
         }
    }
}
