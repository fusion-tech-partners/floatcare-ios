
//
//  OpenShitsTableCells.swift
//  Floatcare
//
//  Created by OMNIADMIN on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import UIKit

class OpenShitsTableCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var shiftNameLabel: UILabel!
    @IBOutlet weak var shiftTitleLabel: UILabel!
    @IBOutlet weak var timingsLabel: UILabel!
    
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var requestShiftBtn: UIButton!
    @IBOutlet weak var businessInfoBtn: UIButton!
    @IBOutlet weak var contentBgView: CornorShadowView!
    
    @IBOutlet weak var colleguesCollectionView: UICollectionView!
    @IBOutlet weak var numberOfWorkersLabel: UILabel!
    
    override func awakeFromNib() {
        
        self.colleguesCollectionView.dataSource = self
        
      //  self.iconImageView.image = UIImage(named: "shift")
        self.shiftNameLabel.font = UIFont(font: .SFUIText, weight: .semibold, size: 18.0)
        self.shiftTitleLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
        self.timingsLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 19.0)
        
        
        self.shiftTitleLabel.textColor = .black
        self.favouriteBtn.tintColor = .primaryColor
        
        self.shiftTitleLabel.textColor = .lightGreyTextColor
        
        self.requestShiftBtn.backgroundColor = .primaryColor
        self.businessInfoBtn.backgroundColor = .profileBgColor
        
        self.requestShiftBtn.setTitleColor(.white, for: .normal)
        self.businessInfoBtn.setTitleColor(.lightGreyTextColor, for: .normal)
        
        self.businessInfoBtn.titleLabel?.font = UIFont(name: Fonts.poppinsSemiBold, size: 14)
        self.requestShiftBtn.titleLabel?.font = UIFont(name: Fonts.poppinsSemiBold, size: 14)
        
        let spacing: CGFloat = 5 // the amount of spacing to appear between image and title
        
        self.requestShiftBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing);
        self.requestShiftBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: 0);
        
        self.businessInfoBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing);
        self.businessInfoBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: 0);
        
        self.requestShiftBtn.layer.cornerRadius = 11.0
        self.businessInfoBtn.layer.cornerRadius = 11.0
        
        
        self.numberOfWorkersLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
        self.numberOfWorkersLabel.textColor = .descriptionColor
        
        let cellSize = CGSize(width:28 , height:28)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.minimumLineSpacing = 1.0
        layout.minimumInteritemSpacing = -10
        self.colleguesCollectionView.setCollectionViewLayout(layout, animated: true)
        
    }
    
    
    func setupCell(openshift: OpenShift?) {
       // let dateOrg = ()!
        guard let date = openshift?.onDate,
            let startTime = openshift?.startTime,
            let endTime = openshift?.endTime else { return}

        self.shiftTitleLabel.text = openshift?.worksiteAddress ?? ""
        self.shiftNameLabel.text = openshift?.staffUserName
        self.timingsLabel.text = "\(date.covertDateToReadableFormat)\n\(startTime) - \(endTime)"
        self.numberOfWorkersLabel.text = "Friends Worked here"
        
      //  if let imgStr = (amazonURL + (openshift?.organization?.logo ?? "")).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                   
//                self.iconImageView?.sd_setImage(with: URL(string: imgStr), placeholderImage: UIImage(named: "placeholder.png"))
        }
    }



class CompanyTableCell: UITableViewCell {
   
   @IBOutlet weak var companyNameLabel: UILabel!
   @IBOutlet weak var detailLabel: UILabel!

   
   override func awakeFromNib() {
       
 
    self.companyNameLabel.font = UIFont(font: .SFUIText, weight: .semibold, size: 14.0)
    self.detailLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 15.0)
    
    self.companyNameLabel.textColor = .appLightGray
    self.detailLabel.textColor = .appDarkGray
    }
}

class OpenShitDetailTableCell: UITableViewCell {
    
    @IBOutlet weak var shiftNameLabel: UILabel!
    @IBOutlet weak var shiftTitleLabel: UILabel!
 
    @IBOutlet weak var contentBgView: CornorShadowView!
    
     @IBOutlet weak var colleguesCollectionView: UICollectionView!
     @IBOutlet weak var numberOfWorkersLabel: UILabel!
    
    override func awakeFromNib() {
        
        self.colleguesCollectionView.dataSource = self
        
        self.shiftNameLabel.font = UIFont(font: .SFUIText, weight: .semibold, size: 18.0)
        self.shiftTitleLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
 
        
        self.shiftTitleLabel.textColor = .black
        
        self.shiftTitleLabel.textColor = .lightGreyTextColor
        
        self.numberOfWorkersLabel.font = UIFont(font: .SFUIText, weight: .regular, size: 14.0)
        self.numberOfWorkersLabel.textColor = .descriptionColor
        
        let cellSize = CGSize(width:28 , height:28)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.minimumLineSpacing = 1.0
        layout.minimumInteritemSpacing = -10
        self.colleguesCollectionView.setCollectionViewLayout(layout, animated: true)
        
        self.setupCell()
    }
    
    func setupCell() {
        
        self.shiftTitleLabel.text = "Memorial Herman • ER"
        self.shiftNameLabel.text = "Med-Surg Nurse"
         self.numberOfWorkersLabel.text = " "
    }
    
}

extension OpenShitDetailTableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkerCell", for: indexPath) as! WorkerCell
        return cell
    }
    
    
}


extension OpenShitsTableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkerCell", for: indexPath) as! WorkerCell
        return cell
    }
    
    
}

class WorkerCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!

    override var bounds: CGRect {
        didSet {
            self.layoutIfNeeded()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        self.imageView.image = UIImage(named: "pic")
        self.imageView.layer.masksToBounds = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        self.setCircularImageView()
    }

    func setCircularImageView() {
        
        self.imageView.backgroundColor = .profileBgColor
        self.imageView.layer.cornerRadius = CGFloat(roundf(Float(self.imageView.frame.size.width / 2.0)))
    }
}
