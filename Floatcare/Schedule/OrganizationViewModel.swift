//
//  OrganizationViewModel.swift
//  Floatcare
//
//  Created by OMNIADMIN on 07/08/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation

class OrganizationViewModel {
    
    var collegues: [UserBasicInformation] = [UserBasicInformation]()
    
    var invitees: [OpenShiftInvitees]?
    
    var business: Businesses?
    
    func getStaffMembers(orgId: String, completion: @escaping OrgDetailsCompletion) {
        
        ApolloManager.shared.client.fetch(query: FindOrganizationByIdQuery(organizationId: orgId),
                                          cachePolicy: .fetchIgnoringCacheData,
                                          context: nil,
                                          queue: .main) {[weak self] result in
                                            
                                            guard let user = try? result.get().data?.resultMap else {
                                                
                                                completion(nil)
                                                return
                                            }
                                            
                                            do {
                                                let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                                                
                                                let decoder = JSONDecoder()
                                                let oragnizationDetails = try decoder.decode(findOrganizationById.self, from: jsonData)
                                                print(user)
                                                if let org = oragnizationDetails.findOrganizationById.first {
                                                    
                                                    self?.collegues = org.staffMembers
                                                    self?.business = org.businesses.first
                                                }
                                                completion(oragnizationDetails.findOrganizationById.first)
                                            }catch  let error{
                                                
                                                print(error)
                                                completion(nil)
                                            }
        }
    }
}
