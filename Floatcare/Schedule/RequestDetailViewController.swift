//
//  RequestDetailViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 04/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class RequestDetailViewController: UIViewController {
    
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var headingTitleView: UIView!
    @IBOutlet weak var headingTitleLabel: UILabel!
    
    @IBOutlet weak var requestDatelabel: UILabel!
    @IBOutlet weak var requestDescriptionLbl: UILabel!
    
    @IBOutlet weak var requestTableview: UITableView!
    
    let viewModel = RequestDetailViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        // Do any additional setup after loading the view.
    }
    
    func setupView() {
        
        self.requestDatelabel.attributedText = viewModel.getRequestDate()
        self.requestDescriptionLbl.text =  viewModel.getRequestDescription()
        
        self.requestDescriptionLbl.font = UIFont(font: .SFUIText, weight: .regular, size: 15.0)
        self.requestDescriptionLbl.textColor = .appDarkGray
        
        self.headingTitleView.roundCorners([ .layerMinXMaxYCorner], radius: 30.0)
        

        
        self.navigationController?.navigationBar.isHidden = true
        
           self.navigationBar.isRightButtonHidden = true
           
           self.headingTitleView.backgroundColor = .profileBgColor
           self.headingTitleLabel.font = UIFont(font: .Athelas, weight: .regular, size: 32)
           self.headingTitleLabel.textColor = .headingTitleColor
           
           self.headingTitleLabel.text = "Sick Day"
           
           self.navigationBar.LeftButtonActionBlock = { sender in
               
               self.navigationController?.popViewController(animated: true)
           }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension RequestDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.ProfileSettingsTableCell.rawValue, for: indexPath) as! ProfileSettingsTableCell
        cell.isLastCell = indexPath.row == 1
        cell.isAccessoryButtonHidden = true
        cell.contentView.alpha = 1.0
        cell.isLastCell = false
        switch indexPath.row {
            case 0:
                cell.profileSettingNameLabel.text = "Messages"
                cell.profileSettingDescriptionLabel.text = "Hey sure, let me check"
                cell.iconImageView.image = UIImage(named: "account")
                cell.isLastCell = true
            case 1:
                cell.profileSettingNameLabel.text = "Edit Request"
                cell.profileSettingDescriptionLabel.text = "Update and modify your request"
                cell.iconImageView.image = UIImage(named: "collegues")
            case 2:
                cell.profileSettingNameLabel.text = "Delete Request"
                cell.profileSettingDescriptionLabel.text = "End your request"
                cell.iconImageView.image = UIImage(named: "account")
            case 3:
                cell.profileSettingNameLabel.text = "Extend Sick Day"
                cell.profileSettingDescriptionLabel.text = ""
                cell.iconImageView.image = UIImage(named: "collegues")
            case 4:
                cell.profileSettingNameLabel.text = "Call HR"
                cell.profileSettingDescriptionLabel.text = "If they didnt see request 1hr before shift"
                cell.iconImageView.image = UIImage(named: "account")
                cell.contentView.alpha = 0.5
            
            default:
                break
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 3 {
            
            let chooseDatesVC = UIStoryboard(name: "RequestTypeVC", bundle: nil).instantiateViewController(withIdentifier: "CRChooseDatesVC") as! CRChooseDatesVC
            chooseDatesVC.requestType = .callOff
            self.navigationController?.pushViewController(chooseDatesVC, animated: true)
        }
    }
}
