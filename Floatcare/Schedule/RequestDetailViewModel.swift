//
//  RequestDetailViewModel.swift
//  Floatcare
//
//  Created by OMNIADMIN on 04/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
import  UIKit

class RequestDetailViewModel {
    
    func getRequestDate() -> NSAttributedString{
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(font: .SFUIText, weight: .regular, size: 14.0), NSAttributedString.Key.foregroundColor : UIColor.appDarkGray]

        let attrs2 = [NSAttributedString.Key.font : UIFont(font: .SFUIText, weight: .regular, size: 14.0), NSAttributedString.Key.foregroundColor : UIColor.primaryColor]

        let attributedString1 = NSMutableAttributedString(string:"Nov 20th, 2020", attributes:attrs1 as [NSAttributedString.Key : Any])

        let attributedString2 = NSMutableAttributedString(string:". Review", attributes:attrs2 as [NSAttributedString.Key : Any])

        attributedString1.append(attributedString2)
        return attributedString1
    }
    
    func getRequestDescription() -> String {
        
        return "I have been down with the flu all week and things have not been great."
    }
}


