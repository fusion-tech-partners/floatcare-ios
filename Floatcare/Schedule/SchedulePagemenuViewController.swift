//
//  SchedulePagemenuViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import Swift_PageMenu

class SchedulePagemenuViewController: PageMenuController {
    
    var defaultIndex: Int = 0
    
    required init?(coder: NSCoder) {
        super.init(coder: coder, options: RoundRectPagerOption())
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabMenuView.roundCorners(.layerMinXMaxYCorner, radius: 30.0)
        
        self.dataSource = self
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SchedulePagemenuViewController: PageMenuControllerDataSource {

    func viewControllers(forPageMenuController pageMenuController: PageMenuController) -> [UIViewController] {
        
//        let openShiftsViewController1 = UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: String(describing: OpenShiftsViewController.self)) as! OpenShiftsViewController
        let openShiftsViewController1 = UIStoryboard(name: "CalendarVC", bundle: nil).instantiateViewController(withIdentifier: String(describing: CalendarVC.self)) as! CalendarVC
        let openShiftsViewController2 = UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: String(describing: MyRequestsViewController.self)) as! MyRequestsViewController
        let openShiftsViewController3 = UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: String(describing: OpenShiftsViewController.self)) as! OpenShiftsViewController
        return [openShiftsViewController1, openShiftsViewController2, openShiftsViewController3]
    }

    func menuTitles(forPageMenuController pageMenuController: PageMenuController) -> [String] {
        return ["My Schedule", "My Requests", "Open Shifts"]
    }

    func defaultPageIndex(forPageMenuController pageMenuController: PageMenuController) -> Int {
        return  self.defaultIndex
    }
}


struct RoundRectPagerOption: PageMenuOptions {

    var isInfinite: Bool = false

    var tabMenuPosition: TabMenuPosition = .top

    var menuItemSize: PageMenuItemSize {
        return .sizeToFit(minWidth: UIScreen.main.bounds.size.width/3.0, height: 55.0)
    }

    var menuTitleColor: UIColor {
        return .lightGreyTextColor
    }

    var menuTitleSelectedColor: UIColor {
        
        return .primaryColor
    }

    var menuCursor: PageMenuCursor {
        let bgColor: UIColor = .headingTitleColor
        return .roundRect(rectColor: bgColor.withAlphaComponent(0.2), cornerRadius: 10, height: 36, borderWidth: 0.0, borderColor: .clear)
    }

    var font: UIFont {
        return UIFont(font: .SFUIText, weight: .medium, size: 14.0)!
    }

    var menuItemMargin: CGFloat {
        return 0
    }

    var tabMenuBackgroundColor: UIColor {
        return .profileBgColor
    }

    var tabMenuContentInset: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 4)
    }

    public init(isInfinite: Bool = false, tabMenuPosition: TabMenuPosition = .top) {
        self.isInfinite = isInfinite
        self.tabMenuPosition = tabMenuPosition
    }
}
