//
//  SchedulesViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 10/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import SideMenu
import LGSideMenuController

class SchedulesViewController: UIViewController {
    
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var headingTitleView: UIView!
    @IBOutlet weak var headingTitleLabel: UILabel!
    @IBOutlet weak var currentDayEventsLabel: UILabel!
    
    open var leftMenuNavigationController: SideMenuNavigationController?
    
    var schedulePageMenuContainer: SchedulePagemenuViewController?
    
    var rcMessagesViewController: RCMessagesViewController?
    
    var lgSideMenuController: LGSideMenuController!
    
    var fetchUserInfo = FetchUserInfo()
    
    @IBOutlet weak var profileBtn: UIButton!
    
    @IBOutlet weak var notificationsBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let locale = Locale.current
        print(locale.regionCode)

        self.setupView()
        
        NotificationCenter.default.addObserver(
        self,
        selector: #selector(onUserSelection(notification:)),
        name: NSNotification.Name(rawValue: "chat"),
        object: nil)
        // Do any additional setup after loading the view.
        if  FloatCareUserDefaults.shared.faceID {
            FloatCareUserDefaults.shared.isUserLogedIn = true
            FloatCareUserDefaults.shared.userName = LoginModel.sharedInstance.email ?? ""
            FloatCareUserDefaults.shared.password = LoginModel.sharedInstance.password ?? ""
        }
        navigationBar.isLeftButtonHidden = true
        
        self.profileBtn.layer.cornerRadius = self.profileBtn.frame.size.width/2.0
        self.profileBtn.layer.masksToBounds = true
        
        self.profileBtn.setImage(#imageLiteral(resourceName: "profilepic"), for: .normal)
        
     
    }
    func movetoLogin()  {
        let loginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: String(describing: LoginViewController.self)) as! LoginViewController
        loginVC.isForLogin = true
         LoginModel.sharedInstance.email = nil
                LoginModel.sharedInstance.password = nil
                LoginModel.sharedInstance.visibility = false
         FloatCareUserDefaults.shared.userName = ""
         FloatCareUserDefaults.shared.password = ""
          FloatCareUserDefaults.shared.isUserLogedIn = false
         FloatCareUserDefaults.shared.userId = ""
         FloatCareUserDefaults.shared.worksiteId = ""
        
        self.navigationController?.pushViewController(loginVC, animated: false)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        guard ((Profile.shared.loginResponse?.data?.token) != nil) else{
//            ReusableAlertController.showAlertOk(title: "Float Care", message: "Something went wrong please login again") { (bool) in
//                self.movetoLogin()
//            }
//            return
//        }
        fetchUserInfo.getUserInfo(userId: Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0) { (userinfo) in
            
            let imgStr = amazonURL + (userinfo?.profilePhoto?.fillSpaces ?? "")
            self.profileBtn.sd_setImage(with: URL.init(string: imgStr), for: .normal, placeholderImage: #imageLiteral(resourceName: "profilepic"), options: [], completed: nil)
//            if(self.profileBtn.imageView?.image == nil){
//        
//            }
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func onUserSelection(notification: NSNotification) {
        
        if let openShiftInvitees = notification.object as? OpenShiftInvitees {
                
            self.navigatoConversations()
            
            self.rcMessagesViewController?.recverid = openShiftInvitees.userId
            
            self.lgSideMenuController.showRightView()
        }
        
    }
    
    @IBAction func createEventClick(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "CalendarVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: CalendarActionsVC.self)) as! CalendarActionsVC
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        
        vc.view.backgroundColor = UIColor.clear
        self.present(vc, animated: true, completion: nil)
    }
    
    func setupView() {
        
        //   self.navigationController?.navigationBar.isHidden = true
        
        self.navigationBar.isRightButtonHidden = true
        
        self.headingTitleView.backgroundColor = .profileBgColor
        self.headingTitleLabel.font = UIFont(font: .Athelas, weight: .regular, size: 32)
        self.headingTitleLabel.textColor = .headingTitleColor
        
        
        self.currentDayEventsLabel.textColor = .lightGreyTextColor
        self.currentDayEventsLabel.font = UIFont(font: .SFUIText, weight: .medium, size: 14.0)
        
        self.currentDayEventsLabel.text = " "
        
        //        FetchUserInfo().getUserInfo(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)") { [weak self] (userInfo) in
        //
        //            Profile.shared.userBasicInformation = userInfo
        //            if userInfo != nil {
        let username =  Profile.shared.loginResponse?.data?.userBasicInformation?.firstName!
        self.headingTitleLabel.text = "Hello, \(username ?? "")!"
        //            }
        //        }
        
        
        self.navigationBar.LeftButtonActionBlock = { sender in
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    func overlayBlurredBackgroundView() {
        
        let blurredBackgroundView = UIVisualEffectView()
        
        blurredBackgroundView.frame = view.frame
        blurredBackgroundView.effect = UIBlurEffect(style: .dark)
        
        view.addSubview(blurredBackgroundView)
        
    }
    @IBAction func addBtnTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "CalendarVC", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: CalendarActionsVC.self)) as! CalendarActionsVC
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        vc.view.backgroundColor = UIColor.clear
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func navigatoProfile() {
        
        let profileVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: String(describing: ProfileViewController.self)) as! ProfileViewController
        profileVC.viewModel = ProfileViewModel()
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @IBAction func navigatetoNotifications(_ sender: Any) {
       
        let NotificationsVc = UIStoryboard(name: "NotificationsIcon", bundle: nil).instantiateViewController(withIdentifier: String(describing: NotificationsIcon.self)) as! NotificationsIcon
        self.navigationController?.pushViewController(NotificationsVc, animated: true)
        
        
    }
    @IBAction func navigatoConversations() {
        
        let conversationsListViewController = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: String(describing:
            ConversationsListViewController.self)) as! ConversationsListViewController
        conversationsListViewController.delegate = self
        
        self.rcMessagesViewController = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "RCMessagesViewController") as? RCMessagesViewController
        
        self.rcMessagesViewController?.delegate = self
        
        self.lgSideMenuController = LGSideMenuController()
        self.lgSideMenuController.delegate = self
        lgSideMenuController.rootViewController = conversationsListViewController
        lgSideMenuController.rightViewController = self.rcMessagesViewController
        lgSideMenuController.rightViewWidth = UIScreen.main.bounds.size.width
        lgSideMenuController.rightViewPresentationStyle = .slideBelow
        self.navigationController?.pushViewController(lgSideMenuController, animated: true)
        
        self.lgSideMenuController.isRightViewSwipeGestureDisabled = true
    }
    
    
    @IBAction func sideMenu(_ sender: Any) {
        let vc = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        leftMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
        leftMenuNavigationController?.menuWidth = UIScreen.main.bounds.width - 50
        leftMenuNavigationController?.leftSide = true
        vc.delegate = self
        present(leftMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ScheduleTab" {
            
            self.schedulePageMenuContainer = segue.destination as? SchedulePagemenuViewController
        }
    }
    
}


extension SchedulesViewController: SideMenuNavigateTabsOnSchedulesPage {
    func movetoPageIndex(page: SchedulesPagination) {
        
        self.schedulePageMenuContainer?.defaultIndex = page.rawValue
        self.schedulePageMenuContainer?.reloadPages()
    }
}

extension SchedulesViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController:presented, presenting: presenting)
    }
}

extension UIPageViewController {
    var isPagingEnabled: Bool {
        get {
            var isEnabled: Bool = true
            for view in view.subviews {
                if let subView = view as? UIScrollView {
                    isEnabled = subView.isScrollEnabled
                }
            }
            return isEnabled
        }
        set {
            for view in view.subviews {
                if let subView = view as? UIScrollView {
                    subView.isScrollEnabled = newValue
                }
            }
        }
    }
}


extension SchedulesViewController: ConversationsListViewControllerDelegate {
    func navigateToChat(chatId: String) {
        
        print("chat id =======\(chatId)")
        if self.lgSideMenuController == nil {
            self.navigatoConversations()
        }
        if (self.navigationController?.viewControllers.last as? LGSideMenuController) == nil {
            
            self.navigationController?.pushViewController(lgSideMenuController, animated: true)
        }
        self.lgSideMenuController.showRightViewAnimated()
        if chatId != self.rcMessagesViewController?.recverid {
            self.rcMessagesViewController?.recverid = chatId
        }
     }
}


extension SchedulesViewController: RCMessagesViewControllerDelegate {
    func navigateToChatList() {
        
        self.lgSideMenuController.hideRightViewAnimated()
        
    }

}



extension SchedulesViewController: LGSideMenuDelegate {
    
    func didShowRightView(_ rightView: UIView, sideMenuController: LGSideMenuController) {
        
        self.lgSideMenuController.isRightViewSwipeGestureDisabled = false
    }
    
    func didHideRightView(_ rightView: UIView, sideMenuController: LGSideMenuController) {
        
        self.lgSideMenuController.isRightViewSwipeGestureDisabled = true
    }
}
