//
//  ShiftMenuViewController.swift
//  Floatcare
//
//  Created by OMNIADMIN on 18/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ShiftMenuViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ShiftMenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCellIdentifiers.NotificationSettingTableCell.rawValue, for: indexPath) as! NotificationSettingTableCell
        switch indexPath.row {
            case 0:
                
                let datestr = OpenShiftUtility.shared.openshift.onDate ?? ""
                cell.profileSettingNameLabel.text = Utility.getDateInFormat(inputDate: datestr, inputFormat: "yyyy-MM-dd", outputFormat: "MMM dd, yyyy")
            cell.profileSettingDescriptionLabel.text = "Date"
            case 1:
            cell.profileSettingNameLabel.text = "\(OpenShiftUtility.shared.openshift.startTime ?? "") - \(OpenShiftUtility.shared.openshift.endTime ?? "")"
            cell.profileSettingDescriptionLabel.text = "Time"
            case 2:
            cell.profileSettingNameLabel.text = OpenShiftUtility.shared.openshift.staffUserName ?? ""
            cell.profileSettingDescriptionLabel.text = "Professional Role"
            case 3:
            cell.profileSettingNameLabel.text = "Regular"
            cell.profileSettingDescriptionLabel.text = "Shift Role Type"
            default:
            break
        }
        
        return cell
    }
    
    
    
}
