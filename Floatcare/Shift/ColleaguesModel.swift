//
//  ColleaguesModel.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 26/08/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import Apollo

struct ColleaguesModel : Codable {
    let colleagues : [Colleague]?
}
struct Colleague : Codable {
    let firstName : String?
    let profilePhoto : String?
    let userId : String?
}

class FetchColleagues {
    typealias FetchedColleagues = ([Colleague]) -> ()
    func fetchColleagusFromUserWorkspace(userId:String,completion: @escaping FetchedColleagues)  {
        let query = FetchColleaguesFromWorkSpaceQuery.init(userId: userId)
        ApolloManager.shared.client.fetch(query: query) { (result) in
                print("\(result)")

                guard let colleaguesData = try? result.get().data?.resultMap else {
                    return
                }
                do {
                    if let col = colleaguesData["getWorkspaceByUserId"] as? [Any], col.count > 0{
                    let jsonData = try JSONSerialization.data(withJSONObject: col[0], options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    let colleagueObj = try decoder.decode(ColleaguesModel.self, from: jsonData)
                    print(colleagueObj.colleagues as Any)
                    completion(colleagueObj.colleagues!)
                    }else{
                      Progress.shared.hideProgressView()
                    }
                }catch {
                    
                }
        
            }
    }
}


