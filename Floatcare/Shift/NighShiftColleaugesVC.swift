//
//  NighShiftColleaugesVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 11/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import LGSideMenuController

class NightShiftColleaugesVM {
    static let shared = NightShiftColleaugesVM()
    let sectionHeaderTitles = ["Schedule","Unschedule"]
    var dataText : [[Colleague]]?
    
}

class NighShiftColleaugesVC : UIViewController {

    @IBOutlet weak var shiftTableView: UITableView!
    @IBOutlet weak var sortBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    
    @IBOutlet weak var searchView: UIView!
    let shiftVM = NightShiftColleaugesVM.shared
    @IBOutlet weak var titleLabel: LabelHeader!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    var delegate: ConversationsListViewControllerDelegate?
    
    var rcMessagesViewController: RCMessagesViewController?
    var lgSideMenuController: LGSideMenuController!
    
     let identifier = "NightColleaugeCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        shiftTableView.reloadData()
        // Do any additional setup after loading the view.
        FetchColleagues().fetchColleagusFromUserWorkspace(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)") { (colleaguesList) in
            let colList = colleaguesList.filter({ $0.userId != Profile.shared.userId})
            self.shiftVM.dataText = [colList]
            self.shiftTableView.reloadData()
            Progress.shared.hideProgressView()
        }
        setupInitialUI()
    }
    func setupInitialUI(){
        backgroundView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        backgroundView.backgroundColor = .profileBgColor
        titleLabel.font = UIFont(name: Fonts.athleasRegular, size: 36)
        titleLabel.textColor = UIColor().rgb(r: 72, g: 93, b: 186, k: 1)// #colorLiteral(red: 0.2823529412, green: 0.3647058824, blue: 0.7294117647, alpha: 1)
        searchView.removeFromSuperview()
    }

    
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sortClick(_ sender: Any) {
    }
    @IBAction func searchClick(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NighShiftColleaugesVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
         1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let cou = shiftVM.dataText?[section].count {
            return cou
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! NightColleaugeCell
        let colleague = shiftVM.dataText?[indexPath.section][indexPath.row]
        cell.empolyeeName.setTitle(colleague?.firstName ?? "", for: .normal)
        let imgUrl = amazonURL + (colleague?.profilePhoto ?? "")
        cell.messageButton.tag = indexPath.row
        cell.empolyeeName.tag = indexPath.row
        cell.messageButton.addTarget(self, action: #selector(moveToChatViewController(sender:)), for: .touchUpInside)
        cell.empolyeeName.addTarget(self, action: #selector(moveToColleagueInfoView(sender:)), for: .touchUpInside)
        cell.employeeProfilePic.sd_setImage(with: URL(string: imgUrl), completed: nil)
        return cell
    }
    @objc func moveToChatViewController(sender:UIButton)  {

        let currentColleague = shiftVM.dataText?[0][sender.tag]
        delegate?.navigateToChat(chatId: currentColleague?.userId ?? "0")
    }
    @objc func moveToColleagueInfoView(sender:UIButton)  {
        let vc = UIStoryboard.init(name: "Colleauge", bundle: nil).instantiateViewController(withIdentifier: "ColleaugeProfileVC") as! ColleaugeProfileVC
        let colleague = shiftVM.dataText?[0][sender.tag]
        vc.colleaugeId = colleague?.userId ?? "0"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    
    
    
}

