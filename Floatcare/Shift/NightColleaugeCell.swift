//
//  NightColleaugeCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 11/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

typealias OnMessageButtonClicked = (Int) -> ()

class NightColleaugeCell: UITableViewCell {
    
    var userBasicInformation: UserBasicInformation?
    var onMessageButtonClicked : OnMessageButtonClicked?
    @IBOutlet weak var empolyeeName: UIButton!
    @IBOutlet weak var employeeProfilePic: UIImageView!
    @IBOutlet weak var messageButton: UIButton!
    
    @IBAction func messageBtnClicked(sender: UIButton) {
        
        self.onMessageButtonClicked?(sender.tag)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        employeeProfilePic.layer.cornerRadius = 24
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

