//
//  NightShiftVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 11/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class NightShiftInfoVM {
     static let sharedInstance = NightShiftInfoVM()
     let headerTitles = [["","",""],["Role","Shift Type"]]
     let images = [["Time","Schedule","myWork"],["workExperience","Shift Type"]]
        
    
    var shiftTime = ""
    var shiftDate = ""
    var shiftPlace = ""
    var shiftRole = ""
    var shiftType = ""
    var values = [[String]]()

    func getDataFromJson() {
        self.shiftTime = "12am - 5pm"
        self.shiftDate = "May5th, 2020"
        self.shiftPlace = "Cedars Sinai, OBGYN"
        self.shiftRole = "ER Nurse"
        self.shiftType = "Charge Shift"
        self.values = [[shiftTime,shiftDate,shiftPlace],[shiftRole,shiftType]]
    }
    
}


class NightShiftInfoVC: UIViewController {
   
    
    @IBOutlet weak var shiftTableView: UITableView!
    fileprivate var shiftVM = NightShiftInfoVM.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        shiftVM.getDataFromJson()
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    
    
}

extension NightShiftInfoVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return shiftVM.headerTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shiftVM.headerTitles[section].count
       }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
        let cell = tableView.dequeueReusableCell(withIdentifier: "NightShitCell", for: indexPath) as! NightShitInfoCell
        cell.setData(shiftVM: shiftVM, indexPath: indexPath)
        return cell
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect.zero)
        vw.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.9843137255, alpha: 1)
        return vw
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0{
            return 4
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc  = self.storyboard?.instantiateViewController(identifier: "ShiftOptionsVC") as! ShiftOptionsVC
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}


extension NightShiftInfoVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
}

