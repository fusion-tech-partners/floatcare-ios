//
//  NightShiftParentVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 11/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

fileprivate enum Pages: Int {
    case infoPage = 0
    case colleaugesPage = 1
}

class NightShiftParentVC: UIViewController {
    
    @IBOutlet weak var titleLabel: LabelHeader!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var colleaugesBtn: UIButton!
    var pageVC : ShiftPageVC?
    var arr = [UIButton]()
    override func viewDidLoad() {
        super.viewDidLoad()
        arr = [infoBtn,colleaugesBtn]
        // Do any additional setup after loading the view.
        arr.forEach {
            $0.layer.cornerRadius = 10
            $0.backgroundColor = .clear
        }
        backgroundView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        titleLabel.font = UIFont(name: Fonts.athleasRegular, size: 36)
        titleLabel.textColor = UIColor().rgb(r: 72, g: 93, b: 186, k: 1)//
        infoCLick(infoBtn)
    }
    
    @IBAction func colleaugesCLick(_ sender: UIButton) {
        pageVC?.setViewControllertoPage(index: Pages.colleaugesPage.rawValue)
        arr.forEach {
            $0.backgroundColor = .clear
        }
        sender.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9058823529, blue: 0.9490196078, alpha: 1)

    }
    @IBAction func infoCLick(_ sender: UIButton) {
        pageVC?.setViewControllertoPage(index: Pages.infoPage.rawValue)
        arr.forEach {
            $0.backgroundColor = .clear
        }
        sender.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9058823529, blue: 0.9490196078, alpha: 1)
    }
    
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShiftPageVC"{
            pageVC = (segue.destination as! ShiftPageVC)
        }
    }


}
