//
//  NightShitCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 11/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class NightShitInfoCell: UITableViewCell {
        @IBOutlet weak var thumbnailImg: UIImageView!
        @IBOutlet weak var imgVw: UIImageView!
        @IBOutlet weak var floatLabel: UILabel!
        @IBOutlet weak var nameTF: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        thumbnailImg.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(shiftVM:NightShiftInfoVM, indexPath:IndexPath)  {
        
        if shiftVM.headerTitles[indexPath.section][indexPath.row].isEmpty{
            floatLabel.removeFromSuperview()
        }else{
            floatLabel.text = shiftVM.headerTitles[indexPath.section][indexPath.row]
        }
        nameTF.text = shiftVM.values[indexPath.section][indexPath.row]
        imgVw.image = UIImage.init(named: shiftVM.images[indexPath.section][indexPath.row])
        
        self.layoutIfNeeded()
    }
    
    @IBAction func rightButtonClick(_ sender: Any) {
        nameTF.becomeFirstResponder()
    }

}
