//
//  ShiftOptionCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 13/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ShiftOptionCell: UITableViewCell {
        @IBOutlet weak var thumbnailImg: UIImageView!
        @IBOutlet weak var imgVw: UIImageView!
        @IBOutlet weak var nameTF: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        thumbnailImg.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
}
