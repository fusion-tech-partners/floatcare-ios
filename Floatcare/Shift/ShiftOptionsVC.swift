//
//  ShiftOptionsVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 13/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
class ShiftOtionsVM {
    static let shared = ShiftOtionsVM()
    let sections = ["Contact MoD","Share"]
    let images = ["Basic Account","Basic Account"]
}

class ShiftOptionsVC: UIViewController {
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var optionsTableview: UITableView!
    var identifier = "ShiftOptionCell"
    var objVM = ShiftOtionsVM.shared
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ShiftOptionsVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        objVM.sections.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:identifier , for: indexPath) as! ShiftOptionCell
        cell.nameTF.text = objVM.sections[indexPath.row]
        cell.imgVw.image = UIImage.init(named: objVM.images[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        64
    }
    
}
