//
//  ShiftPageVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 11/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ShiftPageVC: UIPageViewController {
    var shiftInfoVC  : NightShiftInfoVC?
    var shiftColleaugesVC  : NighShiftColleaugesVC?
    var currentIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
//        dataSource = self
        shiftInfoVC = (self.storyboard?.instantiateViewController(identifier: "NightShiftInfoVC") as! NightShiftInfoVC)
        shiftColleaugesVC = (self.storyboard?.instantiateViewController(identifier: "NighShiftColleaugesVC") as! NighShiftColleaugesVC)
        // Do any additional setup after loading the view.
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                direction: .forward,
                animated: true,
                completion: nil)
        }
        
        for subview in self.view.subviews {
            if let scrollView = subview as? UIScrollView {
                scrollView.bounces = false
                break;
            }
        }
    }
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [shiftInfoVC!,
        shiftColleaugesVC!]
    }()
    
    func setViewControllertoPage(index:Int) {
        var direction = UIPageViewController.NavigationDirection.forward
        if currentIndex > index{
            direction = .reverse
        }
        currentIndex = index
        setViewControllers([orderedViewControllers[index]],
                           direction: direction,
                           animated: true,
                           completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ShiftPageVC : UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }

        let previousIndex = viewControllerIndex - 1

        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }

        guard orderedViewControllers.count > previousIndex else {
            return nil
        }

        return orderedViewControllers[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }

        let nextIndex = viewControllerIndex + 1
        guard orderedViewControllers.count != nextIndex else {
            return orderedViewControllers.first
        }

        guard orderedViewControllers.count > nextIndex else {
            return nil
        }

        return orderedViewControllers[nextIndex]
    }



}

