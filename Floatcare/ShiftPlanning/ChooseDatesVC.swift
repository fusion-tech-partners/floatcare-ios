//
//  ChooseDatesVC.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 03/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
protocol ChooseDateDelegates {
    func shiftPalanningSubmitted()
}

class ChooseDatesVC: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance  {
    
    @IBOutlet weak var satday: UILabel!
    @IBOutlet weak var friday: UILabel!
    @IBOutlet weak var thuday: UILabel!
    @IBOutlet weak var wedday: UILabel!
    @IBOutlet weak var tuesday: UILabel!
    @IBOutlet weak var monday: UILabel!
    @IBOutlet weak var sunday: UILabel!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var saveAsDraftBtn: UIButton!
    @IBOutlet weak var finalizeBtn: UIButton!
    @IBOutlet weak var calendar: FSCalendar!
    var scheduleLayoutArr : [ShiftPlanningLayout]?
    var selectedDatesArr = [String]()
    @IBOutlet weak var monthNameLbl: UILabel!
    var requestType : RequestTypes?
    //Calendar    
    var delegate: ChooseDateDelegates?
    //MARK: Calendar
    
      fileprivate let gregorian = Calendar(identifier: .gregorian)
      fileprivate let formatter: DateFormatter = {
          let formatter = DateFormatter()
          formatter.dateFormat = "yyyy-MM-dd"
          return formatter
      }()
    fileprivate weak var eventLabel: UILabel!
           
           private var currentPage: Date?
           
           private lazy var today: Date = {
               return Date()
           }()
           fileprivate lazy var dateFormatter1: DateFormatter = {
               let formatter = DateFormatter()
               formatter.dateFormat = "yyyy/MM/dd"
               return formatter
           }()
           fileprivate lazy var dateFormatter2: DateFormatter = {
               let formatter = DateFormatter()
               formatter.dateFormat = "yyyy-MM-dd"
               return formatter
           }()
    //       var navigationView = NavigationView()
    //       var headerView = UIView()
    var datesWithShiftPlanning = [String]()
           var datesWithNoEvents = ["2020-07-04", "2020-07-03", "2020-07-02", "2020-07-08", "2020-07-09"]
           var datesWithOnCall = ["2020-07-15"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.headerLbl.font = UIFont(font: .Athelas, weight: .bold, size: 32)
               self.headerLbl.textColor = .black
        
        self.finalizeBtn.backgroundColor = .primaryColor
               self.finalizeBtn.setTitleColor(.white, for: .normal)
        self.finalizeBtn.layer.cornerRadius = 6
        self.saveAsDraftBtn.backgroundColor = .white
                      self.saveAsDraftBtn.setTitleColor(.primaryColor, for: .normal)
        self.saveAsDraftBtn.layer.borderColor = UIColor.primaryColor.cgColor
        self.saveAsDraftBtn.layer.cornerRadius = 6
         self.saveAsDraftBtn.layer.borderWidth = 1
        self.setUpCalendar()
        
//        let dates = [
//            self.gregorian.date(byAdding: .day, value: -1, to: Date()),
//            Date(),
//            self.gregorian.date(byAdding: .day, value: 1, to: Date())
//        ]
//        dates.forEach { (date) in
//            self.calendar.select(date, scrollToDate: false)
//        }
        // For UITest
        self.calendar.accessibilityIdentifier = "calendar"
     //   self.calendar.select(Date())
        self.calendar.placeholderType = .fillHeadTail
        self.getAllRequestForUser()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAsDrafBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func finalizeBtnTapped(_ sender: Any) {
       // self.navigationController?.popViewController(animated: true)
       
        guard self.selectedDatesArr.count > 0 else{
            ReusableAlertController.showAlert(title: "Floatcare", message: "Please select dates")
            return
        }
        
        Progress.shared.showProgressView()
               
       // let mutation = CreateScheduleAvailabilityMutation.init(userId: Profile.shared.userId, startDate: ScheduleLayout.shiftPlanningDetails.shiftPlanningStartDate, endDate: ScheduleLayout.shiftPlanningDetails.shiftPlanningEndDate, startTime: "", endTime: "", onDate: self.selectedDatesArr, scheduleLayoutId: ScheduleLayout.shiftPlanningDetails.scheduleLayoutId!)

        let mutation = CreateAvailabilityMutation.init(userId: Profile.shared.userId , scheduleLayoutId: ScheduleLayout.shiftPlanningDetails.scheduleLayoutId!, startDate: ScheduleLayout.shiftPlanningDetails.shiftPlanningStartDate, endDate: ScheduleLayout.shiftPlanningDetails.shiftPlanningEndDate,onDate: self.selectedDatesArr)
    
               ApolloManager.shared.client.perform(mutation: mutation){[weak self] (result) in
                   print("\(result)")
                   DispatchQueue.main.async {
                       let alert = UIAlertController(title: "FloatCare", message: "Shift Planning Updated Successfully", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification_Myrequest), object: nil, userInfo: nil)
                        self?.navigationController?.popViewController(animated: true)
                        self?.delegate?.shiftPalanningSubmitted()
                       }))
                       self?.present(alert, animated: true, completion: nil)
                   }
                   Progress.shared.hideProgressView()
               }
    }
    // MARK: - calendar Methods
    func formattedDateFromString(dateString: String) -> String? {

             let inputFormatter = DateFormatter()
             inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

             if let date = inputFormatter.date(from: dateString) {

                 let outputFormatter = DateFormatter()
               outputFormatter.dateFormat = "MMM d, yyyy"

                 return outputFormatter.string(from: date)
             }

             return nil
         }
    func setUpCalendar() {
    
            self.calendar.backgroundColor = UIColor.white
          calendar.allowsMultipleSelection = false
          calendar.rowHeight = 80
          calendar.pagingEnabled = false
            self.calendar.calendarHeaderView.isHidden = true
            calendar.appearance.headerMinimumDissolvedAlpha = 0
            self.calendar.appearance.headerTitleFont = UIFont(name: "SF Pro Text", size: 0.0)
            self.calendar.headerHeight = 0.0
            //calendar.appearance.caseOptions = [ .weekdayUsesUpperCase]
            calendar.appearance.weekdayFont = UIFont(name: "SF Pro Text", size: 0.0)
            calendar.appearance.eventSelectionColor = UIColor.white
            calendar.appearance.eventOffset = CGPoint(x: 0, y: -7)
            calendar.today = nil // Hide the today circle
            calendar.register(CalendarCell.self, forCellReuseIdentifier: "cell")
            
            calendar.swipeToChooseGesture.isEnabled = false // Swipe-To-Choose
            calendar.appearance.headerTitleColor = UIColor.black
            calendar.appearance.weekdayTextColor = UIColor.black
            let scopeGesture = UIPanGestureRecognizer(target: calendar, action: #selector(calendar.handleScopeGesture(_:)));
            calendar.addGestureRecognizer(scopeGesture)
            
            
            let btnLeft = UIButton(frame:  CGRect(x: calendar.frame.minX + 10, y:  calendar.frame.minY + 10, width: 30, height: 30))
            btnLeft.addTarget(self, action: #selector(self.btnLeftAction(_:)), for: .touchUpInside)
            btnLeft.titleLabel?.textAlignment = .left
            btnLeft.setImage(UIImage(named: "prev"), for: .normal)
            btnLeft.setTitleColor(.black, for: .normal)
            calendar.calendarHeaderView.isUserInteractionEnabled = true
            // calendar.calendarHeaderView.isExclusiveTouch = true
          // self.view.addSubview(btnLeft)
            // calendar.calendarHeaderView.addSubview(btnLeft)
            
            let btnRight = UIButton(frame:  CGRect(x: calendar.frame.maxX - 60, y: calendar.frame.minY + 10, width: 30, height: 30))
            btnRight.addTarget(self, action: #selector(self.btnRightAction(_:)), for: .touchUpInside)
            btnRight.titleLabel?.textAlignment = .right
            btnRight.setImage(UIImage(named: "next"), for: .normal)
            btnRight.setTitleColor(.black, for: .normal)
          //  self.view.addSubview(btnRight)
        let month = Calendar.current.component(.month, from: Date())
           let year = Calendar.current.component(.year, from: Date())
        let monthName = self.monthName(month: month)
        
        self.monthNameLbl.textColor = .black
        let attributedString = NSMutableAttributedString(string: "\(monthName)  \(year)")
        attributedString.addAttribute(.font, value: UIFont(name: Fonts.sFProTextMedium, size: 14) ?? "", range: NSRange(location: 0, length: attributedString.length - 4))
        attributedString.addAttribute(.font, value:UIFont(name: Fonts.sFProTextMedium, size: 14) ?? "", range: NSRange(location: attributedString.length - 4, length: 4))
        
        self.monthNameLbl.attributedText = attributedString
        let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "EEEE"
               let dayInWeek = dateFormatter.string(from: Date())
                self.dayName(weekday: dayInWeek)
        
        let startDate = self.dateFormatter2.date(from:  ScheduleLayout.shiftPlanningDetails.shiftPlanningStartDate!)!
               let endDate = self.dateFormatter2.date(from:  ScheduleLayout.shiftPlanningDetails.shiftPlanningEndDate!)!
               var datesBetweenArray = Date.dates(from: startDate , to: endDate)
               self.datesWithShiftPlanning = datesBetweenArray
               datesBetweenArray = []
        }
    @objc  func btnLeftAction(_ sender: Any){
           self.moveCurrentPage(moveUp: false)
       }
       @objc  func btnRightAction(_ sender: Any){
           self.moveCurrentPage(moveUp: true)
       }
       private func moveCurrentPage(moveUp: Bool) {
           var dateComponents = DateComponents()
           if self.calendar.scope == .month {
               dateComponents.month = moveUp ? 1 : -1
           }else {
               dateComponents.weekOfYear = moveUp ? 1 : -1
           }
           self.currentPage = self.gregorian.date(byAdding: dateComponents, to: self.currentPage ?? self.today)
           self.calendar.setCurrentPage(self.currentPage!, animated: true)
       }
    func monthName(month : Int)->String {
        var name = ""
        switch month {
        case 1:
            name = "JANUARY"
            return name
        case 2:
            name = "FEBRUARY"
            return name
        case 3:
            name = "MARCH"
            return name
        case 4:
            name = "APRIL"
            return name
        case 5:
            name = "MAY"
            return name
        case 6:
            name = "JUNE"
            return name
        case 7:
            name = "JULY"
            return name
        case 8:
            name = "AUGUST"
            return name
        case 9:
            name = "SEPTEMBER"
            return name
        case 10:
            name = "OCTOBER"
            return name
        case 11:
            name = "NOVEMBER"
            return name
        case 12:
            name = "DECEMBER"
            return name
        default:
            return name
            
        }
    }
    func dayName(weekday:String){
        self.sunday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.monday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.tuesday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.wedday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.thuday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.friday.font = UIFont(name: "SF Pro Text", size: 9.0)
        self.satday.font = UIFont(name: "SF Pro Text", size: 9.0)
        switch weekday {
        case "Sunday":
            self.sunday.textColor = .primaryColor
        case "Monday":
            self.monday.textColor = .primaryColor
        case "Tuesday":
             self.tuesday.textColor = .primaryColor
        case "Wednesday":
             self.wedday.textColor = .primaryColor
        case "Thursday":
             self.thuday.textColor = .primaryColor
        case "Friday":
             self.friday.textColor = .primaryColor
        case "Saturday":
             self.satday.textColor = .primaryColor
        default:
             self.sunday.font = UIFont(name: "SF Pro Text", size: 9.0)
        }
    }
    // MARK:- FSCalendarDataSource
        // MARK:- FSCalendarDataSource
              func minimumDate(for calendar: FSCalendar) -> Date {
               
       //        let dateFormatter = DateFormatter()
       //        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"
             //  let date = Calendar.current.date(byAdding: .month, value: 0, to: Date()) ?? Date()
//                let minimumDate = self.dateFormatter2.string(from: date)
                 let minimumDate =  ScheduleLayout.shiftPlanningDetails.shiftPlanningStartDate!
                
               let month = Calendar.current.component(.month, from: self.dateFormatter2.date(from: minimumDate)!)
                  let year = Calendar.current.component(.year, from: self.dateFormatter2.date(from: minimumDate)!)
               let monthName = self.monthName(month: month)
               
               self.monthNameLbl.textColor = .black
               let attributedString = NSMutableAttributedString(string: "\(monthName)  \(year)")
               attributedString.addAttribute(.font, value: UIFont(name: Fonts.poppinsSemiBold, size: 14) ?? "", range: NSRange(location: 0, length: attributedString.length - 4))
               attributedString.addAttribute(.font, value:UIFont(name: Fonts.poppinsRegular, size: 14) ?? "", range: NSRange(location: attributedString.length - 4, length: 4))
               
               self.monthNameLbl.attributedText = attributedString
                  return self.dateFormatter2.date(from: minimumDate)!
              }
                  
              func maximumDate(for calendar: FSCalendar) -> Date {
             //  let date = Calendar.current.date(byAdding: .weekday, value: 1, to: Date()) ?? Date()
//               let maxDate = self.dateFormatter2.string(from: date)
                 let maxDate = ScheduleLayout.shiftPlanningDetails.shiftPlanningEndDate!
                  return self.dateFormatter2.date(from: maxDate)!
              }
          
       func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
           let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
           return cell
       }
       
       func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
           self.configure(cell: cell, for: date, at: position)
       }
       func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
           let currentPageDate = calendar.currentPage

           let month = Calendar.current.component(.month, from: currentPageDate)
           let year = Calendar.current.component(.year, from: currentPageDate)
        let monthName = self.monthName(month: month)
        self.monthNameLbl.text = "\(monthName)  \(year)"
            print("did select date \(year) \(month)")
       }
       
       func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
           return 2
       }
       
       // MARK:- FSCalendarDelegate
       
       func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
           self.calendar.frame.size.height = bounds.height
           self.eventLabel.frame.origin.y = calendar.frame.maxY + 10
       }
       
//       func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
//           return monthPosition == .current
//       }
//       
//       func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
//           return monthPosition == .current
//       }
       
       func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
           print("did select date \(self.formatter.string(from: date))")
         let dateSelected = self.dateFormatter2.string(from:date)
//        let filter = self.scheduleLayoutArr?.filter({$0.onDate == dateSelected })
//        if filter?.count ?? 0 > 0 {
//            let alert = UIAlertController(title: "FloatCare", message: "Date already been selected", preferredStyle: .alert)
//                          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
//
//                          }))
//                           self.present(alert, animated: true, completion: nil)
//        }else {
            if self.selectedDatesArr.contains(dateSelected){
                self.selectedDatesArr.remove(at: self.selectedDatesArr.firstIndex(of: dateSelected)!)
            }else{
                self.selectedDatesArr.append(dateSelected)
            }
//        }
//        if self.scheduleLayoutArr != nil && self.scheduleLayoutArr?.count ?? 0 > 0  { // To apply layout in the current page
//                       for scheduleLayout in (self.scheduleLayoutArr)! {
//
//                        let dateSelected = self.dateFormatter2.string(from:date)
//                       // let filter = scheduleLayout.filter{($0.onDate  == dateSelected)}
//
//                         let layoutDateString = scheduleLayout.onDate!
//                        if layoutDateString == dateSelected { // If already scheuled date is choosen
//                            return
//                        }
//
//            }
//        }else {
//           let dateSelected = self.dateFormatter2.string(from:date)
//            self.selectedDatesArr.append(dateSelected)
//        }
        
       
        
//           self.configureVisibleCells()
        let cellFSC = calendar.cell(for: date, at: monthPosition) as! FSCalendarCell
        self.configureSelectedCell(cell: cellFSC)
       // self.calendarDateSelected(date: date)
       }
       
       func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
           print("did deselect date \(self.formatter.string(from: date))")
        let dateSelected = self.dateFormatter2.string(from: date )
        self.selectedDatesArr = self.selectedDatesArr.filter() { $0 != dateSelected }
           self.configureVisibleCells()
       }
       
       func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
           if self.gregorian.isDateInToday(date) {
               return [UIColor.orange]
           }
           return [appearance.eventDefaultColor]
       }
       
       // MARK: - Private functions
       
       private func configureVisibleCells() {
           calendar.visibleCells().forEach { (cell) in
               let date = calendar.date(for: cell)
               let position = calendar.monthPosition(for: cell)
               self.configure(cell: cell, for: date!, at: position)
           }
       }
    
        private func configureSelectedCell(cell : FSCalendarCell) {
               let date = calendar.date(for: cell)
               let position = calendar.monthPosition(for: cell)
               self.configure(cell: cell, for: date!, at: position)
        }
       
       private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
                  
                  let diyCell = (cell as! CalendarCell)
               diyCell.eventIndicator.isHidden = true
                   diyCell.titleLabel.font = UIFont(name: "SF Pro Text", size: 12.0)
               diyCell.preferredTitleOffset = CGPoint(x: 0, y: 4)
               
                  if position == .current { // Current month dates in current page
                     
                   if self.gregorian.isDateInToday(date) {
                        diyCell.titleLabel.textColor = UIColor.black
                               diyCell.bgImgView.image = UIImage(named: "current_current_not")
                              diyCell.shift1.isHidden = true
                              diyCell.shift2.isHidden = true
                              diyCell.shift3.isHidden = true
                              diyCell.ws1.isHidden = true
                       diyCell.bgImgView.layer.cornerRadius = 0.0
                       diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                       diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                       diyCell.bgImgView.layer.shadowRadius = 0
                       diyCell.bgImgView.layer.shadowOpacity = 0
                   }else if date < Date() {
                        diyCell.titleLabel.textColor = UIColor.lightGray
                       diyCell.bgImgView.image = UIImage(named: "")
                         diyCell.shift1.isHidden = true
                         diyCell.shift2.isHidden = true
                         diyCell.shift3.isHidden = true
                         diyCell.ws1.isHidden = true
                         
                         diyCell.bgImgView.backgroundColor = UIColor.white
                         diyCell.bgImgView.layer.cornerRadius = 10.0
                         diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                         diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                         diyCell.bgImgView.layer.shadowRadius = 2.0
                       diyCell.bgImgView.layer.shadowOpacity = 0.4

                   }else {
                        diyCell.titleLabel.textColor = UIColor.black
                       diyCell.bgImgView.image = UIImage(named: "")
                         diyCell.shift1.isHidden = true
                         diyCell.shift2.isHidden = true
                         diyCell.shift3.isHidden = true
                         diyCell.ws1.isHidden = true
                         
                         diyCell.bgImgView.backgroundColor = UIColor.white
                         diyCell.bgImgView.layer.cornerRadius = 10.0
                         diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                         diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                         diyCell.bgImgView.layer.shadowRadius = 2.0
                       diyCell.bgImgView.layer.shadowOpacity = 0.4
                   }
                 
                   
                   if self.scheduleLayoutArr != nil && self.scheduleLayoutArr?.count ?? 0 > 0  { // To apply layout in the current page
                       for scheduleLayout in (self.scheduleLayoutArr)! {
                           
                           let celldate = self.dateFormatter2.string(from: date)
                           let layoutDateString = scheduleLayout.onDate!
                           let shiftName = scheduleLayout.shiftTypeName!
                           let layoutDate = self.dateFormatter2.date(from: layoutDateString) as! Date
       //                    if celldate != layoutDateString {
       //                        return
       //                    }
                           let openShift = scheduleLayout.openShift!
                           let request = scheduleLayout.request!
                           let schedule = (scheduleLayout.assigned! && scheduleLayout.available!)
                           let available = scheduleLayout.available!
                           if schedule && celldate == layoutDateString {
                               diyCell.shift1.isHidden = true
                               diyCell.shift2.isHidden = true
                               diyCell.ws1.isHidden = false
                               diyCell.shift3.isHidden = true
                               diyCell.bgImgView.layer.cornerRadius = 0.0
                               diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                               diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                               diyCell.bgImgView.layer.shadowRadius = 0
                               diyCell.bgImgView.layer.shadowOpacity = 0
                               if celldate == layoutDateString && self.gregorian.isDateInToday(layoutDate) {
                                   //if dateVal >
                                   diyCell.bgImgView.image = UIImage(named: "current_current")
                                   
                                   
                                   if shiftName.lowercased() == "night shift" {
                                       diyCell.shift3.isHidden = false
                                       diyCell.shift3.setImage(UIImage(named: "dayshift"), for: .normal)
                                   }else  if shiftName.lowercased() == "day shift" {
                                       diyCell.shift3.isHidden = false
                                       diyCell.shift3.setImage(UIImage(named: "night"), for: .normal)
                                       
                                   }else  if shiftName.lowercased() == "swing shift" {
                                       diyCell.shift3.isHidden = false
                                       diyCell.shift3.setImage(UIImage(named: "swing"), for: .normal)
                                   }
                                   
                                   
                               }else if Date() < layoutDate && celldate == layoutDateString  {
                                   diyCell.bgImgView.image = UIImage(named: "current_future")
       //                            diyCell.shift1.isHidden = true
       //                            diyCell.shift2.isHidden = true
       //                            diyCell.ws1.isHidden = false
       //                            diyCell.shift3.isHidden = true
       //                            diyCell.bgImgView.layer.cornerRadius = 0.0
       //                            diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
       //                            diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
       //                            diyCell.bgImgView.layer.shadowRadius = 0
       //                            diyCell.bgImgView.layer.shadowOpacity = 0
                                   
                                   if shiftName.lowercased() == "night shift" {
                                       diyCell.shift3.isHidden = false
                                       diyCell.shift3.setImage(UIImage(named: "dayshift"), for: .normal)
                                   }else  if shiftName.lowercased() == "day shift" {
                                       diyCell.shift3.isHidden = false
                                       diyCell.shift3.setImage(UIImage(named: "night"), for: .normal)
                                       
                                   }else  if shiftName.lowercased() == "swing shift" {
                                       diyCell.shift3.isHidden = false
                                       diyCell.shift3.setImage(UIImage(named: "swing"), for: .normal)
                                   }
                                   
                                   
                               }else if Date() > layoutDate && celldate == layoutDateString {
                                   diyCell.bgImgView.image = UIImage(named: "current_past")
       //                            diyCell.shift1.isHidden = true
       //                            diyCell.shift2.isHidden = true
       //                            diyCell.ws1.isHidden = false
       //                            diyCell.shift3.isHidden = true
       //                            diyCell.bgImgView.layer.cornerRadius = 0.0
       //                            diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
       //                            diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
       //                            diyCell.bgImgView.layer.shadowRadius = 0
       //                            diyCell.bgImgView.layer.shadowOpacity = 0
                                   
                                   if shiftName.lowercased() == "night shift" {
                                       diyCell.shift3.isHidden = false
                                       diyCell.shift3.setImage(UIImage(named: "dayshift"), for: .normal)
                                   }else if shiftName.lowercased() == "day shift" {
                                       diyCell.shift3.isHidden = false
                                       diyCell.shift3.setImage(UIImage(named: "night"), for: .normal)
                                       
                                   }else if shiftName.lowercased() == "swing shift" {
                                       diyCell.shift3.isHidden = false
                                       diyCell.shift3.setImage(UIImage(named: "swing"), for: .normal)
                                   }
                                   
                                   
                               }
                           }else if openShift  && celldate == layoutDateString {
                               diyCell.bgImgView.image = UIImage(named: "select")
                               diyCell.shift1.isHidden = true
                               diyCell.shift2.isHidden = true
                               diyCell.shift3.isHidden = true
                               diyCell.ws1.isHidden = true
                               
                               diyCell.bgImgView.layer.cornerRadius = 0.0
                               diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                               diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                               diyCell.bgImgView.layer.shadowRadius = 0
                               diyCell.bgImgView.layer.shadowOpacity = 0
                           }else if request && celldate == layoutDateString {
                               diyCell.bgImgView.image = UIImage(named: "select")
                               diyCell.shift1.isHidden = true
                               diyCell.shift2.isHidden = true
                               diyCell.shift3.isHidden = true
                               diyCell.ws1.isHidden = true
                               diyCell.bgImgView.layer.cornerRadius = 0.0
                                                      diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                                                      diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                                      diyCell.bgImgView.layer.shadowRadius = 0
                                                      diyCell.bgImgView.layer.shadowOpacity = 0
                               
                           }else if available  && celldate == layoutDateString {
                               diyCell.bgImgView.image = UIImage(named: "select")
                               diyCell.shift1.isHidden = true
                               diyCell.shift2.isHidden = true
                               diyCell.shift3.isHidden = true
                               diyCell.ws1.isHidden = true
                            
                               diyCell.bgImgView.layer.cornerRadius = 0.0
                               diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                               diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                               diyCell.bgImgView.layer.shadowRadius = 0
                               diyCell.bgImgView.layer.shadowOpacity = 0
                           }
                       }

                   }else {
                       diyCell.bgImgView.image = UIImage(named: "")
                                 
                                   diyCell.shift1.isHidden = true
                                   diyCell.shift2.isHidden = true
                                   diyCell.shift3.isHidden = true
                                   diyCell.ws1.isHidden = true
                                   
                                   diyCell.bgImgView.backgroundColor = UIColor.white
                                   diyCell.bgImgView.layer.cornerRadius = 10.0
                                   diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                                   diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                   diyCell.bgImgView.layer.shadowRadius = 2.0
                                 diyCell.bgImgView.layer.shadowOpacity = 0.4
                   }
                    if self.selectedDatesArr.count > 0 {
                        let key = self.dateFormatter2.string(from: date)
                           if self.selectedDatesArr.contains(key) {
                            
                                diyCell.bgImgView.image = UIImage(named: "select")
                                diyCell.shift1.isHidden = true
                                diyCell.shift2.isHidden = true
                                diyCell.shift3.isHidden = true
                                diyCell.ws1.isHidden = true
                            diyCell.bgImgView.layer.cornerRadius = 0.0
                            diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                            diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                            diyCell.bgImgView.layer.shadowRadius = 0
                            diyCell.bgImgView.layer.shadowOpacity = 0
                            }

                    }

                  }else  if position == .previous { // previous month dates in current page
                      diyCell.titleLabel.textColor = UIColor.lightGray
                      diyCell.bgImgView.image = UIImage(named: "")
                      diyCell.shift1.isHidden = true
                      diyCell.shift2.isHidden = true
                      diyCell.shift3.isHidden = true
                      diyCell.ws1.isHidden = true
                      
                      diyCell.bgImgView.backgroundColor = UIColor.white
                      diyCell.bgImgView.layer.cornerRadius = 10.0
                      diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                      diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                      diyCell.bgImgView.layer.shadowRadius = 2.0
                      diyCell.bgImgView.layer.shadowOpacity = 0.4
                  }
                  else  if position == .next { // next month dates in current page

                   diyCell.titleLabel.textColor = UIColor.clear
                             // diyCell.bgImgView.isHidden = true
                                diyCell.bgImgView.image = UIImage(named: "")
                                 diyCell.shift1.isHidden = true
                                 diyCell.shift2.isHidden = true
                                 diyCell.ws1.isHidden = true
                                 diyCell.shift3.isHidden = true
                              diyCell.bgImgView.layer.cornerRadius = 0.0
                                                 diyCell.bgImgView.layer.shadowColor = UIColor.lightGray.cgColor
                                                 diyCell.bgImgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                                                 diyCell.bgImgView.layer.shadowRadius = 0
                                                 diyCell.bgImgView.layer.shadowOpacity = 0
                     
                  }
              

//               if self.dateswithRequests.count > 0 {
//                   let key = self.dateFormatter2.string(from: date)
//                    if self.dateswithRequests.contains(key) {
//                       diyCell.bgImgView.backgroundColor = .lightGray
//                   }
//               }
               
                  let key1 = self.dateFormatter2.string(from: date)
                  if self.datesWithShiftPlanning.contains(key1) {
                   // diyCell.bgImgView.image = UIImage(named: "past_work")
//                      diyCell.shift1.isHidden = true
//                      diyCell.shift2.isHidden = true
//                      diyCell.shift3.isHidden = false
//                      diyCell.ws1.isHidden = true
                    diyCell.titleLabel.textColor = .primaryColor
                      
                  }
                  let key2 = self.dateFormatter2.string(from: date)
                  if self.datesWithOnCall.contains(key2) {
                      diyCell.bgImgView.image = UIImage(named: "pOncall")
                      diyCell.shift1.isHidden = true
                      diyCell.shift2.isHidden = true
                      diyCell.shift3.isHidden = false
                      diyCell.ws1.isHidden = false
                  }
              }
    func getAllRequestForUser() {
          Progress.shared.showProgressView()

          ScheduleAPI().getAllSchedulesForuser(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", successHandler: { (scheduleLayout) in
              DispatchQueue.main.async {
                  self.scheduleLayoutArr = scheduleLayout
                 // self.datesRange()
                Progress.shared.hideProgressView()
                   self.configureVisibleCells()
              }
              
          }) { (message) in
                DispatchQueue.main.async {
                  Progress.shared.hideProgressView()
                }
          }

      }
}
