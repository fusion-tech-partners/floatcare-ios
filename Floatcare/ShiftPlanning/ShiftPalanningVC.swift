//
//  ShiftPalanningVC.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 03/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ShiftPalanningVC: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var planningTblView: UITableView!
    @IBOutlet weak var understandBtn: UIButton!
    var sourceArr = ["Select the days you are available.","Submit it to send it to your supervisor","They will then assign days for you","If you are not satisfied or have a change of mind, you can send a request."]
    var imgArr = ["assignShift","submitSup","availday","makereq"]
     var selectedShiftPlanning : ScheduleLayout?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.understandBtn.layer.cornerRadius = 6
        self.understandBtn.backgroundColor = .primaryColor
        self.understandBtn.setTitleColor(.white, for: .normal)
        self.planningTblView.tableFooterView = UIView()
        self.titleLbl.font = UIFont(font: .Athelas, weight: .bold, size: 32)
        self.titleLbl.textColor = .black
    }
    
    @IBAction func understandBtnTapped(_ sender: Any) {
        
        let chooseDatesVC = UIStoryboard(name: "ShiftPalanningVC", bundle: nil).instantiateViewController(withIdentifier: String(describing: ChooseDatesVC.self)) as! ChooseDatesVC
        chooseDatesVC.delegate = self
        self.navigationController?.pushViewController(chooseDatesVC, animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backBtnTapped(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
}
extension ShiftPalanningVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "shiftPlanCell", for: indexPath) as! ShiftPlanTableCell
        cell.imageView?.image = UIImage(named: imgArr[indexPath.row])
        cell.textlbl.text = sourceArr[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    
    
}

extension ShiftPalanningVC : ChooseDateDelegates {
    func shiftPalanningSubmitted() {
        self.navigationController?.popViewController(animated: false)
    }
}
