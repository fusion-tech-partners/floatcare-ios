//
//  ShiftPlanTableCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 03/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ShiftPlanTableCell: UITableViewCell {

    @IBOutlet weak var textlbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
