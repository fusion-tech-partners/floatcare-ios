//
//  ShiftPlaningAPI.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 21/08/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import Foundation
class ScheduleLayoutByUserId {
    
    func GetAllScheduleLayoutByUserId(userId: String = "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)", completion: @escaping ([ScheduleLayout]?) -> ()) {
       
    
        ApolloManager.shared.client.fetch(query:FindScheduleLayoutByUserIdQuery(userId: userId), cachePolicy: .fetchIgnoringCacheData, context: nil,queue: .main ) { result in
                        
                        guard let user = try? result.get().data?.resultMap else {
                            completion(nil)
                            return
                        }
                        
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: user, options: .prettyPrinted)
                            let decoder = JSONDecoder()
                            let layouts = try decoder.decode(AllScheduleLayout.self, from: jsonData)
                        // CreateReqParams.createReqParams.requestForUser = requests.getAllRequestsByUserId
                         completion(layouts.findScheduleLayoutByUserId)

                        }catch {
                             completion(nil)
                        }
        }
    }
}
struct AllScheduleLayout: Codable {
    
    var findScheduleLayoutByUserId: [ScheduleLayout]?
}
struct ScheduleLayout: Codable {
    static var shiftPlanningDetails: ScheduleLayout = ScheduleLayout()
    var  scheduleLayoutId : String?
    var  departmentId : String?
    var  departmentName : String?
    var  shiftPlanningStartDate: String? // ignore this
    var  shiftPlanningEndDate: String? // ignore this
    var  scheduleStartDate: String?
    var  scheduleEndDate: String?
    var  shiftPlanningPhaseLength: String?
   var   makeStaffAvailabilityDeadLineLength: String?
   var   makeStaffAvailabilityDeadLine: String?
    var  maxQuota: String?
   var   scheduleLength: String?
   var   publishedOn: String?
}
