//
//  ShiftPlanningTableViewCell.swift
//  Floatcare
//
//  Created by Mounika.x.muduganti on 21/08/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class ShiftPlanningTableViewCell: UITableViewCell {

    @IBOutlet weak var contentBgView: CornorShadowView!
    @IBOutlet weak var workspaceLbl: UILabel!
    @IBOutlet weak var departmentLbl: UILabel!
    @IBOutlet weak var deadlineLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var headerLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentBgView.backgroundColor = UIColor().buttonViolet
        self.headerLbl.font = UIFont(name: Fonts.poppinsSemiBold, size: 18)
               self.durationLbl.font = UIFont(name: Fonts.sFProText, size: 13)
               self.deadlineLbl.font = UIFont(name: Fonts.sFProText, size: 13)
        self.departmentLbl.font = UIFont(name: Fonts.sFProText, size: 13)
        self.workspaceLbl.font = UIFont(name: Fonts.sFProText, size: 13)
        
        self.headerLbl.textColor = .white
        self.durationLbl.textColor = .white
        self.deadlineLbl.textColor = .white
        self.departmentLbl.textColor = .white
        self.workspaceLbl.textColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
