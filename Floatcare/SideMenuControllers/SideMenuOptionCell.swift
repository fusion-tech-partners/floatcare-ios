//
//  SideMenuOptionCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 29/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class SideMenuOptionCell: UITableViewCell {

    @IBOutlet weak var optionName: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        countView.roundCorners(corners: .allCorners, radius: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
