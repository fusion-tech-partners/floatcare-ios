//
//  SideMenuViewController.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 29/07/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
enum SchedulesPagination:Int {
    case mySchedulesPage = 0
    case myRequestsPage = 1
    case openShiftsPage = 2
}
protocol SideMenuNavigateTabsOnSchedulesPage {
    func movetoPageIndex(page:SchedulesPagination)
}
enum Options: String {
    case openShifts = "Open Shifts"
    case mySchedule = "My Schedule"
    case myRequest = "My Request"
    case messages = "Messages"
    
    case notifications = "Notifications"
    case profile = "Profile"
    case settings = "Settings and Preferences"
    case help = "Help"
    case logout = "Logout"
    

}

class SideMenuViewController: UIViewController {

    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var menuTable: UITableView!
    var delegate : SideMenuNavigateTabsOnSchedulesPage?
    let sections : [[Options]] = [[.openShifts,.mySchedule,.myRequest,.messages],[.profile,.logout] /*[.notifications,.profile,.settings,.help,.logout]*/]
    private let sessionProvider = URLSessionProvider()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.menuView.roundCorners([ .layerMaxXMinYCorner,.layerMinXMaxYCorner], radius: 40.0)
        // Do any additional setup after loading the view.
        menuView.clipsToBounds = false
      //  menuView.roundCorners(corners: [.topRight,], radius: 40)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func closeSideMenu(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension SideMenuViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return  1
        case 1: return  sections[0].count
        case 2: return  sections[1].count
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuHeaderCell", for: indexPath) as! SideMenuHeaderCell
            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuOptionCell", for: indexPath) as! SideMenuOptionCell
            if indexPath.section == 1 {
                cell.optionName.text = sections[0][indexPath.row].rawValue
            }
            else if indexPath.section == 2 {
                cell.optionName.text = sections[1][indexPath.row].rawValue
            }
            cell.countView.isHidden = true
            return cell
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect.zero)
        vw.backgroundColor = .clear
        return vw
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
           case 0: return  20
           case 1: return  40
           case 2: return  30
           default:
               break
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch sections[indexPath.section-1][indexPath.row] {
            
        case .mySchedule:
            delegate?.movetoPageIndex(page: .mySchedulesPage)
            self.dismiss(animated: true, completion: nil)
        case .myRequest:
            delegate?.movetoPageIndex(page: .myRequestsPage)
            self.dismiss(animated: true, completion: nil)
        case.openShifts:
            delegate?.movetoPageIndex(page: .openShiftsPage)
            self.dismiss(animated: true, completion: nil)
        case .profile:
            let profileVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: String(describing: ProfileViewController.self)) as! ProfileViewController
            profileVC.viewModel = ProfileViewModel()
            self.navigationController?.pushViewController(profileVC, animated: true)
        case .messages:
            let conversationsListViewController = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: String(describing: ConversationsListViewController.self)) as! ConversationsListViewController
            self.navigationController?.pushViewController(conversationsListViewController, animated: true)
        case .logout:
            ReusableAlertController.showAlertCompletion(title:"Are you sure you want to logout", message: "") { (code) in
                self.logOutCall()
                }.self
        default:
            print("default")
        }
    }
    
    func logOutCall() {
        sessionProvider.request(type: LogoutResponse.self, service: LogoutService.logout) { response in
            switch response {
            case let .success(posts):
                 DispatchQueue.main.async {
                Progress.shared.hideProgressView()
                }

            case let .failure(error):
                DispatchQueue.main.async {
                               Progress.shared.hideProgressView()
                               }
                if error == .irrelevent {
                                   DispatchQueue.main.async {
                                    let loginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: String(describing: LoginViewController.self)) as! LoginViewController
                                    loginVC.isForLogin = true
                                    LoginModel.sharedInstance.email = nil
                                           LoginModel.sharedInstance.password = nil
                                           LoginModel.sharedInstance.visibility = false
                                    FloatCareUserDefaults.shared.userName = ""
                                    FloatCareUserDefaults.shared.password = ""
                                     FloatCareUserDefaults.shared.isUserLogedIn = false
                                    FloatCareUserDefaults.shared.userId = ""
                                     FloatCareUserDefaults.shared.worksiteId = ""
                                    
                                    self.navigationController?.pushViewController(loginVC, animated: true)
                                   }
                               }
            }
        }
    }
    
}
