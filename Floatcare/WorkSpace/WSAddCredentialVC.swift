//
//  WSAddCredentialVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 29/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

enum WSAddCredentialCells : CaseIterable {
    case mediaCell
    case expiration
    case license
    case state
    case compactLicense

}

class WSAddCredentialVM {
    static let shared = WSAddCredentialVM()
    var cells : [WSAddCredentialCells] = [.mediaCell,.expiration,.license,.state,.compactLicense]
}

class WSAddCredentialVC: UIViewController {
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var backgrounView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    var vmObj = WSAddCredentialVM.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgrounView.roundCorners(corners: [.bottomLeft], radius: 40)
        bottomView.roundCorners(corners: [.topLeft], radius: 40)
        // Do any additional setup after loading the view.
    }
    @IBAction func getStartedClick(_ sender: Any) {
    }
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension WSAddCredentialVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vmObj.cells.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch vmObj.cells[indexPath.row] {
        case .mediaCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "WSCredentialMediaCell", for: indexPath) as! WSCredentialMediaCell
            return cell
        case .expiration,.license,.state,.compactLicense:
            let cell = tableView.dequeueReusableCell(withIdentifier: "WSAddCredentialFieldCell", for: indexPath) as! WSAddCredentialFieldCell
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell
            cell.headerTitle.text = "Fill Out license information"
        return cell.contentView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
}
