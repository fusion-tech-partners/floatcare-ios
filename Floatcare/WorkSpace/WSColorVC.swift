//
//  WSColorVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 27/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
protocol WorSiteColorDelegate {
    func workspaceColorUpdated()
}
class WSColorsVM {
    static let shared = WSColorsVM()
    var colorNames = ["Pink","Orange","Red","Yellow","Orchid","Steel Blue","Persian Blue","Cornflower Blue","Turqoise","Tiffany Blue"]
    var colorCodes : [UIColor] = [.pinkColor,.orangeColor,.redColor,.yellowColor,.orchidColor,.steelBlueColor,.persianBlueColor,.cornflowerBlue,.turquoise,.tiffanyBlue]
  
}
class WSColorVC: UIViewController {
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var backgrounView: UIView!
    var vmObj = WSColorsVM.shared
    var currentColorCode = UIColor()
    var delegate : WorSiteColorDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        backgrounView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        // Do any additional setup after loading the view.
//        currentColorCode = WSSettingsVM.shared.wsColorCode
    }
    @IBAction func getStartedClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        updateColorCode()
    }
    
    func updateColorCode()  {
        Progress.shared.showProgressView()
        let mutation = UpdateWorkSiteSettingsMutation.init(userId: Profile.shared.userId, worksiteSettingId: WorkSpaceSettingsUtility.shared.wsSettingsId, worksiteId: WorkSpaceSettingsUtility.shared.workSiteId, organizationId: WorkSpaceSettingsUtility.shared.orgId, color: currentColorCode.htmlRGBColor)
        ApolloManager.shared.client.perform(mutation: mutation) { result in
                        
            FetchWorkSiteSettings().fetchWorksitesByUserId(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)") { (settings) in
                Progress.shared.hideProgressView()
                ReusableAlertController.showAlertOk(title: "Floatcare", message: "Worksite color code updated successfully") { (status) in
                    self.dismiss(animated: false, completion: nil)
                    WorkSpaceSettingsUtility.shared.wsColorCode = self.currentColorCode.htmlRGBColor
                    self.delegate?.workspaceColorUpdated()
                }
            }
            
        }
        print(currentColorCode.htmlRGBColor)
    }
      
      @IBAction func backClick(_ sender: Any) {
//          self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WSColorVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vmObj.colorNames.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WSColorCell", for: indexPath) as! WSColorCell
        cell.thumbnailImg.backgroundColor = vmObj.colorCodes[indexPath.row]
        if cell.thumbnailImg.backgroundColor == currentColorCode {
            cell.imgVw.image = #imageLiteral(resourceName: "Tick")
        }else{
            cell.imgVw.image = UIImage.init(named: "")
        }
        cell.nameTF.text = vmObj.colorNames[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! WSColorCell
        currentColorCode = cell.thumbnailImg.backgroundColor!
        tableView.reloadData()
    }
    
}

extension UIColor {
       public convenience init?(hexa: String) {
           let r, g, b, a: CGFloat

           if hexa.hasPrefix("#") {
               let start = hexa.index(hexa.startIndex, offsetBy: 1)
               let hexColor = String(hexa[start...])

               if hexColor.count == 8 {
                   let scanner = Scanner(string: hexColor)
                   var hexNumber: UInt64 = 0

                   if scanner.scanHexInt64(&hexNumber) {
                       r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                       g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                       b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                       a = CGFloat(hexNumber & 0x000000ff) / 255

                       self.init(red: r, green: g, blue: b, alpha: a)
                       return
                   }
               }
           }

           return nil
       }
   }

extension String{
    func hexaStringToUIColor() -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

}



