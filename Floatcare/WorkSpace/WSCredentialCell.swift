//
//  WSCredentialCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 26/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
enum  CredentialsStates: String {
    case Upload
    case Reviewing
    case Approved
}
class WSCredentialCell: UITableViewCell {
    @IBOutlet weak var thumbnailImg: UIImageView!
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var nameTF: UILabel!
    @IBOutlet weak var accessoryBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        thumbnailImg.layer.cornerRadius = 10
    }
    func credentialState(state:CredentialsStates) {
        var str = ""
        let image1Attachment = NSTextAttachment()
        switch state {
        case .Upload:
            descriptionLabel.text = "Upload by May 12, 2020"
            break
        case .Reviewing:
            image1Attachment.image = UIImage(named: "Green")
        case .Approved:
            image1Attachment.image = UIImage(named: "Greens")
        }
        if(state == .Approved || state == .Reviewing){
            str = state.rawValue
            accessoryBtn.isHidden = true
            let image1String = NSAttributedString(attachment: image1Attachment)
            let fullString = NSMutableAttributedString(string: "")
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: " \(str)"))
            descriptionLabel.attributedText = fullString
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
