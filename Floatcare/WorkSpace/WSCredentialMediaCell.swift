//
//  WSCredentialMediaCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 29/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit



class WSCredentialMediaCell: UITableViewCell {

    @IBOutlet weak var ocrRoundImage: UIImageView!
    @IBOutlet weak var pcrShadowView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pcrShadowView.dropShadow(color: #colorLiteral(red: 0.8784313725, green: 0.8823529412, blue: 0.937254902, alpha: 1), opacity: 1, offSet: .zero, radius: 10, scale: true)
        pcrShadowView.layer.cornerRadius = 10
        ocrRoundImage.layer.cornerRadius = 10
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func frontCamClick(_ sender: Any) {
    }
    
    @IBAction func backCamClick(_ sender: Any) {
    }
    @IBAction func advancedpcrClick(_ sender: Any) {
    }
}
