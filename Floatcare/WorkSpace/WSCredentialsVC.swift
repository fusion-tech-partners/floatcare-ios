//
//  WSCredentialsVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 26/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

enum WSCredentialsSections : String {
    case requiredDoc = "Required Documents"
    
}
class WSCredentialsVM {
    static let shared = WSCredentialsVM()
    let sections : [WSCredentialsSections] = [.requiredDoc]
    func getThumbNailsImages(sec :WSCredentialsSections) -> [UIImage]{
        switch sec {
            case .requiredDoc: return [#imageLiteral(resourceName: "License"),#imageLiteral(resourceName: "License"),#imageLiteral(resourceName: "Certificate")]
        }
    }
    func getCellDescriptions(sec :WSCredentialsSections) -> [String]{
        switch sec {
            case .requiredDoc: return ["Upload by May 12, 2020","Upload by May 12, 2020","Upload by May 12, 2020","End Date"]
        }
    }
    func getCellTitles(sec :WSCredentialsSections) -> [String]{
        switch sec {
            case .requiredDoc: return ["Texas Nursing License", "Advanced CPR","US Nursing Training"]
        }
    }
    
    
}

class WSCredentialsVC: UIViewController {
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var backgrounView: UIView!
    var vmObj = WSCredentialsVM.shared
    override func viewDidLoad() {
        super.viewDidLoad()
        backgrounView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ReusableAlertController.showAlert(title: "FloatCare", message: "Credentials Work In Progress")
    }
    @IBAction func getStartedClick(_ sender: Any) {
        
    }
      
      @IBAction func backClick(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WSCredentialsVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        vmObj.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vmObj.getCellTitles(sec: vmObj.sections[section]).count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WSCredentialCell", for: indexPath) as! WSCredentialCell
        cell.imgVw.image = vmObj.getThumbNailsImages(sec: vmObj.sections[indexPath.section])[indexPath.row]
        cell.descriptionLabel.text = vmObj.getCellDescriptions(sec: vmObj.sections[indexPath.section])[indexPath.row]
        cell.nameTF.text = vmObj.getCellTitles(sec: vmObj.sections[indexPath.section])[indexPath.row]
        cell.accessoryBtn.isHidden = false
        
        switch indexPath.row {
        case 0: cell.credentialState(state: .Upload)
        case 1: cell.credentialState(state: .Reviewing)
        case 2: cell.credentialState(state: .Approved)
        default: print("Cell State")
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell
        cell.headerTitle.text = vmObj.sections[section].rawValue
        return cell.contentView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "WSAddCredentialVC") as! WSAddCredentialVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
