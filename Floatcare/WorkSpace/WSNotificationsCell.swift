//
//  WSNotificationsCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 30/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class WSNotificationsCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!    
    @IBOutlet weak var radioBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
