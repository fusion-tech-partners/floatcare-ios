//
//  WSNotificationsVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 30/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
class WSNotificationsVM {
    static let shared = WSNotificationsVM()
    var cellTitles = ["None","At Start","15 minute before","30 minute before","1 hour before","2 hours before","4 hours before"]
}
class WSNotificationsVC: UIViewController {
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var backgrounView: UIView!
    var vmObj = WSNotificationsVM.shared
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func getStartedClick(_ sender: Any) {
    }
      
      @IBAction func backClick(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WSNotificationsVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        vmObj.cellTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WSNotificationsCell", for: indexPath) as! WSNotificationsCell
        cell.titleLbl.text = vmObj.cellTitles[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
}
