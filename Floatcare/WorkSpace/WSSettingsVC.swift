//
//  WSSettingsVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 27/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

enum WSSettingsSections : String {
    case wsColor
    case configureRemainders = "Configure Remainders"
    
}
class WSSettingsVM {
    static let shared = WSSettingsVM()
    let sections : [WSSettingsSections] = [.wsColor,.configureRemainders]
    var wsColorCode : UIColor = #colorLiteral(red: 0.5607843137, green: 0.5960784314, blue: 0.7019607843, alpha: 1)
    func getThumbNailsImages(sec :WSSettingsSections) -> [UIImage]{
        switch sec {
            case .wsColor : return [UIImage()]
            case .configureRemainders: return [#imageLiteral(resourceName: "License"),#imageLiteral(resourceName: "License"),#imageLiteral(resourceName: "License"),#imageLiteral(resourceName: "License"),#imageLiteral(resourceName: "License"),#imageLiteral(resourceName: "License")]
        }
    }
    func getCellDescriptions(sec :WSSettingsSections) -> [String]{
        switch sec {
            case .wsColor : return ["Cedars Sinai Medical Center"]
            case .configureRemainders: return ["15 min, before 1 hr before","None","At Start","15 min, 30 min","30 min","1 hour before"]
        }
    }
    func getCellTitles(sec :WSSettingsSections) -> [String]{
        switch sec {
            case .wsColor : return ["Choose Worksite Color"]
            case .configureRemainders: return ["Event Notifications", "Assigned Work Shifts","On Call Shift","Meetings","Education","Events"]
        }
    }
    
    
}


class WSSettingsVC: UIViewController {
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var backgrounView: UIView!
    var vmObj = WSSettingsVM.shared
    override func viewDidLoad() {
        super.viewDidLoad()
        backgrounView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ReusableAlertController.showAlert(title: "Float Care", message: "WorkSpace Settings is in Progress")
        infoTableView.reloadData()
    }
    @IBAction func getStartedClick(_ sender: Any) {
    }
      
      @IBAction func backClick(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WSSettingsVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        vmObj.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vmObj.getCellTitles(sec: vmObj.sections[section]).count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WSsettingsCell", for: indexPath) as! WSsettingsCell
        cell.setDataForIndexPath(vmObj: vmObj, indexPath: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if vmObj.sections[section] == .configureRemainders{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell
            cell.headerTitle.text = vmObj.sections[section].rawValue
            return cell.contentView
        }
        return nil
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if vmObj.sections[section] == .wsColor{
            let vw = UIView.init(frame: CGRect.zero)
            vw.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.9843137255, alpha: 1)
            return vw
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if vmObj.sections[section] == .configureRemainders{
           return 60
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if vmObj.sections[section] == .wsColor{
            return 4
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if vmObj.sections[indexPath.section] == .wsColor{
            if indexPath.row == 0{
                let vc = self.storyboard?.instantiateViewController(identifier: "WSColorVC") as! WSColorVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        if vmObj.sections[indexPath.section] == .configureRemainders{
            if indexPath.row == 0{
                let vc = self.storyboard?.instantiateViewController(identifier: "WSNotificationsVC") as! WSNotificationsVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

