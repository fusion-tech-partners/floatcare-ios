//
//  WSsettingsCell.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 27/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class WSsettingsCell: UITableViewCell {
    @IBOutlet weak var thumbnailImg: UIImageView!
      @IBOutlet weak var imgVw: UIImageView!
      @IBOutlet weak var nameTF: UILabel!
      @IBOutlet weak var accessoryBtn: UIButton!
      @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        thumbnailImg.layer.cornerRadius = 10
        // Initialization code
    }
    
    func setDataForIndexPath(vmObj:WSSettingsVM, indexPath: IndexPath) {
        imgVw.image = vmObj.getThumbNailsImages(sec: vmObj.sections[indexPath.section])[indexPath.row]
        descriptionLabel.text = vmObj.getCellDescriptions(sec: vmObj.sections[indexPath.section])[indexPath.row]
        nameTF.text = vmObj.getCellTitles(sec: vmObj.sections[indexPath.section])[indexPath.row]
        accessoryBtn.isHidden = false
        
        if vmObj.sections[indexPath.section] == .wsColor{
            thumbnailImg.backgroundColor = vmObj.wsColorCode
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
