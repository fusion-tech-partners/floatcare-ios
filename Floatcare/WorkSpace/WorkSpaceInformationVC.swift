//
//  WorkSpaceInformationVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 26/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

enum WSSections : String {
    case jobInformation = "Job Information"
    case businessInformation = "Business information"
    case address = "Address"
    case settings = "Settings"
}
class WSInfoVM {
    static let shared = WSInfoVM()
    var wsDetails : WorkSiteDetails?
    
    let sections : [WSSections] = [.jobInformation, .businessInformation,.address,.settings]
    func getThumbNailsImages(sec :WSSections) -> [UIImage]{
        switch sec {
            case .jobInformation: return [#imageLiteral(resourceName: "Position"),#imageLiteral(resourceName: "Shift Type"),#imageLiteral(resourceName: "Schedule"),#imageLiteral(resourceName: "Position")]
            case .businessInformation: return [#imageLiteral(resourceName: "Business Name"),#imageLiteral(resourceName: "myWork"),#imageLiteral(resourceName: "myWork")]
            case .address: return [#imageLiteral(resourceName: "Street"),#imageLiteral(resourceName: "City"),#imageLiteral(resourceName: "State")]
            case .settings: return [#imageLiteral(resourceName: "dayshift")]
        }
    }
    func getCellDescriptions(sec :WSSections) -> [String]{
        switch sec {
            case .jobInformation: return ["Position","Employment Status","Hiring Date"]
            case .businessInformation: return ["Business Name","Department Name"]
            case .address: return ["Street","City","State"]
            case .settings : return [""]
        }
    }
    func getCellTitles(sec :WSSections) -> [String]{
        switch sec {
        case .jobInformation: return [wsDetails?.staffUserCoreTypeName ?? "",wsDetails?.fteStatus ?? "",wsDetails?.joinedDate?.covertDateToReadableFormat ?? ""]
        case .businessInformation: return [wsDetails?.worksites?.last?.name ?? "" ,wsDetails?.workspaceName ?? ""]
        case .address: return [wsDetails?.worksites?.last?.address ?? "",wsDetails?.worksites?.last?.city ?? "",wsDetails?.worksites?.last?.state ?? ""]
        case .settings: return["Settings"]
        }
    }
    
    
}
class WorkSpaceInformationVC: UIViewController {

    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var backgrounView: UIView!
    
    var vmObj = WSInfoVM.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         backgrounView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
    }
    
    @IBAction func getStartedClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "WSCredentialsVC") as! WSCredentialsVC
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WorkSpaceInformationVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        vmObj.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vmObj.getCellTitles(sec: vmObj.sections[section]).count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WSInfoCell", for: indexPath) as! WSInfoCell
        cell.imgVw.image = vmObj.getThumbNailsImages(sec: vmObj.sections[indexPath.section])[indexPath.row]
        cell.descriptionLabel.text = vmObj.getCellDescriptions(sec: vmObj.sections[indexPath.section])[indexPath.row]
        cell.nameTF.text = vmObj.getCellTitles(sec: vmObj.sections[indexPath.section])[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell
        cell.headerTitle.text = vmObj.sections[section].rawValue
        return cell.contentView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch vmObj.sections[indexPath.section] {
        case .settings:
            let vc = self.storyboard?.instantiateViewController(identifier: "WSSettingsVC") as! WSSettingsVC
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            print("")
        }
    }
    
}
