//
//  WorkSpaceInviteCodeVC.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 26/06/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
protocol WorkSpaceInviteDelegate {
    func workspaceUpdated(wsInfoVC:WorkSpaceInviteCodeVC)
}
class WorkSpaceInviteCodeVC: UIViewController {
    @IBOutlet weak var skipBtn: UIButton!
    
    @IBOutlet weak var tltleLabel: LabelHeader!
    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var inviteCodeTF: SkyFloatingLabelTextFieldWithIcon!
    var delegate : WorkSpaceInviteDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        tltleLabel.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        tltleLabel.font = UIFont(name: Fonts.athleasBold, size: 32)
        // Do any additional setup after loading the view.
        inviteCodeTF.delegate = self
    }
    @IBAction func skipNowClick(_ sender: Any) {
       loadWelcome()
    }
    
    @IBAction func joinWorkSpaceClick(_ sender: Any) {
//        let vc = self.storyboard?.instantiateViewController(identifier: "WorkSpaceInformationVC") as! WorkSpaceInformationVC
//        self.navigationController?.pushViewController(vc, animated: true)
        updateWorkspace()
    }
    func loadWelcome() {
        let registrationVC = UIStoryboard(name: "Verification", bundle:     nil).instantiateViewController(withIdentifier: String(describing: WelcomeViewController.self)) as! WelcomeViewController
               self.navigationController?.pushViewController(registrationVC, animated: true)
    }
    
    func updateWorkspace()  {
        
        guard let invitationCode = inviteCodeTF.text, !invitationCode.isEmpty else{
            self.view.makeToast("Please enter Invitation Code")
            return
        }
        Progress.shared.showProgressView()
        let mutation = UpdateUserWorkspaceMutation.init(invitationCode: invitationCode, userId: Profile.shared.userId)
                
        ApolloManager.shared.client.perform(mutation: mutation) { (result) in
            DispatchQueue.main.async {
                Progress.shared.hideProgressView()
            }
            guard let data = try? result.get().data else {
                ReusableAlertController.showAlert(title: "Float Care", message: "Something went wrong")
                return
            }
            if data.updateUserWorkspace != nil  {
                if ((self.delegate as? WorkSpaceListViewController) != nil){
                    self.delegate?.workspaceUpdated(wsInfoVC: self)
                }else{
                    self.showAlert("Workspace updated successfully")
                }
            } else {
                var errorMsg = "Something went wrong"
                if let msg = try? result.get().errors?.last?.message{
                    errorMsg = msg
                }
                ReusableAlertController.showAlert(title: "Float Care", message: errorMsg)
            }
        }
        
    }
    
    fileprivate func showAlert(_ message: String) {
        let alert = UIAlertController(title: "Float Care", message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
                self.loadWelcome()
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func backClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WorkSpaceInviteCodeVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
