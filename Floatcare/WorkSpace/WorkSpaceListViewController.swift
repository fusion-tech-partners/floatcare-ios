//
//  WorkSpaceListViewController.swift
//  Floatcare
//
//  Created by Dineshkumar kothuri on 03/08/20.
//  Copyright © 2020 Floatcare. All rights reserved.
//

import UIKit

class WSListVM {
    static let shared = WSListVM()
    let wsRows = [""]
    
}

class WorkSpaceListViewController: UIViewController {

    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var backgrounView: UIView!
    
    var vmObj = WSListVM.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         backgrounView.roundCorners(corners: [.bottomLeft,], radius: 40.0)
    }
    
    @IBAction func getStartedClick(_ sender: Any) {
        let wsVC = self.storyboard!.instantiateViewController(identifier: "WorkSpaceInviteCodeVC") as! WorkSpaceInviteCodeVC
        wsVC.modalPresentationStyle = .fullScreen
        wsVC.delegate = self
        self.present(wsVC, animated: true) {
            wsVC.skipBtn.isHidden = true
        }
    }
    
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WorkSpaceListViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Profile.shared.userWorkSpaceDetails?.getWorkspaceByUserId?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyRequestsTableViewCell", for: indexPath) as! MyRequestsTableViewCell
        let workSpace = Profile.shared.userWorkSpaceDetails?.getWorkspaceByUserId![indexPath.row]
        cell.shiftNameLbl.text = workSpace!.workspaceName
        cell.shiftDescriptionLbl.text = workSpace!.organizationName
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ColleaugeHeaderCell") as! ColleaugeHeaderCell
        cell.headerTitle.text = "WorkSpaces"
        return cell.contentView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let wsInfoVC = self.storyboard?.instantiateViewController(identifier: "WorkSpaceInformationVC") as! WorkSpaceInformationVC
        WSInfoVM.shared.wsDetails = Profile.shared.userWorkSpaceDetails?.getWorkspaceByUserId![indexPath.row]
        self.navigationController?.pushViewController(wsInfoVC, animated: true)
    }
    
}

extension WorkSpaceListViewController : WorkSpaceInviteDelegate{
    func workspaceUpdated(wsInfoVC: WorkSpaceInviteCodeVC) {
        wsInfoVC.dismiss(animated: true, completion: nil)
        ReusableAlertController.showAlert(title: "FloatCare", message: "Worksapce updated successfully")
        GetWorkSiteDetails().checkForWorkSpaceId(userId: "\(Profile.shared.loginResponse?.data?.userBasicInformation?.userID ?? 0)") { (success) in
            self.infoTableView.reloadData()
        }
    }
}
